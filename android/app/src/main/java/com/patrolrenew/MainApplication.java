package com.patrolhero;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
import org.wonday.pdf.RCTPdfView;
import com.rjblopes.opensettings.OpenSettingsPackage;
import com.microsoft.codepush.react.CodePush;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.jamesisaac.rnbackgroundtask.BackgroundTaskPackage;
import com.ocetnik.timer.BackgroundTimerPackage;
import com.horcrux.svg.SvgPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.rnfs.RNFSPackage;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.cubicphuse.RCTTorch.RCTTorchPackage;
import com.zmxv.RNSound.RNSoundPackage;
import io.sentry.RNSentryPackage;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import community.revteltech.nfc.NfcManagerPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.airbnb.android.react.lottie.LottiePackage;
import com.cmcewen.blurview.BlurViewPackage;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import org.pgsqlite.SQLitePluginPackage;
import com.rssignaturecapture.RSSignatureCapturePackage; // <-- add this import

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
    }

    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(new MainReactPackage(), new ImagePickerPackage(), new VectorIconsPackage(),
          new RNViewShotPackage(), new RCTPdfView(), new OpenSettingsPackage(),
          new CodePush("oaL_jQq_cfxz3j2bqV250Og3CNJK230d8ea5-db45-4e18-8be1-71de4392e2a2", MainApplication.this,
              BuildConfig.DEBUG),
          // new CodePush(null, getApplicationContext(), BuildConfig.DEBUG),
          new ReactNativeOneSignalPackage(), new BlurViewPackage(), new ReactSliderPackage(),
          new BackgroundTaskPackage(), new BackgroundTimerPackage(), new SvgPackage(), new RNFetchBlobPackage(),
          new RNFSPackage(), new ReactNativeAudioPackage(), new RCTTorchPackage(), new RNSoundPackage(),
          new RNSentryPackage(MainApplication.this), new ReactNativeRestartPackage(), new NfcManagerPackage(),
          new ReactNativeLocalizationPackage(), new RNDeviceInfo(), new RNCameraPackage(), new LottiePackage(),
          new SQLitePluginPackage(), new RSSignatureCapturePackage());

    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    BackgroundTaskPackage.useContext(this);
    SoLoader.init(this, /* native exopackage */ false);
  }
}
