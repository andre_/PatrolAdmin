import "./src/configs/reactotronConfigs";
import React, { Component } from "react";
import { AppRegistry, PermissionsAndroid } from "react-native";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import App from "./src/App";
import configureStore from "./src/store/configureStore";
import { configure } from "./src/configs/sentryConfigs";
import { SENTRY_URL } from "./src/configs/keys";
// import codePush from "react-native-code-push";

const { persistor, store } = configureStore();

/* Configure sentry */
configure(SENTRY_URL);

class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.requestLocationPermission();
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location Permission",
          message: "This app needs access to your location"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location");
      } else {
        console.log("Location permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    );
  }
}

// let codePushOptions = {
//   checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
//   installMode: codePush.InstallMode.ON_APP_RESUME,
//   updateDialog: {
//     title: "New update available"
//   }
// };
// AppContainer = codePush(codePushOptions)(AppContainer);

// export default AppContainer;

AppRegistry.registerComponent("PatrolHero", () => AppContainer);
