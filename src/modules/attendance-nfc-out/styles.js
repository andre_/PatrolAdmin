import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.danger
  },
  animationWrapper: {
    flex: 1
  },
  modal: {
    alignItems: "center",
    justifyContent: "center",
    height: 300
  },
  bottonWrapper: {
    width: "100%",
    height: 100,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.light,
    position: "absolute",
    bottom: 0
  },
  title: {
    fontFamily: "JosefinSans-Bold",
    fontSize: 18,
    color: colors.dark
  },
  text: {
    fontFamily: "JosefinSans-Regular",
    color: colors.text
  },
  btnRight: {
    width: 20,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 20
  },
  imgBtnRight: {
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  }
};

export default styles;
