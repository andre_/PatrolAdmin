import React, { Component } from "react";
import {
  View,
  Text,
  Vibration,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Switch,
  BackHandler,
  ToastAndroid
} from "react-native";
import Torch from "react-native-torch";
import { connect } from "react-redux";

import axios from "axios";
import moment from "moment";

import {
  initScannedCp,
  clear,
  submitClocking,
  submitTelegram
} from "../../redux/ducks/clockingRedux";

import { stopSirene } from "@src/actions/notificationActions";

import { RNCamera } from "react-native-camera";
import { colors } from "@styles";

const mapStateToProps = state => ({
  account: state.account,
  patroling: state.patroling,
  checkpoint: state.checkpoint,
  notification: state.notification,
  scanning: state.scanning
});

const mapDispatchToPros = dispatch => ({
  add(data) {
    dispatch(initScannedCp(data));
  },
  clear() {
    dispatch(clear());
  },
  save(data, copied) {
    dispatch(submitClocking(data, copied));
  },
  telegram(uri) {
    dispatch(submitTelegram(uri));
  },
  stopAlarm() {
    dispatch(stopSirene());
  },
  isLate(val) {
    dispatch(isLate(val));
  }
});

class QrScannerFaster extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      isTorchOn: false
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  callTimestamp = () => {
    return new Promise((resolve, reject) => {
      axios({
        method: "GET",
        url: "https://my.trackerhero.com/api/timestamp",
        timeout: 1000 * 3,
        headers: {
          "Content-type": "application/json"
        }
      })
        .then(res => {
          resolve(res.data.timestamp);
        })
        .catch(err => {
          resolve(Math.floor(new Date().getTime() / 1000));
        });
    });
  };

  _findItem = id => {
    const {
      checkpoint: { checkpoint }
    } = this.props;

    let findCheckpoint = (checkpoint || []).find(
      v => v.id === parseInt(id.data)
    )
      ? true
      : false;

    return findCheckpoint;
  };

  handleDetected = ({ barcodes }) => {
    let [barcode] = barcodes;

    Vibration.vibrate(100);
    this.props.stopAlarm();
    this.props.navigation.goBack();

    if (this._findItem(barcode)) {
      this.callTimestamp()
        .then(data => {
          obj = {
            id: parseInt(barcode.data),
            time: data,
            status: "1"
          };
          this.props.add(obj);
        })
        .catch(err => {
          ToastAndroid.show(
            "Server time failed. Using device time",
            ToastAndroid.SHORT
          );
        });
    } else {
      alert("Checkpoint not recognized");
    }
  };

  switchTorch = () => {
    this.setState(prevState => {
      return { isTorchOn: !prevState.isTorchOn };
    });

    Torch.switchState(this.state.isTorchOn);
  };

  render() {
    const {
      checkpoint: { checkpoint },
      patroling: { display }
    } = this.props;

    const { isTorchOn } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.cameraWrapper}
          type={RNCamera.Constants.Type.back}
          flashMode={
            isTorchOn
              ? RNCamera.Constants.FlashMode.torch
              : RNCamera.Constants.FlashMode.off
          }
          onGoogleVisionBarcodesDetected={this.handleDetected}
        >
          <View style={styles.overlayTop} />
          <View style={styles.overlayLeft} />

          <View style={styles.qrWrapper} />

          <View style={styles.overlayRight} />
          <View style={styles.overlayBottom} />
        </RNCamera>

        <View style={styles.content}>
          <View style={styles.switchWrapper}>
            <Switch value={isTorchOn} onValueChange={this.switchTorch} />
          </View>

          <FlatList
            data={checkpoint}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
              const filtering = display.find(x => x.id === item.id)
                ? true
                : false;
              const filterData = display.find(x => x.id === item.id);

              return (
                <View style={styles.list}>
                  <View style={styles.wrapper}>
                    <Text style={styles.textBold}>{item.description}</Text>

                    {filterData ? (
                      <Text style={styles.text}>
                        {moment
                          .unix(filterData.time)
                          .format("HH:mm:ss DD/MM/YYYY")}
                      </Text>
                    ) : (
                      <Text style={styles.text}>Not Scanned</Text>
                    )}
                  </View>

                  <View style={styles.wrapper}>
                    {filtering ? (
                      <TouchableOpacity
                        style={[
                          styles.btn,
                          { backgroundColor: colors.success }
                        ]}
                      />
                    ) : (
                      <TouchableOpacity style={styles.btn} />
                    )}
                  </View>
                </View>
              );
            }}
          />

          {/* {(checkpoint || []).map((v, k) => {
          })} */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cameraWrapper: {
    width: "100%",
    height: 300,
    alignItems: "center",
    justifyContent: "center"
  },
  content: {
    flex: 1,
    backgroundColor: colors.background,
    padding: 15
  },
  list: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.light,
    borderRadius: 4,
    height: 70,
    padding: 15,
    marginBottom: 10
  },
  text: {
    fontFamily: "OpenSans-Regular"
  },
  textBold: {
    fontSize: 16,
    fontFamily: "OpenSans-SemiBold",
    color: colors.dark
  },
  qrWrapper: {
    alignSelf: "center",
    width: 220,
    height: 200,
    borderWidth: 2,
    borderColor: colors.success
  },
  overlayTop: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 50,
    backgroundColor: "#000"
  },
  overlayBottom: {
    alignItems: "center",
    justifyContent: "center",
    zIndex: 10,
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    height: 50,
    backgroundColor: "#000"
  },
  overlayLeft: {
    position: "absolute",
    left: 0,
    width: 70,
    height: 300,
    backgroundColor: "#000",
    zIndex: -1
  },
  overlayRight: {
    position: "absolute",
    right: 0,
    width: 70,
    height: 300,
    backgroundColor: "#000"
  },
  btn: {
    width: 30,
    height: 30,
    borderRadius: 50,
    backgroundColor: colors.border
  },
  switchWrapper: {
    marginTop: 10,
    marginBottom: 25
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToPros
)(QrScannerFaster);
