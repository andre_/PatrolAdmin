import { colors } from "@styles";
import { ScaledSheet } from "react-native-size-matters";

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.background
  },

  iconCalendar: {
    width: "25@s",
    height: "25@vs",
    resizeMode: "contain",
    marginRight: "15@s",
    alignSelf: "flex-end"
  },

  card: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 4,
    marginBottom: 14,
    padding: 15,
    backgroundColor: colors.light
  },
  cardLeft: {
    width: "70%",
    borderRightWidth: 1,
    borderColor: colors.border
  },
  cardRight: {
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontFamily: "Montserrat-Regular"
  },
  label: {
    width: 40,
    height: 20,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  labelIn: {
    backgroundColor: colors.success
  },
  labelOut: {
    backgroundColor: colors.danger
  },
  inline: {
    flexDirection: "row",
    marginBottom: 6
  },

  avatar: {
    width: 30,
    height: 30,
    borderRadius: 40
  },
  images: {
    width: "100%",
    height: "100%",
    borderRadius: 40
  },
  list: {
    backgroundColor: colors.light,
    padding: 15,
    borderRadius: 4
  },
  content: {
    marginBottom: 15
  },
  bottomInfo: {
    backgroundColor: colors.success,
    height: 50,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4
  },
  cols: {
    alignItems: "center",
    justifyContent: "center"
  },
  iconCalendar: {
    width: 25,
    height: 25
  }
});

export default styles;
