import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  BackHandler,
  Image
} from "react-native";
import { connect } from "react-redux";
import { Grid, Col } from "react-native-easy-grid";
import { API } from "@src/services/APIService";

const leftBack = require("../../assets/icons/left-back.png");

import { colors, configs } from "@styles";

const mapStateToProps = state => ({
  posts: state.posts
});

const List = props => (
  <TouchableOpacity
    style={styles.list}
    onPress={() =>
      props.navigation.navigate("VisitorInformation", {
        data: props
      })
    }
  >
    <View style={styles.listTop}>
      <Grid>
        <Col
          style={[
            styles.cols,
            {
              paddingLeft: 15,
              alignItems: "center",
              justifyContent: "flex-start",
              flexDirection: "row"
            }
          ]}
        >
          <View style={styles.userInformation}>
            <Text style={styles.name}>{props.name}</Text>
            <Text style={styles.position}>Phone: {props.phone}</Text>
          </View>
        </Col>
        <Col
          style={[
            styles.cols,
            {
              justifyContent: "flex-end",
              flexDirection: "row",
              paddingRight: 15
            }
          ]}
        >
          <Text style={styles.text}>{props.type}</Text>
        </Col>
      </Grid>
    </View>
    <View style={styles.listBottom}>
      <Grid>
        <Col style={styles.cols}>
          <Text style={[styles.name, { color: colors.light }]}>
            {props.check_in}
          </Text>
          <Text style={[styles.text, { color: colors.light }]}>Check In</Text>
        </Col>
        <Col style={styles.cols}>
          <Text style={[styles.name, { color: colors.light }]}>
            {props.check_out || "-"}
          </Text>
          <Text style={[styles.text, { color: colors.light }]}>Check Out</Text>
        </Col>
      </Grid>
    </View>
  </TouchableOpacity>
);

class VisitorInDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>VISITOR IN</Text>
    ),
    headerTintColor: colors.light,
    headerLeft: (
      <TouchableOpacity
        style={configs.backButton}
        onPress={() => navigation.goBack()}
      >
        <Image source={leftBack} style={configs.imageBack} />
      </TouchableOpacity>
    ),
    headerRight: <View />
  });

  state = {
    visitor: [],
    loading: false
  };

  componentDidMount() {
    this.initVisitor();
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  initVisitor = () => {
    const {
      posts: { posts }
    } = this.props;

    this.setState({ loading: true });

    API()
      .get(`/v2/visitor?post_id=${posts.post_id}&status=check_in`)
      .then(res => {
        console.log("ini talkshow 621", res.data);
        this.setState({ loading: false, visitor: res.data.data });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  };

  render() {
    const { visitor, loading } = this.state;

    return (
      <View style={styles.container}>
        {loading ? (
          <Text>Loading</Text>
        ) : (visitor || []).length ? (
          <FlatList
            data={visitor}
            renderItem={({ item }) => <List {...item} {...this.props} />}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          <Text style={styles.text}>Visitor empty</Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.background
  },
  list: {
    backgroundColor: colors.light,
    elevation: 1,
    borderRadius: 4,
    marginBottom: 15
  },
  listTop: {
    height: 70
  },
  listBottom: {
    backgroundColor: colors.border,
    height: 60,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4
  },
  cols: {
    alignItems: "center",
    justifyContent: "center"
  },
  avatar: {
    width: 50,
    marginRight: 15
  },
  name: {
    fontFamily: "OpenSans-SemiBold"
  },
  text: {
    fontFamily: "OpenSans-Regular"
  },
  position: {
    fontFamily: "OpenSans-Regular"
  }
});

export default connect(mapStateToProps)(VisitorInDetail);
