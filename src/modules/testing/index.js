import React, { Component } from "react";
import { View, Text, TextInput, Dimensions, StyleSheet } from "react-native";
import * as Progress from "react-native-progress";

import { colors } from "@styles";

const screenWidth = Math.max(
  Dimensions.get("screen").width,
  Dimensions.get("window").width
);

class Testing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 0
    };
  }

  componentDidMount() {
    this.playSound();
  }

  playSound = () => {
    const timer = setInterval(() => {
      this.setState({
        time: this.state.time + 1
      });

      if (this.state.time === 10) {
        clearInterval(timer);
      }
    }, 1000);
  };
  render() {
    let timer = this.state.time / 10;
    console.log("timer", timer);

    return (
      <View style={styles.container}>
        <View style={styles.formGroup}>
          <Text style={styles.label}>Detail</Text>
          <TextInput placeholder="Message" />
        </View>

        <View style={styles.formGroup}>
          <Text style={styles.label}>Audio</Text>
          <Progress.Bar
            progress={timer}
            width={screenWidth - 35}
            style={styles.progress}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.background
  },
  text: {
    color: colors.dark,
    fontFamily: "OpenSans-Regular"
  },
  label: {
    color: colors.dark,
    fontFamily: "OpenSans-Regular",
    marginBottom: 4
  },
  formGroup: {
    height: 80
  },
  progress: {
    marginTop: 10
  }
});

export default Testing;
