import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  content: {
    padding: 30
  },
  title: {
    color: colors.dark,
    fontSize: 20,
    lineHeight: 35,
    textAlign: "center",
    fontFamily: "JosefinSans-SemiBold"
  },
  animationWrapper: {
    height: 280
  },
  result: {
    marginTop: 20,
    backgroundColor: colors.light,
    height: 80,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  textResult: {
    fontSize: 20,
    color: colors.dark,
    fontFamily: "JosefinSans-Regular"
  }
};

export default styles;
