import React, { Component } from "react";
import { View, Text, BackHandler } from "react-native";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import { connect } from "react-redux";
import LottieView from "lottie-react-native";

import styles from "./styles";

const barcodeJson = require("../../assets/lottie/barcode.json");

const mapStateToProps = state => ({
  account: state.account
});

class GuardsVerify extends Component {
  state = { result: null };

  componentDidMount() {
    this.checkNfc();
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.stopDetection();
    this.props.navigation.goBack();
    return true;
  };

  _parseText = tag => {
    if (tag.ndefMessage) {
      return NdefParser.parseText(tag.ndefMessage[0]);
    }
    return null;
  };

  checkNfc = () => {
    NfcManager.isSupported().then(supported => {
      if (supported) {
        this.startDetection();
      }
    });
  };

  _getGuard = id => {
    const {
      account: {
        account: { staff }
      }
    } = this.props;

    return new Promise((resolve, reject) => {
      let [data] = staff.filter(v => v.id === parseInt(id));

      if (data) {
        resolve(data);
      } else {
        reject(null);
      }
    });
  };
  _onTagDiscoveredCopy = tag => {
    let text = this._parseText(tag);

    this._getGuard(text)
      .then(result => {
        console.log("result", result);
        this.setState({ result });
      })
      .catch(err => {
        console.log("not found");
        this.setState({ result: null });
      });
  };

  startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscoveredCopy)
      .then(result => {})
      .catch(error => {});
  };

  stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {})
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.title}>
            Place your NFC Tag to identify the checkpoint
          </Text>

          <View style={styles.animationWrapper}>
            <LottieView source={barcodeJson} autoPlay />
          </View>

          <View style={styles.result}>
            {this.state.result === null ? (
              <Text style={styles.textResult}>- - - - - - - - -</Text>
            ) : (
              <Text style={styles.textResult}>{this.state.result.name}</Text>
            )}
          </View>
        </View>
      </View>
    );
  }
}

export default connect(mapStateToProps)(GuardsVerify);
