import React, {Component} from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';

import styles from './style';
import {colors, configs} from '@styles';

const call = require('../../assets/icons/call.png');
const avatar = require('../../assets/icons/avatar-icon.png');
const idCard = require('../../assets/icons/id-card.png');
const leftBack = require('../../assets/icons/left-back.png');

const ImageUpload = props => {
  console.log(props.source);
  return (
    <Image
      source={{
        uri: props.source,
        headers: {
          'Accept-Encoding': 'gzip',
        },
      }}
      style={styles.images}
    />
  );
};

class VisitorInformation extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: colors.info,
    },
    headerTitle: (
      <Text style={[configs.titleHeader, {fontSize: 14}]}>VISITOR DETAIL</Text>
    ),
    headerRight: <View />,
    headerLeft: (
      <TouchableOpacity
        style={configs.backButton}
        onPress={() => navigation.goBack()}>
        <Image source={leftBack} style={configs.imageBack} />
      </TouchableOpacity>
    ),
    headerTintColor: colors.light,
  });

  render() {
    const data = this.props.navigation.getParam('data');
    const images = data.image.split(';');

    let [first, ic, plate, last] = images;

    let icPict = ic.trim();
    let imgIcPict = `https://my.trackerhero.com/photos/${icPict}`;

    return (
      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={idCard} style={styles.icon} />
            </View>

            <View style={styles.formRight}>
              <Text style={styles.label}>Pass Number*</Text>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text style={styles.text}>{data.pass_no}</Text>
              </View>
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={avatar} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Name</Text>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text style={styles.text}>{data.name}</Text>
              </View>
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={avatar} style={styles.icon} />
            </View>

            <View style={styles.formRight}>
              <Text style={styles.label}>Unit</Text>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text style={styles.text}>{data.unit || '-'}</Text>
              </View>
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={call} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Phone Number</Text>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text style={styles.text}>123456</Text>
              </View>
            </View>
          </View>

          <View style={styles.imageWrapper}>
            <View style={styles.imageBox}>
              <Text style={[styles.label, {height: 40}]}>
                IC / LICENSE / PASSPORT
              </Text>

              <TouchableOpacity style={styles.imageUpload} onPress={this.getIc}>
                <ImageUpload source={imgIcPict} />
              </TouchableOpacity>
            </View>

            <View style={styles.imageBox}>
              <Text style={[styles.label, {height: 40}]}>CAR PLATE</Text>

              <TouchableOpacity
                style={styles.imageUpload}
                onPress={this.getPlate}>
                <Image
                  source={{
                    uri: `https://my.trackerhero.com/photos/${plate.trim()}`,
                    headers: {
                      'Accept-Encoding': 'gzip',
                    },
                  }}
                  style={styles.images}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default VisitorInformation;
