import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ActivityIndicator
} from "react-native";

import { RNCamera } from "react-native-camera";
import { colors } from "@styles";

const cancel = require("../../assets/icons/cancel-white.png");
const check = require("../../assets/icons/tick-white.png");

class CheckinCamera extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    time: null,
    source: null,
    isLoading: false,
    x: 0,
    y: 0
  };

  componentDidMount() {
    this.startCounter();
  }

  componentWillUnmount() {
    clearInterval(this.counting);
  }

  startCounter = () => {
    this.setState({
      time: 3,
      source: null
    });

    this.counting = setInterval(() => {
      if (this.state.time === 1) {
        this.takePicture();
        this.setState({ isLoading: true });
        clearInterval(this.counting);
      }

      this.setState({
        time: this.state.time - 1
      });

      console.log(this.state.time);
    }, 1000);
  };

  takePicture = async () => {
    if (this.camera) {
      const options = {
        quality: 0.5
      };

      const data = await this.camera.takePictureAsync(options);

      this.setState(
        {
          source: data.uri,
          isLoading: false
        },
        () => {
          this.props.navigation;
        }
      );
    }
  };

  getPicture = () => {
    const { source } = this.state;

    this.props.navigation.state.params.returnFromCamera(source);
    this.props.navigation.goBack();
  };

  getLayout = event => {
    const layout = event.nativeEvent.layout;
    this.setState({
      x: layout.x,
      y: layout.y
    });
  };

  render() {
    const { time, source, isLoading, x, y } = this.state;
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1
        }}
        type={RNCamera.Constants.Type.front}
        onGoogleVisionBarcodesDetected={this.handleDetected}
      >
        <View style={styles.container}>
          <View style={[styles.overlayTop, { height: y }]} />
          <View style={[styles.overlayLeft, { width: x }]} />

          {source !== null ? (
            <View style={styles.wrapper}>
              <Image
                source={{ uri: this.state.source }}
                style={styles.imagesFull}
              />
            </View>
          ) : (
            <View style={styles.wrapper} onLayout={this.getLayout}>
              {isLoading && (
                <View style={styles.loadingWrapper}>
                  <ActivityIndicator size="large" />
                </View>
              )}
            </View>
          )}

          <View style={[styles.overlayRight, { width: x }]} />
          <View style={[styles.overlayBottom, { height: y }]}>
            {time ? (
              <Text style={styles.counter}>{this.state.time}</Text>
            ) : (
              <View style={styles.btnBottomWrapper}>
                <TouchableOpacity
                  style={styles.btnCircle}
                  onPress={this.startCounter}
                >
                  <Image source={cancel} />
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.btnCircle}
                  onPress={this.getPicture}
                >
                  <Image source={check} />
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </RNCamera>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  overlayTop: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 98,
    backgroundColor: "#333",
    opacity: 0.8
  },
  overlayBottom: {
    alignItems: "center",
    justifyContent: "center",
    zIndex: 10,
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    height: 98,
    backgroundColor: "#333",
    opacity: 0.8
  },
  overlayLeft: {
    position: "absolute",
    left: 0,
    width: 40,
    height: 420,
    backgroundColor: "#333",
    opacity: 0.8,
    zIndex: -1
  },
  overlayRight: {
    position: "absolute",
    right: 0,
    width: 40,
    height: 420,
    backgroundColor: "#333",
    opacity: 0.8
  },
  wrapper: {
    marginTop: -80,
    position: "relative",
    borderWidth: 5,
    borderRadius: 1,
    borderStyle: "dotted",
    borderColor: "#fff",
    width: 280,
    height: 420,
    backgroundColor: "transparent"
  },
  counter: {
    color: colors.light,
    fontSize: 30,
    fontFamily: "OpenSans-Bold"
  },
  btnBottomWrapper: {
    width: 280,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  btnCircle: {
    width: 55,
    height: 55,
    borderRadius: 50,
    borderWidth: 3,
    alignItems: "center",
    justifyContent: "center",
    borderColor: colors.light
  },
  imagesFull: {
    width: "100%",
    height: "100%"
  },
  loadingWrapper: {
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    backgroundColor: colors.light,
    opacity: 0.7
  }
});

export default CheckinCamera;
