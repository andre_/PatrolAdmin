import React, { Component } from "react";
import {
  View,
  Text,
  Platform,
  TouchableOpacity,
  Linking,
  Image,
  StyleSheet,
  BackHandler
} from "react-native";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import LottieView from "lottie-react-native";

import { colors } from "@styles";

const RtdType = {
  URL: 0,
  TEXT: 1
};

const scanAnim = require("../../assets/lottie/scan-nfc.json");

class GuardsScanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      supported: true,
      enabled: false,
      isWriting: false,
      isFormat: false,
      urlToWrite: "www.google.com",
      rtdType: RtdType.TEXT,
      parsedText: null,
      tag: {}
    };
  }

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: colors.light
  };

  componentDidMount() {
    NfcManager.isSupported().then(supported => {
      this.setState({ supported });
      if (supported) {
        this._startNfc();
        this._startDetection();
      }
    });

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentWillUnmount() {
    if (this._stateChangedSubscription) {
      this._stateChangedSubscription.remove();
    }

    NfcManager.unregisterTagEvent(this._onTagDiscovered)
      .then(result => {})
      .catch(error => {});
  }

  handleBackButton() {
    NfcManager.unregisterTagEvent(this._onTagDiscovered)
      .then(result => {})
      .catch(error => {});

    this.props.navigation.goBack();
    return true;
  }

  _cancelOperation() {
    this.props.navigation.goBack();
  }

  render() {
    let { isWriting } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.textWrapper}>
            <Text style={styles.text}>
              {isWriting ? "SCANNING..." : "SCAN GUARD"}
            </Text>

            <View style={styles.imageWrapper}>
              {isWriting ? (
                <View style={{ width: 300, height: 80 }}>
                  <LottieView source={scanAnim} autoPlay />
                </View>
              ) : (
                <Image
                  source={require("./../../assets/nfc.png")}
                  style={{ width: 300, height: 80, resizeMode: "contain" }}
                />
              )}
            </View>

            <View style={styles.buttonWrapper}>
              <TouchableOpacity
                style={[styles.button, { backgroundColor: "#06D6A0" }]}
                onPress={
                  isWriting ? this._cancelNdefWrite : this._requestNdefWrite
                }
              >
                <Text style={styles.textThin}>
                  {isWriting ? "Cancel" : "Start Scanning"}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }

  _requestFormat = () => {
    let { isFormat } = this.state;
    if (isFormat) {
      return;
    }

    this.setState({ isFormat: true });
    NfcManager.requestNdefWrite(null, { format: true })
      .then(() => console.log("format completed"))
      .catch(err => console.warn(err))
      .then(() => this.setState({ isFormat: false }));
  };

  _requestNdefWrite = () => {
    function strToBytes(str) {
      let result = [];
      for (let i = 0; i < str.length; i++) {
        result.push(str.charCodeAt(i));
      }
      return result;
    }

    function buildUrlPayload(valueToWrite) {
      const urlBytes = strToBytes(valueToWrite);
      // in this example, we always use `http://`
      const headerBytes = [0xd1, 0x01, urlBytes.length + 1, 0x55, 0x03];
      return [...headerBytes, ...urlBytes];
    }

    function buildTextPayload(valueToWrite) {
      const textBytes = strToBytes(valueToWrite);
      // in this example. we always use `en`
      const headerBytes = [
        0xd1,
        0x01,
        textBytes.length + 3,
        0x54,
        0x02,
        0x65,
        0x6e
      ];
      return [...headerBytes, ...textBytes];
    }

    let { isWriting, urlToWrite } = this.state;
    if (isWriting) {
      return;
    }

    let bytes;

    // change urlToWrite to navigation.getParam, later
    // bytes = buildTextPayload(urlToWrite);
    console.log("build", this.props.navigation.getParam("id"));
    bytes = buildTextPayload(this.props.navigation.getParam("id"));

    let coords = `${this.state.lat},${this.state.lng}`;

    this.setState({ isWriting: true });
    NfcManager.requestNdefWrite(bytes)
      .then(() => {
        console.log("write completed");
        this._stopDetection();
        this.props.navigation.goBack();
      })
      .catch(err => console.warn(err))
      .then(() => this.setState({ isWriting: false }));
  };

  _cancelNdefWrite = () => {
    this.setState({ isWriting: false });
    NfcManager.cancelNdefWrite()
      .then(() => console.log("write cancelled"))
      .catch(err => console.warn(err));
  };

  _startNfc() {
    NfcManager.start({
      onSessionClosedIOS: () => {
        console.log("ios session closed");
      }
    })
      .then(result => {
        console.log("start OK", result);
      })
      .catch(error => {
        console.warn("start fail", error);
        this.setState({ supported: false });
      });

    if (Platform.OS === "android") {
      NfcManager.getLaunchTagEvent()
        .then(tag => {
          console.log("launch tag", tag);
          if (tag) {
            this.setState({ tag });
          }
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.isEnabled()
        .then(enabled => {
          this.setState({ enabled });
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.onStateChanged(event => {
        if (event.state === "on") {
          this.setState({ enabled: true });
        } else if (event.state === "off") {
          this.setState({ enabled: false });
        } else if (event.state === "turning_on") {
        } else if (event.state === "turning_off") {
        }
      })
        .then(sub => {
          this._stateChangedSubscription = sub;
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }

  _onTagDiscovered = tag => {
    console.log("Tag Discovered", tag);
    this.setState({ tag });
    let url = this._parseUri(tag);
    if (url) {
      Linking.openURL(url).catch(err => {
        console.warn(err);
      });
    }

    let text = this._parseText(tag);
    this.setState({ parsedText: text });
  };

  _startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscovered)
      .then(result => {
        console.log("registerTagEvent OK", result);
      })
      .catch(error => {
        console.warn("registerTagEvent fail", error);
      });
  };

  _stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {
        console.log("unregisterTagEvent OK", result);
      })
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  _clearMessages = () => {
    this.setState({ tag: null });
  };

  _goToNfcSetting = () => {
    if (Platform.OS === "android") {
      NfcManager.goToNfcSetting()
        .then(result => {
          console.log("goToNfcSetting OK", result);
        })
        .catch(error => {
          console.warn("goToNfcSetting fail", error);
        });
    }
  };

  _parseUri = tag => {
    if (tag.ndefMessage) {
      let result = NdefParser.parseUri(tag.ndefMessage[0]),
        uri = result && result.uri;
      if (uri) {
        console.log("parseUri: " + uri);
        return uri;
      }
    }
    return null;
  };

  _parseText = tag => {
    if (tag.ndefMessage) {
      return NdefParser.parseText(tag.ndefMessage[0]);
    }
    return null;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2EB6E8",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  wrapper: {
    width: 300,
    borderRadius: 4
  },
  imageWrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 25,
    marginBottom: 55
  },
  text: {
    fontSize: 20,
    color: colors.light,
    fontFamily: "JosefinSans-SemiBold",
    textAlign: "center",
    paddingBottom: 25
  },
  textThin: {
    color: colors.light,
    fontFamily: "JosefinSans-Light"
  },
  buttonWrapper: {
    alignItems: "center"
  },
  button: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 220,
    height: 58,
    backgroundColor: colors.background,
    borderRadius: 4
  },
  cancelButton: {
    position: "absolute",
    bottom: 0,
    padding: 10
  },
  cancelWrapper: {
    height: 50,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default GuardsScanner;
