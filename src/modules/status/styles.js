import { configs, colors } from "@styles";

const styles = {
  container: {
    flex: 1
  },
  avatarCol: {
    width: 80,
    alignSelf: "center"
  },
  avatarGrid: {
    width: "100%",
    height: 40
  },
  avatarImg: {
    width: "100%",
    height: "100%"
  },
  guardList: {
    width: "100%",
    height: 80,
    paddingLeft: 15,
    paddingRight: 15,
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: colors.border
  },
  guardCol: {
    paddingLeft: 15,
    paddingRight: 15,
    alignSelf: "center"
  },
  btnCol: {
    borderBottomColor: colors.border,
    width: 80,
    alignSelf: "center"
  },
  btn: {
    width: 80,
    height: 40,
    backgroundColor: colors.success,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4
  },
  modal: {
    alignItems: "center",
    justifyContent: "center",
    height: 300
  },
  textActive: {
    fontFamily: "JosefinSans-SemiBold",
    color: colors.success
  },
  textNonActive: {
    fontFamily: "JosefinSans-SemiBold",
    color: colors.danger
  }
};

export default styles;
