import React, { Component } from "react";
import {
  Dimensions,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Platform,
  BackHandler,
  RefreshControl
} from "react-native";
import { API } from "@src/services/APIService";
import { Grid, Col } from "react-native-easy-grid";
import { connect } from "react-redux";

import { colors, text } from "@styles";
import styles from "./styles";

const mapStateToProps = state => ({
  account: state.account,
  posts: state.posts
});

const GuardList = props => (
  <View style={styles.guardList}>
    <Grid>
      <Col style={styles.avatarCol}>
        <View style={styles.avatarGrid}>
          <Image
            source={{ uri: "https://picsum.photos/id/1060/200/300" }}
            style={styles.avatarImg}
          />
        </View>
      </Col>
      <Col style={styles.guardCol}>
        <Text style={text.regular}>{props.name}</Text>
        <Text style={[text.regular, { color: colors.text }]}>Supervisor</Text>
      </Col>
      <Col style={styles.btnCol}>
        <Text
          style={[
            styles.text,
            props.checkin_token ? styles.textActive : styles.textNonActive
          ]}
        >
          {props.checkin_token ? "CHECKED" : "NOT CHECKED"}
        </Text>
      </Col>
    </Grid>
  </View>
);

class Status extends Component {
  state = {
    staff: [],
    loading: false
  };

  navigateToScan = item => {
    this.props.navigation.navigate("GuardScanner", { id: item.id.toString() });
  };

  componentDidMount() {
    this.getStaff();
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  getStaff = () => {
    const {
      posts: { posts }
    } = this.props;

    const obj = {
      post_id: posts.post_id
    };

    this.setState({ loading: true });

    API()
      .post("get_post_detail", obj)
      .then(res => {
        this.setState({
          staff: res.data.staff,
          loading: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleRefresh = () => {
    this.setState(
      {
        loading: true
      },
      () => this.getStaff()
    );
  };

  render() {
    const { staff } = this.state;
    // const {
    //   account: {
    //     account: { staff }
    //   }
    // } = this.props;

    return (
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this.handleRefresh}
          />
        }
      >
        <FlatList
          data={staff}
          renderItem={({ item }) => (
            <GuardList onPress={() => this.navigateToScan(item)} {...item} />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </ScrollView>
    );
  }
}

export default connect(mapStateToProps)(Status);
