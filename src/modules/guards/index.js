import React, { Component } from "react";
import {
  BackHandler,
  Dimensions,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList
} from "react-native";
import { Grid, Col } from "react-native-easy-grid";
import { connect } from "react-redux";

import { colors, text } from "@styles";
import styles from "./styles";

const fingerprintImg = require("../../assets/icons/fingerprint.png");

const mapStateToProps = state => ({
  account: state.account
});

const GuardList = props => (
  <View style={styles.guardList}>
    <Grid>
      <Col style={styles.avatarCol}>
        <View style={styles.avatarGrid}>
          <Image
            source={{ uri: "https://picsum.photos/id/1060/200/300" }}
            style={styles.avatarImg}
          />
        </View>
      </Col>
      <Col style={styles.guardCol}>
        <Text style={text.regular}>{props.name}</Text>
        <Text style={[text.regular, { color: colors.text }]}>Supervisor</Text>
      </Col>
      <Col style={styles.btnCol}>
        <TouchableOpacity style={styles.btn} onPress={props.onPress}>
          <Text style={[text.regular, { color: colors.light }]}>Scan</Text>
        </TouchableOpacity>
      </Col>
    </Grid>
  </View>
);

class Guards extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerRight: (
      <TouchableOpacity
        style={styles.btnRight}
        onPress={() => navigation.navigate("GuardVerify")}
      >
        <Image source={fingerprintImg} style={styles.imgBtnRight} />
      </TouchableOpacity>
    )
  });

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  navigateToScan = item => {
    this.props.navigation.navigate("GuardScanner", { id: item.id.toString() });
  };

  render() {
    const {
      account: {
        account: { staff }
      }
    } = this.props;

    return (
      <View style={styles.container}>
        <FlatList
          data={staff}
          renderItem={({ item }) => (
            <GuardList onPress={() => this.navigateToScan(item)} {...item} />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps)(Guards);
