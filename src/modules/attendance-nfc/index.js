import React, { Component } from "react";
import { View, Text, BackHandler, Image, TouchableOpacity } from "react-native";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import ImagePicker from "react-native-image-picker";
import LottieView from "lottie-react-native";
import DeviceInfo from "react-native-device-info";
import store from "react-native-simple-store";
import { API } from "@src/services/APIService";
import { connect } from "react-redux";

import { initStaff } from "@src/actions/staffActions";

import styles from "./styles";
import { colors } from "@styles";

const radar = require("../../assets/lottie/radar.json");
const list = require("../../assets/icons/list.png");

const mapStateToProps = state => ({
  account: state.account
});

const mapDispatchToProps = dispatch => ({
  doInitStaff(data) {
    dispatch(initStaff(data));
  }
});

class AttendanceNfc extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: colors.light,
    headerTransparent: true,
    headerRight: (
      <TouchableOpacity
        style={styles.btnRight}
        onPress={() => navigation.navigate("Status")}
      >
        <Image source={list} style={styles.imgBtnRight} />
      </TouchableOpacity>
    )
  });

  state = {
    supported: true,
    enabled: false,
    isWriting: false,
    isFormat: false,
    loading: false,
    parsedText: null,
    tag: {},
    selected: null,
    albums: [],
    latitude: null,
    longitude: null
  };

  componentDidMount() {
    this.checkNfc();
    this.getLocation();
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  getLocation = () => {
    navigator.geolocation.getCurrentPosition(
      location => {
        console.log("location", location);
        this.setState({
          latitude: location.coords.latitude,
          longitude: location.coords.longitude
        });
      },
      err => console.log(err),
      {}
    );
  };

  _parseText = tag => {
    if (tag.ndefMessage) {
      return NdefParser.parseText(tag.ndefMessage[0]);
    }
    return null;
  };

  _onTagDiscoveredCopy = tag => {
    let text = this._parseText(tag);

    this._getGuard(text)
      .then(result => {
        if (result) {
          this.setState(
            {
              selected: result
            },
            () => this.openCamera()
          );
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleBack = () => {
    this.stopDetection();
    this.props.navigation.goBack();
    return true;
  };

  checkNfc = () => {
    NfcManager.isSupported().then(supported => {
      if (supported) {
        this.startDetection();
      }
    });
  };

  _getGuard = id => {
    const {
      account: {
        account: { staff }
      }
    } = this.props;

    return new Promise((resolve, reject) => {
      let [data] = staff.filter(v => v.id === parseInt(id));

      if (data) {
        resolve(data);
      } else {
        reject(null);
      }
    });
  };

  startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscoveredCopy)
      .then(result => {})
      .catch(error => {});
  };

  stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {})
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  checkinIn = () => {
    const { selected } = this.state;

    this.setState({ loading: true });
    this.props.doInitStaff(selected);

    let fd = new FormData();
    fd.append("id", selected.id);
    fd.append("post_id", selected.post_id);
    fd.append("deviceid", DeviceInfo.getDeviceId());
    fd.append("gps", `${this.state.latitude},${this.state.longitude}`); // wajib isi

    API()
      .post("checkin", fd)
      .then(res => {
        this.uploadImage(res.data);
      })
      .catch(err => {
        console.log(err);

        // save offline
        store
          .push("checkin", {
            id: selected.id,
            post_id: selected.post_id,
            deviceid: DeviceInfo.getDeviceId(),
            gps: `${this.state.latitude},${this.state.longitude}`
          })
          .then(() => store.get("checkin"))
          .then(checkin => {
            this.props.navigation.navigate("CheckinStatus", { for: "staff" });
          })
          .catch(err => {
            console.log("fail to save storage");
          });
      });
  };

  uploadImage = result => {
    const obj = new FormData();
    obj.append("attendance_id", result.id);
    obj.append("image", {
      uri: this.state.albums.uri,
      type: "image/jpeg",
      name: `photo-${result.id}`
    });

    API()
      .post("send_image_attendance", obj)
      .then(res => {
        this.stopDetection();
        this.props.navigation.navigate("CheckinStatus", { for: "staff" });
      })
      .catch(err => {
        alert("Send image for attendance fail");
      });
  };

  openCamera = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log("cancel");
      } else {
        this.setState(
          {
            albums: response
          },
          this.checkinIn
        );
      }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.animationWrapper}>
          <LottieView source={radar} autoPlay />
        </View>

        {this.state.selected !== null && (
          <View style={styles.bottonWrapper}>
            <Text style={styles.title}>
              {this.state.selected !== null ? this.state.selected.name : null}
            </Text>
            <View>
              {this.state.loading ? (
                <Text style={styles.text}>Checkin in</Text>
              ) : (
                <Text style={styles.text}>Taking Photos</Text>
              )}
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AttendanceNfc);
