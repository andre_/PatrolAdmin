import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';

import {RNCamera} from 'react-native-camera';
import {colors} from '@styles';

const cancel = require('../../assets/icons/cancel-white.png');
const check = require('../../assets/icons/tick-white.png');
const camera = require('../../assets/icons/photo-camera.png');

class ReportCamera extends Component {
  static navigationOptions = {
    header: null,
  };

  state = {
    time: null,
    source: null,
    isLoading: false,
  };

  componentDidMount() {}

  componentWillUnmount() {
    clearInterval(this.counting);
  }

  takePicture = async () => {
    if (this.camera) {
      const options = {
        quality: 0.5,
      };

      this.setState({isLoading: true});

      const data = await this.camera.takePictureAsync(options);

      this.setState(
        {
          source: data.uri,
          isLoading: false,
        },
        () => {
          this.props.navigation;
        },
      );
    }
  };

  getPicture = () => {
    const {source} = this.state;
    const images = {
      uri: source,
    };

    this.props.navigation.state.params.returnFromCamera(images);
    this.props.navigation.goBack();
  };

  render() {
    const {time, source, isLoading} = this.state;
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
        }}
        type={RNCamera.Constants.Type.back}
        onGoogleVisionBarcodesDetected={this.handleDetected}>
        <View style={styles.container}>
          {source !== null ? (
            <View style={styles.wrapper}>
              <Image
                ref={img => {
                  this.backgroundImage = img;
                }}
                source={{uri: this.state.source}}
                style={styles.imagesFull}
              />
            </View>
          ) : (
            <View
              style={[styles.wrapper, {borderColor: 'transparent'}]}
              onLayout={this.getLayout}>
              {isLoading && (
                <View style={styles.loadingWrapper}>
                  <ActivityIndicator size="large" />
                </View>
              )}
            </View>
          )}

          <View style={[styles.overlayBottom]}>
            {source === null ? (
              <TouchableOpacity
                style={styles.btnCircle}
                onPress={this.takePicture}>
                <Image source={camera} style={styles.camera} />
              </TouchableOpacity>
            ) : (
              <View style={styles.btnBottomWrapper}>
                <TouchableOpacity
                  style={styles.btnCircle}
                  onPress={this.startCounter}>
                  <Image source={cancel} />
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.btnCircle}
                  onPress={this.getPicture}>
                  <Image source={check} />
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </RNCamera>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  overlayTop: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: 98,
    backgroundColor: '#333',
    opacity: 0.8,
  },
  overlayBottom: {
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    height: 98,
    backgroundColor: '#333',
  },
  overlayLeft: {
    position: 'absolute',
    left: 0,
    width: 40,
    height: 420,
    backgroundColor: '#333',
    opacity: 0.8,
    zIndex: -1,
  },
  overlayRight: {
    position: 'absolute',
    right: 0,
    width: 40,
    height: 420,
    backgroundColor: '#333',
    opacity: 0.8,
  },
  wrapper: {
    marginTop: -80,
    position: 'relative',
    borderWidth: 5,
    borderRadius: 1,
    borderStyle: 'dotted',
    borderColor: '#fff',
    width: 280,
    height: 420,
    backgroundColor: 'transparent',
  },
  counter: {
    color: colors.light,
    fontSize: 30,
    fontFamily: 'OpenSans-Bold',
  },
  btnBottomWrapper: {
    width: 280,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnCircle: {
    width: 55,
    height: 55,
    borderRadius: 50,
    borderWidth: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.light,
  },
  imagesFull: {
    width: '100%',
    height: '100%',
  },
  loadingWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: colors.light,
    opacity: 0.7,
  },
  camera: {
    width: 25,
    height: 25,
  },
});

export default ReportCamera;
