import React, { Component } from "react";
import { DrawerNavigator } from "react-native";
import Sidebar from "./Sidebar";

import HomeMainComponent from "@components/home/HomeMainComponent";

const routeConfig = {
  Home: {
    screen: HomeMainComponent
  }
};

const drawerConfig = {
  initialRouteName: "Home",
  contentComponent: Sidebar,
  drawerWith: 280
};

export const NavigationWithDrawer = DrawerNavigator(routeConfig, drawerConfig);
