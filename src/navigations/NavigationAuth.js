import React from "react";
import { Platform, Animated, Easing, View, Text, Image } from "react-native";
import {
  StackNavigator,
  DrawerNavigator,
  TabNavigator
} from "react-navigation";
import getSlideFromRightTransition from "react-navigation-slide-from-right-transition";
import { colors, configs } from "@styles/";

/* Import views */
import Clocking from "@src/scenes/clocking/containers";
import NewClocking from "@src/scenes/new-clocking/containers";
import IncompleteReport from "@src/scenes/incomplete-report/container";

import Achievment from "@src/scenes/achievment/containers";

import Flag from "@src/scenes/flag/containers";
import Settings from "@src/containers/settings/SettingsContainer";
import Checkpoint from "@src/containers/checkpoint/CheckpointContainer";
import Users from "@src/containers/users/UsersContainer";
import HomeMain from "@src/containers/home/HomeMainContainer";
import HomeCheckin from "@src/containers/home/HomeCheckinContainer";
import HomeCheckout from "@src/containers/home/HomeCheckoutContainer";
import Checkin from "@src/containers/checkin/CheckinContainer";
import Checkout from "@src/containers/checkout/CheckoutContainer";
import Language from "@src/containers/language/LanguageContainer";
import Timeclock from "@src/containers/timeclock/TimeclockContainer";

import VisitorMainComponent from "@components/visitor/VisitorMainComponent";
import VisitorCheckinComponent from "@components/visitor/VisitorCheckinComponent";
import VisitorCheckoutComponent from "@components/visitor/VisitorCheckoutComponent";
import VisitorQr from "@src/scenes/visitor-qr/containers";
import VisitorScanner from "@src/scenes/visitor-scanner/containers";
import VisitorFlag from "@src/scenes/visitor-flag/components/";
import VisitorOfflineList from "@src/scenes/visitor-offline-list";
import VisitorInDetail from "@src/modules/visitor-in-detail";
import VisitorSummary from "@src/modules/visitor-summary";
import VisitorInformation from "@src/modules/visitor-information";
import Testing from "@src/modules/testing";

import ModalVisitor from "@src/scenes/modal-visitor/components";
import DeliveryComponent from "@src/scenes/delivery/containers";
import FilesComponent from "@components/files/FilesComponent";
import CameraBox from "@src/components/partials/CameraBox";
import CameraPreview from "@src/components/partials/CameraPreview";
import CheckinStatus from "@components/checkin/StatusComponent";
import CheckoutStatus from "@components/checkout/StatusComponent";
import ReportComponent from "@components/report/ReportComponent";
import ReportListComponent from "@components/report/ReportListComponent";

import ReportPosts from "@src/scenes/report-posts/containers";
import FaceRecognition from "@src/scenes/face-recognition/containers";
import FaceScanner from "@src/scenes/face-scanner/containers";
import FaceResultIn from "@src/scenes/face-result-in/containers";
import FaceResultOut from "@src/scenes/face-result-out/containers";
import Scanning from "@src/scenes/scanning/containers";

import QrCodeComponent from "@components/qr-code/QrCodeComponent";
import QrShowComponent from "@components/qr-show/QrShowComponent";
import QrScanComponent from "@components/qr-scan/QrScanComponent";
import NfcScan from "@components/nfc-scan/NfcScan";
import NfcRead from "@components/nfc-read/NfcRead";
import ScanningComponent from "@components/scanning/ScanningComponent";
import NotificationComponent from "@components/notification/NotificationComponent";

import ExpiredContainer from "../scenes/expired/containers";
import SummaryContainer from "../scenes/summary/containers";
import SummaryPanicContainer from "../scenes/summary-panic/containers";
import PdfContainer from "../scenes/pdf/containers";
import SopContainer from "../scenes/sop/containers";
import SopDetailContainer from "../scenes/sop-detail/containers";

import Sidebar from "@components/partials/Sidebar";
import MapsComponent from "@components/partials/MapsComponent";
import PanicComponent from "@components/panic/PanicComponent";
import FarAway from "@components/partials/FarAway";
import WarningClocking from "@components/partials/WarningClocking";
import ClockingReportComponent from "@components/clocking-report/ClockingReportComponent";

import AiIn from "../modules/ai-in/";
import AiOut from "../modules/ai-out/";
import AttendanceNfc from "../modules/attendance-nfc";
import AttendanceNfcOut from "../modules/attendance-nfc-out";
import CheckinCamera from "../modules/checkin-camera/index";
import ReportCamera from "../modules/report-camera";
import Guards from "../modules/guards";
import GuardsScanner from "../modules/guards-scanner";
import GuardsVerify from "../modules/guards-verify";
import Status from "../modules/status";
import QrScannerFaster from "../modules/qr-scanner-faster";

import Camera from "@src/scenes/camera";

const tabConfig = {
  Clocking: {
    screen: SummaryContainer,
    navigationOptions: {
      tabBarIcon: <Image source={require("../assets/icons/clock.png")} />,
      tabBarLabel: (
        <Text style={[configs.titleHeader, { marginTop: 5, fontSize: 12 }]}>
          Clocking
        </Text>
      )
    }
  },
  Panic: {
    screen: SummaryPanicContainer,
    navigationOptions: {
      tabBarIcon: <Image source={require("../assets/icons/caution.png")} />,
      tabBarLabel: (
        <Text style={[configs.titleHeader, { marginTop: 5, fontSize: 12 }]}>
          Panic
        </Text>
      )
    }
  }
};

const routeConfig = {
  Achievment: {
    screen: Achievment
  },
  Camera: {
    screen: CameraBox,
    navigationOptions: {
      header: null
    }
  },
  Preview: {
    screen: CameraPreview,
    navigationOptions: {
      header: null
    }
  },
  Home: {
    screen: HomeMain
  },
  HomeCheckin: {
    screen: HomeCheckin,
    backButtonTitle: "",
    navigationOptions: {
      headerTitleStyle: {
        fontFamily: "JosefinSans-Light",
        color: colors.light,
        textAlign: "center",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  HomeCheckout: {
    screen: HomeCheckout,
    navigationOptions: {
      headerTitleStyle: {
        fontFamily: "JosefinSans-Light",
        color: colors.light,
        textAlign: "center",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      headerTitleStyle: {
        color: "#fff",
        textAlign: "center",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Users: {
    screen: Users,
    navigationOptions: {
      headerTitleStyle: {
        textAlign: "center",
        color: "#fff",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  AiIn: {
    screen: AiIn
  },
  AiOut: {
    screen: AiOut
  },
  AttendanceNfc: {
    screen: AttendanceNfc
  },
  AttendanceNfcOut: {
    screen: AttendanceNfcOut
  },
  Guards: {
    screen: Guards,
    navigationOptions: {
      headerTitleStyle: {
        textAlign: "center",
        color: "#fff",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Status: {
    screen: Status,
    navigationOptions: {
      headerTitleStyle: {
        textAlign: "center",
        color: "#fff",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  GuardScanner: {
    screen: GuardsScanner,
    header: null
  },
  GuardVerify: {
    screen: GuardsVerify,
    navigationOptions: {
      headerTitleStyle: {
        textAlign: "center",
        color: "#fff",
        width: "90%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Checkin: {
    screen: Checkin,
    navigationOptions: {
      headerStyle: {
        position: "absolute",
        backgroundColor: "transparent",
        top: 0,
        left: Platform.OS === "ios" ? 4 : -10,
        right: 0
      }
    }
  },
  CheckinCamera: {
    screen: CheckinCamera
  },
  ReportCamera: {
    screen: ReportCamera
  },
  CheckinStatus: {
    screen: CheckinStatus,
    navigationOptions: {
      header: null
    }
  },
  Flag: {
    screen: Flag
  },
  Checkout: {
    screen: Checkout,
    navigationOptions: {
      headerStyle: {
        position: "absolute",
        backgroundColor: "transparent",
        top: 0,
        left: Platform.OS === "ios" ? 4 : -10,
        right: 0
      }
    }
  },
  CheckoutStatus: {
    screen: CheckoutStatus,
    navigationOptions: {
      header: null
    }
  },
  DeliveryComponent: {
    screen: DeliveryComponent
  },
  Visitor: {
    screen: StackNavigator(
      {
        Main: {
          screen: VisitorMainComponent
        },
        VisitorCheckin: {
          screen: VisitorQr
        },
        VisitorInDetail: {
          screen: VisitorInDetail
        },

        VisitorSummary: {
          screen: VisitorSummary
        },
        VisitorInformation: {
          screen: VisitorInformation
        },
        VisitorCheckout: {
          screen: VisitorCheckoutComponent
        },
        VisitorOfflineList: {
          screen: VisitorOfflineList
        },
        ModalVisitor: {
          screen: ModalVisitor
        }
      },
      {
        initialRouteName: "Main",
        mode: "modal",
        headerMode: "none"
      }
    )
  },
  VisitorScanner: {
    screen: VisitorScanner
  },
  VisitorFlag: {
    screen: VisitorFlag
  },
  Camera: {
    screen: Camera,
    navigationOptions: {
      header: null
    }
  },
  Checkpoint: {
    screen: Checkpoint,
    navigationOptions: {
      title: "Checkpoint",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: "#2493DD"
      }
    }
  },
  Clocking: {
    screen: Clocking,
    navigationOptions: {
      headerStyle: {
        backgroundColor: colors.info,
        borderBottomWidth: 0
      },
      headerTintColor: colors.light,
      headerLeft: null
    }
  },
  NewClocking: {
    screen: NewClocking
  },
  IncompleteReport: {
    screen: IncompleteReport,
    navigationOptions: {
      headerStyle: {
        backgroundColor: colors.info,
        borderBottomWidth: 0
      },
      headerTintColor: colors.light
    }
  },
  FaceRecognition: {
    screen: FaceRecognition,
    navigationOptions: {
      headerStyle: {
        backgroundColor: colors.info,
        borderBottomWidth: 0
      },
      headerTintColor: colors.light
    }
  },
  FaceScanner: {
    screen: FaceScanner,
    navigationOptions: {
      headerTransparent: true
    }
  },
  FaceResultIn: {
    screen: FaceResultIn,
    navigationOptions: {
      headerTransparent: true
    }
  },

  FaceResultOut: {
    screen: FaceResultOut,
    navigationOptions: {
      headerTransparent: true
    }
  },

  ScanningFace: {
    screen: Scanning,
    navigationOptions: {
      header: null
    }
  },

  QrCode: {
    screen: QrCodeComponent,
    navigationOptions: {
      headerStyle: {
        backgroundColor: colors.info,
        borderBottomWidth: 0
      },
      headerTintColor: colors.light
    }
  },
  QrScan: {
    screen: QrScanComponent
  },
  QrShow: {
    screen: QrShowComponent,
    navigationOptions: {
      title: "Qr Code",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  QrFaster: {
    screen: QrScannerFaster
  },
  Language: {
    screen: Language,
    navigationOptions: {
      title: "Language",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Report: {
    screen: ReportComponent
  },
  ReportList: {
    screen: ReportListComponent,
    navigationOptions: {
      title: "Report",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  ReportPosts: {
    screen: ReportPosts
  },
  ClockingReport: {
    screen: ClockingReportComponent
  },
  Files: {
    screen: FilesComponent,
    navigationOptions: {
      title: "Files",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Maps: {
    screen: MapsComponent,
    navigationOptions: {
      header: null
    }
  },
  Panic: {
    screen: PanicComponent,
    navigationOptions: {
      title: "Panic",
      headerTitleStyle: {
        color: "#fff"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  FarAway: {
    screen: FarAway,
    navigationOptions: {
      header: null
    }
  },
  Warning: {
    screen: WarningClocking,
    navigationOptions: {
      header: null
    }
  },
  Nfc: {
    screen: NfcScan,
    navigationOptions: {
      header: null
    }
  },
  NfcRead: {
    screen: NfcRead,
    navigationOptions: {
      header: null
    }
  },
  Expired: {
    screen: ExpiredContainer,
    navigationOptions: {
      header: null
    }
  },
  Summary: {
    screen: TabNavigator(tabConfig, {
      tabBarPosition: "top",
      tabBarOptions: {
        showIcon: true,
        activeBackgroundColor: colors.info,
        tabStyle: {},
        inactiveTintColor: "lightgrey",
        style: {
          backgroundColor: colors.info
        }
      },
      lazy: true
    }),
    navigationOptions: {
      headerTitle: <Text style={[configs.titleHeader]}>History</Text>,
      headerRight: <View />
    }
  },
  Scanning: {
    screen: ScanningComponent,
    navigationOptions: {
      title: "Scanning Method",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Notification: {
    screen: NotificationComponent,
    navigationOptions: {
      title: "Notification",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Timeclock: {
    screen: Timeclock,
    navigationOptions: {
      title: "Change Time Mode",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Timeclock: {
    screen: Timeclock,
    navigationOptions: {
      title: "Language",
      headerTitleStyle: {
        color: "#fff",
        width: "100%"
      },
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: colors.info
      }
    }
  },
  Pdf: {
    screen: PdfContainer
  },
  Sop: {
    screen: SopContainer
  },
  SopDetail: {
    screen: SopDetailContainer
  },
  Testing: {
    screen: Testing
  }
};

const stackConfig = {
  initialRouteName: "Home",
  transitionConfig: () => ({
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0
    }
  })
};

const drawerConfig = {
  initialRouteName: "Home",
  contentComponent: Sidebar
};

export const NavigationAuth = DrawerNavigator(
  {
    Home: {
      screen: StackNavigator(routeConfig, stackConfig)
    }
  },
  drawerConfig
);
