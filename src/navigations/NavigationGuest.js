import React, { Component } from "react";
import { Text, Icon } from "native-base";
import { StackNavigator } from "react-navigation";
import getSlideFromRightTransition from "react-navigation-slide-from-right-transition";
import { colors } from "@styles/";

/* Import views */
import Login from "@src/containers/login/LoginContainer";
import Landing from "@components/landing/LandingComponent";

const routeConfig = {
  Landing: {
    screen: Landing,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  }
};

const stackConfig = {
  initialRouteName: "Landing",
  transitionConfig: getSlideFromRightTransition
};

export const NavigationGuest = StackNavigator(routeConfig, stackConfig);
