import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";

Reactotron.configure({ host: "localhost", name: "Patrol app" })
  .useReactNative()
  // .use(reactotronRedux()) // cant use for redux right now, because redux-persist
  .connect();
