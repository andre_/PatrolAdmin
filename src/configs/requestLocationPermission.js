import { PermissionsAndroid } from "react-native";

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location Permission",
          message: "Apps need you to activate GPS"
        }
      );

      console.log("granted", granted);
      if (granted) {
        console.log("You can use location");
      } else {
        console.log("Location permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }