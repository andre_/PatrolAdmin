import { Sentry, SentryLog, SentrySeverity } from "react-native-sentry";

const configure = url => {
  Sentry.config(url, {
    logLevel: SentryLog.Debug
  }).install();
};

const captureMessage = msg => {
  Sentry.captureMessage(msg, {
    level: SentrySeverity.Error
  });
};

const captureException = err => {
  Sentry.captureException(err, {});
};

export { configure, captureMessage, captureException };
