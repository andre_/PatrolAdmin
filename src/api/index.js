import { httpService } from "@src/services/APIService";
import * as endpoint from "./constants";

class Api {
  static saveClocking(data, copiedData) {
    return httpService()
      .post(`${endpoint.SAVE_CLOCKING}?checkpoint=${JSON.stringify(data)}`)
      .then(resp => resp);
  }

  static saveTelegram(fd) {
    return httpService()
      .post(endpoint.SAVE_TELEGRAM, fd)
      .then(resp => resp);
  }

  static getListCheckpoint(obj) {
    return httpService()
      .post(endpoint.GET_LIST_CHECKPOINT, obj)
      .then(resp => resp);
  }

  static checkinAI(obj) {
    return httpService()
      .post("v2/attendance/ai/checkin", obj)
      .then(resp => resp);
  }

  static checkoutAI(obj) {
    return httpService()
      .post("v2/attendance/ai/checkout", obj)
      .then(resp => resp);
  }
}

export default Api;
