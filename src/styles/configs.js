import { light, background, success } from "./colors";

module.exports = {
  content: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 10,
    paddingRight: 10,
    position: "relative"
  },
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: background,
    overflow: "hidden",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonHeader: {
    width: "100%",
    paddingLeft: 10,
    paddingRight: 10,
    bottom: 0,
    position: "absolute",
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between"
  },
  buttonHeaderIn: {
    borderRadius: 4,
    width: "48%",
    height: 60,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  bar: {
    flex: 1,
    marginTop: 0,
    height: 280,
    alignItems: "center",
    justifyContent: "center"
  },
  card: {
    borderRadius: 4,
    padding: 15,
    backgroundColor: light,
    minHeight: 40,
    display: "flex",
    flexDirection: "column"
  },
  addMarginBottom: {
    marginBottom: 14
  },
  buttonBigDefault: {
    width: "48%",
    height: 60,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  spaceBetween: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  noHeader: {
    marginTop: 45
  },
  noButtonShadow: {
    shadowOffset: {
      height: 0,
      width: 0
    },
    shadowOpacity: 0,
    elevation: 0
  },
  textCenter: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  flexRowCenter: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  flexColumnCenter: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  notification: {
    height: 40,
    borderRadius: 4,
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: success
  },
  textClocking: {
    color: light,
    fontSize: 20,
    fontFamily: "JosefinSans-Light"
  },
  notFound: {
    width: "90%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: "#f2f2f2",
    marginTop: 20,
    marginLeft: "auto",
    marginRight: "auto",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: light
  },
  modalBackground: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "rgba(0, 0, 0, 0.6)"
  },
  activityIndicatorWrapper: {
    backgroundColor: "#ffffff",
    width: 100,
    height: 100,
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around"
  },
  panicButton: {
    position: "absolute",
    bottom: 0,
    width: 100,
    height: 200,
    backgroundColor: success
  },
  titleHeader: {
    fontSize: 20,
    width: "100%",
    color: light,
    fontFamily: "OpenSans-SemiBold",
    textAlign: "center"
  },
  backButton: {
    width: 50,
    height: 20
  },
  imageBack: {
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  },
  textWhite: {
    color: light
  },
  text: {
    fontFamily: "OpenSans-Regular"
  }
};
