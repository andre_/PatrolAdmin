import colors from "./colors";

module.exports = {
  h1: {
    fontSize: 32
  },
  h2: {
    fontSize: 24
  },
  h3: {
    fontSize: 18.72
  },
  h4: {
    color: colors.dark,
    fontFamily: "JosefinSans-Bold",
    fontSize: 16
  },
  p: {
    fontFamily: "JosefinSans-Regular",
    fontSize: 16,
    color: "#999"
  },
  regular: {
    fontFamily: "JosefinSans-Regular",
    fontSize: 14,
    color: "#333"
  },
  spaceBottom: {
    marginBottom: 4
  },
  setCenter: {
    textAlign: "center"
  },
  textActive: {
    color: "mediumseagreen"
  },
  textNotActive: {
    color: "tomato"
  },
  bold: {
    fontWeight: "500"
  }
};
