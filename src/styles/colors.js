module.exports = {
  dark: "#4a4a4a",
  black: "#000",
  light: "#fff",
  danger: "#FD6855",
  info: "#2493DD",
  text: "#999",
  success: "#5FC039",
  background: "#F5F5F5",
  border: "#dadada",
  warning: "#FFA23D"
};
