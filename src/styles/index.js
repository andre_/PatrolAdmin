import configs from "@styles/configs";
import colors from "@styles/colors";
import text from "@styles/text";

module.exports = {
  text,
  configs,
  colors
};
