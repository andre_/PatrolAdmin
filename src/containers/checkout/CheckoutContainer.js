import React, { Component } from "react";
import { connect } from "react-redux";
import { initAttendance } from "@src/actions/attendanceActions";
import CheckoutComponent from "@components/checkout/CheckoutComponent";

const mapStateToProps = state => ({
  staff: state.staff,
  flag: state.flag
});

const mapDispatchToProps = dispatch => ({
  shareAttendance(data) {
    console.log("attendance container checkout", data);
    dispatch(initAttendance(data));
  }
});

const CheckoutContainer = props => <CheckoutComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutContainer);
