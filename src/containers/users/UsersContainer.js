import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import UsersComponent from "@components/users/UsersComponent";

const mapStateToProps = state => ({
  loading: state.loading,
  account: state.account
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  }
});

const UsersContainer = props => <UsersComponent {...props} />;

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);
