import React, { Component } from "react";
import { connect } from "react-redux";
import { initAttendance } from "@src/actions/attendanceActions";
import { staffIn } from "@src/actions/staffActions";
import { supervisorImage } from "@src/actions/supervisorActions";
import CheckinComponent from "@components/checkin/CheckinComponent";

const mapStateToProps = state => ({
  staff: state.staff,
  flag: state.flag,
  supervisor: state.supervisor
});

const mapDispatchToProps = dispatch => ({
  shareAttendance(data) {
    dispatch(initAttendance(data));
  },

  setStaffIn(data) {
    dispatch(staffIn(data));
  },

  supervisorImage(data) {
    dispatch(supervisorImage(data));
  }
});

const CheckinContainer = props => <CheckinComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckinContainer);
