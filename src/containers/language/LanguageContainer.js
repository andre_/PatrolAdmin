import React, { Component } from "react";
import { connect } from "react-redux";
import LanguageComponent from "@components/language/LanguageComponent";
import { setToEn, setToMy, setToTh } from "@src/actions/languangeActions";

const mapStateToProps = state => ({
  languange: state.languange
});

const mapDispatchToProps = dispatch => ({
  setEn() {
    dispatch(setToEn());
  },

  setMy() {
    dispatch(setToMy());
  },

  setTh() {
    dispatch(setToTh());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageComponent);
