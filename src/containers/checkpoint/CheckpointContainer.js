import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import {
  initCheckpoint,
  addCheckpoint,
  deleteCheckpoint,
  sharingId,
  searchCheckpoint
} from "@src/actions/checkpointActions";
import CheckpointComponent from "@components/checkpoint/CheckpointComponent";

const mapStateToProps = state => ({
  loading: state.loading,
  connection: state.connection,
  posts: state.posts,
  checkpoint: state.checkpoint,
  sharingId: state.sharingId,
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  initCheckpoint(data) {
    dispatch(initCheckpoint(data));
  },

  addNewCheckpoint(data) {
    dispatch(addCheckpoint(data));
  },

  deleteCheckpoint(id) {
    dispatch(deleteCheckpoint(id));
  },

  sharingId(id) {
    dispatch(sharingId(id));
  },

  searchCheckpoint(data) {
    dispatch(searchCheckpoint(data));
  }
});

const CheckpointContainer = props => <CheckpointComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckpointContainer);
