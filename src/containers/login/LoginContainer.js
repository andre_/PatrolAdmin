import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import { initPosts } from "@src/actions/postsActions";
import { initToken } from "@src/actions/authActions";
import { initPhone, destroyPhone } from "@src/actions/accountActions";
import { endClocking } from "@src/actions/clockingActions";
import LoginComponent from "@components/login/LoginComponent";

const mapStateToProps = state => ({
  loading: state.loading,
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  initPosts(data) {
    dispatch(initPosts(data));
  },

  initToken(data) {
    dispatch(initToken(data));
  },

  initPhone(data) {
    dispatch(initPhone(data));
  },

  destroyPhone(data) {
    dispatch(destroyPhone(data));
  },

  endClocking() {
    dispatch(endClocking());
  }
});

const LoginContainer = props => <LoginComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);
