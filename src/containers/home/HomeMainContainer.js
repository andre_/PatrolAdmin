import React, { Component } from "react";
import { Text, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import { initAccount } from "@src/actions/accountActions";
import { initDistance } from "@src/actions/distanceActions";
import { playSirene, stopSirene } from "@src/actions/notificationActions";
import { isLate } from "@src/actions/accountActions";
import {
  initSupervisor,
  destroySupervisor
} from "@src/actions/supervisorActions";
import { endClocking } from "@src/actions/clockingActions";
import { destroyToken } from "@src/actions/authActions";
import { initCheckpoint } from "@src/actions/checkpointActions";
import { showNotification, hideNotification } from "@src/actions/globalActions";
import HomeMainComponent from "@components/home/HomeMainComponent";
import HeaderProfile from "@components/partials/HeaderProfile";
import { configs, colors } from "@styles";

const mapStateToProps = state => {
  return {
    account: state.account,
    loading: state.loading,
    posts: state.posts,
    distance: state.distance,
    message: state.message,
    notification: state.notification,
    clocking: state.clocking,
    supervisor: state.supervisor,
    languange: state.languange,
    connection: state.connection,
    scanning: state.scanning,
    flag: state.flag
  };
};

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  gatherAccount(data) {
    dispatch(initAccount(data));
  },

  initCheckpoint(data) {
    dispatch(initCheckpoint(data));
  },

  initSupervisor(data) {
    dispatch(initSupervisor(data));
  },

  destroySupervisor() {
    dispatch(destroySupervisor());
  },

  endClocking() {
    dispatch(endClocking());
  },

  destroyToken() {
    dispatch(destroyToken());
  },

  isLate(data) {
    dispatch(isLate(data));
  },

  initDistance(data) {
    dispatch(initDistance(data));
  },

  doPlaySirene() {
    dispatch(playSirene());
  },

  doStopSirene() {
    dispatch(stopSirene());
  },

  doShowNotification(msg) {
    dispatch(showNotification(msg));
  },

  doHideNotification() {
    dispatch(hideNotification());
  },

  stopAlarm() {
    dispatch(stopSirene());
  }
});

class HomeMainContainer extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Text style={configs.titleHeader}>Home</Text>,
      headerStyle: {
        backgroundColor: colors.info
      },
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.navigate("DrawerOpen")}>
          <Image
            source={require("../../assets/icons/menu.png")}
            style={{ width: 25, height: 25, marginLeft: 15 }}
          />
        </TouchableOpacity>
      ),
      headerRight: <HeaderProfile />
    };
  };

  render() {
    return <HomeMainComponent {...this.props} />;
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeMainContainer);
