import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import { initStaff } from "@src/actions/staffActions";
import { searchStaffIn } from "@src/actions/accountActions";
import HomeCheckinComponent from "@components/home/HomeCheckinComponent";

const mapStateToProps = state => ({
  loading: state.loading,
  account: state.account,
  staff: state.staff,
  supervisor: state.supervisor
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  initStaff(data) {
    dispatch(initStaff(data));
  },

  searchStaffIn(data) {
    dispatch(searchStaffIn(data));
  }
});

const HomeCheckinContainer = props => <HomeCheckinComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeCheckinContainer);
