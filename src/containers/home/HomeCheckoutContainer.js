import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import { initStaff } from "@src/actions/staffActions";
import { searchStaffIn } from "@src/actions/accountActions";
import HomeCheckoutComponent from "@components/home/HomeCheckoutComponent";

const mapStateToProps = state => ({
  loading: state.loading,
  account: state.account
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  initStaff(data) {
    dispatch(initStaff(data));
  },

  searchStaffIn(data) {
    dispatch(searchStaffIn(data));
  }
});

const HomeCheckoutContainer = props => <HomeCheckoutComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeCheckoutContainer);
