import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import {
  addDummyCheckpoint,
  addFailedData,
  shareClocking,
  endClocking,
  initClocking,
  firstCheckpoint,
  finishClocking,
  getClockingId,
  destroyClockingId,
  uploadImageTelegram
} from "@src/actions/clockingActions";
import ClockingComponent from "@components/ClockingComponent";

const mapStateToProps = state => ({
  account: state.account,
  clocking: state.clocking,
  checkpoint: state.checkpoint,
  connection: state.connection,
  loading: state.loading,
  posts: state.posts,
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  addDummyCheckpoint(data) {
    dispatch(addDummyCheckpoint(data));
  },

  shareClocking(data) {
    dispatch(shareClocking(data));
  },

  clearClocking() {
    dispatch(endClocking());
  },

  initClocking(data) {
    dispatch(initClocking(data));
  },

  doUploadImageTelegram(data) {
    dispatch(uploadImageTelegram(data));
  },

  getOneCheckpoint(data) {
    dispatch(firstCheckpoint(data));
  },

  doFinishClocking(data) {
    dispatch(finishClocking(data));
  },

  getClockingId(data) {
    dispatch(getClockingId(data));
  },

  destroyClockingId() {
    dispatch(destroyClockingId);
  },

  addFailedData(data) {
    dispatch(addFailedData(data));
  }
});

const ClockingContainer = props => <ClockingComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClockingContainer);
