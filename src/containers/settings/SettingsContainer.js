import React, { Component } from "react";
import { connect } from "react-redux";
import { startLoading, stopLoading } from "@src/actions/loadingActions";
import { destroyToken } from "@src/actions/authActions";
import { destroyAccount } from "@src/actions/accountActions";
import SettingsComponent from "@components/settings/SettingsComponent";

const mapStateToProps = state => ({
  loading: state.loading,
  posts: state.posts,
  account: state.account,
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  startLoading() {
    dispatch(startLoading());
  },

  stopLoading() {
    dispatch(stopLoading());
  },

  destroyToken() {
    dispatch(destroyToken());
  },

  destroyAccount() {
    dispatch(destroyAccount());
  }
});

const SettingsContainer = props => <SettingsComponent {...props} />;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsContainer);
