import { connect } from "react-redux";
import TimeclockComponent from "@components/timeclock/TimeclockComponent";
import {
  setTimeToDefault,
  setTimeFromServer
} from "@src/actions/timeclockActions";

const mapStateToProps = state => ({
  timeclock: state.timeclock
});

const mapDispatchToProps = dispatch => ({
  setTimeToDefault() {
    dispatch(setTimeToDefault());
  },

  setTimeFromServer() {
    dispatch(setTimeFromServer());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeclockComponent);
