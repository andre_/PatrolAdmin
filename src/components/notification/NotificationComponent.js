import React, { Component } from "react";
import { View, Text, BackHandler } from "react-native";
import { List, ListItem, Icon, Left, Body } from "native-base";
import { connect } from "react-redux";
import { colors } from "@styles/";
import { changeBuzztype } from "@src/actions/notificationActions";

const mapStateToProps = state => ({
  notification: state.notification
});

const mapDispatchToProps = dispatch => ({
  doChangeBuzztype(data) {
    dispatch(changeBuzztype(data));
  }
});

class ScanningComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    const {
      notification: { buzztype }
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: colors.light }}>
        <List>
          <ListItem
            icon
            onPress={() => {
              this.props.doChangeBuzztype("normal");
            }}
          >
            <Left style={{ width: 40 }}>
              {buzztype === "normal" && (
                <Icon type="FontAwesome" name="check" />
              )}
            </Left>
            <Body>
              <Text>Normal</Text>
            </Body>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.doChangeBuzztype("infinite");
            }}
          >
            <Left style={{ width: 40 }}>
              {buzztype === "infinite" && (
                <Icon type="FontAwesome" name="check" />
              )}
            </Left>
            <Body>
              <Text>Infinite</Text>
            </Body>
          </ListItem>
        </List>
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScanningComponent);
