import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  BackHandler,
  Image,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { Button, Input, Item, Label, List, Icon, Spinner } from "native-base";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import { colors, configs } from "@styles";
import { API } from "@src/services/APIService";
import ListCheckpoint from "./ListCheckpoint";
import ActionButton from "react-native-action-button";

const slideAnimation = new SlideAnimation({
  slideFrom: "bottom"
});

export default class CheckpointComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      visible: false,
      confirm: false,
      refreshing: false,
      description: "",
      dataOffline: [],
      qr: "andre",
      number: 0,
      lat: "",
      lng: ""
    };

    this.handleBackButton = this.handleBackButton.bind(this);
    this._getLocation = this._getLocation.bind(this);
  }

  componentDidMount() {
    this._initCheckpoint();
    this._getLocation();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  _getLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      },
      err => {
        console.log("error maps", err.message);
      }
    );
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _handleRefresh() {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this._initCheckpoint();
      }
    );
  }

  _initCheckpoint() {
    const {
      posts: { posts },
      initCheckpoint
    } = this.props;

    const obj = new FormData();
    obj.append("post_id", posts.post_id);

    API()
      .post("get_list_checkpoint", obj)
      .then(res => {
        this.setState({ refreshing: false });
        initCheckpoint(res.data);
      })
      .catch(err => {
        this.setState({ refreshing: false });
        console.log(err);
      });
  }

  _addCheckpoint() {
    const {
      posts: { posts },
      scanning: { method },
      checkpoint: { checkpoint }
    } = this.props;

    let coords = `${this.state.lat},${this.state.lng}`;
    let isDuplicateName = (checkpoint || []).filter(
      v => v.description === this.state.description
    );

    if (method === "qrcode") {
      const obj = {
        description: this.state.description,
        number: this.state.number++,
        post_id: posts.post_id,
        gps: coords
      };

      API()
        .post("add_checkpoint", obj)
        .then(res => {
          this.addCheckpoint.dismiss();
          this.props.addNewCheckpoint({
            id: parseInt(res.data.id),
            description: this.state.description
          });
          this.setState({ visible: false, description: "" });
        })
        .catch(err => {
          alert("Failed to add qr code.");
        });
    } else {
      console.log("isDuplicateName", isDuplicateName.length ? true : false);

      if (this.state.description === "") {
        alert("Please fill the description first");
      } else if (isDuplicateName.length) {
        alert("Duplicate checkpoint name");
      } else {
        this.setState({
          number: this.state.number + 1
        });

        const obj = {
          description: this.state.description,
          number: checkpoint && checkpoint.length + 1,
          post_id: posts.post_id,
          gps: coords
        };

        API()
          .post("add_checkpoint", obj)
          .then(res => {
            this.addCheckpoint.dismiss();
            this.props.navigation.navigate("Nfc", {
              qrId: res.data.id,
              description: this.state.description
            });
            this.setState({ visible: false, description: "" });
          })
          .catch(err => {
            alert("Failed to add nfc. Check your network");
          });
      }
    }
  }

  _deleteCheckpoint() {
    this.popupDialog.dismiss();

    const {
      checkpoint: { sharingId }
    } = this.props;

    this.props.deleteCheckpoint(sharingId);
  }

  readTagId() {
    getTagId();
  }

  searchForCheckpoint(text) {
    this.props.searchCheckpoint(text);
  }

  render() {
    const {
      loading: { show },
      checkpoint: { checkpoint, selected }
    } = this.props;

    return (
      <KeyboardAvoidingView
        contentContainerStyle={{ flex: 1, height: "100%" }}
        style={{ flex: 1, height: "100%" }}
      >
        <View style={{ flex: 1, backgroundColor: colors.light }}>
          <View
            style={[
              {
                display: "flex",
                justifyContent: "center",
                backgroundColor: colors.info,
                height: 70,
                padding: 10
              }
            ]}
          >
            <Input
              placeholder="Search"
              style={{
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: colors.light,
                borderRadius: 4
              }}
              onChangeText={text => this.searchForCheckpoint(text)}
            />
          </View>

          <ScrollView style={{ flex: 1, zIndex: -1 }}>
            {checkpoint && checkpoint.length === 0 && (
              <View style={configs.notFound}>
                <Image
                  source={require("../../assets/404-error.png")}
                  style={{ width: 100, height: 100 }}
                />
                <Text
                  style={{
                    color: colors.dark,
                    fontSize: 18,
                    paddingTop: 8,
                    fontFamily: "JosefinSans-SemiBold"
                  }}
                >
                  Checkpoint not found
                </Text>
              </View>
            )}

            <List>
              <FlatList
                data={selected}
                refreshing={this.state.refreshing}
                onRefresh={() => this._handleRefresh()}
                renderItem={({ item, index }) => {
                  return (
                    <ListCheckpoint
                      index={index}
                      {...item}
                      {...this.props}
                      loading={false}
                      visible={this.state.isVisible}
                      popupDialog={this.popupDialog}
                    />
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </List>
          </ScrollView>

          <ActionButton
            style={{
              position: "absolute",
              bottom: 10
            }}
          >
            <ActionButton.Item
              buttonColor={colors.success}
              title="New Checkpoint"
              onPress={() => this.addCheckpoint.show()}
            >
              <Icon name="md-create" style={configs.actionButtonIcon} />
            </ActionButton.Item>
            {/* <ActionButton.Item
              buttonColor="#6163ad"
              title="Read Checkpoint"
              onPress={() => this.props.navigation.navigate("NfcRead")}
            >
              <Icon name="md-search" style={configs.actionButtonIcon} />
            </ActionButton.Item> */}
          </ActionButton>
          {/* </View> */}

          <PopupDialog
            dialogStyle={{
              width: 300,
              height: 220,
              marginTop: -180
            }}
            dialogTitle={<DialogTitle title="Add Checkpoint" />}
            ref={popupDialog => {
              this.addCheckpoint = popupDialog;
            }}
          >
            <View
              style={{
                paddingTop: 10,
                paddingRight: 24,
                paddingLeft: 24
              }}
            >
              <Item
                stackedLabel
                style={{
                  marginTop: 10,
                  height: 80,
                  borderWidth: 1
                }}
              >
                <Label style={{ fontSize: 16 }}>
                  Please enter checkpoint name
                </Label>
                <Input
                  placeholder="Type here"
                  value={this.state.description}
                  onChangeText={text => this.setState({ description: text })}
                />
              </Item>
            </View>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 12,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                success
                onPress={() => this._addCheckpoint()}
                style={{
                  marginTop: 20,
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>OK</Text>
              </Button>
            </View>
          </PopupDialog>

          <PopupDialog
            dialogStyle={{
              width: 300,
              height: 150
            }}
            containerStyle={{ marginTop: -50 }}
            dialogTitle={<DialogTitle title="Delete" />}
            ref={popupDialog => {
              this.popupDialog = popupDialog;
            }}
            dialogAnimation={slideAnimation}
          >
            <View
              style={{
                flex: 1,
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 20
              }}
            >
              <Text>Delete this checkpoint?</Text>
              <View
                style={{
                  position: "absolute",
                  right: 4,
                  bottom: 18,
                  display: "flex",
                  flexDirection: "row"
                }}
              >
                <TouchableOpacity
                  danger
                  onPress={() => this.popupDialog.dismiss()}
                  style={{
                    backgroundColor: colors.success,
                    marginRight: 10,
                    width: 80,
                    height: 30,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 20
                  }}
                >
                  <Text style={{ color: colors.light }}>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  success
                  onPress={() => this._deleteCheckpoint()}
                  style={{
                    backgroundColor: colors.danger,
                    marginRight: 10,
                    width: 80,
                    height: 30,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 20
                  }}
                >
                  {show ? (
                    <Spinner color={colors.light} />
                  ) : (
                    <Text style={{ color: colors.light }}>DELETE</Text>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </PopupDialog>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
