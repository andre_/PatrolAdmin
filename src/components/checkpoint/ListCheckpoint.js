import React from "react";
import PropTypes from "prop-types";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { ListItem, Button, Icon, Body } from "native-base";
import { connect } from "react-redux";
import { colors, text } from "@styles";

const mapStateToProps = state => ({
  scanning: state.scanning
});

class ListCheckpoint extends React.Component {
  constructor(props) {
    super(props);
    this.showDialog = this.showDialog.bind(this);
  }

  _conditional() {
    if (this.props.scanning.method === "nfc") {
      this.props.navigation.navigate("NfcRead");
    } else {
      this.props.navigation.navigate("QrShow", { qrId: id });
    }
  }

  showDialog(id) {
    this.props.sharingId(id);
    this.props.popupDialog.show();
  }

  render() {
    const { id, description, index } = this.props;

    return (
      <ListItem
        style={{
          display: "flex",
          justifyContent: "space-between",
          height: 60,
          paddingTop: 5,
          paddingBottom: 0
        }}
      >
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Body>
            <Text
              style={[
                text.h4,
                {
                  fontWeight: "bold",
                  fontSize: 16,
                  color: colors.dark
                }
              ]}
            >
              {description}
            </Text>
            <Text>Position: {index + 1}</Text>
          </Body>
        </View>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <TouchableOpacity
            rounded
            info
            style={[styles.button, { backgroundColor: colors.info }]}
            onPress={() => this._conditional()}
          >
            <Icon
              type="FontAwesome"
              name="eye"
              style={{
                marginTop: 20,
                fontSize: 16,
                width: 35,
                height: 35,
                display: "flex",
                alignItems: "center",
                textAlign: "center",
                color: colors.light
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            rounded
            style={[
              styles.button,
              {
                marginLeft: 10,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: colors.danger
              }
            ]}
            onPress={() => this.showDialog(id)}
          >
            <Text style={{ color: "#fff" }}>X</Text>
            {/* {loading ? (
            <ActivityIndicator color="#fff" />
          ) : (
            <Icon type="FontAwesome" name="trash" style={{ fontSize: 16 }} />
          )} */}
          </TouchableOpacity>
        </View>
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 0,
    elevation: 0,
    width: 40,
    height: 40
  }
});

ListCheckpoint.defaultProps = {
  loading: false
};

ListCheckpoint.propTypes = {
  loading: PropTypes.bool
};

export default connect(mapStateToProps)(ListCheckpoint);
