import { Platform } from "react-native";
import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  body: {
    width: "100%",
    height: Platform.OS === "ios" ? 700 : 480,
    marginTop: 40,
    marginLeft: "auto",
    marginRight: "auto"
  },
  buttonPlace: {
    position: "absolute",
    width: "100%",
    bottom: 0,
    left: 0
  },
  button: {
    backgroundColor: colors.info,
    height: 60,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontFamily: "OpenSans-SemiBold",
    color: colors.light,
    fontSize: 16
  },
  textFirst: {
    fontFamily: "OpenSans-Bold",
    color: colors.dark,
    fontSize: 24,
    marginBottom: 20,
    marginTop: 40
  },
  textDesc: {
    fontFamily: "OpenSans-Regular",
    color: "#333",
    textAlign: "center",
    fontSize: 14,
    width: "80%",
    lineHeight: 28
  },
  slide1: {
    width: 300,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: Platform.OS === "ios" ? 40 : 0,
    elevation: 8,
    shadowOpacity: 0.2,
    shadowRadius: 4,
    shadowOffset: {
      height: 0,
      width: 0
    },
    borderRadius: 10,
    height: Platform.OS === "ios" ? "80%" : "90%",
    backgroundColor: colors.light,
    justifyContent: "center",
    alignItems: "center"
  }
};

export default styles;
