import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import Swiper from "react-native-swiper";
import styles from "./styles";

export default class LandingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0
    };
  }

  componentWillUpdate(newProps, newState) {
    newState.index === 4 && this.props.navigation.navigate("Login");
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <Swiper paginationStyle={{ bottom: 0 }} ref="swiper">
            <View style={styles.slide1}>
              <View
                style={{
                  width: "80%"
                }}
              >
                <Image
                  source={require("../../assets/trackerhero.jpg")}
                  style={{ width: "100%", height: 50 }}
                />
              </View>
              <Text style={styles.textFirst}>Welcome</Text>
              <Text style={styles.textDesc}>
                Workforce Management Simplified
              </Text>
            </View>
            <View style={styles.slide1}>
              <Text style={styles.textFirst}>Clocking</Text>
              <Text style={styles.textDesc}>
                We provide features such as attendance, reporting, clocking and
                visitor management.
              </Text>
            </View>
            <View style={styles.slide1}>
              <Text style={styles.textFirst}>Attendance</Text>
              <Text style={styles.textDesc}>
                We provide features such as attendance, reporting, clocking and
                visitor management.
              </Text>
            </View>
            <View style={styles.slide1}>
              <Text style={styles.textFirst}>Visitor</Text>
              <Text style={styles.textDesc}>
                We provide features such as attendance, reporting, clocking and
                visitor management.
              </Text>
            </View>
            <View style={styles.slide1}>
              <Image source={require("../../assets/flags.png")} />
              <Text style={styles.textFirst}>Files</Text>
              <Text style={styles.textDesc}>
                Thank you for choosing TrackerHero as a solutions to manage your
                workforce.
              </Text>
            </View>
          </Swiper>
        </View>

        <View style={styles.buttonPlace}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate("Login")}
          >
            <Text style={styles.text}>GET STARTED</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
