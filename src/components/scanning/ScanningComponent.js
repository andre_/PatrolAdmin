import React, { Component } from "react";
import { View, Text, BackHandler } from "react-native";
import { List, ListItem, Icon, Left, Body } from "native-base";
import { connect } from "react-redux";
import RNRestart from "react-native-restart";
import { colors } from "@styles/";
import { setToNfc, setToQr } from "@src/actions/scanningActions";

const mapStateToProps = state => ({
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  setToNfc() {
    dispatch(setToNfc());
  },

  setToQr() {
    dispatch(setToQr());
  }
});

class ScanningComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    const {
      scanning: { method }
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: colors.light }}>
        <List>
          <ListItem
            icon
            onPress={() => {
              this.props.setToNfc();
              // RNRestart.Restart();
            }}
          >
            <Left style={{ width: 40 }}>
              {method === "nfc" && <Icon type="FontAwesome" name="check" />}
            </Left>
            <Body>
              <Text>NFC</Text>
            </Body>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.setToQr();
              // RNRestart.Restart();
            }}
          >
            <Left style={{ width: 40 }}>
              {method === "qrcode" && <Icon type="FontAwesome" name="check" />}
            </Left>
            <Body>
              <Text>QR Code</Text>
            </Body>
          </ListItem>
        </List>
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScanningComponent);
