import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

class AudioTrack extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Audio track</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default AudioTrack;
