import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Grid, Col } from "react-native-easy-grid";
import store from "react-native-simple-store";

import { colors } from "@styles";

class Attendance extends Component {
  state = {
    attendance: []
  };

  componentDidMount() {
    store
      .get("checkin")
      .then(checkin => {
        console.log("checkin", checkin);
        this.setState({
          attendance: checkin
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  sendCheckin = item => {
    console.log(item);
  };

  deleteItem = v => {
    console.log(v);
    let filterData = this.state.attendance.filter(x => x.id !== v.id);

    this.setState({
      attendance: filterData
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.btnDelete}>
          <Text style={[styles.textBtn, { color: colors.text }]}>Delete</Text>
        </TouchableOpacity>

        {this.state.attendance.length ? (
          this.state.attendance.map((v, key) => (
            <View style={styles.list} key={key}>
              <Grid>
                <Col style={[styles.cols, { width: 50 }]}>
                  <Text>ID - {v.id}</Text>
                </Col>

                <Col style={styles.cols}>
                  <Text>POST ID - {v.post_id}</Text>
                </Col>

                <Col style={styles.cols}>
                  <TouchableOpacity style={styles.btn}>
                    <Text style={styles.textBtn}>Send</Text>
                  </TouchableOpacity>
                </Col>

                <Col
                  style={{
                    width: 50,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity
                    style={styles.btnSmall}
                    onPress={() => this.deleteItem(v)}
                  >
                    <Text style={styles.textBtn}>X</Text>
                  </TouchableOpacity>
                </Col>
              </Grid>
            </View>
          ))
        ) : (
          <View style={styles.emptyWrapper}>
            <Text>Empty</Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1
  },
  cols: {
    alignItems: "center",
    justifyContent: "center"
  },
  list: {
    borderBottomWidth: 1,
    borderBottomColor: colors.border,
    height: 50,
    alignItems: "center"
  },
  btn: {
    backgroundColor: colors.success,
    width: 80,
    height: 30,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  btnSmall: {
    backgroundColor: colors.danger,
    width: 40,
    height: 30,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  textBtn: {
    fontFamily: "JosefinSans-Regular",
    color: colors.light
  },
  btnDelete: {
    alignItems: "center",
    justifyContent: "center",
    width: 120,
    height: 40,
    borderWidth: 1,
    borderColor: colors.border,
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 20
  },
  emptyWrapper: {
    width: "100%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  }
};

export default Attendance;
