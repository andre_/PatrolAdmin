import React, { Component, Fragment } from "react";
import {
  ScrollView,
  Alert,
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import store from "react-native-simple-store";
import { captureException } from "@src/configs/sentryConfigs";
import { colors, configs } from "@styles";
import { connect } from "react-redux";
import { API } from "@src/services/APIService";

const mapStateToProps = state => ({
  checkpoint: state.checkpoint
});

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offlinePackage: [],
      visible: false
    };
  }

  componentDidMount() {
    store
      .get("packageOffline")
      .then(packageOffline => {
        this.setState({
          offlinePackage: packageOffline
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  deleteAll = () => {
    Alert.alert(
      "Are you sure",
      "Are you sure to delete?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            store.delete("offlinePackage");
            store.delete("packageOffline");
            this.setState({
              offlinePackage: []
            });
          }
        }
      ],
      { cancelable: false }
    );
  };

  render() {
    const {
      checkpoint: { checkpoint }
    } = this.props;

    const { offlinePackage } = this.state;

    return (
      <Fragment>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity style={styles.button} onPress={this.deleteAll}>
            <Text>Delete All</Text>
          </TouchableOpacity>
        </View>
        <ScrollView
          style={[styles.container]}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexDirection: "column-reverse" }}
        >
          {offlinePackage ? (
            (offlinePackage || []).map((v, k) => (
              <View style={[styles.clock]} key={k}>
                <View style={styles.accordion}>
                  <View style={styles.accordionHeader}>
                    <Text style={styles.accordionText}>Clocking</Text>
                  </View>
                  <View style={styles.accordionBody}>
                    {v.clocking.map((x, k) => (
                      <Text style={styles.text} key={k}>
                        Checkpoint {x.id}
                      </Text>
                    ))}
                  </View>
                </View>

                <View style={styles.accordion}>
                  <View style={styles.accordionHeader}>
                    <Text style={styles.accordionText}>Report</Text>
                  </View>
                  <View style={styles.accordionBody}>
                    <Text style={styles.text}>
                      Priority: {v.report.priority}
                    </Text>
                    <Text style={styles.text}>{v.report.description}</Text>
                    <View>
                      {v.photos !== undefined &&
                        v.photos.map((x, k) => (
                          <Image
                            key={k}
                            style={styles.imageReport}
                            source={{ uri: x.uri }}
                          />
                        ))}
                    </View>
                    <View>
                      {v.images !== undefined &&
                        v.images.map((x, k) => (
                          <Image
                            key={k}
                            style={styles.imageReport}
                            source={{ uri: x.uri }}
                          />
                        ))}
                    </View>
                  </View>
                </View>
              </View>
            ))
          ) : (
            <Text>Data not found</Text>
          )}
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20
  },
  list: {
    backgroundColor: colors.light,
    height: 50,
    padding: 15,
    marginBottom: 12,
    borderRadius: 4
  },
  listSuccess: {
    backgroundColor: colors.success
  },
  listFail: {
    borderWidth: 1,
    borderColor: colors.warning
  },
  text: {
    color: colors.dark
  },
  clock: {
    borderWidth: 2,
    borderColor: "#dadada",
    borderStyle: "dotted",
    backgroundColor: colors.light,
    marginBottom: 30
  },
  listClock: {
    padding: 15,
    borderBottomWidth: 1,
    borderColor: "#f9f9f9"
  },
  sendAgain: {
    alignSelf: "flex-end",
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    width: 100,
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.info
  },
  loadingModal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    alignSelf: "center",
    backgroundColor: colors.light,
    width: 60,
    height: 60
  },
  deleteButton: {
    width: 30,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "flex-end",
    backgroundColor: colors.danger
  },
  button: {
    width: 80,
    height: 50,
    backgroundColor: colors.light,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: colors.text
  },
  buttonWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    paddingTop: 20
  },
  accordion: {},
  accordionHeader: {
    backgroundColor: colors.background,
    height: 40,
    display: "flex",
    justifyContent: "center",
    paddingLeft: 20
  },
  accordionText: {
    fontFamily: "OpenSans-SemiBold",
    fontSize: 16
  },
  accordionBody: {
    padding: 20
  },
  imageReport: {
    marginTop: 15,
    width: 70,
    height: 70
  },
  text: {
    fontFamily: "OpenSans-Regular",
    lineHeight: 30
  }
});

export default connect(mapStateToProps)(Report);
