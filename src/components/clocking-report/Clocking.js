import React, { Component, Fragment } from "react";
import {
  Alert,
  View,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Text,
  StyleSheet
} from "react-native";
import { Tab, Tabs } from "native-base";
import store from "react-native-simple-store";
import { captureException } from "@src/configs/sentryConfigs";
import moment from "moment";
import { colors } from "@styles";
import { connect } from "react-redux";
import { API } from "@src/services/APIService";
import Modal from "react-native-modal";

const mapStateToProps = state => ({
  checkpoint: state.checkpoint
});

class Clocking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failedData: [],
      visible: false
    };

    this.sendAll = this.sendAll.bind(this);
    this.deleteAll = this.deleteAll.bind(this);
  }

  componentDidMount() {
    store
      .get("failedData")
      .then(failedData => {
        this.setState({
          failedData
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  sendAgain(value, index) {
    this.setState({ visible: true });

    API()
      .post(`save_clockings?checkpoint=${JSON.stringify(value)}`)
      .then(res => {
        this.setState({
          visible: false
        });

        store.delete("failedData");
      })
      .catch(err => {
        alert("Sending clocking failed");
        captureException(err);
        this.setState({ visible: false });
      });
  }

  deleteStatic(v, k) {
    let newData = [
      ...this.state.failedData.slice(0, k),
      ...this.state.failedData.slice(k + 1)
    ];

    this.setState({
      failedData: newData
    });
  }

  sendAll() {
    this.setState({ visible: true });

    Promise.all(
      this.state.failedData.map(v => {
        return API()
          .post(`save_clockings?checkpoint=${JSON.stringify(v)}`)
          .then(res => {
            return res.data;
          })
          .catch(err => {
            captureException(err);
            throw err;
          });
      })
    )
      .then(res => {
        this.setState({ visible: false, failedData: [] });
        alert("Data successfully send");
      })
      .catch(err => {
        alert("Fail to send");
        captureException(err);
        this.setState({ visible: false });
      });
  }

  deleteAll() {
    Alert.alert(
      "Are you sure",
      "Are you sure to delete?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            store.delete("failedData");
            this.setState({
              failedData: []
            });
          }
        }
      ],
      { cancelable: false }
    );
  }

  render() {
    const {
      checkpoint: { checkpoint }
    } = this.props;

    let timeFormat, getDesc;

    return (
      <Fragment>
        <Modal isVisible={this.state.visible}>
          <View style={styles.loadingModal}>
            <ActivityIndicator color={colors.success} />
          </View>
        </Modal>

        {this.state.failedData && (
          <View style={styles.buttonWrapper}>
            <TouchableOpacity style={styles.button} onPress={this.sendAll}>
              <Text>Send All</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={this.deleteAll}>
              <Text>Delete All</Text>
            </TouchableOpacity>
          </View>
        )}

        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexDirection: "column-reverse" }}
        >
          {this.state.failedData ? (
            this.state.failedData.map((v, k) => (
              <View key={k} style={styles.clock}>
                <TouchableOpacity
                  style={styles.deleteButton}
                  onPress={() => this.deleteStatic(v, k)}
                >
                  <Text style={{ color: colors.light }}>X</Text>
                </TouchableOpacity>
                {v.map((w, key) => {
                  timeFormat = moment
                    .unix(w.time)
                    .format("MM/DD/YYYY hh:mm:ss");
                  getDesc = checkpoint && checkpoint.find(x => x.id === w.id);

                  return (
                    <Text key={key} style={styles.listClock}>
                      Checkpoint {getDesc.description} on {timeFormat}
                    </Text>
                  );
                })}

                <TouchableOpacity
                  style={styles.sendAgain}
                  onPress={() => this.sendAgain(v, k)}
                >
                  <Text style={{ color: "#fff" }}>Try send</Text>
                </TouchableOpacity>
              </View>
            ))
          ) : (
            <Text>Data not found</Text>
          )}
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20
  },
  list: {
    backgroundColor: colors.light,
    height: 50,
    padding: 15,
    marginBottom: 12,
    borderRadius: 4
  },
  listSuccess: {
    backgroundColor: colors.success
  },
  listFail: {
    borderWidth: 1,
    borderColor: colors.warning
  },
  text: {
    color: colors.dark
  },
  clock: {
    backgroundColor: colors.light,
    marginBottom: 30
  },
  listClock: {
    padding: 15,
    borderBottomWidth: 1,
    borderColor: "#f9f9f9"
  },
  sendAgain: {
    alignSelf: "flex-end",
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    width: 100,
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.info
  },
  loadingModal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    alignSelf: "center",
    backgroundColor: colors.light,
    width: 60,
    height: 60
  },
  deleteButton: {
    width: 30,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "flex-end",
    backgroundColor: colors.danger
  },
  button: {
    width: 80,
    height: 50,
    backgroundColor: colors.light,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: colors.text
  },
  buttonWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    paddingTop: 20
  }
});

export default connect(mapStateToProps)(Clocking);
