import React, { Component, Fragment } from "react";
import { BackHandler, Text } from "react-native";
import { Tab, Tabs } from "native-base";
import { colors, configs } from "@styles";

import Attendance from "./Attendance";
import Clocking from "./Clocking";
import Incident from "./Incident";
import Report from "./Report";

class ClockingReportComponent extends Component {
  constructor(props) {
    super(props);
    this._handleBackButton = this._handleBackButton.bind(this);
  }

  static navigationOptions = {
    headerTitle: (
      <Text style={[configs.titleHeader, { marginLeft: -25 }]}>
        Offline Report
      </Text>
    ),
    headerTintColor: colors.light,
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    }
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this._handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._handleBackButton
    );
  }

  _handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    return (
      <Fragment>
        <Tabs>
          <Tab
            heading="Clocking"
            activeTabStyle={{ backgroundColor: colors.info }}
            tabStyle={{ backgroundColor: colors.info }}
            activeTextStyle={{ fontFamily: "OpenSans-Light" }}
          >
            <Clocking />
          </Tab>

          <Tab
            heading="Report"
            activeTabStyle={{ backgroundColor: colors.info }}
            tabStyle={{ backgroundColor: colors.info }}
          >
            <Report />
          </Tab>

          {/* <Tab
            heading="Attendance"
            activeTabStyle={{ backgroundColor: colors.info }}
            tabStyle={{ backgroundColor: colors.info }}
          >
            <Attendance />
          </Tab> */}
        </Tabs>
      </Fragment>
    );
  }
}

export default ClockingReportComponent;
