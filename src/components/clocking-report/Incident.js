import React, { Component, Fragment } from "react";
import { ScrollView, Text, StyleSheet, View, Image } from "react-native";
import store from "react-native-simple-store";
import { colors, configs } from "@styles";
import { connect } from "react-redux";

const mapStateToProps = state => ({
  checkpoint: state.checkpoint
});

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reports: [],
      visible: false
    };
  }

  componentDidMount() {
    store
      .get("report")
      .then(report => {
        this.setState({
          reports: report
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  render() {
    const {} = this.props;

    const { reports } = this.state;

    return (
      <Fragment>
        <ScrollView
          style={[styles.container]}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexDirection: "column-reverse" }}
        >
          {reports ? (
            (reports || []).map((v, k) => (
              <View style={[styles.clock]} key={k}>
                <View style={styles.accordion}>
                  <View style={styles.accordionHeader}>
                    <Text style={styles.accordionText}>Report</Text>
                  </View>
                  <View style={styles.accordionBody}>
                    <Text style={styles.text}>
                      Priority: {v.report.priority}
                    </Text>
                    <Text style={styles.text}>{v.report.description}</Text>
                    <View>
                      {v.photos !== undefined &&
                        v.photos.map((x, k) => (
                          <Image
                            key={k}
                            style={styles.imageReport}
                            source={{ uri: x.uri }}
                          />
                        ))}
                    </View>
                    <View>
                      {v.images !== undefined &&
                        v.images.map((x, k) => (
                          <Image
                            key={k}
                            style={styles.imageReport}
                            source={{ uri: x.uri }}
                          />
                        ))}
                    </View>
                  </View>
                </View>
              </View>
            ))
          ) : (
            <Text>Data not found</Text>
          )}
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20
  },
  list: {
    backgroundColor: colors.light,
    height: 50,
    padding: 15,
    marginBottom: 12,
    borderRadius: 4
  },
  listSuccess: {
    backgroundColor: colors.success
  },
  listFail: {
    borderWidth: 1,
    borderColor: colors.warning
  },
  text: {
    color: colors.dark
  },
  clock: {
    borderWidth: 2,
    borderColor: "#dadada",
    borderStyle: "dotted",
    backgroundColor: colors.light,
    marginBottom: 30
  },
  listClock: {
    padding: 15,
    borderBottomWidth: 1,
    borderColor: "#f9f9f9"
  },
  sendAgain: {
    alignSelf: "flex-end",
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    width: 100,
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.info
  },
  loadingModal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    alignSelf: "center",
    backgroundColor: colors.light,
    width: 60,
    height: 60
  },
  deleteButton: {
    width: 30,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "flex-end",
    backgroundColor: colors.danger
  },
  button: {
    width: 80,
    height: 50,
    backgroundColor: colors.light,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: colors.text
  },
  buttonWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    paddingTop: 20
  },
  accordion: {},
  accordionHeader: {
    backgroundColor: colors.background,
    height: 40,
    display: "flex",
    justifyContent: "center",
    paddingLeft: 20
  },
  accordionText: {
    fontFamily: "OpenSans-SemiBold",
    fontSize: 16
  },
  accordionBody: {
    padding: 20
  },
  imageReport: {
    marginTop: 15,
    width: 70,
    height: 70
  },
  text: {
    fontFamily: "OpenSans-Regular",
    lineHeight: 30
  }
});

export default connect(mapStateToProps)(Report);
