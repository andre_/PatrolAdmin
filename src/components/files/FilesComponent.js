import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  ScrollView,
  BackHandler,
  TouchableOpacity,
  ToastAndroid
} from "react-native";
import { connect } from "react-redux";
import { API } from "@src/services/APIService";
import { colors } from "@styles";
import RNFS from "react-native-fs";

const mapStateToProps = state => ({
  posts: state.posts
});

class FilesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      files: []
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    this._initFiles();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _initFiles() {
    const {
      posts: { posts }
    } = this.props;

    API()
      .post(`get_file_post?post_id=${posts.post_id}`)
      .then(res => {
        console.log(res.data);
        this.setState({ files: res.data });
      })
      .catch(err => {
        console.log(err);
      });
  }

  _downloadFiles(data) {
    console.log(data);
    if (data.type === "pdf") {
      this.props.navigation.navigate("Pdf", {
        path: data.path
      });
    } else {
      RNFS.downloadFile({
        fromUrl: data.path,
        toFile: `${RNFS.DocumentDirectoryPath}/${data.name}`
      })
        .promise.then(res => {
          console.log("disimpan di", RNFS.DocumentDirectoryPath);
          ToastAndroid.show("Download completed", ToastAndroid.SHORT);
        })
        .catch(err => {
          console.log("error download", err);
        });
    }
  }

  formatBytes(a, b) {
    if (0 == a) return "0 Bytes";
    var c = 1024,
      d = b || 2,
      e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      f = Math.floor(Math.log(a) / Math.log(c));
    return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f];
  }

  render() {
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        {this.state.files.map((v, keys) => (
          <TouchableOpacity
            style={styles.card}
            key={keys}
            onPress={() => this._downloadFiles(v)}
          >
            <Text style={styles.text}>{v.name.toUpperCase()}</Text>
            <Text style={styles.textSmall}>{this.formatBytes(v.size_b)}</Text>
            <Text style={styles.textSmall}>{v.created_at.date}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20
  },
  card: {
    borderRadius: 4,
    marginBottom: 20,
    minHeight: 80,
    padding: 15,
    backgroundColor: colors.light
  },
  text: {
    fontFamily: "JosefinSans-Bold",
    color: colors.dark,
    fontSize: 16
  },
  textSmall: {
    fontFamily: "JosefinSans-Light",
    color: colors.text
  }
});

export default connect(mapStateToProps)(FilesComponent);
