import React, { Component } from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image } from "react-native";
import { Spinner } from "native-base";
import DeviceInfo from "react-native-device-info";
import ImagePicker from "react-native-image-picker";
import { colors, text, configs } from "@styles";
import { API } from "@src/services/APIService";
import moment from "moment";
import store from "react-native-simple-store";

export default class CheckinComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      getLocation: false,
      imageUri: null,
      lat: 0,
      lng: 0
    };
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      },
      error => {
        console.log(error);
      },
      {}
    );
  }

  returnFromCamera = data => {
    this.setState({
      imageUri: data
    });
  };

  _openCamera = () => {
    const {
      flag: { attendanceCameraVersion }
    } = this.props;

    if (attendanceCameraVersion === "v1") {
      this.props.navigation.navigate("CheckinCamera", {
        returnFromCamera: this.returnFromCamera
      });
    } else {
      const options = {
        quality: 0.5,
        maxWidth: 500,
        maxHeight: 500,
        cameraType: "front"
      };

      ImagePicker.launchCamera(options, response => {
        if (response.error) {
          alert("Camera error");
        } else if (response.didCancel) {
          console.log("user cancel");
        } else {
          this.setState({
            imageUri: response.uri
          });
        }
      });
    }
  };

  _doCheckin = () => {
    const obj = new FormData();

    const {
      staff: { staff }
    } = this.props;

    this.setState({ loading: true });

    obj.append("id", staff.id);
    obj.append("post_id", staff.post_id);
    obj.append("deviceid", DeviceInfo.getDeviceId());
    obj.append("gps", `${this.state.lat},${this.state.lng}`);

    API()
      .post("checkin", obj)
      .then(res => {
        this.setState({ loading: false });

        const pict = new FormData();
        pict.append("attendance_id", res.data.id);
        pict.append("image", {
          uri: this.state.imageUri,
          type: "image/jpeg",
          name: `photo-${res.data.id}`
        });

        this.props.navigation.navigate("CheckinStatus", {
          data: pict
        });
      })
      .catch(err => {
        store
          .push("checkin", {
            id: staff.id,
            post_id: staff.post_id,
            deviceid: DeviceInfo.getDeviceId()
          })
          .then(() => store.get("checkin"))
          .then(checkout => {
            console.log("checkin", checkout);
            this.props.navigation.navigate("CheckinStatus");
          })
          .catch(err => {
            console.log("fail to save storage");
          });
      });
  };

  render() {
    const { loading, imageUri } = this.state;

    return (
      <View style={styles.modalBackground}>
        <View style={styles.modalContent}>
          <View style={[configs.flexColumnCenter, { flex: 1 }]}>
            <View style={styles.imageBorder}>
              {this.state.imageUri !== null ? (
                <Image style={styles.images} source={{ uri: imageUri }} />
              ) : null}
            </View>

            <View style={{ width: 280 }}>
              <Text style={[text.h2, text.spaceBottom, { color: colors.info }]}>
                TIME
              </Text>
              <Text
                style={[
                  text.h2,
                  {
                    color: colors.info,
                    fontWeight: "bold",
                    marginBottom: 10
                  }
                ]}
              >
                {/* 4:42:23 PM */}
                {moment().format("HH:mm:ss")}
              </Text>
              <Text style={[text.h2, { color: colors.info }]}>
                {/* Mon 12 April 2018 */}
                {moment().format("dddd, DD MMMM YYYY")}
              </Text>

              {imageUri === null ? (
                <TouchableOpacity
                  onPress={this._openCamera}
                  style={[
                    configs.flexRowCenter,
                    {
                      marginTop: 40,
                      height: 50,
                      backgroundColor: colors.info,
                      borderRadius: 4
                    }
                  ]}
                >
                  <Text style={[text.h3, { color: colors.light }]}>
                    Take Picture
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={this._doCheckin}
                  style={[
                    configs.flexRowCenter,
                    {
                      marginTop: 40,
                      height: 50,
                      backgroundColor: colors.info,
                      borderRadius: 4
                    }
                  ]}
                >
                  {loading && (
                    <Spinner
                      color={colors.light}
                      style={{ position: "absolute", left: 10 }}
                    />
                  )}
                  <Text style={[text.h3, { color: colors.light }]}>
                    Check In
                  </Text>
                </TouchableOpacity>
              )}

              {/* {loading ? (
                <TouchableOpacity
                  style={[
                    configs.flexRowCenter,
                    {
                      marginTop: 40,
                      height: 50,
                      backgroundColor: colors.light,
                      borderRadius: 4
                    }
                  ]}
                >
                  <Text>Checking in</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={this._doCheckin}
                  style={[
                    configs.flexRowCenter,
                    {
                      marginTop: 40,
                      height: 50,
                      backgroundColor: colors.info,
                      borderRadius: 4
                    }
                  ]}
                >
                  {loading && (
                    <Spinner
                      color={colors.light}
                      style={{ position: "absolute", left: 10 }}
                    />
                  )}
                  <Text style={[text.h3, { color: colors.light }]}>
                    Check In
                  </Text>
                </TouchableOpacity>
              )} */}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.6)"
  },
  modalContent: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.light
  },
  imageBorder: {
    top: -30,
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: colors.border,
    borderRadius: 4
  },
  images: {
    borderRadius: 4,
    width: "100%",
    height: "100%"
  }
});
