import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Spinner
} from "react-native";
import { connect } from "react-redux";
import { colors } from "@styles";
import moment from "moment";
import { API } from "@src/services/APIService";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

class StatusComponent extends Component {
  state = {
    isLoading: false
  };

  _setCheckinDone = () => {
    const obj = this.props.navigation.getParam("data");

    this.setState({
      isLoading: true
    });

    API()
      .post("send_image_attendance", obj)
      .then(res => {
        if (res) {
          this.setState({
            isLoading: false
          });
          this.props.navigation.navigate("Home");
        }
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        console.log(err);
      });
  };

  render() {
    const timeStatus = moment().format("DD:MM:YYYY HH:mm:ss");

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.success}>
            <View style={styles.flexRow}>
              <Text style={styles.text}>Time In</Text>
              <Text style={styles.text}>{timeStatus}</Text>
            </View>
            <View style={styles.greenScreen}>
              <Text style={[styles.text, styles.textLight]}>STATUS</Text>
              <Text style={[styles.bigText, styles.bold, styles.textLight]}>
                CHECK IN
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={[styles.button, styles.flexCenter]}
          onPress={this._setCheckinDone}
        >
          {this.state.loading && (
            <Spinner
              color={colors.light}
              style={{ position: "absolute", left: 10 }}
            />
          )}
          <Text style={[styles.textLight, styles.bold]}>DONE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.black
  },
  wrapper: {
    padding: 20,
    backgroundColor: colors.light,
    borderRadius: 10,
    marginLeft: "auto",
    marginRight: "auto",
    width: "90%",
    height: 150
  },
  flexCenter: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  success: {
    flex: 1
  },
  greenScreen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    width: "100%",
    height: 60,
    borderRadius: 40,
    bottom: 0,
    backgroundColor: "mediumseagreen"
  },
  button: {
    backgroundColor: colors.info,
    width: "90%",
    marginTop: 20,
    height: 50
  },
  flexRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  text: {
    color: colors.dark,
    fontSize: 16
  },
  textLight: {
    color: colors.light
  },
  bold: {
    fontWeight: "bold"
  },
  bigText: {
    fontSize: 20,
    textAlign: "center",
    width: 300,
    marginTop: 2
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusComponent);
