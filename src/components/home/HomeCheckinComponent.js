import React, { Component } from "react";
import {
  Animated,
  ScrollView,
  Image,
  View,
  Text,
  Linking,
  BackHandler,
  Platform
} from "react-native";
import { Container, Button, Footer, FooterTab, Input } from "native-base";
import { configs, colors, text } from "@styles";
import GuardGrid from "@components/partials/GuardGrid";
import languange from "@src/constants/languange";
import Torch from "react-native-torch";
import PopupDialog from "react-native-popup-dialog";

const HEADER_MAX_HEIGHT = 240;
const HEADER_MIN_HEIGHT = 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class HomeCheckinComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      light: false
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  // CREATE HOC FOR THIS ONE
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _doingCall(v) {
    Linking.openURL(`tel: ${parseInt(v.number)}`);
  }

  _switchTorch() {
    this.setState({
      light: !this.state.light
    });

    Torch.switchState(true);
  }

  _searchGuard(text) {
    this.props.searchStaffIn(text);
  }

  render() {
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: "clamp"
    });

    const {
      account: {
        account: { staff, post },
        selected
      }
    } = this.props;

    return (
      <Container
        style={{
          backgroundColor: colors.background
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {
                  y: this.state.scrollY
                }
              }
            }
          ])}
        >
          <View
            style={{
              width: this.state.normalWidth,
              height: 260,
              backgroundColor: colors.light,
              display: "flex",
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <View
              style={{
                alignSelf: "center",
                marginTop: -25,
                width: "100%",
                height: "100%"
              }}
            >
              <Image
                source={require("../../assets/attendance.jpg")}
                style={{
                  width: "100%",
                  height: "100%"
                }}
              />
            </View>
            <View style={configs.buttonHeader}>
              <Button
                style={[
                  configs.buttonHeaderIn,
                  { backgroundColor: colors.success, width: "100%" }
                ]}
              >
                <Input
                  placeholder="Search"
                  placeholderTextColor="#fff"
                  onChangeText={text => this._searchGuard(text)}
                  style={{
                    textAlign: "center",
                    color: colors.light
                  }}
                />
              </Button>
            </View>
          </View>
          <View style={[configs.content, configs.addMarginBottom]}>
            {selected &&
              selected.map((v, keys) => (
                <GuardGrid key={keys} data={v} {...this.props} />
              ))}
          </View>
        </ScrollView>

        <View>
          <Footer>
            <FooterTab style={{ backgroundColor: colors.light }}>
              <Button onPress={() => this.callPopup.show()}>
                <Image
                  source={require("../../assets/call.png")}
                  style={{ width: 25, height: 25 }}
                />
                <Text
                  style={{
                    color: colors.info,
                    fontFamily: "JosefinSans-Light"
                  }}
                >
                  {languange.call}
                </Text>
              </Button>
              <Button onPress={() => this.props.navigation.navigate("Panic")}>
                <Image
                  source={require("../../assets/panic-01.png")}
                  style={{ width: 25, height: 25 }}
                />
                <Text
                  style={{
                    color: colors.info,
                    fontFamily: "JosefinSans-Light"
                  }}
                >
                  Panic
                </Text>
              </Button>
              <Button onPress={() => this._switchTorch()}>
                <Image
                  source={require("../../assets/torch.png")}
                  style={{ width: 25, height: 25 }}
                />
                <Text
                  style={{
                    color: this.state.light ? colors.info : colors.text,
                    fontFamily: "JosefinSans-Light"
                  }}
                >
                  {this.state.light ? languange.on : languange.off}
                </Text>
              </Button>
            </FooterTab>
          </Footer>
        </View>

        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 150,
            marginTop: -50
          }}
          ref={popupDialog => {
            this.callPopup = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text>Who you wanna call?</Text>
            <View style={{ marginTop: 20 }}>
              {post &&
                post.call.map((v, keys) => (
                  <Text
                    key={keys}
                    style={{
                      marginBottom: 10,
                      fontSize: 18,
                      fontWeight: "500"
                    }}
                    onPress={() => this._doingCall(v)}
                  >
                    {v.name}
                  </Text>
                ))}
            </View>
          </View>
        </PopupDialog>
      </Container>
    );
  }
}
