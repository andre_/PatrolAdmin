import React, { Component, Fragment } from "react";
import {
  BackHandler,
  Animated,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Linking,
  Image,
  Platform
} from "react-native";
import { Container, Button, Footer, FooterTab, Thumbnail } from "native-base";
import PopupDialog, {
  SlideAnimation,
  DialogTitle
} from "react-native-popup-dialog";
import Torch from "react-native-torch";
import { configs, colors, text } from "@styles";
import { API } from "@src/services/APIService";
import languange from "@src/constants/languange";
import NfcManager from "react-native-nfc-manager";
import DeviceSettings from "react-native-device-settings";
import Modal from "react-native-modal";
import { generateHumanDate } from "@src/utils/getDay";

class HomeMainComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      visible: false,
      light: false,
      passwordAdmin: "",
      error: "",
      supported: false,
      showNotif: false,
      nfcEnabled: true,
      initialCounter: 3000
    };

    this.handleBackButton = this.handleBackButton.bind(this);
    this._switchTorch = this._switchTorch.bind(this);
    this.openDeviceSettings = this.openDeviceSettings.bind(this);
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: "red"
    }
  };

  componentDidMount() {
    this._initAccount();
    this._initClockingCheckpoint();
    this._isExpired();

    const {
      languange: { lang }
    } = this.props;

    languange.setLanguage(lang);
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    if (this._stateChangedSubscription) {
      this._stateChangedSubscription.remove();
    }

    navigator.geolocation.clearWatch(this.watchId);
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  openDeviceSettings() {
    DeviceSettings.open();
  }

  _isExpired() {
    this.props.navigation.addListener("didFocus", () => {
      if (this.props.account.account.expired) {
        this.props.navigation.navigate("Expired");
      }
    });

    this.props.navigation.addListener("didBlur", () => {
      if (this.props.account.account.expired) {
        this.props.navigation.navigate("Expired");
      }
    });
  }

  _initAccount() {
    const {
      gatherAccount,
      posts: { posts }
    } = this.props;

    const obj = {
      post_id: posts.post_id
    };

    API()
      .post("get_post_detail", obj)
      .then(res => {
        gatherAccount(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleBackButton() {
    return true;
  }

  _initClockingCheckpoint() {
    const {
      posts: { posts },
      initCheckpoint
    } = this.props;

    const obj = new FormData();
    obj.append("post_id", posts.post_id);

    API()
      .post("get_list_checkpoint", obj)
      .then(res => {
        initCheckpoint(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }

  _doingCall(v) {
    Linking.openURL(`tel: ${v.number}`);
  }

  _showModal() {
    this.setState({ visible: true });
  }

  _hideModal() {
    this.setState({ visible: false });
  }

  _switchTorch() {
    this.setState({
      light: !this.state.light
    });

    Torch.switchState(this.state.light);
  }

  _doLogoutPost() {
    const { passwordAdmin } = this.state;
    const {
      account: {
        account: { post }
      }
    } = this.props;

    if (passwordAdmin === post.installation_code) {
      this.props.destroySupervisor();
      this.props.destroyToken();
    } else {
      alert("installation code does not match");
    }
  }

  conditional(distance) {
    const {
      clocking: { clocking }
    } = this.props;

    if (distance.far) {
      this.props.navigation.navigate("FarAway");
    } else {
      if (clocking.length >= 1) {
        this.startOver.show();
      } else {
        this.props.navigation.navigate("Clocking");
      }
    }
  }

  conditionalNfc = () => {
    this.props.navigation.navigate("NewClocking");
    // if (Platform.OS === "ios") {
    //   this.props.navigation.navigate("Clocking");
    // } else {
    //   if (this.props.scanning.method === "nfc") {
    //     NfcManager.isEnabled()
    //       .then(result => {
    //         if (result) {
    //           this.props.navigation.navigate("NewClocking");
    //         } else {
    //           this.settingNfc.show();
    //         }
    //       })
    //       .catch(err => {
    //         alert("Error checking nfc");
    //       });
    //   } else {
    //     this.props.navigation.navigate("Clocking");
    //   }
    // }
  };

  _startOver() {
    this.props.endClocking();
    this.startOver.dismiss();
    this.props.navigation.navigate("Clocking");
  }

  conditionalCheckin = () => {
    const {
      flag: { attendanceType }
    } = this.props;

    if (attendanceType === "nfc") {
      this.props.navigation.navigate("AttendanceNfc");
    } else if (attendanceType === "ai") {
      this.props.navigation.navigate("FaceScanner", { type: "in" });
    } else {
      this.props.navigation.navigate("HomeCheckin");
    }
  };

  conditionalCheckout = () => {
    const {
      flag: { attendanceType }
    } = this.props;

    if (attendanceType === "nfc") {
      this.props.navigation.navigate("AttendanceNfcOut");
    } else if (attendanceType === "ai") {
      this.props.navigation.navigate("FaceScanner", { type: "out" });
    } else {
      this.props.navigation.navigate("HomeCheckout");
    }
  };

  stopAlarm = () => {
    this.props.isLate(false);
    this.props.stopAlarm();
  };

  render() {
    const {
      account: {
        account: { post, features }
      },
      flag: { usingFace },
      supervisor: { supervisor: supervisorSingle },
      message: { showNotification },
      distance
    } = this.props;

    const slideAnimation = new SlideAnimation({
      slideFrom: "bottom"
    });

    return (
      <Fragment>
        <Container
          style={{
            backgroundColor: colors.background
          }}
        >
          <Modal
            isVisible={showNotification}
            onBackButtonPress={() => this.props.doHideNotification()}
            onBackdropPress={() => this.props.doHideNotification()}
          >
            <View
              style={{
                backgroundColor: colors.light,
                width: 260,
                height: 260,
                padding: 15,
                borderRadius: 4,
                alignSelf: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.doHideNotification()}
                style={{
                  width: 30,
                  height: 30,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "#999",
                  borderRadius: 60,
                  position: "absolute",
                  top: 10,
                  right: 10,
                  zIndex: 1
                }}
              >
                <Text
                  style={{ fontFamily: "OpenSans-Bold", color: colors.light }}
                >
                  X
                </Text>
              </TouchableOpacity>
              <View style={{ width: "100%", height: 80 }}>
                {this.props.message.type === "success" ? (
                  <Image
                    source={require("../../assets/icons/success_clocking.png")}
                    style={{
                      resizeMode: "contain",
                      width: "100%",
                      height: "100%"
                    }}
                  />
                ) : (
                  <Image
                    source={require("../../assets/icons/fail_clocking.png")}
                    style={{
                      resizeMode: "contain",
                      width: "100%",
                      height: "100%"
                    }}
                  />
                )}
              </View>
              <View style={{ paddingTop: 8, paddingBottom: 8 }}>
                <Text
                  style={{
                    fontFamily: "OpenSans-SemiBold",
                    fontSize: 16,
                    color: colors.dark,
                    textAlign: "center"
                  }}
                >
                  {this.props.message.message}
                </Text>
                <Text
                  style={{
                    fontFamily: "OpenSans-Regular",
                    fontSize: 12,
                    textAlign: "center",
                    marginTop: 5
                  }}
                >
                  Dont worry, it will be printed as success clocking.
                </Text>
              </View>
              <View style={{ marginTop: 15 }}>
                <TouchableOpacity
                  onPress={() => this.props.doHideNotification()}
                  style={{
                    width: "100%",
                    height: 40,
                    backgroundColor:
                      this.props.message.type === "success"
                        ? colors.info
                        : colors.danger,
                    borderRadius: 4,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "OpenSans-SemiBold",
                      color: colors.light
                    }}
                  >
                    Okay!
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

          <ScrollView
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={16}
            onScroll={Animated.event([
              {
                nativeEvent: {
                  contentOffset: {
                    y: this.state.scrollY
                  }
                }
              }
            ])}
          >
            <View
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                width: this.state.normalWidth,
                height: 220,
                backgroundColor: this.props.account.warning
                  ? colors.dark
                  : colors.light
              }}
            >
              {this.props.account.warning ? (
                <TouchableOpacity
                  onLongPress={this.stopAlarm}
                  style={{
                    marginTop: -65,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <View>
                    <Image source={require("../../assets/alarm.png")} />
                  </View>
                  <Text
                    style={{
                      fontSize: 22,
                      color: colors.light,
                      alignSelf: "center",
                      fontFamily: "OpenSans-Bold"
                    }}
                  >
                    It's time to clocking
                  </Text>
                </TouchableOpacity>
              ) : (
                <View
                  style={{
                    alignSelf: "center",
                    marginTop: -25,
                    width: 290,
                    height: 100
                  }}
                >
                  <Image
                    source={require("../../assets/TrackerHero-Logo.png")}
                    style={{
                      alignSelf: "center",
                      width: "100%",
                      height: 65
                    }}
                  />
                </View>
              )}

              {features && !features.enable_attendance_in_patrol ? (
                <View
                  style={[
                    configs.buttonHeader,
                    {
                      backgroundColor: colors.info,
                      borderRadius: 4,
                      width: "90%",
                      justifyContent: "space-between",
                      flexDirection: "column",
                      alignSelf: "center",
                      height: 60
                    }
                  ]}
                >
                  <Text
                    style={{
                      marginTop: 10,
                      alignSelf: "flex-start",
                      color: colors.light,
                      fontFamily: "OpenSans-SemiBold"
                    }}
                  >
                    {post && post.name}
                  </Text>
                  <Text
                    style={{
                      marginBottom: 10,
                      alignSelf: "flex-start",
                      color: colors.light,
                      fontSize: 12,
                      fontFamily: "OpenSans-Light"
                    }}
                  >
                    {generateHumanDate()}
                  </Text>
                </View>
              ) : null}

              {features && features.enable_attendance_in_patrol ? (
                <View style={configs.buttonHeader}>
                  <Button
                    onPress={this.conditionalCheckin}
                    style={[
                      configs.buttonHeaderIn,
                      { backgroundColor: colors.info }
                    ]}
                  >
                    <Image
                      source={require("../../assets/in.png")}
                      style={{ width: 30, height: 30 }}
                    />
                    <Text
                      style={[text.p, { color: colors.light, marginLeft: 10 }]}
                    >
                      {languange.checkin}
                    </Text>
                  </Button>

                  <Button
                    onPress={() => this.conditionalCheckout(distance)}
                    style={[
                      configs.buttonHeaderIn,
                      { backgroundColor: colors.danger }
                    ]}
                  >
                    <Image
                      source={require("../../assets/out.png")}
                      style={{ width: 30, height: 30 }}
                    />
                    <Text
                      style={[text.p, { color: colors.light, marginLeft: 10 }]}
                    >
                      {languange.checkout}
                    </Text>
                  </Button>
                </View>
              ) : null}
            </View>

            <View style={[configs.content]}>
              {features && features.enable_clocking_in_patrol ? (
                <TouchableOpacity
                  style={[
                    configs.card,
                    configs.addMarginBottom,
                    {
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
                  onPress={this.conditionalNfc}
                >
                  <Image
                    source={require("../../assets/clocking.png")}
                    style={{ width: 40, height: 40 }}
                  />
                  <Text style={[text.h4, text.spaceBottom, { marginLeft: 10 }]}>
                    {languange.clocking}
                  </Text>
                </TouchableOpacity>
              ) : null}

              {features && features.enable_inspection_in_patrol ? (
                <TouchableOpacity
                  style={[
                    configs.card,
                    configs.addMarginBottom,
                    {
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
                  onPress={() => this.props.navigation.navigate("Sop")}
                >
                  <Image
                    source={require("../../assets/icons/shield.png")}
                    style={{ width: 40, height: 40 }}
                  />
                  <Text style={[text.h4, text.spaceBottom, { marginLeft: 10 }]}>
                    SOP Inspections
                  </Text>
                </TouchableOpacity>
              ) : null}

              {features && features.enable_report_in_patrol ? (
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("ReportPosts")}
                  style={[
                    configs.card,
                    configs.addMarginBottom,
                    {
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
                >
                  <Image
                    source={require("../../assets/compose.png")}
                    style={{ width: 40, height: 40 }}
                  />
                  <Text style={[text.h4, text.spaceBottom, { marginLeft: 10 }]}>
                    Logbook
                  </Text>
                </TouchableOpacity>
              ) : null}

              {usingFace ? (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("FaceRecognition")
                  }
                  style={[
                    configs.card,
                    configs.addMarginBottom,
                    {
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
                >
                  <Image
                    source={require("../../assets/camera-diaphragm.png")}
                    style={{ width: 40, height: 40 }}
                  />
                  <Text style={[text.h4, text.spaceBottom, { marginLeft: 10 }]}>
                    Face Recognition
                  </Text>
                </TouchableOpacity>
              ) : null}

              {features && features.enable_visitor_in_patrol ? (
                <TouchableOpacity
                  style={[
                    configs.card,
                    configs.addMarginBottom,
                    {
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
                  onPress={() => this.props.navigation.navigate("Visitor")}
                >
                  <Image
                    source={require("../../assets/visitor.png")}
                    style={{ width: 40, height: 40 }}
                  />
                  <Text style={[text.h4, text.spaceBottom, { marginLeft: 10 }]}>
                    {languange.visitor}
                  </Text>
                </TouchableOpacity>
              ) : null}

              {features && features.enable_files_in_patrol ? (
                <TouchableOpacity
                  style={[
                    configs.card,
                    configs.addMarginBottom,
                    {
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
                  onPress={() => this.props.navigation.navigate("Files")}
                >
                  <Image
                    source={require("../../assets/report-01.png")}
                    style={{ width: 40, height: 40 }}
                  />
                  <Text style={[text.h4, text.spaceBottom, { marginLeft: 10 }]}>
                    Document
                  </Text>
                </TouchableOpacity>
              ) : null}
            </View>
          </ScrollView>

          <View style={{ borderWidth: 1, borderColor: "#f1f1f1" }}>
            <Footer>
              <FooterTab
                style={{
                  backgroundColor: colors.light
                }}
              >
                <Button onPress={() => this.callPopup.show()}>
                  <Image
                    source={require("../../assets/call.png")}
                    style={{ width: 25, height: 25 }}
                  />
                  <Text
                    style={{
                      color: colors.info,
                      fontFamily: "JosefinSans-Light"
                    }}
                  >
                    {languange.call}
                  </Text>
                </Button>
                <Button
                  style={{
                    backgroundColor: colors.danger
                  }}
                  onPress={() => this.props.navigation.navigate("Panic")}
                >
                  <Image
                    source={require("../../assets/panic2.png")}
                    style={{ width: 25, height: 25 }}
                  />
                  <Text
                    style={{
                      color: colors.light,
                      fontFamily: "JosefinSans-Light"
                    }}
                  >
                    Panic
                  </Text>
                </Button>
                <Button onPress={this._switchTorch.bind(this)}>
                  <Image
                    source={require("../../assets/torch.png")}
                    style={{ width: 25, height: 25 }}
                  />
                  <Text
                    style={{
                      color: this.state.light ? colors.info : colors.text,
                      fontFamily: "JosefinSans-Light"
                    }}
                  >
                    {this.state.light ? languange.on : languange.off}
                  </Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
          <PopupDialog
            dialogStyle={{
              width: 300,
              height: 250,
              marginTop: -50
            }}
            dialogAnimation={slideAnimation}
            dialogTitle={<DialogTitle title="SUPERVISOR" />}
            ref={popupDialog => {
              this.accountPopup = popupDialog;
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Thumbnail
                large
                source={{ uri: "https://placehold.it/300x300" }}
              />
              <Text style={[text.h3, { color: colors.dark, marginTop: 10 }]}>
                {(supervisorSingle && supervisorSingle.name) || ""}
              </Text>

              <TouchableOpacity
                onPress={() => this._doLogoutSupervisor()}
                style={{
                  backgroundColor: colors.info,
                  borderRadius: 10,
                  marginTop: 20,
                  width: 120,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text style={{ color: colors.light, textAlign: "center" }}>
                  LOGOUT
                </Text>
              </TouchableOpacity>
            </View>
          </PopupDialog>

          <PopupDialog
            dialogStyle={{
              width: 300,
              height: 150,
              marginTop: -50
            }}
            ref={popupDialog => {
              this.callPopup = popupDialog;
            }}
          >
            <View
              style={{
                flex: 1,
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 20
              }}
            >
              <Text>Who you wanna call?</Text>
              <View style={{ marginTop: 20 }}>
                {post &&
                  post.call.map((v, keys) => (
                    <Text
                      key={keys}
                      style={{
                        marginBottom: 10,
                        fontSize: 18,
                        fontWeight: "500"
                      }}
                      onPress={() => this._doingCall(v)}
                    >
                      {v.name}
                    </Text>
                  ))}
              </View>
            </View>
          </PopupDialog>

          <PopupDialog
            dialogStyle={{
              width: 300,
              height: 180
            }}
            ref={popupDialog => {
              this.adminPassword = popupDialog;
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  backgroundColor: "#fff",
                  marginTop: 20,
                  paddingRight: 20,
                  paddingLeft: 20
                }}
              >
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: "bold",
                    color: "#333"
                  }}
                >
                  Admin Password
                </Text>

                <TextInput
                  secureTextEntry
                  placeholder="Password"
                  underlineColorAndroid="#fff"
                  style={{
                    borderColor: "#dadada",
                    marginTop: 18,
                    paddingLeft: 10,
                    borderRadius: 4
                  }}
                />
              </View>

              <View
                style={{
                  position: "absolute",
                  right: 4,
                  bottom: 18,
                  display: "flex",
                  flexDirection: "row"
                }}
              >
                <Button
                  onPress={() => {
                    this.props.navigation.navigate("Settings");
                  }}
                  success
                  style={{
                    marginRight: 10,
                    width: 80,
                    height: 30,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 20
                  }}
                >
                  <Text style={{ color: colors.light }}>OK</Text>
                </Button>
              </View>
            </View>
          </PopupDialog>
        </Container>
        {/* )} */}

        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 240
          }}
          dialogAnimation={slideAnimation}
          dialogTitle={<DialogTitle title="SUPERVISOR ASSIGN" />}
          ref={popupDialog => {
            this.supervisorSign = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text style={{ marginBottom: 10, fontSize: 14, fontWeight: "500" }}>
              Supervisor Not Assigned
            </Text>
            <Text style={text.p}>
              Supervisor must check in first before access to this feature is
              allowed
            </Text>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                danger
                onPress={() => this.supervisorSign.dismiss()}
                style={{
                  elevation: 0,
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text
                  style={{
                    color: colors.light,
                    fontFamily: "JosefinSans-Thin"
                  }}
                >
                  LATER
                </Text>
              </Button>
              <Button
                onPress={() => {
                  this.supervisorSign.dismiss();
                  this.popupSupervisor.show();
                }}
                success
                style={{
                  elevation: 0,
                  marginRight: 10,
                  width: 150,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text
                  style={{
                    color: colors.light,
                    fontFamily: "JosefinSans-Thin"
                  }}
                >
                  CHECK IN NOW
                </Text>
              </Button>
            </View>
          </View>
        </PopupDialog>

        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 200,
            marginTop: -180
          }}
          dialogTitle={<DialogTitle title="Setting NFC" />}
          ref={popupDialog => {
            this.settingNfc = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text>
              NFC not enabled yet. Please enable NFC in device settings.
            </Text>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                danger
                onPress={() => {
                  this.settingNfc.dismiss();
                }}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Cancel</Text>
              </Button>
              <Button
                success
                onPress={this.openDeviceSettings}
                style={{
                  marginRight: 10,
                  width: 140,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Go To Settings</Text>
              </Button>
            </View>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 230,
            marginTop: -100
          }}
          dialogTitle={<DialogTitle title="Continue clocking" />}
          ref={popupDialog => {
            this.startOver = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text
              style={{
                fontFamily: "JosefinSans-SemiBold",
                marginBottom: 5
              }}
            >
              Resume Clocking
            </Text>
            <Text>
              Your last clocking session is still not finalize, you want to
              resume your last session?
            </Text>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                paddingRight: 15,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity
                danger
                onPress={() => {
                  this.startOver.dismiss();
                  this.props.navigation.navigate("Clocking");
                }}
                style={{
                  backgroundColor: colors.danger,
                  marginTop: 10,
                  width: "100%",
                  height: 40,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  elevation: 0,
                  marginRight: 20
                }}
              >
                <Text style={{ color: colors.light }}>Resume</Text>
              </TouchableOpacity>
            </View>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 150
          }}
          dialogTitle={<DialogTitle title="Logout" />}
          ref={popupDialog => {
            this.logoutPop = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text>Are you sure to logout</Text>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                danger
                onPress={() => this.logoutPop.dismiss()}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Cancel</Text>
              </Button>
              <Button
                success
                onPress={() => {
                  this.logoutPop.dismiss();
                  this.logoutPass.show();
                }}
                style={{
                  marginRight: 10,
                  width: 120,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Yes</Text>
              </Button>
            </View>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 200
          }}
          dialogTitle={<DialogTitle title="Logout" />}
          ref={popupDialog => {
            this.logoutPass = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text>Input your Installation code</Text>
            <View style={{ height: 180 }}>
              <TextInput
                placeholder="type your installation code"
                onChangeText={text => this.setState({ passwordAdmin: text })}
              />
            </View>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                danger
                onPress={() => this.logoutPass.dismiss()}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Cancel</Text>
              </Button>
              <Button
                success
                onPress={() => this._doLogoutPost()}
                style={{
                  marginRight: 10,
                  width: 120,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Yes</Text>
              </Button>
            </View>
          </View>
        </PopupDialog>
      </Fragment>
    );
  }
}

export default HomeMainComponent;
