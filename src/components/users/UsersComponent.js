import React, { Component } from "react";
import { View, Text, Platform, BackHandler } from "react-native";
import {
  Button,
  Container,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Thumbnail,
  Item,
  Label
} from "native-base";
import PopupDialog, { DialogTitle } from "react-native-popup-dialog";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import { APIService } from "@src/services/APIService";
import { colors } from "@styles";

const RtdType = {
  URL: 0,
  TEXT: 1
};

export default class UsersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
      supported: false,
      enabled: false,
      isWriting: false,
      urlToWrite: "www.google.com",
      rtdType: RtdType.TEXT,
      parsedText: null,
      tag: {}
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    const {
      account: {
        account: { staff }
      }
    } = this.props;

    return (
      <Container style={{ backgroundColor: colors.light }}>
        <Content>
          <List style={{ marginTop: 10 }}>
            {staff.map((v, keys) => (
              <ListItem avatar style={{ height: 70 }} key={keys}>
                <Left>
                  <Thumbnail source={require("./../../assets/guards.png")} />
                </Left>
                <Body>
                  <Text style={{ color: colors.dark, fontWeight: "500" }}>
                    {v.name}
                  </Text>
                  <Text note style={{ marginTop: 8 }}>
                    Status: {!v.checkin_token ? "N/A" : "CHECK IN"}
                  </Text>
                </Body>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}
