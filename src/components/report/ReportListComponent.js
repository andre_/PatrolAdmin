import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, BackHandler } from "react-native";
import { connect } from "react-redux";
import { API } from "@src/services/APIService";
import { colors } from "@styles";

const mapStateToProps = state => ({
  posts: state.posts
});

class FilesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      report: []
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    this._initReport();

    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _initReport() {
    const {
      posts: { posts }
    } = this.props;

    API()
      .post(`get_incident?post_id=${posts.post_id}`)
      .then(res => {
        this.setState({ report: res.data });
      })
      .catch(err => {
        console.log(err);
      });
  }

  generateTime = time => {
    return time.substr(0, time.length - 7);
  };

  render() {
    return (
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          display: "flex",
          flexDirection: "column-reverse"
        }}
      >
        {this.state.report.map((v, keys) => (
          <View style={styles.card} key={keys}>
            <View style={styles.left}>
              <View style={styles.circle}>
                <Text style={styles.textSmall}>
                  {v.priority === null ? "-" : v.priority}
                </Text>
              </View>
            </View>
            <View style={styles.right}>
              <Text style={[styles.text, { marginBottom: 10 }]}>
                {v.description}
              </Text>
              <Text style={styles.textSmall}>
                {this.generateTime(v.created_at.date)}
              </Text>
            </View>
          </View>
        ))}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20
  },
  card: {
    borderRadius: 4,
    marginBottom: 20,
    minHeight: 80,
    padding: 15,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.light
  },
  text: {
    fontFamily: "JosefinSans-Bold",
    color: colors.dark,
    fontSize: 16
  },
  textSmall: {
    fontFamily: "JosefinSans-Light",
    color: colors.text
  },
  circle: {
    width: 60,
    height: 60,
    marginRight: 20,
    borderRadius: 100,
    backgroundColor: "#f7f7f7",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  }
});

export default connect(mapStateToProps)(FilesComponent);
