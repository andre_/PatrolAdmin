import { connect } from "react-redux";
import ReportComponent from "./ReportComponent";
import { sendReport } from "@src/actions/reportActions";

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  doSendReport(data) {
    dispatch(sendReport(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportComponent);
