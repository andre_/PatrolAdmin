import React, { Component } from "react";
import { View, StyleSheet, BackHandler } from "react-native";
import { connect } from "react-redux";
import { RNCamera } from "react-native-camera";
import { initClocking } from "@src/actions/clockingActions";

const mapStateToProps = state => ({
  clocking: state.clocking
});

const mapDispatchToPros = dispatch => ({
  initClocking(data) {
    dispatch(initClocking(data));
  }
});

class QrScanComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      picture: null,
      count: 0,
      lat: 0,
      lng: 0
    };

    this._handleBackPress = this._handleBackPress.bind(this);
  }

  componentDidMount() {
    this._getLocation();
    BackHandler.addEventListener("hardwareBackPress", this._handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this._handleBackPress);
  }

  _getLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      },
      err => {
        console.log("error maps", err.message);
      }
    );
  }

  _handleBackPress() {
    this.props.navigation.goBack();
    return true;
  }

  onBarCodeRead(e) {
    const {
      clocking: { shareClock }
    } = this.props;

    if (e.data) {
      if (parseInt(e.data) === shareClock) {
        let location = `${this.state.lat},${this.state.lng}`;
        console.log("location", location);

        this.props.initClocking({
          id: parseInt(e.data),
          time: Math.floor(Date.now() / 1000),
          gps: "",
          status: "1"
        });
        this.props.navigation.goBack();
      } else {
        alert("QR code doesnt match");
        this.props.navigation.goBack();
      }
    }
  }

  render() {
    console.log("state", this.state);
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <RNCamera
          ref={cam => {
            this.camera = cam;
          }}
          type={RNCamera.Constants.Type.front}
          style={{ flex: 1, justifyContent: "flex-end", alignItems: "center" }}
          type={RNCamera.Constants.Type.back}
          onBarCodeRead={this.onBarCodeRead.bind(this)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "black"
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: "#777"
  },
  textBold: {
    fontWeight: "500",
    color: "#000"
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  },
  square: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "red",
    position: "absolute",
    left: 50,
    bottom: 50
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToPros
)(QrScanComponent);
