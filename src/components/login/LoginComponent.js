import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image
} from "react-native";
import { Container, Form, Label, Input, Item, Spinner } from "native-base";
import DeviceInfo from "react-native-device-info";
import { configs, colors, text } from "@styles";
import { API } from "@src/services/APIService";
import styles from "./styles";

class LoginComponent extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      code: "",
      phonenumber: null
    };
  }

  _doLogin() {
    const {
      startLoading,
      stopLoading,
      initPosts,
      initPhone,
      initToken,
      endClocking,
      navigation: { navigate },
      auth: { player }
    } = this.props;
    const obj = new FormData();
    obj.append("deviceid", DeviceInfo.getUniqueID());
    obj.append("installation_code", this.state.code);
    obj.append("onesignal_player_id", player);
    if (this.state.phonenumber === null) {
      alert("Please fill your phone number to login");
      stopLoading();
    } else {
      startLoading();
      API()
        .post("/install_device", obj)
        .then(res => {
          stopLoading();
          if (parseInt(res.data.success)) {
            endClocking();
            initPosts(res.data);
            initPhone(this.state.phonenumber);
            initToken(res.data.api_token);
            navigate("Home");
          } else {
            alert("Please check your installation code");
          }
        })
        .catch(err => {
          alert("Please check your connection");
          console.log(err);
          stopLoading();
        });
    }
  }

  onClose(data) {
    console.log(data);
  }

  render() {
    const {
      loading: { show }
    } = this.props;

    return (
      <KeyboardAvoidingView
        contentContainerStyle={{ flex: 1, height: "100%" }}
        style={{ flex: 1, height: "100%" }}
      >
        <View
          contentContainerStyle={{
            height: "100%",
            backgroundColor: colors.background
          }}
          style={{
            flex: 1,
            height: "100%",
            backgroundColor: colors.background
          }}
        >
          <View style={styles.header}>
            <View style={styles.imageWrapper}>
              <Image
                source={require("../../assets/th2.png")}
                style={styles.image}
              />
            </View>
          </View>

          <View>
            <Form style={styles.formWrapper}>
              <View style={configs.textCenter}>
                <Text
                  style={[
                    text.h3,
                    text.spaceBottom,
                    { fontFamily: "OpenSans-SemiBold" }
                  ]}
                >
                  LOGIN
                </Text>
              </View>
              <Item stackedLabel style={{ height: 80 }}>
                <Label
                  style={{
                    color: colors.info,
                    fontSize: 16,
                    fontFamily: "OpenSans-Regular"
                  }}
                >
                  Installation code
                </Label>
                <Input onChangeText={code => this.setState({ code })} />
              </Item>
              <Item stackedLabel style={{ height: 80 }}>
                <Label
                  style={{
                    color: colors.info,
                    fontSize: 16,
                    fontFamily: "OpenSans-Regular"
                  }}
                >
                  Phone Number
                </Label>
                <Input
                  keyboardType={"numeric"}
                  onChangeText={phonenumber => this.setState({ phonenumber })}
                />
              </Item>
            </Form>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this._doLogin()}
              >
                {show && <Spinner color="white" style={styles.spinner} />}
                <Text
                  style={{
                    color: colors.light,
                    fontSize: 16,
                    fontFamily: "OpenSans-Regular"
                  }}
                >
                  Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default LoginComponent;
