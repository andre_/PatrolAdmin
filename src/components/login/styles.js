import { configs, colors, text } from "@styles";

const styles = {
  header: {
    height: "45%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.info
  },
  formWrapper: {
    borderRadius: 4,
    marginTop: -50,
    width: 320,
    height: 260,
    backgroundColor: colors.light,
    marginLeft: "auto",
    marginRight: "auto",
    paddingTop: 15,
    paddingLeft: 10,
    paddingRight: 27
  },
  buttonWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: -30
  },
  button: {
    marginTop: 0,
    backgroundColor: colors.info,
    width: 240,
    height: 50,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 40
  },
  spinner: {
    position: "absolute",
    left: 10,
    height: 10
  },
  image: {
    alignSelf: "center",
    width: 280,
    marginLeft: -10,
    height: 65
  },
  imageWrapper: {
    width: "100%"
  }
};

export default styles;
