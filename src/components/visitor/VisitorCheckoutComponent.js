import React, { Component } from "react";
import {
  Alert,
  View,
  Text,
  TouchableOpacity,
  Image,
  BackHandler,
  ScrollView,
  RefreshControl
} from "react-native";
import { configs, colors, text } from "@styles/";
import { connect } from "react-redux";
import { API } from "@src/services/APIService";
import { removeVisitor } from "../../scenes/visitor-qr/actions";
import Modal from "react-native-modal";
import styles from "./styles";

const qrCode = require("../../assets/icons/qr-code-scan.png");
const leftBack = require("../../assets/icons/left-back.png");

const mapStateToProps = state => ({
  visitor: state.visitor,
  posts: state.posts,
  flag: state.flag
});

const mapDispatchToProps = dispatch => ({
  doRemove(data) {
    dispatch(removeVisitor(data));
  }
});

class VisitorCheckoutComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkout: [],
      idUser: null,
      visible: false,
      passNumber: null,
      loading: false,
      modalConfirm: false,
      selectedList: null
    };

    this._handleBackButton = this._handleBackButton.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerTintColor: colors.light,
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>CHECKOUT</Text>
    ),
    headerLeft: (
      <TouchableOpacity
        style={configs.backButton}
        onPress={() => navigation.goBack()}
      >
        <Image source={leftBack} style={configs.imageBack} />
      </TouchableOpacity>
    ),
    headerRight: <View />
  });

  componentDidMount() {
    this._getCheckout();

    BackHandler.addEventListener("hardwareBackPress", this._handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._handleBackButton
    );
  }

  _handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  closeModal = () => {
    this.setState({
      modalConfirm: false
    });
  };

  _getCheckout() {
    const {
      posts: { posts }
    } = this.props;

    this.setState({ loading: true });

    API()
      .get(`/v2/visitor?post_id=${posts.post_id}&status=check_in`)
      .then(res => {
        this.setState({
          checkout: res.data.data,
          loading: false
        });
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });
  }

  _doCheckout = () => {
    this.closeModal();

    API()
      .post(`v2/visitor/check_out/${this.state.selectedList.id}`, {
        using: "VISITOR"
      })
      .then(res => {
        this._getCheckout();
        this.props.doRemove();
      })
      .catch(err => {
        console.log("err", err.response);
      });
  };

  doUrgentCheckout = id => {
    API()
      .post(`v2/visitor/check_out/${id}`, {
        using: "VISITOR"
      })
      .then(res => {
        this._getCheckout();
      })
      .catch(err => {
        console.log("err urgent", err.response);
      });
  };

  toggleModal = id => {
    this.setState({
      visible: !this.state.visible,
      idUser: id
    });
  };

  doingCheckout = v => {
    this.setState({
      modalConfirm: true,
      selectedList: v
    });
  };

  getQrValue = data => {
    console.log("qr gune", data);
    API()
      .post(`v2/visitor/check_out/${data}`, { using: "QR_CODE" })
      .then(res => {
        this._getCheckout();
        this.props.doRemove();
      })
      .catch(err => {
        if (err.response.data.mesage === "Visitor Not Found") {
          Alert.alert(
            "Checkout Unknown Visitor",
            "Are you sure to checkout this visitor?",
            [
              {
                style: "cancel",
                text: "CANCEL",
                onPress: () => {
                  console.log("close");
                }
              },
              {
                style: "default",
                text: "OK",
                onPress: () => {
                  this.doUrgentCheckout(data);
                }
              }
            ]
          );
        }
      });
  };

  scanQr = () => {
    this.props.navigation.navigate("VisitorScanner", {
      getQrValue: this.getQrValue
    });
  };

  refreshData = () => {
    const {
      posts: { posts }
    } = this.props;

    this.setState({ loading: true }, () => {
      API()
        .get(`/v2/visitor?post_id=${posts.post_id}`)
        .then(res => {
          this.setState({ loading: false, checkout: res.data.data });
        })
        .catch(err => {
          this.setState({ loading: false });
          console.log(err);
        });
    });
  };

  render() {
    const {
      flag: { usingManual }
    } = this.props;

    return (
      <React.Fragment>
        <Modal
          isVisible={this.state.modalConfirm}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn="fadeIn"
          animationOut="fadeOut"
        >
          <View style={styles.modalWrapper}>
            <Text style={styles.titleModal}>CHECK OUT</Text>
            <View style={styles.contentModal}>
              <Text style={styles.textModal}>Confirm visitor checkout?</Text>
            </View>

            <View style={styles.btnWrapper}>
              <TouchableOpacity
                style={styles.btnClose}
                onPress={this._doCheckout}
              >
                <Text style={[styles.text, { fontFamily: "OpenSans-Bold" }]}>
                  OK
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View style={styles.checkoutContainer}>
          {this.state.loading ? (
            <View style={styles.refreshWrapper}>
              <Text style={[styles.text, { fontSize: 12 }]}>
                Sync in progress
              </Text>
            </View>
          ) : null}

          <ScrollView
            contentContainerStyle={styles.contentWrapper}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this.refreshData}
              />
            }
          >
            {this.state.checkout.length
              ? this.state.checkout.map((v, keys) => (
                  <TouchableOpacity style={styles.btnList} key={keys}>
                    <View style={styles.leftWrapper}>
                      <Text
                        style={[styles.p, { fontFamily: "OpenSans-SemiBold" }]}
                      >
                        Pass Number: {v.pass_no === null ? "-" : v.pass_no}
                      </Text>
                      <Text style={[styles.p, { fontSize: 12 }]}>
                        Checkin: {v.check_in}
                      </Text>
                    </View>

                    {usingManual ? (
                      <TouchableOpacity
                        style={styles.btnCheckout}
                        onPress={() => this.doingCheckout(v)}
                      >
                        <Text style={[styles.text, { fontSize: 12 }]}>
                          CHECKOUT
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                  </TouchableOpacity>
                ))
              : null}
          </ScrollView>
        </View>

        <TouchableOpacity style={styles.btnQrBottom} onPress={this.scanQr}>
          <Image source={qrCode} style={styles.images} />
        </TouchableOpacity>
      </React.Fragment>

      // <Container style={{ backgroundColor: colors.background }}>
      //   <Content
      //     showsVerticalScrollIndicator={false}
      //     style={[configs.content, configs.noHeader]}
      //   >
      //     <View style={{ marginBottom: 20 }}>
      //       <Tabs initialPage={0}>
      //         <Tab
      //           style={{
      //             backgroundColor: colors.background,
      //             borderWidth: 0,
      //             elevation: 0
      //           }}
      //           heading="Visitor In"
      //           activeTabStyle={{ backgroundColor: colors.background }}
      //           activeTextStyle={{ color: colors.info }}
      //           tabStyle={{ backgroundColor: colors.background }}
      //           textStyle={{ color: colors.dark }}
      //         >
      //           <List>
      //             {visitor && visitor.length ? (
      //               visitor.map((v, keys) => (
      //                 <ListItem key={keys}>
      //                   <View style={[configs.spaceBetween, { width: "100%" }]}>
      //                     <Text>{v.name}</Text>
      //                     <TouchableOpacity
      //                       onPress={() => this._doCheckout(v.id)}
      //                     >
      //                       <Text>Check Out</Text>
      //                     </TouchableOpacity>
      //                   </View>
      //                 </ListItem>
      //               ))
      //             ) : (
      //               <ListItem>
      //                 <View style={[configs.spaceBetween, { width: "100%" }]}>
      //                   <Text>Visitor not found</Text>
      //                 </View>
      //               </ListItem>
      //             )}
      //           </List>
      //         </Tab>
      //       </Tabs>
      //     </View>
      //   </Content>
      // </Container>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VisitorCheckoutComponent);
