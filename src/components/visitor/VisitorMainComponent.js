import React, { Component } from "react";
import {
  Alert,
  View,
  ScrollView,
  BackHandler,
  TouchableOpacity,
  RefreshControl
} from "react-native";
import { Col, Grid } from "react-native-easy-grid";
import { Text } from "native-base";
import { configs, colors, text } from "@styles/";
import { initVisitor, offlineVisitor } from "../../scenes/visitor-qr/actions";
import { sendOfflineVisitor } from "@src/actions/offlineActions";
import { connect } from "react-redux";
import store from "react-native-simple-store";
import { API } from "@src/services/APIService";

import styles from "./styles";

const mapStateToProps = state => ({
  visitor: state.visitor,
  posts: state.posts
});

const mapDispatchToProps = dispatch => ({
  doInitVisitor(data) {
    dispatch(initVisitor(data));
  },

  doOfflineVisitor(data) {
    dispatch(offlineVisitor(data));
  },

  doSendOfflineVisitor(data) {
    dispatch(sendOfflineVisitor(data));
  }
});

class VisitorMainComponent extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.info
    },
    headerTintColor: colors.light,
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>VISITOR</Text>
    ),
    headerRight: <View />
  };

  constructor(props) {
    super(props);
    this.state = {
      checkin: [],
      offlineVisitor: [],
      totalVisitor: 0,
      loading: false
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    this._initVisitor();
    this._initTotalVisitor();
    this._initOfflineVisitor();

    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.popToTop();
    return true;
  }

  _initOfflineVisitor() {
    const { doOfflineVisitor } = this.props;

    store
      .get("visitor")
      .then(visitor => {
        doOfflineVisitor(visitor);
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  _initVisitor = () => {
    const {
      posts: { posts },
      doInitVisitor
    } = this.props;

    this.setState({ loading: true });

    API()
      .get(`/v2/visitor?post_id=${posts.post_id}&status=check_in`)
      .then(res => {
        doInitVisitor(res.data.data);
        this.setState({ loading: false });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  };

  _initTotalVisitor = () => {
    const {
      posts: { posts }
    } = this.props;

    this.setState({ loading: true });

    API()
      .get(`/v2/visitor?post_id=${posts.post_id}`)
      .then(res => {
        this.setState({ loading: false, totalVisitor: res.data.data.length });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  };

  refreshVisitor = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        this._initVisitor();
        this._initTotalVisitor();
        this._initOfflineVisitor();
      }
    );
  };

  _sendVisitorOffline = () => {
    Alert.alert("Send queue", "Are you sure?", [
      {
        text: "Cancel",
        style: "cancel"
      },
      {
        text: "OK",
        style: "default",
        onPress: () => {
          store
            .get("visitor")
            .then(visitor => {
              if (visitor !== null) {
                this.props.doSendOfflineVisitor(visitor);
              }
            })
            .catch(err => {
              console.log(err);
            });
        }
      }
    ]);
  };

  render() {
    const {
      visitor: { visitor, offlineVisitor }
    } = this.props;
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: colors.light }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this.refreshVisitor}
          />
        }
      >
        <View style={[configs.content]}>
          <View style={configs.spaceBetween}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("VisitorCheckin")}
              style={[
                configs.buttonBigDefault,
                configs.noButtonShadow,
                { backgroundColor: colors.success }
              ]}
            >
              <Text style={styles.text}>Check In</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("VisitorCheckout")}
              style={[
                configs.noButtonShadow,
                configs.buttonBigDefault,
                { backgroundColor: colors.warning }
              ]}
            >
              <Text style={styles.text}>Check Out</Text>
            </TouchableOpacity>
          </View>

          <View style={configs.spaceBetween}>
            <Grid>
              <Col>
                <TouchableOpacity
                  style={{ paddingTop: 18, paddingBottom: 10 }}
                  onPress={() =>
                    this.props.navigation.navigate("VisitorInDetail")
                  }
                >
                  <Text
                    style={[
                      text.setCenter,
                      text.h2,
                      text.spaceBottom,
                      { color: colors.success }
                    ]}
                  >
                    {visitor}
                  </Text>
                  <Text style={[text.setCenter, text.p]}>Visitor Check In</Text>
                </TouchableOpacity>
              </Col>

              <Col>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("VisitorSummary")
                  }
                  style={{
                    paddingTop: 18,
                    paddingBottom: 10
                  }}
                >
                  <Text
                    style={[
                      text.setCenter,
                      text.h2,
                      text.spaceBottom,
                      { color: colors.warning }
                    ]}
                  >
                    {this.state.totalVisitor}
                  </Text>
                  <Text style={[text.setCenter, text.p]}>
                    Total Visitor Today
                  </Text>
                </TouchableOpacity>
              </Col>

              <Col>
                <View
                  style={{
                    paddingTop: 18,
                    paddingBottom: 10
                  }}
                >
                  <Text
                    style={[
                      text.setCenter,
                      text.h2,
                      text.spaceBottom,
                      { color: colors.dark }
                    ]}
                  >
                    {(offlineVisitor || []).length}
                  </Text>
                  <Text style={[text.setCenter, text.p]}>In Queue</Text>
                  <TouchableOpacity onPress={this._sendVisitorOffline}>
                    <Text
                      style={[text.setCenter, text.p, { color: colors.info }]}
                    >
                      Send Again
                    </Text>
                  </TouchableOpacity>
                  {/* <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("VisitorOfflineList")
                    }
                  >
                    <Text
                      style={[text.setCenter, text.p, { color: colors.text }]}
                    >
                      Detail
                    </Text>
                  </TouchableOpacity> */}
                </View>
              </Col>
            </Grid>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VisitorMainComponent);
