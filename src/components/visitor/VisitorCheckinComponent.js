import React, { Component } from "react";
import {
  ActivityIndicator,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  BackHandler
} from "react-native";
import { Button, Label, Picker, Spinner } from "native-base";
import { configs, colors, text } from "@styles/";
import { connect } from "react-redux";
import { API } from "@src/services/APIService";
import { initForms } from "@src/actions/postsActions";
import ImagePicker from "react-native-image-picker";
import styles from "./styles";

const mapStateToProps = state => ({
  posts: state.posts
});

const mapDispatchToProps = dispatch => ({
  initForms(data) {
    dispatch(initForms(data));
  }
});

class VisitorCheckinComponent extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerLeft: (
      <Image
        source={require("../../assets/icons/left-arrow.png")}
        style={{ width: 15, height: 15, marginLeft: 10 }}
      />
    ),
    headerTitle: (
      <Text style={[configs.titleHeader, { marginLeft: -35 }]}>Check In</Text>
    )
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      ImageSource: ""
    };

    this._openCamera = this._openCamera.bind(this);
    this._handleBackButton = this._handleBackButton.bind(this);
  }

  componentDidMount() {
    this._initForm();

    BackHandler.addEventListener("hardwareBackPress", this._handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._handleBackButton
    );
  }

  _handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _initForm() {
    const {
      posts: { posts }
    } = this.props;

    const obj = new FormData();
    obj.append("post_id", parseInt(posts.post_id));

    API()
      .post("get_visitor_inputs", obj)
      .then(res => {
        console.log("form", res.data);
        this.props.initForms(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });
  }

  _doCheckin() {
    let input = [];
    Object.keys(this.state).map((v, keys) => {
      if (v !== "ImageSource") {
        input.push({ visitor_input_id: parseInt(v), value: this.state[v] });
      }
    });

    let data = {
      visitors: [
        {
          datetime: new Date().toLocaleDateString(),
          inputs: input
        }
      ]
    };

    if (this.state.ImageSource) {
      this.setState({
        loading: true
      });

      API()
        .post(
          "create_multiple_visitors_based_on_dynamic_input_v2?source=patrol",
          data
        )
        .then(res => {
          console.log("visitor", res.data);
          this._uploadPict(res.data.visitor_id_list[0]);
        })
        .catch(err => {
          alert("Check your connection");
          this.setState({
            loading: false
          });
        });
    } else {
      alert("Please attach a picture");
    }
  }

  _openCamera() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.launchCamera(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else {
        this.setState({ ImageSource: response });
      }
    });
  }

  _uploadPict(id) {
    const pict = new FormData();
    pict.append("visitor_id", parseInt(id));
    pict.append("image", {
      uri: this.state.ImageSource.uri,
      name: this.state.ImageSource.fileName,
      type: "image/jpeg"
    });

    API()
      .post("sendimagevisitor", pict)
      .then(res => {
        this.setState({ loading: false });
        this.props.navigation.navigate("Home");
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });
  }

  render() {
    const {
      posts: { forms }
    } = this.props;

    const { loading } = this.state;

    console.log(forms);

    return (
      <ScrollView style={[styles.container]}>
        {forms &&
          forms.map((v, keys) => {
            let options = JSON.parse(v.options);

            return (
              <View stackedLabel key={keys} style={{ marginBottom: 10 }}>
                <Text style={styles.label}>{v.label}</Text>
                {v.type === "dropdown" ? (
                  <Picker
                    mode="dropdown"
                    onValueChange={value => this.setState({ [v.id]: value })}
                    selectedValue={this.state[v.id]}
                  >
                    {options.map((value, key) => (
                      <Picker.Item
                        key={key}
                        label={value.value}
                        value={value.value}
                      />
                    ))}
                  </Picker>
                ) : (
                  <TextInput
                    placeholder="Type here"
                    onChangeText={text => this.setState({ [v.id]: text })}
                    underlineColorAndroid="transparent"
                    style={styles.textInput}
                  />
                )}
              </View>
            );
          })}

        <View
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            alignItems: "flex-end"
          }}
        >
          <TouchableOpacity
            style={styles.viewUpload}
            onPress={this._openCamera.bind(this)}
          >
            {!this.state.ImageSource.uri && (
              <Text
                style={{
                  color: "#333",
                  textAlign: "center",
                  fontFamily: "JosefinSans-Light"
                }}
              >
                Upload Image
              </Text>
            )}

            <Image
              source={{ uri: this.state.ImageSource.uri }}
              style={{ width: "100%", height: "100%" }}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "100%",
            alignSelf: "center"
          }}
        >
          <Button
            onPress={() => this._doCheckin()}
            style={[
              configs.noButtonShadow,
              configs.textCenter,
              {
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                width: "100%",
                height: 50,
                marginBottom: 40,
                backgroundColor: colors.info
              }
            ]}
          >
            {this.state.loading && (
              <ActivityIndicator
                color={colors.light}
                style={{ position: "absolute", left: 10 }}
              />
            )}
            <Text style={{ textAlign: "center", color: colors.light }}>
              Check In
            </Text>
          </Button>
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VisitorCheckinComponent);
