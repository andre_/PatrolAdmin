import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: colors.light
  },
  checkoutContainer: {
    flex: 1,
    backgroundColor: colors.light
  },
  visitList: {
    borderBottomWidth: 1,
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomColor: "#dadada",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  pass: {
    fontFamily: "OpenSans-Regular",
    marginBottom: 4,
    color: colors.dark
  },
  name: {
    fontFamily: "OpenSans-Regular",
    color: colors.dark
  },
  white: {
    color: colors.light,
    fontFamily: "OpenSans-Regular"
  },
  btn: {
    backgroundColor: colors.info,
    padding: 10,
    borderRadius: 4
  },
  label: {
    fontFamily: "OpenSans-Regular",
    color: colors.dark
  },
  textInput: {
    fontFamily: "OpenSans-Regular",
    backgroundColor: "#f2f2f2",
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#dadada",
    marginTop: 10,
    marginBottom: 4
  },
  viewUpload: {
    width: 120,
    height: 120,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#dadada",
    borderStyle: "dashed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20
  },
  divider: {
    width: "100%",
    height: 20,
    borderBottomWidth: 1,
    borderColor: colors.border
  },
  btnDelivery: {
    width: "100%",
    height: 60,
    backgroundColor: colors.dark,
    alignItems: "center",
    justifyContent: "center"
  },
  deliveryWrap: {
    marginTop: 15
  },
  text: {
    color: colors.light,
    fontFamily: "OpenSans-Regular"
  },
  modalWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: 220,
    height: 220,
    borderRadius: 4,
    alignSelf: "center",
    backgroundColor: colors.light
  },
  refreshWrapper: {
    backgroundColor: colors.success,
    alignItems: "center"
  },
  btnList: {
    height: 80,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#dadada"
  },
  contentWrapper: {
    padding: 15
  },
  p: {
    fontFamily: "OpenSans-Regular",
    color: colors.dark
  },
  btnCheckout: {
    borderRadius: 4,
    backgroundColor: colors.info,
    width: 100,
    height: 30,
    alignItems: "center",
    justifyContent: "center"
  },
  btnQrBottom: {
    backgroundColor: colors.info,
    position: "absolute",
    bottom: 20,
    right: 20,
    width: 60,
    height: 60,
    padding: 15,
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center"
  },
  images: {
    width: "100%",
    height: "100%"
  },

  modalWrapper: {
    backgroundColor: colors.light,
    padding: 20,
    paddingBottom: 10,
    width: 280,
    height: 160,
    alignSelf: "center"
  },

  titleModal: {
    fontFamily: "OpenSans-Bold",
    fontSize: 16,
    color: colors.dark
  },

  contentModal: {
    height: 40,
    justifyContent: "center"
  },

  textModal: {
    fontFamily: "OpenSans-Regular"
  },

  btnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  btnClose: {
    backgroundColor: colors.success,
    width: 60,
    height: 40,
    borderRadius: 40,
    alignItems: "center",
    justifyContent: "center"
  }
};

export default styles;
