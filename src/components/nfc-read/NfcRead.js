import React, { Component } from "react";
import {
  View,
  Text,
  Platform,
  TouchableOpacity,
  BackHandler,
  StyleSheet
} from "react-native";
import { connect } from "react-redux";
import { colors } from "@styles";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import { addCheckpoint } from "@src/actions/checkpointActions";

const RtdType = {
  URL: 0,
  TEXT: 1
};

const mapStateToProps = state => ({
  checkpoint: state.checkpoint
});

const mapDispatchToProps = dispatch => ({
  addCheckpoint(data) {
    dispatch(addCheckpoint(data));
  }
});

class NfcRead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      supported: true,
      enabled: false,
      isWriting: false,
      isFormat: false,
      urlToWrite: "www.google.com",
      rtdType: RtdType.TEXT,
      parsedText: "-",
      tag: {}
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    NfcManager.isSupported().then(supported => {
      this.setState({ supported });
      if (supported) {
        this._startNfc();
        this._startDetection();
      }
    });

    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    if (this._stateChangedSubscription) {
      this._stateChangedSubscription.remove();
    }

    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  _getLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      },
      err => {
        console.log("error maps", err.message);
      }
    );
  }

  handleBackButton() {
    this._stopDetection();
    this.props.navigation.goBack();
    return true;
  }

  _cancelOperation() {
    this._stopDetection();
    this.props.navigation.goBack();
  }

  render() {
    let { isWriting, isFormat } = this.state;

    return (
      <View style={styles.container}>
        {Platform.OS === "ios" && <View style={{ height: 60 }} />}

        <View style={styles.wrapper}>
          <View style={styles.textWrapper}>
            <Text style={styles.text}>Read NFC</Text>

            <View style={styles.imageWrapper}>
              <Text style={{ marginBottom: 18, fontFamily: "OpenSans-Light" }}>
                Your NFC Checkpoint name:
              </Text>
              <Text
                style={{
                  fontFamily: "OpenSans-Bold",
                  fontSize: this.state.parsedText !== null ? 40 : 20,
                  color:
                    this.state.parsedText !== null ? colors.dark : colors.danger
                }}
              >
                {this.state.parsedText !== null
                  ? this.state.parsedText
                  : "NFC not created yet"}
              </Text>
            </View>

            {/* <View style={styles.imageWrapper}>
              <Image source={require("./../../assets/nfc.png")} />
            </View> */}

            <View style={styles.buttonWrapper}>
              <TouchableOpacity
                style={[styles.button, { backgroundColor: "#dadada" }]}
                onPress={() => this._cancelOperation()}
              >
                <Text style={{ textAlign: "center", color: colors.dark }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }

  _startNfc() {
    NfcManager.start({
      onSessionClosedIOS: () => {
        console.log("ios session closed");
      }
    })
      .then(result => {
        console.log("start OK", result);
      })
      .catch(error => {
        console.warn("start fail", error);
        this.setState({ supported: false });
      });

    if (Platform.OS === "android") {
      NfcManager.getLaunchTagEvent()
        .then(tag => {
          console.log("launch tag", tag);
          if (tag) {
            this.setState({ tag });
          }
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.isEnabled()
        .then(enabled => {
          this.setState({ enabled });
        })
        .catch(err => {
          console.log(err);
        });
      NfcManager.onStateChanged(event => {
        if (event.state === "on") {
          this.setState({ enabled: true });
        } else if (event.state === "off") {
          this.setState({ enabled: false });
        } else if (event.state === "turning_on") {
        } else if (event.state === "turning_off") {
        }
      })
        .then(sub => {
          this._stateChangedSubscription = sub;
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }

  _onTagDiscovered = tag => {
    const {
      checkpoint: { checkpoint }
    } = this.props;

    this.setState({ tag });

    let text = this._parseText(tag);
    let getDesc = checkpoint && checkpoint.find(v => v.id === parseInt(text));

    if (getDesc !== undefined) {
      this.setState({
        parsedText: getDesc.description
      });
    } else {
      this.setState({
        parsedText: null
      });
    }
  };

  _startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscovered)
      .then(result => {
        console.log("registerTagEvent OK", result);
      })
      .catch(error => {
        console.warn("registerTagEvent fail", error);
      });
  };

  _stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {
        console.log("unregisterTagEvent OK", result);
      })
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  _clearMessages = () => {
    this.setState({ tag: null });
  };

  _parseText = tag => {
    if (tag.ndefMessage) {
      return NdefParser.parseText(tag.ndefMessage[0]);
    }
    return null;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.info,
    alignItems: "center",
    justifyContent: "center"
  },
  wrapper: {
    width: 300,
    height: 340,
    borderRadius: 4,
    alignSelf: "center",
    backgroundColor: colors.light
  },
  imageWrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 25,
    paddingBottom: 15
  },
  text: {
    fontSize: 20,
    color: colors.dark,
    fontFamily: "JosefinSans-SemiBold",
    textAlign: "center",
    paddingTop: 35,
    paddingBottom: 25
  },
  textThin: {
    color: colors.light,
    fontFamily: "JosefinSans-Light"
  },
  buttonWrapper: {
    marginTop: 20,
    height: 30,
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  button: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 220,
    height: 58,
    backgroundColor: colors.background,
    borderRadius: 4
  },
  cancelButton: {
    position: "absolute",
    bottom: 0,
    padding: 10
  },
  cancelWrapper: {
    height: 50,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NfcRead);
