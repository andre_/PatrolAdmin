import React, { Component } from "react";
import { View, StyleSheet, BackHandler } from "react-native";
import { connect } from "react-redux";
import axios from "axios";
import QRCodeScanner from "react-native-qrcode-scanner";
import {
  initScannedCp,
  clear,
  submitClocking,
  sendTelegram
} from "../../scenes/new-clocking/actions";
import store from "react-native-simple-store";

const mapStateToProps = state => ({
  account: state.account,
  patroling: state.patroling,
  checkpoint: state.checkpoint,
  notification: state.notification,
  scanning: state.scanning
});

const mapDispatchToPros = dispatch => ({
  add(data) {
    dispatch(initScannedCp(data));
  },
  clear() {
    dispatch(clear());
  },
  save(data, copied) {
    dispatch(submitClocking(data, copied));
  },
  telegram(uri) {
    dispatch(sendTelegram(uri));
  },
  stopAlarm() {
    dispatch(stopSirene());
  },
  isLate(val) {
    dispatch(isLate(val));
  }
});

class QrCodeComponent extends Component {
  static navigationOptions = {
    title: "Scan Code bar",
    headerTitleStyle: {
      color: "#fff"
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      lat: "",
      lng: ""
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    this._getLocation();

    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _getLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      },
      err => {
        console.log("error maps", err.message);
      }
    );
  }

  callTimestamp = () => {
    return new Promise((resolve, reject) => {
      axios({
        method: "GET",
        url: "https://my.trackerhero.com/api/timestamp",
        timeout: 1000 * 3,
        headers: {
          "Content-type": "application/json"
        }
      })
        .then(res => {
          resolve(res.data.timestamp);
          console.log("server");
        })
        .catch(err => {
          console.log("default");
          resolve(Math.floor(new Date().getTime() / 1000));
        });
    });
  };

  _findItem = id => {
    const {
      checkpoint: { checkpoint }
    } = this.props;

    let findCheckpoint = (checkpoint || []).find(v => v.id === parseInt(id))
      ? true
      : false;

    return findCheckpoint;
  };

  onSuccess = e => {
    this.props.navigation.goBack();
    // if (this._findItem(e.data)) {
    //   this.props.navigation.goBack();
    // this.callTimestamp()
    //   .then(data => {
    //     obj = {
    //       id: parseInt(e.data),
    //       time: data,
    //       status: "1"
    //     };
    //     this.props.add(obj);
    //     this.props.navigation.goBack();
    //   })
    //   .catch(err => {
    //     alert("Error scanning", err);
    //   });
    // } else {
    //   alert("QR Code not recognized");
    // }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <QRCodeScanner onRead={this.onSuccess} showMarker />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: "#777"
  },
  textBold: {
    fontWeight: "500",
    color: "#000"
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToPros
)(QrCodeComponent);
