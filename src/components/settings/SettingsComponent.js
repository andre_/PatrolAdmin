import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  ScrollView,
  BackHandler,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { List, ListItem, Button } from "native-base";
import { colors, text } from "@styles";
import PopupDialog, { DialogTitle } from "react-native-popup-dialog";
import languange from "@src/constants/languange";
import PasswordInputText from "react-native-hide-show-password-input";

export default class SettingsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passwordAdmin: "",
      adminPass: ""
    };

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    this.adminPassword.show();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  _doLogin() {
    const {
      account: { account }
    } = this.props;

    if (account.master_password === this.state.adminPass) {
      this.adminPassword.dismiss();
    } else {
      alert("Wrong admin password");
    }
  }

  _doLogout() {
    const {
      destroyToken,
      destroyAccount,
      account: {
        account: { post }
      }
    } = this.props;
    const { passwordAdmin } = this.state;

    if (passwordAdmin === post.installation_code) {
      destroyAccount();
      destroyToken();
    } else {
      alert("Password does not match");
    }
  }

  render() {
    const {
      account: {
        account: { post }
      }
    } = this.props;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }}>
        <View style={{ flex: 1, backgroundColor: colors.light }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <List>
              <ListItem itemDivider style={{ height: 40 }}>
                <Text>{languange.system}</Text>
              </ListItem>
              <ListItem
                style={{ height: 50 }}
                onPress={() => this.props.navigation.navigate("Checkpoint")}
              >
                <Text>{languange.checkpoint}</Text>
              </ListItem>
              <ListItem
                style={{ height: 50 }}
                onPress={() => this.props.navigation.navigate("Guards")}
              >
                <Text>Guards</Text>
              </ListItem>
              <ListItem itemDivider style={{ height: 40 }}>
                <Text>{languange.account}</Text>
              </ListItem>
              <ListItem style={{ height: 50 }}>
                <Text style={{ width: "100%" }}>
                  {languange.logas}:{" "}
                  <Text style={[text.p, { fontWeight: "500" }]}>
                    {post && post.name}
                  </Text>
                </Text>
              </ListItem>
              <ListItem style={{ height: 50 }}>
                {post && post.account_status_flag ? (
                  <Text style={{ width: "100%" }}>
                    {languange.status} :
                    <Text
                      style={[
                        text.textActive,
                        { marginLeft: 8, fontWeight: "500" }
                      ]}
                    >
                      &nbsp; {languange.active}
                    </Text>
                  </Text>
                ) : (
                  <Text style={[text.textNotActive, { marginLeft: 8 }]}>
                    {languange.notactive}
                  </Text>
                )}
              </ListItem>
              <ListItem style={{ height: 60 }}>
                <Text>
                  {languange.expired}:{" "}
                  <Text style={text.p}>{post && post.expiry_date}</Text>
                </Text>
              </ListItem>
              <ListItem
                style={{ height: 60 }}
                onPress={() => this.props.navigation.navigate("Language")}
              >
                <Text>{languange.lang}</Text>
              </ListItem>
              <ListItem
                style={{ height: 60 }}
                onPress={() => this.props.navigation.navigate("Timeclock")}
              >
                <Text>Time Clocking</Text>
              </ListItem>
              <ListItem
                style={{ height: 60 }}
                onPress={() => this.props.navigation.navigate("Flag")}
              >
                <Text>Features Flag</Text>
              </ListItem>
              <ListItem
                style={{ height: 60 }}
                onPress={() => this.props.navigation.navigate("Notification")}
              >
                <Text>Notification Mode</Text>
              </ListItem>
              <ListItem
                style={{ height: 60 }}
                onPress={() => this.props.navigation.navigate("Scanning")}
              >
                <Text>Scanning Method</Text>
              </ListItem>
              <ListItem
                style={{ height: 60 }}
                onPress={() => this.logoutPass.show()}
              >
                <Text>{languange.logout}</Text>
              </ListItem>
            </List>
          </ScrollView>
        </View>

        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 190,
            marginTop: -200
          }}
          ref={popupDialog => {
            this.adminPassword = popupDialog;
          }}
          dismissOnTouchOutside={false}
          dismissOnHardwareBackPress={false}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center"
            }}
          >
            <View
              style={{
                width: "100%",
                backgroundColor: "#fff",
                marginTop: 20,
                paddingRight: 20,
                paddingLeft: 20
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "bold",
                  color: "#333"
                }}
              >
                Admin Password
              </Text>

              <PasswordInputText
                placeholder="Password"
                underlineColorAndroid="#fff"
                style={{
                  paddingLeft: 10,
                  borderRadius: 4
                }}
                onChangeText={password =>
                  this.setState({ adminPass: password })
                }
              />
            </View>

            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Home")}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20,
                  elevation: 0,
                  backgroundColor: colors.danger
                }}
              >
                <Text style={{ color: colors.light }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this._doLogin()}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20,
                  elevation: 0,
                  backgroundColor: colors.success
                }}
              >
                <Text style={{ color: colors.light }}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </PopupDialog>

        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 220,
            marginTop: -200
          }}
          dialogTitle={<DialogTitle title="Logout" />}
          ref={popupDialog => {
            this.logoutPass = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text style={{ fontSize: 20, color: colors.dark }}>
              Input your installation code
            </Text>
            <View style={{ height: 240, marginTop: 10 }}>
              <TextInput
                placeholder="type your installation code"
                onChangeText={text => this.setState({ passwordAdmin: text })}
              />
            </View>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                danger
                onPress={() => this.logoutPass.dismiss()}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Cancel</Text>
              </Button>
              <Button
                success
                onPress={() => this._doLogout()}
                style={{
                  marginRight: 10,
                  width: 120,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Yes</Text>
              </Button>
            </View>
          </View>
        </PopupDialog>
      </KeyboardAvoidingView>
    );
  }
}
