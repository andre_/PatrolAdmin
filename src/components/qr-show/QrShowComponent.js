import React, { Component } from "react";
import { View, BackHandler, StyleSheet } from "react-native";
import QRCode from "react-native-qrcode";

class QrShowComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qr: "andre"
    };

    this._handleBackButton = this._handleBackButton.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this._handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.addEventListener("hardwareBackPress", this._handleBackButton);
  }

  _handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    const { navigation } = this.props;
    console.log(navigation.getParam("qrId"));

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <QRCode
            value={navigation.getParam("qrId")}
            size={300}
            bgColor="#333"
            fgColor="#fff"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  wrapper: {
    width: 300,
    borderColor: "#333"
  }
});

export default QrShowComponent;
