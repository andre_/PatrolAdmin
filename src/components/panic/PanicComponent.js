import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  BackHandler,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { Button } from "native-base";
import { colors } from "@styles";
import { API } from "@src/services/APIService";

import LottieView from "lottie-react-native";

let SoundPlayer = require("react-native-sound");
let song = null;

const close = require("../../assets/lottie/close.json");

const mapStateToProps = state => ({
  account: state.account,
  posts: state.posts
});

class PanicComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 3,
      pause: false,
      lat: "",
      lng: "",
      ImageSource: null
    };

    song = new SoundPlayer("siren.mp3", SoundPlayer.MAIN_BUNDLE, error => {
      if (error) {
        alert("error playing sound");
      }
    });
  }

  componentDidMount() {
    this.intervalId = setInterval(this.timer.bind(this), 1000);

    BackHandler.addEventListener("hardwareBackPress", this.handleBack);

    this._watchPosition();
    this._getPosition();
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
    this.stopPlay();

    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.stopPlay();
    this.props.navigation.goBack();
    return true;
  };

  _watchPosition = () => {
    this._watchId = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          error: null
        });
      },
      error => this.setState({ error: error.message }),
      {
        maximumAge: 3000
      }
    );
  };

  timer() {
    this.setState({
      time: this.state.time - 1
    });

    if (this.state.time < 1) {
      clearInterval(this.intervalId);
    }
  }

  _getPosition() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          coordinate: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
        });
      },
      error => {
        console.log("error get location");
      },
      {
        enableHighAccuracy: true
      }
    );
  }

  playSound = () => {
    this.setState({ pause: !this.state.pause });
    let coords = `${this.state.lat},${this.state.lng}`;

    const {
      posts: { posts }
    } = this.props;

    if (song != null) {
      if (this.state.pause) {
        song.pause();
      } else {
        const obj = new FormData();
        obj.append("post_id", parseInt(posts.post_id));
        obj.append("gps", coords);

        API()
          .post("sendpanic", obj)
          .then(res => {
            console.log("panic", res);
          })
          .catch(err => {
            console.log(err);
          });

        song.setVolume(1.0);
        song.play(sucess => {
          if (!sucess) {
            alert("error init sound player");
          }
        });

        song.setNumberOfLoops(-1);
      }
    }
  };

  stopPlay = () => {
    if (song != null) {
      song.stop();
      this.setState({ pause: false });
      this.props.navigation.goBack();
    }
  };

  _cancelOperation() {
    this.props.navigation.goBack();
  }

  panicNotAvailable = () => (
    <View style={styles.panicDisabled}>
      <View style={styles.animationWrapper}>
        <LottieView source={close} autoPlay />
      </View>
      <View style={styles.textDescription}>
        <Text style={styles.text}>You not subscribe this features</Text>
        <Text style={styles.textInfo}>
          Please subscribe to use this features
        </Text>
      </View>
    </View>
  );

  panicAvailable = () => (
    <View style={styles.container}>
      {this.state.time > 0 ? (
        <View style={styles.content}>
          <Text style={styles.textNumber}>{this.state.time}</Text>
          <Text style={styles.desc}>Time to send panic</Text>

          <View style={styles.buttonWrapper}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this._cancelOperation()}
            >
              <Text style={styles.textWhite}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <View style={styles.content}>
          <View style={styles.buttonWrapper}>
            <Button style={styles.buttonSiren} onPress={this.playSound}>
              <Text style={styles.textWhite}>
                {this.state.pause ? "Pause" : "Play"} Siren
              </Text>
            </Button>
            <Button style={styles.button} onPress={this.stopPlay}>
              <Text style={styles.textWhite}>Back</Text>
            </Button>
          </View>
        </View>
      )}
    </View>
  );

  render() {
    const {
      account: { features }
    } = this.props.account;

    return (
      <View style={styles.container}>
        {features.enable_mandown_in_patrol
          ? this.panicAvailable()
          : this.panicNotAvailable()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center"
  },
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  textNumber: {
    textAlign: "center",
    fontSize: 100,
    color: "#333"
  },
  buttonWrapper: {
    marginTop: 40
  },
  button: {
    elevation: 0,
    width: 120,
    height: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.info
  },
  buttonSiren: {
    elevation: 0,
    width: 120,
    height: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.danger,
    marginBottom: 20
  },
  textWhite: {
    color: colors.light,
    fontFamily: "JosefinSans-Regular",
    fontSize: 18
  },
  text: {
    fontFamily: "OpenSans-Regular",
    fontSize: 16,
    textAlign: "center",
    marginBottom: 5
  },
  textInfo: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    textAlign: "center"
  },
  desc: {
    fontSize: 16,
    fontFamily: "JosefinSans-Regular"
  },
  animationWrapper: {
    marginTop: -50,
    width: 140,
    height: 140,
    alignSelf: "center"
  },
  textDescription: {
    marginTop: -30
  }
});

export default connect(mapStateToProps)(PanicComponent);
