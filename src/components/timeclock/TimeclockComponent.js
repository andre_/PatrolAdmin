import React, { Component } from "react";
import { View, Text, BackHandler } from "react-native";
import { List, ListItem, Icon, Left, Body } from "native-base";
import { colors } from "@styles/";

export default class TimeclockComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    const {
      timeclock: { type }
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: colors.light }}>
        <List>
          <ListItem
            icon
            onPress={() => {
              this.props.setTimeToDefault();
              this.props.navigation.goBack();
            }}
          >
            <Left style={{ width: 40 }}>
              {type === "default" && <Icon type="FontAwesome" name="check" />}
            </Left>
            <Body>
              <Text>Default</Text>
            </Body>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.setTimeFromServer();
              this.props.navigation.goBack();
            }}
          >
            <Left style={{ width: 40 }}>
              {type === "server" && <Icon type="FontAwesome" name="check" />}
            </Left>
            <Body>
              <Text>From Server</Text>
            </Body>
          </ListItem>
        </List>
      </View>
    );
  }
}
