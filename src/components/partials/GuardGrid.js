import React, { Fragment } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Thumbnail } from "native-base";
import { configs, colors, text } from "@styles/";

const _doNavigate = (data, initStaff, out, navigation) => {
  initStaff(data);

  if (out) {
    if (data.checkin_token) {
      navigation.navigate("Checkout");
    }
  } else {
    if (!data.checkin_token) {
      navigation.navigate("Checkin");
    }
  }
};

const GuardGrid = ({ data, initStaff, out, navigation }) => {
  return (
    <Fragment>
      <TouchableOpacity
        style={[
          configs.card,
          configs.addMarginBottom,
          {
            display: "flex",
            alignItems: "center",
            flexDirection: "row"
          }
        ]}
        onPress={() => _doNavigate(data, initStaff, out, navigation)}
      >
        <View>
          <Thumbnail
            source={require("./../../assets/policeman.png")}
            style={{
              borderWidth: data.checkin_token ? 3 : 0,
              borderColor: data.checkin_token ? colors.success : "transparent",
              width: 50,
              height: 50,
              marginRight: 20
            }}
          />
        </View>
        <View>
          <Text style={[text.h4, text.spaceBottom]}>{data.name}</Text>
          <Text style={text.p}>
            {data.checkin_token ? "Already checkin" : "Have not checkin"}
          </Text>
        </View>
      </TouchableOpacity>
      {/* <View
        style={[
          {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: 100,
            height: 100
          }
        ]}
      >
        <TouchableOpacity
          // onPress={() => navigation.navigate(out ? "Checkout" : "Checkin")}
          onPress={() => _doNavigate(data, initStaff, out, navigation)}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: 60,
            height: 60,
            borderRadius: 100,
            borderWidth: 3,
            borderColor: data.checkin_token ? colors.success : "#999",
            backgroundColor: colors.light
          }}
        >
          <Thumbnail
            source={require("./../../assets/policeman.png")}
            style={{ width: 50, height: 50 }}
          />
        </TouchableOpacity>
        <Text
          style={{
            marginTop: 10,
            textAlign: "center",
            width: 100
          }}
        >
          {data.name}
        </Text>
      </View> */}
    </Fragment>
  );
};

export default GuardGrid;
