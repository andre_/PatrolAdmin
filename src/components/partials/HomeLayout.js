import React, { Component, Fragment } from "react";
import { View, Text } from "react-native";
import { Footer, FooterTab, Button } from "native-base";
import languange from "@src/constants/languange";
import { colors } from "@styles";

const HomeLayout = ({ children }) => (
  <Fragment>
    {children}
    <View style={{ borderWidth: 1, borderColor: "#f1f1f1" }}>
      <Footer>
        <FooterTab style={{ backgroundColor: colors.light }}>
          <Button>
            <Text style={{ color: colors.info }}>{languange.call}</Text>
          </Button>
          <Button onPress={() => this._switchTorch()}>
            <Text
              style={{
                color: colors.info
              }}
            />
          </Button>
          <Button onPress={() => this.props.navigation.navigate("Settings")}>
            <Text style={{ color: colors.info }}>{languange.settings}</Text>
          </Button>
          <Button>
            <Text style={{ color: colors.info }}>{languange.account}</Text>
          </Button>
        </FooterTab>
      </Footer>
    </View>
  </Fragment>
);
