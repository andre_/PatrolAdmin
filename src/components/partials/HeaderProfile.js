import React, { Component, Fragment } from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { colors } from "@styles";

const mapStateToProps = state => ({
  supervisor: state.supervisor,
  connection: state.connection
});

const HeaderProfile = ({ supervisor, connection }) => {
  let image = supervisor.supervisorImage
    ? { uri: supervisor.supervisorImage }
    : require("../../images/logo.png");
  return (
    <Text
      style={{
        marginRight: 10,
        fontFamily: "OpenSans-Bold",
        color: connection.connected ? colors.success : colors.danger
      }}
    >
      {connection.connected ? "ONLINE" : "OFFLINE"}
    </Text>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row"
  },
  caption: {
    alignSelf: "center",
    paddingRight: 5
  }
});

export default connect(mapStateToProps)(HeaderProfile);
