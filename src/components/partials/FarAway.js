import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { colors } from "@styles";
import RNRestart from "react-native-restart";

class FarAway extends Component {
  constructor(props) {
    super(props);
    this.state = {
      far: false
    };
  }

  _restart() {
    RNRestart.Restart();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.imageWrapper}>
            <Image source={require("../../assets/map.png")} />
          </View>

          <Text style={styles.text}>You are far away from the location</Text>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this._restart()}
          >
            <Text style={styles.textWhite}>Refresh Location</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.light
  },
  wrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column",
    width: 250,
    height: 300
  },
  text: {
    fontSize: 22,
    fontFamily: "OpenSans-Bold",
    color: colors.dark,
    textAlign: "center",
    marginBottom: 10
  },
  textWhite: {
    fontFamily: "OpenSans-Light",
    color: colors.light,
    fontSize: 16
  },
  button: {
    width: 180,
    height: 60,
    borderRadius: 4,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.info
  }
});

export default FarAway;
