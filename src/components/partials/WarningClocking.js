import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Vibration
} from "react-native";

let SoundPlayer = require("react-native-sound");
let song = null;

class WarningClocking extends Component {
  constructor(props) {
    super(props);

    this.state = {
      time: 3,
      pause: false
    };

    this._doVibrate = this._doVibrate.bind(this);

    song = new SoundPlayer("siren.mp3", SoundPlayer.MAIN_BUNDLE, error => {
      if (error) {
        alert("error playing sound");
      } else {
        console.log("berhasil");
      }
    });
  }

  componentDidMount() {
    console.log(this.props.account.warning);
    if (this.props.account.warning) {
      this._doVibrate();
    }
    setTimeout(() => {
      this.playSound();
    }, 1000);
  }

  playSound() {
    if (song != null) {
      song.setVolume(1.0);
      song.play(sucess => {
        if (!sucess) {
          alert("error init sound player");
        }
      });
    }
  }

  _doVibrate() {
    Vibration.vibrate(30000);
  }

  cancelVibrate() {
    song.stop();
    Vibration.cancel();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={require("../../assets/alarm.png")} />
        </View>
        <View style={styles.title}>
          <Text
            style={{
              fontSize: 20,
              fontFamily: "OpenSans-SemiBold",
              textAlign: "center"
            }}
          >
            Information
          </Text>
        </View>

        <View style={styles.warning}>
          <Text
            style={{
              lineHeight: 28,
              textAlign: "center",
              fontSize: 16,
              fontFamily: "OpenSans-Light"
            }}
          >
            You do not clocking for any checkpoint right now. Please do
            clocking.
          </Text>
        </View>

        <View
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              song.stop();
              Vibration.cancel();
              this.props.isLate(false);
              this.props.navigation.navigate("Clocking");
            }}
            style={{
              marginTop: 15,
              backgroundColor: "#e34e5f",
              width: 240,
              height: 60,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 30
            }}
          >
            <Text
              style={{
                color: "#fff",
                fontSize: 16,
                fontFamily: "OpenSans-SemiBold"
              }}
            >
              Do Clocking
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.props.isLate(false);
              this.cancelVibrate();
            }}
            style={{
              marginTop: 15,
              backgroundColor: "#dadada",
              width: 240,
              height: 60,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 30
            }}
          >
            <Text
              style={{
                color: "#333",
                fontSize: 16,
                fontFamily: "OpenSans-SemiBold"
              }}
            >
              Silent
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: "30%",
    backgroundColor: "#E34e5f",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    paddingTop: 30,
    width: "85%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  titleCenter: {
    fontSize: 40,
    textAlign: "center"
  },
  warning: {
    marginTop: 20,
    marginBottom: 30,
    width: "85%",
    marginLeft: "auto",
    marginRight: "auto"
  }
});

export default WarningClocking;
