import React, { Component } from "react";
import { View, Text } from "react-native";
import PopupDialog, {
  SlideAnimation,
  FadeAnimation,
  DialogTitle
} from "react-native-popup-dialog";

const ModalFactory = props => {
  return (
    <PopupDialog
      dialogStyle={{
        width: 300,
        height: 150
      }}
      ref={popupDialog => {
        props.refer = popupDialog;
      }}
    >
      <View
        style={{
          flex: 1,
          paddingLeft: 20,
          paddingRight: 20,
          paddingTop: 20
        }}
      >
        <Text>Are you sure end this clocking?</Text>
      </View>
    </PopupDialog>
  );
};

export default ModalFactory;
