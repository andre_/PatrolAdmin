import React, { Component } from "react";
import { View, Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { colors } from "@styles";
import { API } from "@src/services/APIService";

const mapStateToProps = state => ({
  photo: state.photo,
  attendance: state.attendance
});

const mapDispatchToProps = dispatch => ({});

class CameraPreview extends Component {
  _sendImageCheckin() {
    const {
      photo: {
        photo: { path, mediaUri }
      },
      attendance: { attendance },
      navigation: { navigate, getParam }
    } = this.props;

    const obj = new FormData();
    obj.append("attendance_id", attendance.id);
    obj.append("image", {
      uri: mediaUri,
      type: "image/jpeg",
      name: `photo-${attendance.id}`
    });

    API()
      .post("send_image_attendance", obj)
      .then(res => {
        getParam("for")
          ? navigate("CheckoutStatus")
          : navigate("CheckinStatus");
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const {
      photo: {
        photo: { path }
      }
    } = this.props;

    return (
      <View style={styles.container}>
        <Image source={{ uri: path }} style={{ flex: 1 }} />

        <View style={styles.control}>
          <TouchableOpacity
            style={styles.divide}
            onPress={() => this.props.navigation.navigate("Camera")}
          >
            <Text style={{ color: colors.light, fontSize: 16 }}>Try Again</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.divide}
            onPress={() => this._sendImageCheckin()}
          >
            <Text style={{ color: colors.light, fontSize: 16 }}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light
  },
  control: {
    position: "absolute",
    zIndex: 10000,
    left: 0,
    bottom: 0,
    height: 50,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    width: "100%",
    display: "flex",
    flexDirection: "row"
  },
  divide: {
    width: "50%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CameraPreview);
