import React, { Component } from "react";
import { connect } from "react-redux";
import DropdownAlert from "react-native-dropdownalert";

class Notification extends Component {
  componentDidMount() {
    this.dropdown.alertWithType("success", "Success", "Hello Panda");
  }

  render() {
    return <DropdownAlert ref={dropdown => (this.dropdown = dropdown)} />;
  }
}

export default Notification;
