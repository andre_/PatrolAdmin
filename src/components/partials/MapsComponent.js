import { Text, View } from "react-native";
import React, { Component } from "react";
import BackgroundTimer from "react-native-background-timer";

export default class MapsComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null
    };
  }

  componentDidMount() {
    this.getExactPosition();
    // this.getPosition();
    // this.watchId = navigator.geolocation.watchPosition(
    //   position => {
    //     this.setState({
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       error: null
    //     });
    //   },
    //   error => this.setState({ error: error.message }),
    //   {
    //     enableHighAccuracy: false,
    //     timeout: 20000,
    //     maximumAge: 0,
    //     distanceFilter: 1
    //   }
    // );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchPosi);
    BackgroundTimer.removeEventListener(this.counting);
  }

  getPosition = () => {
    this.total = 0;
    this.counting = BackgroundTimer.setInterval(() => {
      if (this.total % 10 === 0) {
        this.getExactPosition();
      }
      this.total = this.total + 1;
    }, 1000);
  };

  getExactPosition = () => {
    this.watchPosi = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      },
      err => {
        console.log("error maps", err.message);
      },
      {
        enableHighAccuracy: true
      }
    );
  };

  render() {
    return (
      <View
        style={{ flexGrow: 1, alignItems: "center", justifyContent: "center" }}
      >
        <Text>Latitude:: {this.state.latitude}</Text>
        <Text>Longitude:: {this.state.longitude}</Text>
        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
      </View>
    );
  }
}
