import React, { Component, Fragment } from "react";
import {
  Alert,
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid
} from "react-native";
import { List, ListItem, Left, Body } from "native-base";
import RNRestart from "react-native-restart";
import DeviceInfo from "react-native-device-info";
import { connect } from "react-redux";
import { endClocking } from "@src/scenes/clocking/actions";
import { sendOfflineVisitor } from "@src/actions/offlineActions";
import { clear, clearStorage } from "@src/scenes/new-clocking/actions";
import { colors } from "@styles";
import store from "react-native-simple-store";
import Modal from "react-native-modal";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const mapStateToProps = state => ({
  account: state.account,
  clocking: state.clocking,
  posts: state.posts
});

const mapDispatchToProps = dispatch => ({
  clearClocking() {
    dispatch(endClocking());
  },

  doSendOfflineVisitor(data) {
    dispatch(sendOfflineVisitor(data));
  },

  clearAction() {
    dispatch(clear());
  },

  clearStorageAction() {
    dispatch(clearStorage());
  }
});

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      text: "",
      loop: 0,
      offlineLength: 0
    };
  }

  resetAll = () => {
    Alert.alert(
      "Are you sure",
      "Are you sure to reset the data?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.props.clearClocking();
            this.props.clearAction();
            this.props.clearStorageAction();
            store.delete("visitor");

            ToastAndroid.show("Refresh finish", ToastAndroid.SHORT);
          }
        }
      ],
      { cancelable: false }
    );
  };

  _checkOffline() {
    store
      .get("visitor")
      .then(visitor => {
        if (visitor !== null) {
          this.props.doSendOfflineVisitor(visitor);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const {
      account: {
        account: { post }
      }
    } = this.props;

    return (
      <View style={styles.container}>
        <Modal isVisible={this.state.isVisible}>
          <View
            style={{
              borderRadius: 4,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: 120,
              height: 120,
              backgroundColor: "#fff",
              alignSelf: "center"
            }}
          >
            <ActivityIndicator color={colors.info} />
            <Text style={{ textAlign: "center", marginTop: 12 }}>
              {this.state.text}
            </Text>
          </View>
        </Modal>
        <View style={styles.wrapper}>
          <View style={styles.header}>
            <View style={styles.imageWrapper}>
              <Image
                source={require("../../assets/trackerhero.jpg")}
                style={{ height: "100%", width: "100%", resizeMode: "contain" }}
              />
            </View>

            <View style={styles.description}>
              <Text style={styles.text}>{post && post.name}</Text>
            </View>
          </View>

          <View style={styles.body}>
            <List>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image
                    source={require("../../assets/refresh.png")}
                    style={{ width: 20, height: 20 }}
                  />
                </Left>
                <Body>
                  <TouchableOpacity onPress={() => RNRestart.Restart()}>
                    <Text>Refresh Account</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image
                    source={require("../../assets/report.png")}
                    style={{ width: 20, height: 20 }}
                  />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("ReportList")}
                  >
                    <Text>Report List</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image
                    source={require("../../assets/icons/analysis.png")}
                    style={{ width: 20, height: 20 }}
                  />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("ClockingReport")
                    }
                  >
                    <Text>Offline Report</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image
                    source={require("../../assets/icons/bar-chart.png")}
                    style={{ width: 20, height: 20 }}
                  />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("Summary")}
                  >
                    <Text>History</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem
                icon
                style={styles.listItem}
                noBorder
                onPress={() => this.props.navigation.navigate("Settings")}
              >
                <Left>
                  <Image
                    source={require("../../assets/cogs.png")}
                    style={{ width: 20, height: 20 }}
                  />
                </Left>
                <Body>
                  <Text>Settings</Text>
                </Body>
              </ListItem>
            </List>
          </View>
        </View>
        <View
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            bottom: 30
          }}
        >
          <Text
            style={{
              fontFamily: "OpenSans-Light",
              fontSize: 14,
              color: "#333",
              marginBottom: 6
            }}
          >
            Version Number: {DeviceInfo.getVersion()}
          </Text>
          <Text
            style={{
              fontFamily: "OpenSans-Light",
              fontSize: 14,
              color: "#333"
            }}
          >
            Build Number: {DeviceInfo.getBuildNumber()}
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <TouchableOpacity
              onPress={this.resetAll}
              style={{
                marginTop: 8,
                padding: 10,
                backgroundColor: colors.danger
              }}
            >
              <Text
                style={{
                  fontFamily: "OpenSans-Light",
                  fontSize: 14,
                  color: "#fff"
                }}
              >
                Reset Data
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                marginTop: 8,
                padding: 10,
                backgroundColor: colors.info
              }}
              onPress={() => this._checkOffline()}
            >
              <Text
                style={{
                  fontFamily: "OpenSans-Light",
                  fontSize: 14,
                  color: "#fff"
                }}
              >
                Check offline
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  wrapper: {
    marginTop: hp("2%"),
    marginBottom: hp("4%"),
    marginLeft: wp("4%"),
    marginRight: wp("4%")
  },
  header: {
    paddingTop: hp("3%"),
    paddingBottom: hp("2%"),
    minHeight: hp("5%"),
    borderBottomWidth: 1,
    borderBottomColor: "#fafafa"
  },
  imageWrapper: { height: hp("8%") },
  description: {
    paddingTop: hp("2%"),
    paddingBottom: hp("2%"),
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 16,
    fontFamily: "JosefinSans-Regular"
  },
  listItem: {
    paddingLeft: 0,
    marginLeft: 0,
    height: hp("9%")
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
