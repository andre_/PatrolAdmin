"use strict";
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler
} from "react-native";
import { Icon, Spinner } from "native-base";
import { connect } from "react-redux";
import { sharingPhoto } from "@src/actions/photoActions";
import { colors } from "@styles";
import ImagePicker from "react-native-image-picker";
import { API } from "@src/services/APIService";

const mapStateToProps = state => ({
  photo: state.photo,
  attendance: state.attendance
});

const mapDispatchToProps = dispatch => ({
  getPhoto(data) {
    dispatch(sharingPhoto(data));
  }
});

BackHandler.addEventListener("hardwareBackPress", () => {});

class CameraBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      picture: null,
      loading: false
    };
  }

  takePicture() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      cameraType: "front",
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        let source = { uri: response.uri };

        const {
          attendance: { attendance },
          navigation: { navigate, getParam }
        } = this.props;

        this.setState({ loading: true });

        const obj = new FormData();
        obj.append("attendance_id", attendance.id);
        obj.append("image", {
          uri: source.uri,
          type: "image/jpeg",
          name: `photo-${attendance.id}`
        });

        API()
          .post("send_image_attendance", obj)
          .then(res => {
            this.setState({ loading: false });
            getParam("for")
              ? navigate("CheckoutStatus")
              : navigate("CheckinStatus");
          })
          .catch(err => {
            this.setState({ loading: false });
            console.log(err);
          });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        >
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={{
              width: 70,
              height: 70,
              backgroundColor: colors.info,
              borderRadius: 100,
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {this.state.loading ? (
              <Spinner color={colors.light} />
            ) : (
              <Icon
                name="camera"
                style={{ alignSelf: "center", color: colors.light }}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    backgroundColor: "#fafafa"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  capture: {
    flex: 0,
    backgroundColor: "#fff",
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: "center",
    margin: 20
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraBox);
