import React, { Component } from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image } from "react-native";
import { Spinner } from "native-base";
import DeviceInfo from "react-native-device-info";
import { colors, text, configs } from "@styles";
import { API } from "@src/services/APIService";
import moment from "moment";
import ImagePicker from "react-native-image-picker";
import store from "react-native-simple-store";

export default class CheckinComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      imageUri: null
    };
  }

  _doCheckout = () => {
    const {
      staff: { staff }
    } = this.props;

    if (staff.checkin_token) {
      const obj = new FormData();
      obj.append("id", staff.id);
      obj.append("post_id", staff.post_id);
      obj.append("deviceid", DeviceInfo.getDeviceId());
      obj.append("checkin_token", staff.checkin_token);

      this.setState({ loading: true });

      API()
        .post("checkout", obj)
        .then(res => {
          const pict = new FormData();
          pict.append("attendance_id", res.data.id);
          pict.append("image", {
            uri: this.state.imageUri,
            type: "image/jpeg",
            name: `photo-${res.data.id}`
          });

          this.props.navigation.navigate("CheckoutStatus", {
            data: pict
          });
        })
        .catch(err => {
          this.setState({ loading: true });
          store
            .push("checkout", {
              id: staff.id,
              post_id: staff.post_id,
              deviceid: DeviceInfo.getDeviceId(),
              checkin_token: staff.checkin_token
            })
            .then(() => store.get("checkout"))
            .then(checkout => {
              console.log("checkout", checkout);
              this.props.navigation.navigate("CheckoutStatus");
            })
            .catch(err => {
              console.log("fail to save storage");
            });
        });
    } else {
      new Error("Token not found");
    }
  };

  returnFromCamera = data => {
    this.setState({
      imageUri: data
    });
  };

  _openCamera = () => {
    const {
      flag: { attendanceCameraVersion }
    } = this.props;

    if (attendanceCameraVersion === "v1") {
      this.props.navigation.navigate("CheckinCamera", {
        returnFromCamera: this.returnFromCamera
      });
    } else {
      const options = {
        quality: 0.5,
        maxWidth: 500,
        maxHeight: 500,
        cameraType: "front"
      };
      ImagePicker.launchCamera(options, response => {
        if (response.error) {
          alert("Camera error");
        } else if (response.didCancel) {
          console.log("did cancel");
        } else {
          this.setState({
            imageUri: response.uri
          });
        }
      });
    }
  };

  render() {
    const { imageUri } = this.state;

    return (
      <View style={styles.modalBackground}>
        <View style={styles.modalContent}>
          <View style={[configs.flexColumnCenter, { flex: 1 }]}>
            <View style={styles.imageBorder}>
              {imageUri !== null ? (
                <Image style={styles.images} source={{ uri: imageUri }} />
              ) : null}
            </View>

            <View style={{ width: 280 }}>
              <Text
                style={[text.h2, text.spaceBottom, { color: colors.danger }]}
              >
                TIME
              </Text>
              <Text
                style={[
                  text.h2,
                  {
                    color: colors.danger,
                    fontWeight: "bold",
                    marginBottom: 10
                  }
                ]}
              >
                {moment().format("HH:mm:ss")}
              </Text>
              <Text style={[text.h2, { color: colors.danger }]}>
                {moment().format("dddd, DD MMMM YYYY")}
              </Text>

              {imageUri === null ? (
                <TouchableOpacity
                  onPress={this._openCamera}
                  style={[
                    configs.flexRowCenter,
                    {
                      marginTop: 40,
                      height: 50,
                      backgroundColor: colors.danger,
                      borderRadius: 4
                    }
                  ]}
                >
                  <Text style={[text.h3, { color: colors.light }]}>
                    Take Picture
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={this._doCheckout}
                  style={[
                    configs.flexRowCenter,
                    {
                      marginTop: 40,
                      height: 50,
                      backgroundColor: colors.danger,
                      borderRadius: 4
                    }
                  ]}
                >
                  {this.state.loading && (
                    <Spinner
                      color={colors.light}
                      style={{ position: "absolute", left: 10 }}
                    />
                  )}
                  <Text style={[text.h3, { color: colors.light }]}>
                    Check Out
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.6)"
  },
  modalContent: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.light
  },
  imageBorder: {
    top: -30,
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: colors.border,
    borderRadius: 4
  },
  images: {
    borderRadius: 4,
    width: "100%",
    height: "100%"
  }
});
