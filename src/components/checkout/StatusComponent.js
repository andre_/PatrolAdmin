import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Spinner
} from "react-native";
import { connect } from "react-redux";
import { colors, text } from "@styles";
import { API } from "@src/services/APIService";

const mapStateToProps = state => ({
  staff: state.staff
});

class StatusComponent extends Component {
  state = {
    isLoading: false
  };

  generateTime = time => {
    return time.substr(0, time.length - 7);
    // {() => this.generateTime(staff.updated_at.date)}
  };

  sendImageAttendance = () => {
    const obj = this.props.navigation.getParam("data");

    this.setState({
      isLoading: true
    });

    API()
      .post("send_image_attendance", obj)
      .then(res => {
        this.props.navigation.navigate("Home");
      })
      .catch(err => {
        this.setState({ isLoading: false });
        alert("Send image for attendance fail");
        console.log("fail checkout", err);
      });
  };

  render() {
    const {
      staff: { staff }
    } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.success}>
            <View style={styles.flexRow}>
              <Text style={styles.text}>Time In</Text>
              <Text style={styles.text}>
                {(staff || []).latest_datetime || "-"}
              </Text>
            </View>
            <View style={styles.redScreen}>
              <Text style={[styles.text, styles.textLight]}>STATUS</Text>
              <Text style={[styles.bigText, styles.bold, styles.textLight]}>
                CHECK OUT
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={[styles.button, styles.flexCenter]}
          onPress={this.sendImageAttendance}
        >
          {this.state.loading && (
            <Spinner
              color={colors.light}
              style={{ position: "absolute", left: 10 }}
            />
          )}
          <Text style={[styles.textLight, styles.bold]}>DONE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.black
  },
  wrapper: {
    padding: 20,
    backgroundColor: colors.light,
    borderRadius: 10,
    marginLeft: "auto",
    marginRight: "auto",
    width: "90%",
    height: 180
  },
  flexCenter: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  success: {
    flex: 1
  },
  redScreen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    width: "100%",
    height: 60,
    borderRadius: 40,
    bottom: 0,
    backgroundColor: "tomato"
  },
  button: {
    backgroundColor: colors.info,
    width: "90%",
    marginTop: 20,
    height: 50
  },
  flexRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 4
  },
  text: {
    color: colors.dark,
    fontSize: 16
  },
  textLight: {
    color: colors.light
  },
  bold: {
    fontWeight: "bold"
  },
  bigText: {
    fontSize: 20,
    textAlign: "center",
    width: 300,
    marginTop: 2
  }
});

export default connect(mapStateToProps)(StatusComponent);
