import { INIT_POSTS } from "@src/constants/actionTypes";

export const initPosts = data => {
  return {
    type: INIT_POSTS,
    data
  };
};

export const initForms = data => ({
  type: "INIT_FORMS",
  data
});
