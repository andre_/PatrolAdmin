import * as types from "@src/constants/actionTypes";
import { API } from "@src/services/APIService";
import store from "react-native-simple-store";

export const sendReport = data => {
  return dispatch => {
    dispatch({ type: types.SEND_REPORT_REQUEST });

    API()
      .post(`send_incident?phone=${data.phone}`, data.obj)
      .then(res => {
        console.log("uploading report success", res.data);
        dispatch(uploadPicture(data.pict, res.data.id));
      })
      .catch(err => {
        this.setState({ loadingModal: false });
        store
          .push("report", obj)
          .then(() => store.get("report"))
          .then(report => {
            // this.props.destroyClockingId();
          })
          .catch(err => {
            console.log("uploading report", err);
          });
      });
  };
};

export const uploadPicture = (data, id) => {
  return dispatch => {
    const { albums } = data.pict;
    const pict = new FormData();
    pict.append("id", parseInt(id));

    albums.map(v => {
      pict.append("attachments[]", {
        uri: v.uri,
        name: v.fileName,
        type: "image/jpeg"
      });
    });

    API()
      .post("send_attachment_incident", pict)
      .then(res => {
        console.log("uploading image succes", res.data);
        this.setState({ loadingModal: false });
      })
      .catch(err => {
        console.log("uploading picture", err);
      });
  };
};
