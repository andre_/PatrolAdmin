import { SET_TO_EN, SET_TO_TH, SET_TO_MY } from "@src/constants/actionTypes";

export const setToEn = () => ({
  type: SET_TO_EN
});

export const setToMy = () => ({
  type: SET_TO_MY
});

export const setToTh = () => ({
  type: SET_TO_TH
});
