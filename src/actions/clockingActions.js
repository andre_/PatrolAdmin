import * as types from '@src/constants/actionTypes';
import {API} from '@src/services/APIService';
import store from 'react-native-simple-store';

export const addDummyCheckpoint = data => {
  return {
    type: types.ADD_DUMMY_CHECKPOINT,
    data,
  };
};

export const addFailedData = data => {
  return {
    type: types.ADD_FAILED_DATA,
    data,
  };
};

export const initClocking = data => {
  navigator.geolocation.getCurrentPosition(
    position => {
      data.gps = `${position.coords.latitude},${position.coords.longitude}`;
    },
    err => {
      console.log(err);
    },
    {
      enableHighAccuracy: true,
    },
  );

  return dispatch => {
    dispatch({type: types.INIT_CLOCKING, data});
  };
};

export const finishClocking = data => {
  return dispatch => {
    dispatch({type: types.FINISH_CLOCKING_REQUEST});

    API()
      .post(`save_clockings?checkpoint=${JSON.stringify(data)}`)
      .then(res => {
        if (parseInt(res.data.success)) {
          dispatch({type: types.FINISH_CLOCKING_FULFILLED});
          dispatch(endClocking());
        }
      })
      .catch(err => {
        store
          .push('failedData', data)
          .then(() => store.get('failedData'))
          .then(failedData => {
            dispatch({type: types.FINISH_CLOCKING_REJECTED});
            dispatch(endClocking());
          })
          .catch(err => {
            alert('Fail to save to storage');
          });
      });
  };
};

export const uploadImageTelegram = data => {
  return dispatch => {
    API()
      .post(`send_image_report?post_id=${data.post_id}`, data.obj)
      .then(res => {})
      .catch(err => {
        console.log(err);
      });
  };
};

export const shareClocking = data => ({
  type: types.SHARE_CLOCKING,
  data,
});

export const endClocking = data => ({
  type: types.END_CLOCKING,
  data,
});

export const firstCheckpoint = data => ({
  type: types.FIRST_CHECKPOINT,
  data,
});

export const getClockingId = data => ({
  type: types.GET_CLOCKING_ID,
  data,
});

export const destroyClockingId = () => ({
  type: types.DESTROY_CLOCKING_ID,
});
