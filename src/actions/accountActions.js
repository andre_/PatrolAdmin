import { INIT_ACCOUNT, DESTROY_ACCOUNT } from "@src/constants/actionTypes";

export const initAccount = data => ({
  type: INIT_ACCOUNT,
  data
});

export const initCoordinate = data => ({
  type: "INIT_COORDINATE",
  data
});

export const destroyAccount = () => ({
  type: DESTROY_ACCOUNT
});

export const initPhone = data => ({
  type: "INIT_PHONE",
  data
});

export const destroyPhone = () => ({
  type: "DESTROY_PHONE"
});

export const searchStaffIn = data => ({
  type: "SEARCH_STAFF_IN",
  data
});

export const isLate = data => {
  return dispatch => {
    dispatch({ type: "IS_LATE", data });
  };
};

export const setIntervalCount = data => ({
  type: "SET_INTERVAL_COUNT",
  data
});
