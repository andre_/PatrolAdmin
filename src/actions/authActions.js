import {
  INIT_TOKEN,
  INIT_PLAYER,
  DESTROY_PLAYER,
  DESTROY_TOKEN
} from "@src/constants/actionTypes";

export const initToken = data => {
  return {
    type: INIT_TOKEN,
    data
  };
};

export const destroyToken = () => {
  return {
    type: DESTROY_TOKEN
  };
};

export const initPlayer = data => {
  return {
    type: INIT_PLAYER,
    data
  };
};

export const destroyPlayer = () => {
  return {
    type: DESTROY_PLAYER
  };
};
