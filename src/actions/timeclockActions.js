import {
  SET_TIME_TO_DEFAULT,
  SET_TIME_FROM_SERVER
} from "@src/constants/actionTypes";

export const setTimeToDefault = () => ({
  type: SET_TIME_TO_DEFAULT
});

export const setTimeFromServer = () => ({
  type: SET_TIME_FROM_SERVER
});
