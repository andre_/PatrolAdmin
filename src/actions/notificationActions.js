import * as types from "@src/constants/actionTypes";
import { Vibration } from "react-native";

let SoundPlayer = require("react-native-sound");
let pattern = [1000, 1000, 1000];
let song = new SoundPlayer("siren.mp3", SoundPlayer.MAIN_BUNDLE, error => {
  if (error) {
    alert("error playing sound");
  }
});

export const playSirene = () => {
  return (dispatch, getState) => {
    dispatch({ type: types.PLAY_SIRENE });
    if (song != null) {
      song.setVolume(1);
      if (getState().notification.buzztype === "infinite") {
        song.setNumberOfLoops(-1);
        Vibration.vibrate(pattern, true);
      } else {
        Vibration.vibrate(30000);
      }
      song.play();
    }
  };
};

export const stopSirene = () => {
  return dispatch => {
    dispatch({ type: types.STOP_SIRENE });
    Vibration.cancel();
    song.stop();
  };
};

export const changeBuzztype = data => ({
  type: "CHANGE_BUZZTYPE",
  data
});
