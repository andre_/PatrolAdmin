import {
  INIT_CHECKPOINT,
  ADD_CHECKPOINT,
  DELETE_CHECKPOINT,
  SHARING_ID,
  SEARCH_CHECKPOINT
} from "@src/constants/actionTypes";
import { API } from "@src/services/APIService";

export const initCheckpoint = data => ({
  type: INIT_CHECKPOINT,
  data
});

export const addCheckpoint = data => ({
  type: ADD_CHECKPOINT,
  data
});

export const deleteCheckpoint = id => {
  return dispatch => {
    dispatch({ type: DELETE_CHECKPOINT, data: id });

    API()
      .post("delete_checkpoint", { checkpoint_id: id })
      .then(res => {})
      .catch(err => {
        console.log("err delete checkpoint", err);
      });
  };
};

export const sharingId = id => {
  return {
    type: SHARING_ID,
    data: id
  };
};

export const searchCheckpoint = data => {
  return {
    type: SEARCH_CHECKPOINT,
    data
  };
};
