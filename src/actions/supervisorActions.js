import {
  INIT_SUPERVISOR,
  DESTROY_SUPERVISOR,
  SET_SUPERVISOR_LOGIN
} from "@src/constants/actionTypes";

export const initSupervisor = data => {
  return {
    type: INIT_SUPERVISOR,
    data
  };
};

export const destroySupervisor = () => {
  return {
    type: DESTROY_SUPERVISOR
  };
};

export const setSupervisorLogin = () => {
  return {
    type: SET_SUPERVISOR_LOGIN
  };
};

export const supervisorImage = data => ({
  type: "SUPERVISOR_IMAGE",
  data
});
