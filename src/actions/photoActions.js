import { SHARING_PHOTO } from "@src/constants/actionTypes";

export const sharingPhoto = data => {
  return {
    type: SHARING_PHOTO,
    data
  };
};
