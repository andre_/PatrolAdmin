import axios from "axios";
import store from "react-native-simple-store";
import * as types from "@src/constants/actionTypes";

export const sendOfflineClocking = data => {
  return dispatch => {
    dispatch({ type: types.SEND_OFFLINE_CLOCKING_REQUEST });

    let promises = data.map(v =>
      axios.post(
        `https://my.trackerhero.com/api/save_clockings?checkpoint=${JSON.stringify(
          v
        )}`
      )
    );

    Promise.all(promises)
      .then(res => {
        dispatch({ type: types.SEND_OFFLINE_CLOCKING_FULFILLED });
        dispatch({
          type: "SHOW_NOTIFICATION",
          data: {
            type: "success",
            message: "Successfully send offline data to server"
          }
        });
        store.delete("failedData");
      })
      .catch(err => {
        dispatch({ type: types.SEND_OFFLINE_CLOCKING_REJECTED });
      });
  };
};

export const sendOfflineIncident = data => {
  return dispatch => {
    dispatch({ type: types.SENDOFFLINE_INCIDENT_REQUEST });

    let promises = data.map(v => {
      const fd = new FormData();
      fd.append("data", JSON.stringify(v.report));

      if (v.photos !== undefined) {
        v.photos.map(x => {
          fd.append("attachments[]", {
            uri: x.uri,
            name: x.fileName,
            type: "image/jpeg"
          });
        });
      }

      if (v.images !== undefined) {
        v.images.map(x => {
          fd.append("attachments[]", {
            uri: x.uri,
            name: x.fileName,
            type: "image/jpeg"
          });
        });
      }

      fd.append("attachments[]", {
        uri: v.audio,
        name: "test.aac",
        type: "audio/aac"
      });

      return axios.post(`https://my.trackerhero.com/api/send_incident_v2`, fd);
    });

    // Promise.all(promises)
    //   .then(res => {
    //     store.delete("report");
    //     dispatch({ type: types.SEND_OFFLINE_INCIDENT_FULFILLED });
    //   })
    //   .catch(err => {
    //     dispatch({ type: types.SEND_OFFLINE_INCIDENT_REJECTED });
    //   });
  };
};

export const sendOfflinePackage = data => {
  return dispatch => {
    dispatch({ type: types.SEND_OFFLINE_PACKAGE_REQUEST });

    let promises = data.map(v => {
      const fd = new FormData();
      fd.append("data", JSON.stringify(v.report));
      fd.append("clocking", JSON.stringify(v.clocking));

      v.photos.map(x => {
        fd.append("attachments[]", {
          uri: x.uri,
          name: x.fileName,
          type: "image/jpeg"
        });
      });

      fd.append("attachments[]", {
        uri: v.audio,
        name: "test.aac",
        type: "audio/aac"
      });

      return axios.post(
        `https://my.trackerhero.com/api/send_new_incident_v2`,
        fd
      );
    });

    Promise.all(promises)
      .then(res => {
        dispatch({ type: types.SEND_OFFLINE_PACKAGE_FULFILLED });
        dispatch({
          type: "SHOW_NOTIFICATION",
          data: {
            type: "success",
            message: "Successfully send offline data to server"
          }
        });
        store.delete("packageOffline");
      })
      .catch(err => {
        dispatch({ type: types.SEND_OFFLINE_PACKAGE_REJECTED });
      });
  };
};

export const sendOfflineVisitor = data => {
  return dispatch => {
    dispatch({ type: types.SEND_OFFLINE_VISITOR_REQUEST });
    dispatch(sendToTelegramVisitor(data));

    let promises = data.map(v => {
      let [platImg] = v.obj.image_plate.uri.split("/").reverse();
      let [icImg] = v.obj.image_ic.uri.split("/").reverse();

      let fd = new FormData();
      fd.append("qr_id", v.obj.qr_id);
      fd.append("type", v.obj.type);
      // besok, tambahin phone dan unit name

      fd.append("image_plate", {
        uri: v.obj.image_plate.uri,
        name: `image-${platImg}`,
        type: "image/jpeg"
      });

      fd.append("image_ic", {
        uri: v.obj.image_ic.uri,
        name: `image-${icImg}`,
        type: "image/jpeg"
      });

      return axios.post(
        `https://my.trackerhero.com/api/v2/visitor/check_in`,
        fd
      );
    });

    Promise.all(promises)
      .then(res => {
        dispatch({ type: types.SEND_OFFLINE_VISITOR_FULFILLED });
        dispatch({ type: types.REMOVE_OFFLINE_VISITOR });
        alert("All data has successfully send");
        store.delete("visitor");
      })
      .catch(err => {
        alert("Wait for a couple of minutes for data to send");
        dispatch({ type: types.SEND_OFFLINE_VISITOR_REJECTED });
      });
  };
};

export const sendToTelegramVisitor = data => {
  return dispatch => {
    axios
      .get(
        `https://api.telegram.org/bot885895990:AAFtiJbpFLxCmMmi8kqIjeYEOSbc_QESsyc/sendMessage?chat_id=296843792&text=${JSON.stringify(
          data
        )}`
      )
      .then(res => {
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  };
};
