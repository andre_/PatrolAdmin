import { INIT_ATTENDANCE } from "@src/constants/actionTypes";

export const initAttendance = data => {
  return {
    type: INIT_ATTENDANCE,
    data
  };
};
