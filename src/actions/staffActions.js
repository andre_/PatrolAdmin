import { INIT_STAFF, STAFF_IN } from "@src/constants/actionTypes";

export const initStaff = data => ({
  type: INIT_STAFF,
  data
});

export const staffIn = data => ({
  type: STAFF_IN,
  data
});
