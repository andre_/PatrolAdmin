import * as types from "@src/constants/actionTypes";

export const setMessage = data => ({
  type: types.SET_MESSAGE,
  data
});

export const showNotification = data => ({
  type: "SHOW_NOTIFICATION",
  data
});

export const hideNotification = () => ({
  type: "HIDE_NOTIFICATION"
});

export const setRef = data => ({
  type: types.SET_REF,
  data
});

export const playSirene = () => {
  return dispatch => {
    dispatch({ type: types.PLAY_SIRENE });
  };
};
