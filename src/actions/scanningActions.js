import { CHANGE_METHOD_SCANNING } from "@src/constants/actionTypes";

export const changeMethodScanning = data => ({
  type: CHANGE_METHOD_SCANNING,
  data
});

export const setToNfc = () => ({
  type: "SET_TO_NFC"
});

export const setToQr = () => ({
  type: "SET_TO_QR"
});

export const setVisitorManual = flag => ({
  type: "SET_VISITOR_MANUAL",
  data: flag
});
