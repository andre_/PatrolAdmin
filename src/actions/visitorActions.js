import { INIT_VISITOR } from "@src/constants/actionTypes";

export const initVisitor = data => ({
  type: INIT_VISITOR,
  data
});

export const removeVisitor = data => ({
  type: "REMOVE_VISITOR",
  data
});
