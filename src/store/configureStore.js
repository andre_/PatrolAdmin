import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import storage from "redux-persist/es/storage";

import rootReducers from "@src/reducers/";

/* config reducers */
const config = {
  key: "primary",
  storage,
  blacklist: [
    "distance",
    "attendance",
    "loading",
    "supervisor",
    "visitor",
    "message",
    "sop"
  ]
};

const middleware = [thunk];

let persistedReducer = persistReducer(config, rootReducers);

export default () => {
  let store = createStore(
    persistedReducer,
    composeWithDevTools(compose(applyMiddleware(...middleware)))
  );
  let persistor = persistStore(store, { timeout: 1000 });

  return {
    store,
    persistor
  };
};
