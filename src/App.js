import React, { Component, Fragment } from "react";
import { NetInfo } from "react-native";
import { connect } from "react-redux";
import { NavigationAuth } from "@src/navigations/NavigationAuth";
import { NavigationGuest } from "@src/navigations/NavigationGuest";
import { getNetworkStatus } from "@src/actions/connectionActions";
import { isLate, setIntervalCount } from "@src/actions/accountActions";
import { playSirene } from "@src/actions/notificationActions";
import {
  sendOfflineIncident,
  sendOfflineClocking,
  sendOfflinePackage,
  sendOfflineVisitor
} from "@src/actions/offlineActions";
import { initPlayer } from "@src/actions/authActions";
import BackgroundTimer from "react-native-background-timer";
import OneSignal from "react-native-onesignal";
import store from "react-native-simple-store";
import NavigationService from "./commons/navigationService";

const mapStateToProps = state => ({
  auth: state.auth,
  account: state.account,
  posts: state.posts,
  clocking: state.clocking,
  connection: state.connection
});

const mapDispatchToProps = dispatch => ({
  getNetworkStatus(data) {
    dispatch(getNetworkStatus(data));
  },

  isLate(data) {
    dispatch(isLate(data));
  },

  setIntervalCount(data) {
    dispatch(setIntervalCount(data));
  },

  initPlayer(data) {
    dispatch(initPlayer(data));
  },

  doSendOfflineIncident(data) {
    dispatch(sendOfflineIncident(data));
  },

  doSendOfflineClocking(data) {
    dispatch(sendOfflineClocking(data));
  },

  doSendOfflinePackage(data) {
    dispatch(sendOfflinePackage(data));
  },

  doSendOfflineVisitor(data) {
    dispatch(sendOfflineVisitor(data));
  },

  doPlaySirene() {
    dispatch(playSirene());
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clockLength: 0,
      offlineLength: 0,
      showLoading: false
    };

    OneSignal.init("d18758ac-16c0-4852-a997-a0f0b187cf42");
    OneSignal.addEventListener("ids", this.updatePushId.bind(this));
    OneSignal.addEventListener("received", this.onReceived.bind(this));
    OneSignal.inFocusDisplaying(0);
    OneSignal.configure();

    this._checkConnection = this._checkConnection.bind(this);
  }

  componentDidMount() {
    NetInfo.getConnectionInfo().then(connectionInfo => {
      this.props.getNetworkStatus(
        connectionInfo.type === "none" ? false : true
      );
    });

    NetInfo.addEventListener("connectionChange", this._checkConnection);
    this._checkStorage();
  }

  updatePushId(device) {
    this.props.initPlayer(device.userId);
  }

  onReceived(notification) {
    if (notification.payload.additionalData.type === "warning") {
      this.props.doPlaySirene();
      this.props.isLate(true);
    }

    if (notification.payload.additionalData.type === "info") {
      fetch(notification.payload.additionalData.absence_url)
        .then(result => result.json())
        .then(resp => console.log(resp))
        .catch(err => {
          console.log(err);
        });
      // this.props.doPlaySirene();
      // this.props.isLate(true);
    }
  }

  componentWillUnmount() {
    BackgroundTimer.clearInterval(this.checkingStorage);
  }

  _checkStorage = () => {
    this.total = 0;
    this.checkingStorage = BackgroundTimer.setInterval(() => {
      if (this.total === 120) {
        this._finishingAllStorage();
        this.total = 0;
      }
      this.total = this.total + 1;
    }, 1000);
  };

  _finishingAllStorage() {
    store.get("packageOffline").then(packageOffline => {
      if (packageOffline !== null) {
        this.props.doSendOfflinePackage(packageOffline);
      }
    });

    store
      .get("failedData")
      .then(failedData => {
        if (failedData !== null) {
          this.props.doSendOfflineClocking(failedData);
        }
      })
      .catch(err => {
        console.log(err);
      });

    // store
    //   .get("visitor")
    //   .then(visitor => {
    //     if (visitor !== null) {
    //       this.props.doSendOfflineVisitor(visitor);
    //     }
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });

    // store
    //   .get("report")
    //   .then(report => {
    //     if (report !== null) {
    //       this.props.doSendOfflineIncident(report);
    //     }
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });
  }

  _checkConnection(connectionInfo) {
    let status = connectionInfo.type === "none" ? false : true;
    this.props.getNetworkStatus(connectionInfo.type === "none" ? false : true);

    if (status) {
      this._finishingAllStorage();
    }
  }

  render() {
    const {
      auth: { isLogin }
    } = this.props;

    return (
      <Fragment>
        {isLogin ? (
          <NavigationAuth
            ref={navigatorRef =>
              NavigationService.setTopLevelNavigator(navigatorRef)
            }
          />
        ) : (
          <NavigationGuest />
        )}
      </Fragment>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
