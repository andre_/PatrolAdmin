import { NavigationActions } from "react-navigation";
import { NavigationAuth } from "@src/navigations/NavigationAuth";

const nav = (state, action) => {
  let newState;

  switch (action.type) {
    case "Warning": {
      newState = NavigationAuth.router.getStateForAction(
        NavigationActions.navigate({ routeName: "Warning" })
      );
    }

    default: {
      newState = NavigationAuth.router.getStateForAction(action, state);
    }
  }
  return newState || state;
};

export default nav;
