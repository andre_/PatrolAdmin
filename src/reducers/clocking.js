import * as types from "@src/scenes/clocking/constants";

const initialState = {
  clocking: [],
  clockingID: "",
  dummy: [],
  timeclock: [],
  failed: [],
  shareClock: "",
  first: null
};

const clocking = (state = initialState, action) => {
  switch (action.type) {
    case types.INIT_CLOCKING: {
      let newData;
      if (state.clocking.length) {
        let filterData = (state.clocking || []).filter(
          v => v.id === action.data.id || false
        );

        if (filterData.length) {
          newData = [...state.clocking];
        } else {
          newData = [...state.clocking, action.data];
        }
      } else {
        newData = [...state.clocking, action.data];
      }

      return {
        ...state,
        clocking: newData
      };
    }

    case types.ADD_DUMMY_CHECKPOINT: {
      let isNotDuplicate =
        state.dummy.find(v => v.id === action.data.id) === undefined;

      if (isNotDuplicate) {
        let newData = [...state.dummy, action.data];
        return {
          ...state,
          dummy: newData
        };
      }
    }

    case types.SHARE_CLOCKING: {
      return {
        ...state,
        shareClock: action.data
      };
    }

    case types.END_CLOCKING: {
      return {
        ...state,
        clocking: [],
        dummy: [],
        failed: [],
        first: null,
        timeclock: []
      };
    }

    case types.CLEAR_CACHE: {
      return {
        ...state,
        dummy: []
      };
    }

    default: {
      return state;
    }
  }
};

export default clocking;
