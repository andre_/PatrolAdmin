const SET_MANUAL_VISITOR = "SET_MANUAL_VISITOR";
const SET_ATTENDANCE_FACE = "SET_ATTENDANCE_FACE";
const SET_ATTENDANCE_TYPE = "SET_ATTENDANCE_TYPE";
const SET_ATTENDANCE_CAMERA = "SET_ATTENDANCE_CAMERA";
const SET_IMAGE_PICKER = "SET_IMAGE_PICKER";
const SET_THRESHOLD = "SET_THRESHOLD";
const SET_SEND_BACKGROUND_CLOCKING = "SET_SEND_BACKGROUND_CLOCKING";

const initialState = {
  usingManual: true,
  usingFace: false,
  usingImagePicker: false,
  attendanceType: "manual",
  threshold: 50,
  attendanceCameraVersion: "v1",
  sendBackground: true
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_MANUAL_VISITOR: {
      return {
        ...state,
        usingManual: action.data
      };
    }

    case SET_ATTENDANCE_FACE: {
      return {
        ...state,
        usingFace: action.data
      };
    }

    case SET_ATTENDANCE_CAMERA: {
      return {
        ...state,
        attendanceCameraVersion: action.data
      };
    }

    case SET_SEND_BACKGROUND_CLOCKING: {
      return {
        ...state,
        sendBackground: action.data
      };
    }

    case SET_IMAGE_PICKER: {
      return {
        ...state,
        usingImagePicker: action.data
      };
    }

    case SET_ATTENDANCE_TYPE: {
      return {
        ...state,
        attendanceType: action.data
      };
    }

    case SET_THRESHOLD: {
      return {
        ...state,
        threshold: action.data
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
