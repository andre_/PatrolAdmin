import {
  INIT_VISITOR,
  ADD_VISITOR,
  REMOVE_VISITOR,
  OFFLINE_VISITOR,
  REMOVE_OFFLINE_VISITOR
} from "@src/constants/actionTypes";

const initialState = {
  visitor: [],
  offlineVisitor: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_VISITOR: {
      return {
        ...state,
        visitor: (action.data || []).length
      };
    }

    case ADD_VISITOR: {
      return {
        ...state,
        visitor: state.visitor + 1
      };
    }

    case REMOVE_VISITOR: {
      return {
        ...state,
        visitor: state.visitor ? state.visitor - 1 : 0
      };
    }

    case OFFLINE_VISITOR: {
      return {
        ...state,
        offlineVisitor: action.data
      };
    }

    case REMOVE_OFFLINE_VISITOR: {
      return {
        ...state,
        offlineVisitor: []
      };
    }

    default: {
      return state;
    }
  }
};

export default reducer;
