import { INIT_STAFF, STAFF_IN } from "@src/constants/actionTypes";

const initialState = {
  staff: [],
  staffIn: []
};

const reducers = (state = initialState, actions) => {
  switch (actions.type) {
    case INIT_STAFF: {
      return Object.assign({}, state, {
        staff: actions.data
      });
    }

    case STAFF_IN: {
      return {
        ...state,
        staffIn: [...state.staffIn, action.data]
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
