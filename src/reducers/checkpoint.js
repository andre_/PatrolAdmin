import {
  INIT_CHECKPOINT,
  ADD_CHECKPOINT,
  DELETE_CHECKPOINT,
  SEARCH_CHECKPOINT,
  SHARING_ID
} from "@src/constants/actionTypes";

const initialState = {
  checkpoint: [],
  selected: [],
  sharingId: ""
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case INIT_CHECKPOINT: {
      return Object.assign({}, state, {
        checkpoint: action.data.list,
        selected: action.data.list
      });
    }

    case ADD_CHECKPOINT: {
      return {
        ...state,
        checkpoint: [...state.checkpoint, action.data],
        selected: [...state.selected, action.data]
      };
    }

    case DELETE_CHECKPOINT: {
      let obj = state.checkpoint.filter(v => v.id !== action.data);

      return Object.assign({}, state, {
        checkpoint: obj,
        selected: obj
      });
    }

    case SHARING_ID: {
      return Object.assign({}, state, {
        sharingId: action.data
      });
    }

    case SEARCH_CHECKPOINT: {
      let copyCheckpoint = state.checkpoint;

      copyCheckpoint = copyCheckpoint.filter(v => {
        return (
          v.description.toLowerCase().search(action.data.toLowerCase()) !== -1
        );
      });

      console.log("staff", copyCheckpoint);

      return Object.assign({}, state, {
        selected: copyCheckpoint
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
