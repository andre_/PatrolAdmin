import { SHARING_PHOTO } from "@src/constants/actionTypes";

const initialState = {
  photo: false
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case SHARING_PHOTO: {
      return Object.assign({}, state, {
        photo: action.data
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
