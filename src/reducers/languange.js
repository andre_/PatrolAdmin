import {
  CHANGE_LANGUAGE,
  SET_TO_EN,
  SET_TO_TH,
  SET_TO_MY
} from "@src/constants/actionTypes";

const initialState = {
  lang: "en"
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_TO_EN: {
      return Object.assign({}, state, {
        lang: "en"
      });
    }

    case SET_TO_MY: {
      return Object.assign({}, state, {
        lang: "my"
      });
    }

    case SET_TO_TH: {
      return Object.assign({}, state, {
        lang: "th"
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
