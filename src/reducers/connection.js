const initialState = {
  connected: ""
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case "GET_NETWORK_STATUS": {
      return {
        ...state,
        connected: action.data
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
