import {
  SET_TIME_TO_DEFAULT,
  SET_TIME_FROM_SERVER
} from "@src/constants/actionTypes";

const initialState = {
  type: "default"
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_TIME_TO_DEFAULT: {
      return {
        ...state,
        type: "default"
      };
    }

    case SET_TIME_FROM_SERVER: {
      return {
        ...state,
        type: "server"
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
