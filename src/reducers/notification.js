import {} from "@src/constants/actionTypes";

const initialState = {
  buzztype: "normal"
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "CHANGE_BUZZTYPE": {
      return {
        ...state,
        buzztype: action.data
      };
    }

    default: {
      return state;
    }
  }
};

export default reducer;
