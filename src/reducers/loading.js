import { START_LOADING, STOP_LOADING } from "@src/constants/actionTypes";

const initialState = {
  show: false
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case START_LOADING: {
      return Object.assign({}, state, {
        show: true
      });
    }

    case STOP_LOADING: {
      return Object.assign({}, state, {
        show: false
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
