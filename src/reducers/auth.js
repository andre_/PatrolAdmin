import {
  INIT_TOKEN,
  INIT_PLAYER,
  DESTROY_PLAYER,
  DESTROY_TOKEN
} from "@src/constants/actionTypes";

const initialState = {
  isLogin: false,
  token: "",
  player: ""
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case INIT_TOKEN: {
      return Object.assign({}, state, {
        isLogin: true,
        token: action.data
      });
    }

    case INIT_PLAYER: {
      return {
        ...state,
        player: action.data
      };
    }

    case DESTROY_TOKEN: {
      return {
        ...state,
        isLogin: false,
        token: ""
      };
    }

    case DESTROY_PLAYER: {
      return {
        ...state,
        player: ""
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
