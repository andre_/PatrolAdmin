const initialState = {
  posts: [],
  forms: []
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case "INIT_POSTS": {
      return Object.assign({}, state, {
        posts: action.data
      });
    }

    case "INIT_FORMS": {
      return Object.assign({}, state, {
        forms: action.data
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
