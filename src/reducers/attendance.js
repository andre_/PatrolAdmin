import { INIT_ATTENDANCE } from "@src/constants/actionTypes";

const initialState = {
  attendance: []
};

const reducers = (state = initialState, actions) => {
  switch (actions.type) {
    case INIT_ATTENDANCE: {
      return {
        ...state,
        attendance: actions.data
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
