const initialState = {
  far: false
};

const distance = (state = initialState, action) => {
  switch (action.type) {
    case "INIT_DISTANCE": {
      console.log("dari action distance", action.data);
      return Object.assign({}, state, {
        far: action.data > 3000 ? true : false
      });
    }

    default: {
      return state;
    }
  }
};

export default distance;
