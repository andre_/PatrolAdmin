import { CHANGE_METHOD_SCANNING } from "@src/constants/actionTypes";

const initialState = {
  method: "nfc",
  visitorManual: "false"
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_METHOD_SCANNING: {
      return Object.assign({}, state, {
        method: action.data
      });
    }

    case "SET_TO_NFC": {
      return Object.assign({}, state, {
        method: "nfc"
      });
    }

    case "SET_TO_QR": {
      return Object.assign({}, state, {
        method: "qrcode"
      });
    }

    case "SET_VISITOR_MANUAL": {
      return {
        ...state,
        visitorManual: action.data
      };
    }

    default: {
      return state;
    }
  }
};

export default reducer;
