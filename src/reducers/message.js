import * as types from "@src/constants/actionTypes";

const initialState = {
  message: "",
  showNotification: false,
  type: "",
  ref: null
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_MESSAGE: {
      return {
        ...state,
        message: action.data
      };
    }

    case types.SET_REF: {
      return {
        ...state,
        ref: action.data
      };
    }

    case "SHOW_NOTIFICATION": {
      return {
        ...state,
        showNotification: true,
        type: action.data.type,
        message: action.data.message
      };
    }

    case "HIDE_NOTIFICATION": {
      return {
        ...state,
        showNotification: false,
        message: ""
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
