import { INIT_ACCOUNT, DESTROY_ACCOUNT } from "@src/constants/actionTypes";
const initialState = {
  account: [],
  selected: [],
  phone: "",
  interval: 0,
  warning: false,
  isFarAway: false
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case INIT_ACCOUNT: {
      return Object.assign({}, state, {
        account: action.data,
        selected: action.data.staff
      });
    }

    case "INIT_COORDINATE": {
      return Object.assign({}, state, {
        isFarAway: action.data > 3000 ? true : false
      });
    }

    case "IS_LATE": {
      return {
        ...state,
        warning: action.data
      };
    }

    case "SET_INTERVAL_COUNT": {
      return {
        ...state,
        intervalCount: action.data
      };
    }

    case "INIT_PHONE": {
      return Object.assign({}, state, {
        phone: action.data
      });
    }

    case "DESTROY_PHONE": {
      return Object.assign({}, state, {
        phone: ""
      });
    }

    case DESTROY_ACCOUNT: {
      return {
        ...state,
        account: []
      };
    }

    case "SEARCH_STAFF_IN": {
      let copyStaff = state.account.staff;
      copyStaff = copyStaff.filter(v => {
        return v.name.toLowerCase().search(action.data.toLowerCase()) !== -1;
      });

      console.log("staff", copyStaff);

      return Object.assign({}, state, {
        selected: copyStaff
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
