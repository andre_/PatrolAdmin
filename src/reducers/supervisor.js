import {
  INIT_SUPERVISOR,
  SET_SUPERVISOR_LOGIN,
  DESTROY_SUPERVISOR
} from "@src/constants/actionTypes";

const initialState = {
  supervisor: {},
  isSupervisorLogin: false,
  supervisorImage: ""
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case INIT_SUPERVISOR: {
      return Object.assign({}, state, {
        supervisor: action.data
      });
    }

    case SET_SUPERVISOR_LOGIN: {
      return Object.assign({}, state, {
        isSupervisorLogin: true
      });
    }

    case DESTROY_SUPERVISOR: {
      return Object.assign({}, state, {
        supervisor: {},
        isSupervisorLogin: false
      });
    }

    case "SUPERVISOR_IMAGE": {
      return Object.assign({}, state, {
        supervisorImage: action.data
      });
    }

    default: {
      return state;
    }
  }
};

export default reducers;
