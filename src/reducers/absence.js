const initialState = {
  data: []
};

export const ADD_CHECKIN = "ADD_CHECKIN";

export const addCheckin = data => ({
  type: ADD_CHECKIN,
  data
});

export default function reducers(state = initialState, action) {
  switch (action.type) {
    case ADD_CHECKIN: {
      return Object.assign(state, {
        [action.data.userID]: {
          id: action.data.checkinID
        }
      });
    }

    default: {
      return state;
    }
  }
}
