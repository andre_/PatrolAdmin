import { combineReducers } from "redux";
import auth from "./auth";
import absence from "./absence";
import clocking from "./clocking";
import posts from "./posts";
import loading from "./loading";
import account from "./account";
import checkpoint from "./checkpoint";
import connection from "./connection";
import photo from "./photo";
import supervisor from "./supervisor";
import flag from "./flag";
import staff from "./staff";
import attendance from "./attendance";
import message from "./message";
import languange from "./languange";
import timeclock from "./timeclock";
import scanning from "./scanning";
import notification from "./notification";
import visitor from "./visitor";
import distance from "./distance";
import sop from "../scenes/sop/reducers";
import patroling from "../redux/ducks/clockingRedux";

export default combineReducers({
  auth,
  absence,
  account,
  clocking,
  posts,
  loading,
  checkpoint,
  connection,
  photo,
  supervisor,
  message,
  staff,
  attendance,
  scanning,
  notification,
  languange,
  timeclock,
  visitor,
  sop,
  flag,
  distance,
  patroling
});
