import Api from "@src/api";
import store from "react-native-simple-store";

const INIT_LIST_CLOCKING = "INIT_LIST_CLOCKING";
const INIT_SCANNED_CP = "INIT_SCANNED_CP";

const SUBMIT_CLOCKING_REQUEST = "SUBMIT_CLOCKING_REQUEST";
const SUBMIT_CLOCKING_FULFILLED = "SUBMIT_CLOCKING_FULFILLED";
const SUBMIT_CLOCKING_REJECTED = "SUBMIT_CLOCKING_REJECTED";

const CLEAR_CP = "CLEAR_CP";
const CLEAR_STORAGE = "CLEAR_STORAGE";

const initialState = {
  isLoading: false,
  scanned: [],
  display: [],
  copied: []
};

function getUnique(arr, comp) {
  const unique = arr
    .map(e => e[comp])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
}

export const initScannedCp = data => {
  return (dispatch, getState) => {
    navigator.geolocation.getCurrentPosition(
      position => {
        data.gps = `${position.coords.latitude},${position.coords.longitude}`;
      },
      err => {},
      {}
    );

    dispatch({ type: INIT_SCANNED_CP, data });

    if (getState().flag.sendBackground) {
      if (
        getState().patroling.scanned.length ===
        getState().checkpoint.checkpoint.length
      ) {
        dispatch(submitClockingInBackground());
      }
    }
  };
};

export const submitClockingInBackground = () => {
  return (dispatch, getState) => {
    dispatch({ type: SUBMIT_CLOCKING_REQUEST });

    let dataClocking = getState().patroling.scanned;

    Api.saveClocking(dataClocking)
      .then(res => {
        dispatch({ type: SUBMIT_CLOCKING_FULFILLED });
      })
      .catch(err => {
        dispatch({ type: SUBMIT_CLOCKING_REJECTED });
      });
  };
};

export const clear = () => ({
  type: CLEAR_CP
});

export const clearStorage = () => ({
  type: CLEAR_STORAGE
});

export const submitTelegram = uri => {
  return (dispatch, getState) => {
    let fd = new FormData();
    fd.append("image", {
      uri,
      name: `image-telegram-${new Date().toLocaleDateString()}`,
      type: "image/jpeg"
    });
    fd.append("post_id", parseInt(getState().posts.posts.post_id));

    Api.saveTelegram(fd)
      .then(res => {
        console.log("telegram", res);
      })
      .catch(err => {
        console.log("err telegram", err);
      });
  };
};

export const submitClocking = (data, copiedData) => {
  return dispatch => {
    dispatch({ type: SUBMIT_CLOCKING_REQUEST });

    Api.saveClocking(data)
      .then(res => {
        dispatch({ type: SUBMIT_CLOCKING_FULFILLED });
      })
      .catch(err => {
        dispatch({ type: SUBMIT_CLOCKING_REJECTED });

        store
          .push("failedData", copiedData)
          .then(() => store.get("failedData"))
          .then(failedData => {
            dispatch({ type: CLEAR_STORAGE });
          })
          .catch(err => {
            alert("fail to save storage. Please contact to customer service");
          });
      });
  };
};

export default function reducers(state = initialState, action) {
  switch (action.type) {
    case INIT_SCANNED_CP: {
      let scannedData = [...state.scanned, action.data];
      let displayData = [...state.display, action.data];
      let copiedData = [...state.copied, action.data];

      return {
        ...state,
        scanned: getUnique(scannedData, "id"),
        display: getUnique(displayData, "id"),
        copied: getUnique(copiedData, "id")
      };
    }

    case SUBMIT_CLOCKING_REQUEST: {
      return {
        ...state,
        isLoading: true
      };
    }

    case SUBMIT_CLOCKING_FULFILLED: {
      return {
        ...state,
        isLoading: false,
        scanned: []
      };
    }

    case SUBMIT_CLOCKING_REJECTED: {
      return {
        ...state,
        isLoading: false
      };
    }

    case "CLEAR_CP": {
      return {
        ...state,
        scanned: [],
        display: []
      };
    }

    case "CLEAR_STORAGE": {
      return {
        ...state,
        copied: []
      };
    }

    default: {
      return state;
    }
  }
}
