import React, { Component } from "react";
import { View } from "react-native";
import styled from "styled-components/native";

const Column = styled.View`
  width: ${props => props.width};
  flex-direction: ${props => (props.isColumn ? "column" : "row")};
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  align-self: ${props => props.alignSelf};
`;

const Spacing = styled.View`
  margin-top: ${props => props.marginTop};
  margin-bottom: ${props => props.marginBottom};
  margin-left: ${props => props.marginLeft};
  margin-right: ${props => props.marginRight};
`;

Column.defaultProps = {
  width: "100%",
  justifyContent: "flex-start",
  alignItems: "flex-start",
  alignSelf: "flex-start"
};

Spacing.defaultProps = {
  marginTop: 0,
  marginBottom: 0,
  marginRight: 0,
  marginLeft: 0
};

export { Column, Spacing };
