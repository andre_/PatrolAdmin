import React from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { ScaledSheet } from "react-native-size-matters";
import { DotIndicator } from "react-native-indicators";
import { colors } from "@styles";

const Button = ({
  title,
  onPress,
  type,
  style,
  textStyle,
  icon,
  isLoading
}) => (
  <TouchableOpacity
    onPress={onPress}
    style={[
      styles.button,
      { backgroundColor: type ? colors[type] : colors.light },
      style
    ]}
  >
    {icon ? (
      <View style={styles.iconWrapper}>
        <Image source={icon} style={styles.icon} />
      </View>
    ) : null}

    {isLoading ? (
      <DotIndicator color={colors.info} size={5} />
    ) : (
      <Text
        style={[
          styles.text,
          type !== "background"
            ? { color: colors.light }
            : { color: colors.text },
          textStyle
        ]}
      >
        {title}
      </Text>
    )}
  </TouchableOpacity>
);

const styles = ScaledSheet.create({
  button: {
    width: "160@s",
    height: "50@vs",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8
  },
  text: {
    fontFamily: "OpenSans-Regular",
    color: colors.light,
    fontSize: 14
  },
  btnSend: {
    marginTop: 15,
    marginBottom: 15,
    alignSelf: "center"
  },
  iconWrapper: {
    width: 25,
    height: 25,
    marginRight: 10
  },
  icon: {
    width: "100%",
    height: "100%",
    position: "absolute"
  },
  textLogo: {
    fontSize: 20
  }
});

export default Button;
