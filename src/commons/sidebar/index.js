import React, { Component, Fragment } from "react";
import {
  Alert,
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ToastAndroid
} from "react-native";
import { List, ListItem, Left, Body } from "native-base";
import RNRestart from "react-native-restart";
import DeviceInfo from "react-native-device-info";
import { connect } from "react-redux";
import { endClocking } from "@src/scenes/clocking/actions";
import { sendOfflineVisitor } from "@src/actions/offlineActions";
import { clear, clearStorage } from "@src/scenes/new-clocking/actions";
import { colors } from "@styles";
import store from "react-native-simple-store";
import Modal from "react-native-modal";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const mapStateToProps = state => ({
  account: state.account,
  clocking: state.clocking,
  posts: state.posts
});

const mapDispatchToProps = dispatch => ({
  clearClocking() {
    dispatch(endClocking());
  },

  doSendOfflineVisitor(data) {
    dispatch(sendOfflineVisitor(data));
  },

  clearAction() {
    dispatch(clear());
  },

  clearStorageAction() {
    dispatch(clearStorage());
  }
});

const logo = require("../../assets/trackerhero.jpg");
const icon = {
  refresh: require("../../assets/logo.png"),
  report: require("../../assets/report.png"),
  offline: require("../../assets/icons/analysis.png"),
  history: require("../../assets/icons/bar-chart.png"),
  settings: require("../../assets/cogs.png")
};

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      text: "",
      loop: 0,
      offlineLength: 0
    };
  }

  resetAll = () => {
    Alert.alert(
      "Are you sure",
      "Are you sure to reset the data?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            ToastAndroid.show("Reset Finish");
            this.props.clearClocking();
            this.props.clearAction();
            this.props.clearStorageAction();
            store.delete("visitor");
          }
        }
      ],
      { cancelable: false }
    );
  };

  checkOffline = () => {
    store
      .get("visitor")
      .then(visitor => {
        if (visitor !== null) {
          this.props.doSendOfflineVisitor(visitor);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const {
      account: {
        account: { post }
      }
    } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.header}>
            <View style={styles.imageWrapper}>
              <Image source={logo} style={styles.logo} />
            </View>

            <View style={styles.description}>
              <Text style={styles.text}>{post && post.name}</Text>
            </View>
          </View>

          <View style={styles.body}>
            <List>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image
                    source={icon.refresh}
                    style={{ width: 20, height: 20 }}
                  />
                </Left>
                <Body>
                  <TouchableOpacity onPress={() => RNRestart.Restart()}>
                    <Text>Refresh Account</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image source={icon.report} style={styles.icon} />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("ReportList")}
                  >
                    <Text>Report List</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image source={icon.offline} style={styles.icon} />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("ClockingReport")
                    }
                  >
                    <Text>Offline Report</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem icon style={styles.listItem} noBorder>
                <Left>
                  <Image source={icon.history} style={styles.icon} />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("Summary")}
                  >
                    <Text>History</Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <ListItem
                icon
                style={styles.listItem}
                noBorder
                onPress={() => this.props.navigation.navigate("Settings")}
              >
                <Left>
                  <Image source={icon.settings} style={styles.icon} />
                </Left>
                <Body>
                  <Text>Settings</Text>
                </Body>
              </ListItem>
            </List>
          </View>
        </View>

        <View style={styles.bottomWrapper}>
          <Text style={styles.textVersion}>
            Version Number: {DeviceInfo.getVersion()}
          </Text>
          <Text style={styles.textVersion}>
            Build Number: {DeviceInfo.getBuildNumber()}
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <TouchableOpacity
              onPress={this.resetAll}
              style={{
                marginTop: 8,
                padding: 10,
                backgroundColor: colors.danger
              }}
            >
              <Text
                style={{
                  fontFamily: "OpenSans-Light",
                  fontSize: 14,
                  color: "#fff"
                }}
              >
                Reset Data
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                marginTop: 8,
                padding: 10,
                backgroundColor: colors.info
              }}
              onPress={this.checkOffline}
            >
              <Text
                style={{
                  fontFamily: "OpenSans-Light",
                  fontSize: 14,
                  color: "#fff"
                }}
              >
                Check offline
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  wrapper: {
    marginTop: hp("2%"),
    marginBottom: hp("4%"),
    marginLeft: wp("4%"),
    marginRight: wp("4%")
  },
  header: {
    paddingTop: hp("3%"),
    paddingBottom: hp("2%"),
    minHeight: hp("5%"),
    borderBottomWidth: 1,
    borderBottomColor: "#fafafa"
  },
  imageWrapper: { height: hp("8%") },
  description: {
    paddingTop: hp("2%"),
    paddingBottom: hp("2%"),
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 16,
    fontFamily: "JosefinSans-Regular"
  },
  listItem: {
    paddingLeft: 0,
    marginLeft: 0,
    height: hp("9%")
  },
  logo: {
    height: "100%",
    width: "100%",
    resizeMode: "contain"
  },
  bottomWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 30
  },
  textVersion: {
    fontFamily: "OpenSans-Light",
    fontSize: 14,
    color: "#333",
    marginBottom: 6
  },
  icon: {
    width: 20,
    height: 20
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
