import React from "react";
import { Text } from "react-native";

import { colors } from "@styles";
import styled from "styled-components/native";

const Title = styled.Text`
  font-size: ${props => props.size};
  font-family: ${props =>
    props.isBold ? "OpenSans-Bold" : "OpenSans-Regular"};
  color: ${props => props.color};
`;

const TitleHeader = styled.Text`
  text-align: center;
  width: 100%;
  font-size: 16px;
  font-family: "OpenSans-Bold";
  color: ${colors.light};
`;

const Caption = styled.Text`
  font-family: "OpenSans-Light";
  font-size: ${props => props.size};
  color: ${props => props.color};
`;

Caption.defaultProps = {
  size: 12,
  color: colors.text
};

Title.defaultProps = {
  color: colors.dark,
  size: 14
};

export { Title, TitleHeader, Caption };
