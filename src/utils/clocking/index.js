import { Platform } from "react-native";
import NfcManager, { NdefParser } from "react-native-nfc-manager";

let isWriting = true;
let bytes;

export const startNFC = () => {
  NfcManager.start({
    onSessionClosedIOS: () => {
      console.log("ios session closed");
    }
  })
    .then(result => {
      console.log("start OK", result);
    })
    .catch(error => {
      console.warn("start fail", error);
    });

  if (Platform.OS === "android") {
    NfcManager.getLaunchTagEvent()
      .then(tag => {
        console.log("launch tag", tag);
      })
      .catch(err => {
        console.log(err);
      });
  }
};

export const parseText = tag => {
  if (tag.ndefMessage) {
    return NdefParser.parseText(tag.ndefMessage[0]);
  }
  return null;
};

export const onTagDiscovered = tag => {
  console.log(parseText(tag));
  return parseText(tag);

  // below is action after parse text
  // if (this._findItem(text)) {
  //   this.callTimestamp()
  //     .then(data => {
  //       obj = {
  //         id: parseInt(text),
  //         time: data,
  //         status: "1"
  //       };
  //       this.props.add(obj);
  //     })
  //     .catch(err => {
  //       alert("Error scanning");
  //     });
  // } else {
  //   alert("Checkpoint not recognized");
  // }
};

/* return Promise, if reject return error */
export const startDetection = () => {
  return NfcManager.registerTagEvent(onTagDiscovered);
};

/* return Promise, if reject return error */
export const stopDetection = () => {
  return NfcManager.unregisterTagEvent();
};

/* return Promise, if reject return error */
export const isSupportNFC = () => {
  return NfcManager.isSupported();
};

export const writeNFC = textToWrite => {
  function strToBytes(str) {
    let result = [];
    for (let i = 0; i < str.length; i++) {
      result.push(str.charCodeAt(i));
    }
    return result;
  }

  function buildTextPayload(valueToWrite) {
    const textBytes = strToBytes(valueToWrite);
    const headerBytes = [
      0xd1,
      0x01,
      textBytes.length + 3,
      0x54,
      0x02,
      0x65,
      0x6e
    ];
    return [...headerBytes, ...textBytes];
  }

  // if (isWriting) {
  //   return;
  // }

  bytes = buildTextPayload(textToWrite);

  return NfcManager.requestNdefWrite(bytes);
};
