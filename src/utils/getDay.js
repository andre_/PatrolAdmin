export const generateHumanDate = () => {
  let rootDate = new Date();
  let days = ["Ahad", "Isnin", "Selasa", "Rabu", "Khamis", "Jumat", "Sabtu"];
  let month = [
    "Januari",
    "Februari",
    "Mac",
    "April",
    "Mei",
    "Jun",
    "Julai",
    "Ogos",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];

  return `${days[rootDate.getDay()]}, ${rootDate.getDate()} ${
    month[rootDate.getMonth()]
  } ${rootDate.getFullYear()}`;
};
