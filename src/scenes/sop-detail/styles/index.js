import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#f4f4f4"
  },
  header: {
    height: 200,
    padding: 25,
    backgroundColor: "#290052",
    justifyContent: "center"
  },
  inlineList: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  list: {
    marginBottom: 12
  },
  text: {
    fontFamily: "OpenSans-Regular",
    color: colors.light
  },
  transparentText: {
    opacity: 0.5
  },
  wrapper: {
    padding: 20
  },
  totalChecklist: {
    backgroundColor: colors.light,
    height: 80,
    padding: 15,
    marginBottom: 20
  },
  item: {
    marginBottom: 12,
    borderRadius: 4,
    backgroundColor: colors.light,
    height: 75,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15,
    paddingRight: 15
  },
  itemLeft: {
    justifyContent: "center"
  },
  itemRight: {
    justifyContent: "center"
  },
  rightText: {
    flexDirection: "row"
  },
  footer: {
    width: "100%",
    backgroundColor: colors.light,
    height: 70,
    position: "absolute",
    bottom: 0
  },
  btnFooter: {
    position: "absolute",
    right: 0,
    width: 100,
    height: "100%",
    // backgroundColor: "#290092",
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: "#290092",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4
  },
  modal: {
    width: 300,
    height: 360,
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 4,
    backgroundColor: colors.light,
    alignSelf: "center"
  },
  modalBottom: {
    position: "absolute",
    bottom: 15,
    alignSelf: "center"
  },
  btn: {
    width: 250,
    marginBottom: 10,
    borderRadius: 4,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  hollowBtn: {
    height: 30
  },
  modalWrapper: {
    width: 250,
    borderRadius: 4,
    borderColor: colors.text,
    alignSelf: "center",
    marginTop: 20
  },
  uploaderWrap: {
    marginTop: 15,
    width: "100%",
    flexDirection: "row"
  },
  btnUploader: {
    width: 50,
    height: 50,
    marginRight: 20,
    backgroundColor: colors.background,
    borderRadius: 300
  },
  imageWrapper: {
    width: 180,
    height: 180,
    alignSelf: "center"
  },
  images: {
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  },
  actionBtn: {
    width: 40,
    height: 40,
    borderRadius: 40,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 10
  }
};

export default styles;
