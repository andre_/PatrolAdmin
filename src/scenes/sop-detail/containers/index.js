import { connect } from "react-redux";
import SopDetailComponent from "../components";

const mapStateToProps = state => ({
  sop: state.sop,
  posts: state.posts
});

export default connect(mapStateToProps)(SopDetailComponent);
