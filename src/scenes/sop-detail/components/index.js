import React, { Component, Fragment } from "react";
import {
  Animated,
  Image,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";
import Modal from "react-native-modal";
import ImagePicker from "react-native-image-picker";
import { colors } from "@styles";
import styles from "../styles";
import { Bar } from "react-native-progress";
import { API } from "@src/services/APIService";
import { generateHumanDate } from "@src/utils/getDay";
import { generate } from "rxjs/observable/generate";

class SopDetailComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      title: null,
      type: null,
      temp: null,
      tempKey: null,
      forms: [],
      images: [],
      description: "",
      percentage: 0
    };
  }

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: colors.light
  };

  componentDidMount() {
    this.getDetailForm();
  }

  getDetailForm = () => {
    const { sop } = this.props;

    API()
      .get(`inspection/rooms?type_id=${sop.unit.type.id}`)
      .then(res => {
        this.setState({
          forms: res.data.data
        });
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  takePicture = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({
          images: [...this.state.images, response]
        });
      }
    });
  };

  markSuccess = (v, k) => {
    this.setState({
      visible: true,
      title: "Do you finish checking?",
      type: "success",
      temp: v,
      tempKey: k
    });
  };

  markFailed = v => {
    this.setState({
      visible: true,
      type: "failed",
      title: "Please write a description report",
      temp: v
    });
  };

  closeModal = () => {
    this.setState({ visible: false });
  };

  updateItem = () => {
    const { sop } = this.props;
    const {
      posts: { posts }
    } = this.props;

    this.setState({ visible: false });
    let obj = {
      post_id: posts.post_id,
      room_id: this.state.temp.id,
      status: true,
      description: "Checked",
      address: sop && sop.unit && sop.unit.address,
      house_number: sop && sop.unit && sop.unit.roomNumber
    };

    const fd = new FormData();
    fd.append("attachments[]", null);
    fd.append("data", JSON.stringify(obj));

    API()
      .post("inspection/report", fd)
      .then(res => {
        this.getDetailForm();
        this.setState({
          images: [],
          description: "",
          temp: ""
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  reportItem = () => {
    const { sop } = this.props;
    const {
      posts: { posts }
    } = this.props;
    this.setState({ visible: false });

    let obj = {
      post_id: posts.post_id,
      room_id: this.state.temp.id,
      status: false,
      description: this.state.description,
      address: sop && sop.unit && sop.unit.address,
      house_number: sop && sop.unit && sop.unit.roomNumber
    };

    const fd = new FormData();
    fd.append("data", JSON.stringify(obj));

    this.state.images.map(v => {
      fd.append("attachments[]", {
        uri: v.uri,
        name: v.fileName,
        type: "image/jpeg"
      });
    });

    API()
      .post("inspection/report", fd)
      .then(res => {
        this.getDetailForm();
        this.setState({ images: [], description: "", temp: "" });
      })
      .catch(err => {
        console.log(err);
      });
  };

  finish = () => {
    this.props.navigation.navigate("Home");
  };

  render() {
    const { sop } = this.props;
    const { forms } = this.state;
    let nowDate = new Date().toLocaleDateString();
    let nowTime = new Date().toLocaleTimeString();
    let total = this.state.forms.filter(v => v.has_checked === true).length;
    let percentage = total / forms.length;

    console.log(forms.length, total);
    console.log("percentage", percentage);

    return (
      <Fragment>
        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.header}>
            <View style={styles.inlineList}>
              <View style={styles.list}>
                <Text style={styles.text}>No. Rumah</Text>
                <Text style={[styles.text, styles.transparentText]}>
                  {sop && sop.unit && sop.unit.roomNumber}
                </Text>
              </View>

              <View style={[styles.list, { width: 120 }]}>
                <Text style={styles.text}>Tarikh</Text>
                <Text style={[styles.text, styles.transparentText]}>
                  {generateHumanDate()}
                </Text>
              </View>
            </View>

            <View style={styles.inlineList}>
              <View style={styles.list}>
                <Text style={styles.text}>Alamat</Text>
                <Text style={[styles.text, styles.transparentText]}>
                  {sop && sop.unit && sop.unit.address}
                </Text>
              </View>

              <View style={[styles.list, { width: 120 }]}>
                <Text style={styles.text}>Type</Text>
                <Text style={[styles.text, styles.transparentText]}>
                  {sop && sop.unit && sop.unit.type.type}
                </Text>
              </View>
            </View>
          </View>

          <View style={[styles.wrapper, { marginBottom: 80 }]}>
            <View style={styles.totalChecklist}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Text
                  style={[
                    styles.text,
                    {
                      color: colors.text,
                      marginBottom: 15,
                      justifyContent: "center"
                    }
                  ]}
                >
                  Total Checklist
                </Text>

                <Text
                  style={[
                    styles.text,
                    { color: colors.text, justifyContent: "center" }
                  ]}
                >
                  {total || 0} / {this.state.forms.length}
                </Text>
              </View>

              {isNaN(percentage) ? null : (
                <Bar progress={percentage} width={280} />
              )}
            </View>

            {this.state.forms.map((v, k) => {
              return v.has_checked ? (
                <View
                  key={k}
                  style={[styles.item, { backgroundColor: "#dadada" }]}
                >
                  <View style={styles.itemLeft}>
                    <View style={styles.leftText}>
                      <Text style={[styles.text, { color: colors.text }]}>
                        {v.name}
                      </Text>
                      <Text
                        style={[
                          styles.text,
                          { color: colors.text, marginTop: 5, opacity: 0.5 }
                        ]}
                      >
                        Description
                      </Text>
                    </View>
                  </View>
                </View>
              ) : (
                <View key={k} style={[styles.item]}>
                  <View style={styles.itemLeft}>
                    <View style={styles.leftText}>
                      <Text style={[styles.text, { color: colors.text }]}>
                        {v.name}
                      </Text>
                      <Text
                        style={[
                          styles.text,
                          { color: colors.text, marginTop: 5, opacity: 0.5 }
                        ]}
                      >
                        Description
                      </Text>
                    </View>
                  </View>

                  <View style={styles.itemRight}>
                    <View style={styles.rightText}>
                      <TouchableOpacity
                        style={[styles.actionBtn]}
                        onPress={() => this.markSuccess(v)}
                      >
                        <Image
                          source={require("../../../assets/success.png")}
                          style={{ width: "100%", height: "100%" }}
                        />
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={[styles.actionBtn]}
                        onPress={() => this.markFailed(v)}
                      >
                        <Image
                          source={require("../../../assets/failed.png")}
                          style={{ width: "100%", height: "100%" }}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>

        <View style={styles.footer}>
          <TouchableOpacity
            style={[
              styles.btnFooter,
              {
                backgroundColor:
                  total === forms.length ? colors.success : "#dadada"
              }
            ]}
            onPress={total === forms.length && this.finish}
          >
            <Text style={[styles.text, { fontFamily: "OpenSans-SemiBold" }]}>
              FINISH
            </Text>
          </TouchableOpacity>
        </View>

        <Modal
          isVisible={this.state.visible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
        >
          <View style={styles.modal}>
            <Text
              style={[
                styles.text,
                { color: colors.text, alignSelf: "center", fontSize: 16 }
              ]}
            >
              {this.state.title}
            </Text>

            <View style={styles.modalWrapper}>
              {this.state.type === "failed" ? (
                <Fragment>
                  <View>
                    <Text style={styles.label}>Description</Text>
                    <TextInput
                      placeholder="Write description"
                      onChangeText={description =>
                        this.setState({ description })
                      }
                    />
                  </View>
                  <View>
                    <Text style={styles.label}>Picture</Text>
                    <ScrollView contentContainerStyle={styles.uploaderWrap}>
                      <TouchableOpacity
                        style={styles.btnUploader}
                        onPress={this.takePicture}
                      />
                      {this.state.images.length
                        ? this.state.images.map((x, key) => (
                            <TouchableOpacity
                              key={key}
                              style={[
                                styles.btnUploader,
                                {
                                  alignItems: "center",
                                  justifyContent: "center"
                                }
                              ]}
                            >
                              <Image
                                source={{ uri: x.uri }}
                                style={{ width: 50, height: 50 }}
                              />
                            </TouchableOpacity>
                          ))
                        : null}
                    </ScrollView>
                  </View>
                </Fragment>
              ) : (
                <View style={styles.imageWrapper}>
                  <Image
                    style={styles.images}
                    source={require("../../../assets/icons/question.png")}
                  />
                </View>
              )}
            </View>

            <View style={styles.modalBottom}>
              <TouchableOpacity
                style={[
                  styles.btn,
                  this.state.type === "failed"
                    ? {
                        backgroundColor: colors.danger
                      }
                    : { backgroundColor: "#290092" }
                ]}
                onPress={
                  this.state.type == "success"
                    ? this.updateItem
                    : this.reportItem
                }
              >
                <Text
                  style={[
                    styles.text,
                    {
                      color: colors.light,
                      fontFamily: "OpenSans-SemiBold",
                      fontSize: 16
                    }
                  ]}
                >
                  {this.state.type === "success" ? "OK" : "Send Report"}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.hollowBtn}
                onPress={this.closeModal}
              >
                <Text
                  style={[
                    styles.text,
                    { color: colors.text, fontSize: 16, alignSelf: "center" }
                  ]}
                >
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

export default SopDetailComponent;
