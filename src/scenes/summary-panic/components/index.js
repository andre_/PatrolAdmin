import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  BackHandler
} from "react-native";
import { API } from "@src/services/APIService";
import { configs, colors } from "@styles";
import moment from "moment";
import styles from "../styles";

class SummaryPanicComponent extends Component {
  static navigationOptions = {
    headerTitle: <Text style={configs.titleHeader}>Summary</Text>,
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerTintColor: colors.light
  };

  constructor(props) {
    super(props);
    this.state = {
      summary: [],
      loading: true
    };

    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentDidMount() {
    this._getSummary();
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  _getSummary = () => {
    const {
      posts: { posts }
    } = this.props;

    API()
      .get(`panic_summary?post=${parseInt(posts.post_id)}`)
      .then(res => {
        this.setState({ summary: res.data, loading: false });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  };

  getToday = () => {
    let day = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ];
    let month = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];

    let nox = new Date();
    let fullDate = `${day[nox.getDay()]}, ${nox.getDate()} ${
      month[nox.getMonth()]
    } ${nox.getFullYear()}`;

    return fullDate;
  };

  navigateWithLat = v => {
    const [lat, lng] = v.location.split(",");

    console.log("location", v.location);
    console.log("lat", lat);
    console.log("lng", lng);

    this.props.navigation.navigate("MapsViewer", {
      coords: {
        lat: parseFloat(lat),
        lng: parseFloat(lng)
      }
    });
  };

  render() {
    const { summary } = this.state;
    let summaryFind = summary.find(v => v);

    console.log("summaryFind for panics", summaryFind);

    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        {/* <Spinner visible={this.state.loading} /> */}
        <View>
          <Text style={[styles.text, { marginBottom: 15 }]}>
            {this.getToday()}
          </Text>
        </View>

        {summaryFind && summaryFind.panics && summaryFind.panics.length ? (
          summaryFind.panics.map((v, k) => {
            return (
              <View style={styles.grid} key={k}>
                <View style={styles.inline}>
                  <Text style={styles.text}>Status</Text>
                  <Text style={styles.text}>Unread</Text>
                </View>
                <View style={styles.inline}>
                  <Text style={styles.text}>Time</Text>
                  <Text style={styles.text}>
                    {moment(v.time).format("HH:mm:ss")}
                  </Text>
                </View>
                <View style={styles.inline}>
                  <Text style={styles.text}>Reason</Text>
                  <Text style={styles.text}>
                    {v.reason === "" ? "Reason not included" : v.reason}
                  </Text>
                </View>
              </View>
            );
          })
        ) : (
          <View>
            <Text style={styles.text}>Panic history empty for today</Text>
          </View>
        )}
      </ScrollView>
    );
  }
}

export default SummaryPanicComponent;
