import { connect } from "react-redux";
import SummaryPanicComponent from "../components";

const mapStateToProps = state => ({
  posts: state.posts
});

export default connect(mapStateToProps)(SummaryPanicComponent);
