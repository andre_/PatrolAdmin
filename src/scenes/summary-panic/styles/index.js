import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.background
  },
  grid: {
    borderRadius: 4,
    backgroundColor: colors.light,
    padding: 15,
    marginBottom: 15
  },
  text: {
    fontFamily: "OpenSans-Regular"
  },
  inline: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 15,
    borderBottomWidth: 1,
    paddingBottom: 15,
    borderBottomColor: "#f8f8f8"
  }
};

export default styles;
