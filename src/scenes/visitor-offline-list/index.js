import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { List, ListItem } from "native-base";
import { colors, configs } from "@styles";
import store from "react-native-simple-store";

class VisitorOfflineList extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: colors.info
    },
    headerTintColor: colors.light,
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>IN QUEUE</Text>
    ),
    headerRight: <View />
  });

  state = {
    queue: []
  };

  componentDidMount() {
    store
      .get("visitor")
      .then(visitor => {
        this.setState({
          queue: visitor
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  render() {
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <TouchableOpacity style={styles.btnList}>
          <View style={styles.leftWrapper}>
            <Text style={[styles.p, { fontFamily: "OpenSans-SemiBold" }]}>
              Pass Number: Null
            </Text>
            <Text style={[styles.p, { fontSize: 12 }]}>Checkin: Yes</Text>
          </View>

          <TouchableOpacity style={styles.btnCheckout}>
            <Text style={[styles.text, { fontSize: 12 }]}>CHECKOUT</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  btnList: {
    height: 80,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#dadada"
  },

  btnCheckout: {
    borderRadius: 4,
    backgroundColor: colors.info,
    width: 100,
    height: 30,
    alignItems: "center",
    justifyContent: "center"
  },

  p: {
    fontFamily: "OpenSans-Regular",
    color: colors.dark
  },

  text: {
    color: colors.light,
    fontFamily: "OpenSans-Regular"
  }
});

export default VisitorOfflineList;
