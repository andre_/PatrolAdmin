import { API } from "@src/services/APIService";
import * as types from "../constants";
import store from "react-native-simple-store";

export const sendReport = (report, images, audio, clocking_id = null) => {
  return dispatch => {
    dispatch({ type: types.SEND_REPORT_REQUEST });

    if (clocking_id !== null) {
      report.clocking_id = clocking_id;
    }

    const fd = new FormData();
    fd.append("data", JSON.stringify(report));

    images.map(v => {
      fd.append("attachments[]", {
        uri: v.uri,
        name: v.fileName,
        type: "image/jpeg"
      });
    });

    fd.append("attachments[]", {
      uri: audio,
      name: "test.aac",
      type: "audio/aac"
    });

    API()
      .post(`send_incident_v2`, fd)
      .then(res => {
        console.log("v2 report", res.data);
        console.log("datanya", fd);
        dispatch({ type: types.SEND_REPORT_FULFILLED });
      })
      .catch(err => {
        dispatch({ type: types.SEND_REPORT_REJECTED });
        console.log("error nya", err);
        store
          .push("report", {
            report,
            photos: images,
            audio
          })
          .then(() => store.get("report"))
          .then(report => {})
          .catch(err => {
            console.log(err);
          });
      });
  };
};
