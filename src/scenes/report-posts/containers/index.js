import { connect } from "react-redux";
import ReportPostsComponent from "../components";
import { destroyClockingId } from "@src/actions/clockingActions";
import { sendReport } from "../actions";

const mapStateToProps = state => ({
  posts: state.posts,
  flag: state.flag,
  account: state.account,
  clocking: state.clocking,
  connection: state.connection
});

const mapDispatchToProps = dispatch => ({
  destroyClockingId() {
    dispatch(destroyClockingId());
  },

  doSendReport(report, images, audio) {
    dispatch(sendReport(report, images, audio));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportPostsComponent);
