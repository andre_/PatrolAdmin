import React, { Component, Fragment } from "react";
import {
  Alert,
  View,
  ScrollView,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  BackHandler,
  Platform,
  Slider,
  PermissionsAndroid
} from "react-native";
import {
  Button,
  Input,
  Form,
  Label,
  Item,
  Icon,
  Radio,
  ListItem,
  Body
} from "native-base";
import { colors, configs } from "@styles";
import { AudioRecorder, AudioUtils } from "react-native-audio";
import { HeaderBackButton } from "react-navigation";
import ImagePicker from "react-native-image-picker";
import Sound from "react-native-sound";
import Modal from "react-native-modal";
import RNFetchBlob from "rn-fetch-blob";

class ReportPostsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coordinate: {
        latitude: 0,
        longitude: 0
      },
      active: false,
      albums: [],
      ImageSource: "",
      level: "low",
      checked: false,
      description: "",
      loading: false,
      loadingModal: false,
      selectedIndex: 0,
      currentTime: 0,
      second: 0,
      hours: 0,
      recording: false,
      paused: false,
      stoppedRecording: false,
      finished: false,
      audioPath: AudioUtils.DocumentDirectoryPath + "/test.mp3",
      hasPermission: undefined,
      song: false,
      playingText: false
    };

    this.toggleCheck = this.toggleCheck.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
    this.playSound = this.playSound.bind(this);
    this.pauseSound = this.pauseSound.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    headerTintColor: colors.light,
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerLeft: (
      <HeaderBackButton
        tintColor={colors.light}
        onPress={() =>
          Alert.alert(
            "Confirmation",
            "Are you sure to go back? Make sure to press submit button to send the report to server",
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              {
                text: "OK",
                style: "default",
                onPress: () => navigation.goBack()
              }
            ]
          )
        }
      />
    )
  });

  prepareRecordingPath(audioPath) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000
    });
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    this._initLocation();

    this._checkPermission().then(hasPermission => {
      this.setState({ hasPermission });

      if (!hasPermission) return;

      this.prepareRecordingPath(this.state.audioPath);

      AudioRecorder.onProgress = data => {
        this.setState({
          currentTime: Math.floor(data.currentTime),
          second: Math.floor(data.currentTime)
        });
      };

      AudioRecorder.onFinished = data => {
        if (Platform.OS === "ios") {
          this._finishRecording(data.status === "OK", data.audioFileURL);
        }
      };
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  _checkPermission() {
    if (Platform.OS !== "android") {
      return Promise.resolve(true);
    }

    const rationale = {
      title: "Microphone Permission",
      message:
        "AudioExample needs access to your microphone so you can record audio."
    };

    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      rationale
    ).then(result => {
      console.log("Permission result:", result);
      return result === true || result === PermissionsAndroid.RESULTS.GRANTED;
    });
  }

  async _record() {
    if (this.state.recording) {
      console.warn("Already recording!");
      return;
    }

    if (!this.state.hasPermission) {
      console.warn("Can't record, no permission granted!");
      return;
    }

    if (this.state.stoppedRecording) {
      this.prepareRecordingPath(this.state.audioPath);
    }

    this.setState({ active: true, recording: true, paused: false });

    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error);
    }
  }

  async _stop() {
    console.log("stop record");
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");
      return;
    }

    this.setState({ stoppedRecording: true, recording: false, paused: false });

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === "android") {
        this._finishRecording(true, filePath);
      }
      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

  playSound() {
    this.setState({ playingText: true });

    this.woosh = new Sound(this.state.audioPath, "", err => {
      if (err) {
        alert("error play sound");
      } else {
        this.woosh.play(
          success => {
            if (success) {
              this.setState({ playingText: false });
            } else {
              this.setState({ playingText: false });
            }
          },
          err => {
            alert(err);
          }
        );
      }
    });
  }

  pauseSound() {
    if (!this.woosh) return;
    this.woosh.pause();
    this.setState({ playingText: false });
  }

  async _play() {
    if (this.state.recording) {
      await this._stop();
    }

    this.setState({
      playingText: true
    });

    setTimeout(() => {
      var sound = new Sound(this.state.audioPath, "", error => {
        if (error) {
          alert("Fail to load sound");
          console.log("failed to load the sound", error);
        }
      });

      sound.play(success => {
        if (success) {
          this.setState({ playingText: false });
          console.log("successfully finished playing");
        } else {
          this.setState({ playingText: false });
          console.log("playback failed due to audio decoding errors");
        }
      });
    }, 100);
  }

  _finishRecording(didSucceed, filePath) {
    this.setState({ finished: didSucceed, song: filePath });
  }

  _initLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          coordinate: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
        });
      },
      error => {
        console.log("error get location");
      },
      {}
    );
  };

  handleBackButton = () => {
    Alert.alert(
      "Confirmation",
      "Are you sure to go back? Make sure to press submit button to send the report to server",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        {
          text: "OK",
          style: "default",
          onPress: () => this.props.navigation.goBack()
        }
      ]
    );
    return true;
  };

  guid() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  submitReport = () => {
    const {
      posts: { posts },
      doSendReport
    } = this.props;

    let coords =
      `${this.state.coordinate.latitude},${this.state.coordinate.longitude}` ||
      0;
    let obj = {
      gps: coords,
      priority: this.state.level,
      description: this.state.description,
      timestamp: JSON.stringify(Math.floor(Date.now() / 1000)),
      post_id: parseInt(posts.post_id),
      request_id: this.guid(),
      type: "incident"
    };

    console.log("object yang dikirim", obj);

    const path = `file://${this.state.audioPath}`;

    if (this.state.ImageSource) {
      doSendReport(obj, this.state.albums, path);
      this.props.navigation.popToTop();
    } else {
      alert("Plese attach a picture or photos");
    }
  };

  toggleCheck(v) {
    this.setState({
      checked: v
    });

    this.setState({ description: v });
  }

  returnFromCamera = data => {
    this.setState({
      ImageSource: data,
      albums: [...this.state.albums, data]
    });
  };

  browsePhoto = () => {
    const {
      flag: { attendanceCameraVersion }
    } = this.props;

    if (attendanceCameraVersion === "v1") {
      this.props.navigation.navigate("ReportCamera", {
        returnFromCamera: this.returnFromCamera
      });
    } else {
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
          path: "images"
        }
      };

      ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          this.setState({
            ImageSource: null,
            albums: []
          });
        } else {
          this.setState({
            ImageSource: response,
            albums: [...this.state.albums, response]
          });
        }
      });
    }
  };

  _changeLevel(level) {
    this.setState({
      level
    });
  }

  _renderButtonRecord() {
    return (
      <Fragment>
        {this.state.recording ? (
          <TouchableOpacity onPress={() => this._stop()}>
            <Icon
              type="FontAwesome"
              name="stop"
              style={{
                alignSelf: "center",
                position: "relative",
                top: -15,
                color: "red"
              }}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => this._record()}>
            <Icon
              type="FontAwesome"
              name="microphone"
              style={{
                alignSelf: "center",
                position: "relative",
                top: -15,
                color: "#333"
              }}
            />
          </TouchableOpacity>
        )}
      </Fragment>
    );
  }

  _deleteEntireImage = v => {
    const {
      flag: { attendanceCameraVersion }
    } = this.props;
    const { albums } = this.state;

    if (attendanceCameraVersion === "v1") {
      this.setState({
        albums: [
          ...albums.splice(0, albums.indexOf(v)),
          ...albums.splice(albums.indexOf(v) + 1, albums.length)
        ]
      });
    } else {
      RNFetchBlob.fs
        .unlink(v.path)
        .then(() => {
          alert("success to delete");
          const { albums } = this.state;
          this.setState({
            albums: [
              ...albums.splice(0, albums.indexOf(v)),
              ...albums.splice(albums.indexOf(v) + 1, albums.length)
            ]
          });
        })
        .catch(err => {
          alert("fail to delete");
        });
    }
  };

  detectChangeDesc = text => {
    this.setState({
      description: text
    });

    if (this.state.description.length === 1) {
      this.setState({
        checked: false
      });
    }
  };

  render() {
    const { level } = this.state;
    const {
      account: {
        account: { post }
      }
    } = this.props;

    return (
      <View style={styles.container}>
        <Modal isVisible={this.state.loadingModal}>
          <View style={styles.modalWrapper}>
            <ActivityIndicator size={"large"} color={colors.info} />
          </View>
        </Modal>
        <ScrollView style={styles.body}>
          <Form style={{ height: "86%" }}>
            <ListItem>
              <View style={styles.buttonWrapper}>
                <Button
                  style={[
                    styles.buttonDivide,
                    {
                      backgroundColor:
                        level === "low" ? colors.success : colors.background
                    }
                  ]}
                  onPress={() => this._changeLevel("low")}
                >
                  <Text
                    style={{
                      color: level === "low" ? colors.light : colors.text
                    }}
                  >
                    Low
                  </Text>
                </Button>
                <Button
                  style={[
                    styles.buttonDivide,
                    {
                      backgroundColor:
                        level === "medium" ? colors.warning : colors.background
                    }
                  ]}
                  onPress={() => this._changeLevel("medium")}
                >
                  <Text
                    style={{
                      color: level === "medium" ? colors.light : colors.text
                    }}
                  >
                    Medium
                  </Text>
                </Button>
                <Button
                  style={[
                    styles.buttonDivide,
                    {
                      backgroundColor:
                        level === "high" ? colors.danger : colors.background
                    }
                  ]}
                  onPress={() => this._changeLevel("high")}
                >
                  <Text
                    style={{
                      color: level === "high" ? colors.light : colors.text
                    }}
                  >
                    High
                  </Text>
                </Button>
              </View>
            </ListItem>

            <ListItem>
              {post &&
                post.incident_template.map((v, key) => (
                  <Fragment key={key}>
                    <Radio
                      onPress={this.toggleCheck.bind(this, v)}
                      selected={this.state.checked === v}
                    />
                    <Body style={{ marginLeft: 10 }}>
                      <Text style={styles.text}>
                        {v.length > 4 ? `${v.substring(0, 5)}..` : v}{" "}
                      </Text>
                    </Body>
                  </Fragment>
                ))}
            </ListItem>
            <Item
              stackedLabel
              style={{
                height: 90
              }}
            >
              <Label style={styles.text}>Description</Label>
              <Input
                value={this.state.description}
                onChangeText={text => this.detectChangeDesc(text)}
              />
            </Item>

            <Item stackedLabel style={{ height: 80 }}>
              <Label style={styles.text}>Audio</Label>
              <View style={styles.audioRecord}>
                <View style={styles.time}>
                  <Text>
                    {this.state.hours < 10
                      ? `0${this.state.hours}`
                      : this.state.hours}
                    :{" "}
                    {this.state.second < 10
                      ? `0${this.state.second}`
                      : this.state.second}
                  </Text>
                </View>
                <View style={{ paddingRight: 10 }}>
                  {this._renderButtonRecord()}
                </View>
              </View>
            </Item>
            {/* {this.state.song && ( */}
            {/* <Item
                style={{
                  height: 70,
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center"
                }}
              >
                <View style={styles.playlist}>
                  <Text style={styles.text}>{this.state.song}</Text>
                  <TouchableOpacity onPress={this.playSound}>
                    <Text style={[styles.text, { color: colors.info }]}>
                      {this.state.playingText ? <Icon name="pause" /> : "Play"}
                    </Text>
                  </TouchableOpacity>
                </View>
              </Item> */}
            {/* )} */}
            <Item>
              {this.state.song && (
                <View style={styles.playlist}>
                  <Slider
                    step={1}
                    minimumValue={0}
                    maximumValue={100}
                    minimumTrackTintColor="#009688"
                    style={{ width: "90%" }}
                  />
                  {this.state.playingText ? (
                    <TouchableOpacity
                      style={{ marginLeft: 12 }}
                      onPress={this.pauseSound}
                    >
                      <Text style={[styles.text, { color: colors.info }]}>
                        <Icon name="pause" />
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={{ marginLeft: 12 }}
                      onPress={this.playSound}
                    >
                      <Text style={[styles.text, { color: colors.info }]}>
                        Play
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              )}
            </Item>
            <Item
              stackedLabel
              style={{
                height: 200
              }}
            >
              <Label style={styles.text}>Attachment</Label>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                  minWidth: 400
                }}
              >
                <TouchableOpacity
                  style={styles.viewUpload}
                  onPress={this.browsePhoto}
                >
                  <Text
                    style={{
                      color: "#333",
                      textAlign: "center",
                      fontFamily: "OpenSans-Light"
                    }}
                  >
                    Upload Image
                  </Text>
                </TouchableOpacity>

                {this.state.albums &&
                  this.state.albums.map((v, keys) => (
                    <TouchableOpacity key={keys} style={styles.viewUpload}>
                      <TouchableOpacity
                        onPress={() => this._deleteEntireImage(v)}
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                          width: 25,
                          height: 25,
                          borderRadius: 100,
                          backgroundColor: colors.danger,
                          position: "absolute",
                          zIndex: 10000,
                          top: 5,
                          right: 5
                        }}
                      >
                        <Text style={{ color: colors.light }}>X</Text>
                      </TouchableOpacity>
                      <Image
                        source={{ uri: v.uri }}
                        style={{ width: "100%", height: "100%" }}
                      />
                    </TouchableOpacity>
                  ))}
              </ScrollView>
            </Item>
          </Form>
          <View style={styles.bottom}>
            <Button
              style={[styles.button, configs.noButtonShadow]}
              onPress={this.submitReport}
            >
              <Text
                style={[styles.text, { color: colors.light, fontSize: 16 }]}
              >
                Submit
              </Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  segment: {
    backgroundColor: colors.light
  },
  text: {
    fontFamily: "OpenSans-Regular"
  },
  body: {
    flex: 1,
    backgroundColor: colors.light
  },
  viewUpload: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderRadius: 4,
    width: 120,
    height: 120,
    marginRight: 10,
    marginTop: 20,
    marginBottom: 20,
    borderColor: "#dadada",
    position: "relative"
  },
  bottom: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    height: 80,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  button: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    backgroundColor: colors.info
  },
  audioRecord: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20,
    height: 60
  },
  buttonWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  buttonDivide: {
    elevation: 0,
    borderRadius: 4,
    borderWidth: 0,
    backgroundColor: colors.background,
    width: 100,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  playlist: {
    borderWidth: 1,
    borderColor: "#f7f7f7",
    height: 40,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 6,
    paddingRight: 20
  },
  buttonDelete: {
    width: 30,
    height: 30,
    backgroundColor: colors.danger,
    borderRadius: 100,
    position: "absolute",
    top: -50,
    right: 10,
    zIndex: 10000
  },
  modalWrapper: {
    width: 80,
    height: 80,
    alignSelf: "center",
    backgroundColor: colors.light,
    borderRadius: 4,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default ReportPostsComponent;
