import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: colors.light
  },
  form: {
    height: 80
  },
  btnWrapper: {
    backgroundColor: colors.light,
    padding: 15
  },
  btn: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    backgroundColor: colors.info
  },
  text: {
    fontFamily: "OpenSans-SemiBold",
    color: colors.light
  },

  modalWrapper: {
    backgroundColor: colors.light,
    padding: 20,
    width: 280,
    height: 300,
    alignSelf: "center"
  },

  btnList: {
    height: 40,
    borderBottomWidth: 1,
    borderColor: colors.background,
    justifyContent: "center"
  },
  search: {
    fontFamily: "OpenSans-Light",
    marginTop: 5,
    marginBottom: 10
  },
  labelBold: {
    fontFamily: "OpenSans-SemiBold"
  },
  label: {
    fontFamily: "OpenSans-Regular",
    color: colors.dark
  },
  form: {
    flexDirection: "row",
    height: 80
  },
  formLeft: {
    justifyContent: "center",
    width: 40
  },
  formRight: {
    flex: 1
  },
  icon: {
    width: 30,
    height: 30
  }
};

export default styles;
