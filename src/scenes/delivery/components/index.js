import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import { colors, configs } from "@styles";
import Modal from "react-native-modal";
import styles from "../styles";

const shipped = require("../../../assets/icons/shipped.png");
const enterprise = require("../../../assets/icons/enterprise.png");

class DeliveryComponent extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerTintColor: colors.light,
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>DELIVERY</Text>
    ),
    headerRight: <View />
  };

  state = {
    modalHost: false,
    host: [
      {
        id: 0,
        name: "Lionel Messi"
      },
      {
        id: 1,
        name: "Luis Suarez"
      },
      {
        id: 2,
        name: "Andre Pratama"
      }
    ]
  };

  showHost = () => {
    this.setState({
      modalHost: true
    });
  };

  closeHost = () => {
    this.setState({
      modalHost: false
    });
  };

  choseHost = ({ name }) => {
    this.setState({
      modalHost: false
    });
  };

  render() {
    return (
      <React.Fragment>
        <Modal
          isVisible={this.state.modalHost}
          onBackButtonPress={this.closeHost}
          onBackdropPress={this.closeHost}
          animationIn="fadeIn"
          animationOut="fadeOut"
        >
          <View style={styles.modalWrapper}>
            <Text style={styles.labelBold}>SELECT HOST</Text>

            <TextInput style={styles.search} placeholder="Search host" />

            <ScrollView
              contentContainerStyle={[styles.contentWrapper, { flexGrow: 1 }]}
              keyboardShouldPersistTaps="handled"
            >
              {this.state.host.map((v, key) => (
                <TouchableOpacity
                  style={styles.btnList}
                  key={key}
                  onPress={() => this.choseHost(v)}
                >
                  <Text>{v.name}</Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </Modal>

        <View style={styles.container}>
          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={enterprise} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Company</Text>
              <TextInput style={styles.input} onFocus={this.showHost} />
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={shipped} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Vehicle Number</Text>
              <TextInput style={styles.input} />
            </View>
          </View>
        </View>

        <View style={styles.btnWrapper}>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.text}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </React.Fragment>
    );
  }
}

export default DeliveryComponent;
