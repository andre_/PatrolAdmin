import {Alert} from 'react-native';
import {API} from '@src/services/APIService';
import * as types from '@src/constants/actionTypes';
import store from 'react-native-simple-store';
import {
  ADD_VISITOR,
  REMOVE_VISITOR,
  ADD_OFFLINE_VISITOR,
  REMOVE_OFFLINE_VISITOR,
} from '../../../constants/actionTypes';

const RESERVE_QR_CODE = 'RESERVE_QR_CODE';
const VISITOR_CHECKIN_REQUEST = 'VISITOR_CHECKIN_REQUEST';
const VISITOR_CHECKIN_FULFILLED = 'VISITOR_CHECKIN_FULFILLED';
const VISITOR_CHECKIN_REJECTED = 'VISITOR_CHECKIN_REJECTED';

export const reserveQrCode = id => {
  return dispatch => {
    dispatch({type: RESERVE_QR_CODE});
    return API().put(`/v2/visitor/reserve_code/${id}`);
  };
};

export const doVisitorCheckin = data => {
  return dispatch => {
    API()
      .put(`/v2/visitor/reserve_code/${data.id}`)
      .then(res => {
        if (res.data.success) {
          dispatch(setVisitorCheckin(data));
        }
      })
      .catch(err => {
        store
          .push('visitor', data)
          .then(() => store.get('visitor'))
          .then(visitor => {})
          .catch(err => {});
      });
  };
};

export const setVisitorCheckin = data => {
  return dispatch => {
    dispatch({type: VISITOR_CHECKIN_REQUEST});

    let [platImg] = data.obj.image_plate.uri.split('/').reverse();
    let [icImg] = data.obj.image_ic.uri.split('/').reverse();

    let fd = new FormData();
    fd.append('qr_id', data.obj.qr_id);
    fd.append('type', data.obj.type);
    fd.append('name', data.obj.name);
    fd.append('phone', data.obj.phone);
    fd.append('unit', data.obj.unit);

    fd.append('image_plate', {
      uri: data.obj.image_plate.uri,
      name: `image-${platImg}`,
      type: 'image/jpeg',
    });

    fd.append('image_ic', {
      uri: data.obj.image_ic.uri,
      name: `image-${icImg}`,
      type: 'image/jpeg',
    });

    API()
      .post('v2/visitor/check_in', fd)
      .then(res => {
        dispatch({type: VISITOR_CHECKIN_FULFILLED});
        dispatch({type: ADD_VISITOR});
      })
      .catch(err => {
        dispatch({type: VISITOR_CHECKIN_REJECTED});
        store
          .push('visitor', data)
          .then(() => store.get('visitor'))
          .then(visitor => {})
          .catch(err => {});
      });
  };
};

export const initVisitor = data => ({
  type: types.INIT_VISITOR,
  data,
});

export const removeVisitor = () => ({
  type: types.REMOVE_VISITOR,
});

export const offlineVisitor = data => ({
  type: types.OFFLINE_VISITOR,
  data,
});

export const addOfflineVisitor = () => ({
  type: typecheck.ADD_OFFLINE_VISITOR,
});

export const removeOfflineVisitor = () => ({
  type: types.REMOVE_OFFLINE_VISITOR,
});
