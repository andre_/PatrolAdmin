import { colors } from "@styles";

const styles = {
  container: {
    backgroundColor: colors.light,
    flex: 1,
    padding: 20
  },
  label: {
    fontFamily: "OpenSans-SemiBold",
    marginBottom: 5
  },
  formGroup: {
    height: 80
  },
  imageWrapper: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  imageBox: {
    width: "50%",
    padding: 5
  },
  imageUpload: {
    alignItems: "center",
    justifyContent: "center",
    height: 180,
    backgroundColor: "#f1f1f1",
    borderStyle: "dashed"
  },
  modalWrapper: {
    backgroundColor: colors.light,
    padding: 20,
    width: 220,
    height: 220,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  text: {
    fontFamily: "OpenSans-Regular"
  },
  btnList: {
    height: 40,
    borderBottomWidth: 1,
    borderColor: colors.background,
    justifyContent: "center"
  },
  search: {
    fontFamily: "OpenSans-Light",
    marginTop: 5,
    marginBottom: 10
  },
  labelBold: {
    fontFamily: "OpenSans-SemiBold"
  },
  btnWrapper: {
    marginTop: 15,
    marginBottom: 15
  },
  btn: {
    width: "100%",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    backgroundColor: colors.info
  },
  textBtn: {
    color: colors.light,
    fontFamily: "OpenSans-SemiBold"
  },
  images: {
    width: "100%",
    height: "100%"
  },

  form: {
    flexDirection: "row",
    height: 100
  },
  formLeft: {
    justifyContent: "center",
    width: 40
  },
  formRight: {
    flex: 1
  },
  icon: {
    width: 30,
    height: 30
  },
  radio: {
    flexDirection: "row",
    width: 120,
    height: 50
  },
  radioBtn: {
    marginRight: 5,
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "#dadada"
  },
  tick: {
    width: 25,
    height: 25,
    position: "absolute",
    left: 0,
    bottom: 0,
    zIndex: 10000
  },
  textRadio: {
    fontFamily: "OpenSans-Regular"
  },
  loadingQr: {
    position: "absolute",
    top: 0,
    left: 0,
    width: 30,
    height: 30,
    zIndex: 1,
    backgroundColor: colors.light,
    opacity: 0.8,
    alignItems: "center",
    justifyContent: "center"
  },
  signatureWrapper: {
    backgroundColor: colors.light,
    width: 300,
    height: 400,
    alignSelf: "center"
  },
  signatureDesc: {
    padding: 15
  },
  signature: {
    flex: 1,
    width: "100%",
    height: "100%"
  }
};

export default styles;
