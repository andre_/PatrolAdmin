import React, { Component } from "react";
import {
  View,
  Text,
  BackHandler,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image
} from "react-native";
import { API } from "@src/services/APIService";
import { SkypeIndicator } from "react-native-indicators";
import { colors, configs } from "@styles";
import ImagePicker from "react-native-image-picker";
import Modal from "react-native-modal";
import SignatureCapture from "react-native-signature-capture";

import styles from "../styles";

const qrCode = require("../../../assets/icons/qr-code-scan.png");
const call = require("../../../assets/icons/call.png");
const avatar = require("../../../assets/icons/avatar-icon.png");
const enterprise = require("../../../assets/icons/enterprise.png");
const addPlus = require("../../../assets/icons/add-icon-plus.png");
const idCard = require("../../../assets/icons/id-card.png");
const tick = require("../../../assets/icons/tick-icon.png");
const tickGreen = require("../../../assets/icons/tick-green.png");
const leftBack = require("../../../assets/icons/left-back.png");

class VisitorQr extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSignature: false,
      passNumber: null,
      modalHost: false,
      loadingQr: false,
      name: null,
      ic: null,
      plate: null,
      type: null,
      otherType: null,
      phone: null,
      unit: null,
      host: [
        {
          id: 0,
          name: "Lionel Messi"
        },
        {
          id: 1,
          name: "Luis Suarez"
        },
        {
          id: 2,
          name: "Andre Pratama"
        }
      ],
      selectedHost: null
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: colors.info
    },
    headerTintColor: colors.light,
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>CHECK IN</Text>
    ),
    headerLeft: (
      <TouchableOpacity
        style={configs.backButton}
        onPress={() => navigation.goBack()}
      >
        <Image source={leftBack} style={configs.imageBack} />
      </TouchableOpacity>
    ),
    headerRight: <View />
  });

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  checkQr = qrData => {
    return API().get(`/v2/visitor/qr_used?qr_id=${qrData}`);
  };

  getQrValue = data => {
    this.setState({ loadingQr: true });
    this.checkQr(data)
      .then(res => {
        this.setState({ loadingQr: false });
        if (res.data.data.used) {
          alert("QR already in use");
        } else {
          this.setState({
            passNumber: data
          });
        }
      })
      .catch(err => {
        this.setState({ loadingQr: false, passNumber: data });
      });
  };

  showHost = () => {
    this.setState({
      modalHost: true
    });
  };

  closeHost = () => {
    this.setState({
      modalHost: false
    });
  };

  scanQr = () => {
    this.props.navigation.navigate("VisitorScanner", {
      getQrValue: this.getQrValue
    });
  };

  chooseType = type => {
    this.setState({
      type,
      otherType: null
    });
  };

  renderRadio = () => {
    const { type } = this.state;
    return (
      <View style={styles.form}>
        <View>
          <View style={styles.radio}>
            <TouchableOpacity
              style={[styles.radioBtn]}
              onPress={() => this.chooseType("pickup")}
            >
              {type === "pickup" ? (
                <Image source={tick} style={styles.tick} />
              ) : null}
            </TouchableOpacity>
            <Text style={[styles.textRadio, { width: 100 }]}>
              Pickup / Drop off
            </Text>
          </View>

          <View style={styles.radio}>
            <TouchableOpacity
              style={[styles.radioBtn]}
              onPress={() => this.chooseType("delivery")}
            >
              {type === "delivery" ? (
                <Image source={tick} style={styles.tick} />
              ) : null}
            </TouchableOpacity>
            <Text style={styles.textRadio}>Delivery</Text>
          </View>
        </View>

        <View>
          <View style={styles.radio}>
            <TouchableOpacity
              style={[styles.radioBtn]}
              onPress={() => this.chooseType("visitor")}
            >
              {type === "visitor" ? (
                <Image source={tick} style={styles.tick} />
              ) : null}
            </TouchableOpacity>
            <Text style={styles.textRadio}>Visitor</Text>
          </View>

          <View style={styles.radio}>
            <TouchableOpacity
              style={[styles.radioBtn]}
              onPress={() => this.chooseType("contractor")}
            >
              {type === "contractor" ? (
                <Image source={tick} style={styles.tick} />
              ) : null}
            </TouchableOpacity>
            <Text style={styles.textRadio}>Contractor</Text>
          </View>
        </View>

        <View>
          <View style={styles.radio}>
            <TouchableOpacity
              style={[styles.radioBtn]}
              onPress={() => this.chooseType("others")}
            >
              {type === "others" ? (
                <Image source={tick} style={styles.tick} />
              ) : null}
            </TouchableOpacity>
            <Text style={styles.textRadio}>Others</Text>
          </View>
        </View>
      </View>
    );
  };

  getLicense = () => {
    const options = {
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log("cancel");
      } else {
        console.log("hasil image picker", response);
        this.setState({
          ic: response
        });
      }
    });
  };

  getIcData = data => {
    this.setState({
      ic: data
    });
  };

  getIc = () => {
    const {
      flag: { usingImagePicker }
    } = this.props;

    if (usingImagePicker) {
      const options = {
        quality: 0.5,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
          path: "images"
        }
      };

      ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          console.log("cancel");
        } else {
          console.log("hasil image picker", response);
          this.setState({
            ic: response
          });
        }
      });
    } else {
      this.props.navigation.navigate("Camera", {
        getIcData: this.getIcData,
        type: "ic"
      });
    }
  };

  getPlateData = data => {
    this.setState({
      plate: data
    });
  };

  getPlate = () => {
    const {
      flag: { usingImagePicker }
    } = this.props;

    if (usingImagePicker) {
      const options = {
        quality: 0.5,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
          path: "images"
        }
      };
      ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          console.log("cancel");
        } else {
          this.setState({
            plate: response
          });
        }
      });
    } else {
      this.props.navigation.navigate("Camera", {
        getPlateData: this.getPlateData,
        type: "plate"
      });
    }
  };

  choseHost = ({ name }) => {
    this.setState({
      selectedHost: name,
      modalHost: false
    });
  };

  addVisitorBefore = () => {
    if (this.state.passNumber === null) {
      alert("Please add pass number");
    } else {
      if (
        this.state.ic === null ||
        this.state.name === null ||
        this.state.plate === null ||
        this.state.type === null
      ) {
        alert("Please complete the form");
      } else {
        this.showSignature();
      }
    }
  };

  addVisitor = () => {
    let obj = {
      qr_id: this.state.passNumber,
      type:
        this.state.otherType !== null ? this.state.otherType : this.state.type,
      image_plate: this.state.plate,
      image_ic: this.state.ic,
      name: this.state.name,
      unit: this.state.unit,
      phone: this.state.phone
    };

    this.setState({ showSignature: false });

    this.props.checkin({ id: this.state.passNumber, obj });
    this.props.navigation.navigate("ModalVisitor");
  };

  showSignature = () => {
    this.setState({
      showSignature: true
    });
  };

  closeSignature = () => {
    this.setState({
      showSignature: false
    });
  };

  render() {
    return (
      <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
        <Modal
          isVisible={this.state.showSignature}
          onBackButtonPress={this.closeSignature}
          onBackdropPress={this.closeSignature}
        >
          <View style={styles.signatureWrapper}>
            <View style={styles.signatureDesc}>
              <Text style={styles.text}>
                You agree with terms and condition. Please sign below.
              </Text>
            </View>
            <SignatureCapture
              showNativeButtons={false}
              style={styles.signature}
            />
            <View>
              <TouchableOpacity
                style={[styles.btn, { height: 50, borderRadius: 0 }]}
                onPress={this.addVisitor}
              >
                <Text style={styles.textBtn}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          isVisible={this.state.modalHost}
          onBackButtonPress={this.closeHost}
          onBackdropPress={this.closeHost}
          animationIn="fadeIn"
          animationOut="fadeOut"
        >
          <View style={styles.modalWrapper}>
            <Text style={styles.text}>CHECKIN SUCCESS</Text>
          </View>
        </Modal>

        <View style={styles.container}>
          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={idCard} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Pass Number*</Text>
              <View style={{ flexDirection: "row", width: "100%" }}>
                <TextInput
                  style={[styles.input, { width: "90%" }]}
                  value={this.state.passNumber}
                  onChangeText={text => this.setState({ passNumber: text })}
                />
                <TouchableOpacity
                  onPress={this.scanQr}
                  style={[
                    styles.btnQr,
                    {
                      width: 30,
                      height: 30,
                      backgroundColor: colors.info,
                      padding: 5
                    }
                  ]}
                >
                  {this.state.loadingQr ? (
                    <View style={styles.loadingQr}>
                      {this.state.loadingQr ? (
                        <SkypeIndicator color={colors.info} size={20} />
                      ) : (
                        <Image source={tickGreen} />
                      )}
                    </View>
                  ) : null}

                  <Image source={qrCode} style={styles.images} />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={avatar} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Name</Text>
              <View style={{ flexDirection: "row", width: "100%" }}>
                <TextInput
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={name => this.setState({ name: name })}
                />
              </View>
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={avatar} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Unit</Text>
              <View style={{ flexDirection: "row", width: "100%" }}>
                <TextInput
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={unit => this.setState({ unit: unit })}
                />
              </View>
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.formLeft}>
              <Image source={call} style={styles.icon} />
            </View>
            <View style={styles.formRight}>
              <Text style={styles.label}>Phone Number</Text>
              <View style={{ flexDirection: "row", width: "100%" }}>
                <TextInput
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={phone => this.setState({ phone })}
                  keyboardType={"numeric"}
                />
              </View>
            </View>
          </View>

          {this.renderRadio()}

          {this.state.type === "others" ? (
            <View style={styles.form}>
              <View style={styles.formLeft}>
                <Image source={enterprise} style={styles.icon} />
              </View>
              <View style={styles.formRight}>
                <Text style={styles.label}>Others</Text>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <TextInput
                    style={[styles.input, { width: "100%" }]}
                    onChangeText={type => this.setState({ otherType: type })}
                  />
                </View>
              </View>
            </View>
          ) : null}

          <View style={styles.imageWrapper}>
            <View style={styles.imageBox}>
              <Text style={[styles.label, { height: 40 }]}>
                IC / LICENSE / PASSPORT
              </Text>

              <TouchableOpacity style={styles.imageUpload} onPress={this.getIc}>
                {this.state.ic !== null ? (
                  <Image
                    source={{ uri: this.state.ic.uri }}
                    style={styles.images}
                  />
                ) : (
                  <Image source={addPlus} style={styles.icon} />
                )}
              </TouchableOpacity>
            </View>

            <View style={styles.imageBox}>
              <Text style={[styles.label, { height: 40 }]}>CAR PLATE</Text>

              <TouchableOpacity
                style={styles.imageUpload}
                onPress={this.getPlate}
              >
                {this.state.plate !== null ? (
                  <Image
                    source={{ uri: this.state.plate.uri }}
                    style={styles.images}
                  />
                ) : (
                  <Image source={addPlus} style={styles.icon} />
                )}
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.btnWrapper}>
            <TouchableOpacity
              style={styles.btn}
              onPress={this.addVisitorBefore}
            >
              <Text style={styles.textBtn}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default VisitorQr;
