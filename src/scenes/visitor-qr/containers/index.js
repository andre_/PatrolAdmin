import VisitorQr from "../components";
import { connect } from "react-redux";
import { doVisitorCheckin } from "../actions";

const mapStateToProps = state => ({
  flag: state.flag
});

const mapDispatchToProps = dispatch => ({
  checkin(data) {
    dispatch(doVisitorCheckin(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VisitorQr);
