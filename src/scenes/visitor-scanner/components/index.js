import React, { Component } from "react";
import { View, StyleSheet, BackHandler } from "react-native";
import { colors } from "@styles";
import QRCodeScanner from "react-native-qrcode-scanner";

class VisitorScanner extends Component {
  static navigationOptions = {
    title: "Scan Code bar",
    headerTitleStyle: {
      color: "#fff"
    },
    headerTintColor: colors.light,
    headerStyle: {
      backgroundColor: colors.info
    }
  };

  constructor(props) {
    super(props);

    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    this.props.navigation.goBack();
    return true;
  }

  onSuccess(e) {
    this.props.navigation.state.params.getQrValue(e.data);
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#000" }}>
        <QRCodeScanner onRead={this.onSuccess.bind(this)} showMarker={true} />
      </View>
    );
  }
}

const styles = StyleSheet.create({});

export default VisitorScanner;
