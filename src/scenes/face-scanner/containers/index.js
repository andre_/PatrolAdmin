import { connect } from "react-redux";
import { addCheckin } from "@src/reducers/absence";
import FaceRecognitionIn from "../components";

const mapStateToProps = state => ({
  flat: state.flag
});

const mapDispatchToProps = dispatch => ({
  checkin(data) {
    dispatch(addCheckin(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FaceRecognitionIn);
