import React, { Component, Fragment } from "react";
import {
  Animated,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler
} from "react-native";
import axios from "axios";
import { httpService } from "@src/services/APIService";
import { RNCamera } from "react-native-camera";
import { DotIndicator } from "react-native-indicators";

import Api from "../../../api";

import Modal from "react-native-modal";

import { colors } from "@styles";

class FaceScanner extends Component {
  state = {
    loading: false,
    showModal: false,
    opacity: new Animated.Value(0),
    zIndex: 0,
    iteration: 1
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    this.setState({ loading: false });
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  navigateToManual = () => {
    this.props.navigation.navigate("CheckinScreen");
  };

  closeModal = () => {
    this.setState({
      showModal: false
    });
  };

  takePicture = () => {
    if (this.camera) {
      this.setState({ loading: true });
      const isCheckin =
        this.props.navigation.getParam("type") === "in" ? true : false;

      this.camera.takePictureAsync().then(res => {
        this.props.navigation.navigate("FaceResultIn", {
          image: res,
          isCheckin
        });
      });
    }
    // const isCheckin =
    //   this.props.navigation.getParam("type") === "in" ? true : false;

    // if (this.camera) {
    //   this.setState({
    //     loading: true
    //   });
    //   this.camera
    //     .takePictureAsync()
    //     .then(res => {
    //       this.props.navigation.navigate("ScanningFace", {
    //         picture: res,
    //         isCheckin
    //       });
    //       this.setState({ loading: false });
    //     })
    //     .catch(err => {
    //       this.setState({
    //         loading: false
    //       });
    //     });
    // }
  };

  generateRandomString = () => {
    return Math.random()
      .toString(36)
      .substr(2, 15);
  };

  checkAI = data => {
    const type = this.props.navigation.getParam("type");

    const fd = new FormData();
    fd.append("post_id", 73);
    fd.append("gps", "0,0");
    fd.append("deviceid", "bla");
    fd.append("face", {
      name: `${this.generateRandomString()}.jpg`,
      type: "image/jpg",
      uri: data.uri
    });

    console.log("type", type);

    if (type === "in") {
      Api.checkinAI(fd)
        .then(result => {
          this.props.navigation.navigate("FaceResultIn", {
            attendance: result.data.data.attendance,
            user: result.data.data.user,
            post: result.data.data.post
          });
        })
        .catch(err => {
          console.log("data", fd);
          console.log("error", err.response.data);
        });
    } else {
      Api.checkoutAI(fd)
        .then(result => {
          this.props.navigation.navigate("FaceResultOut", {
            user: result.data.data.user,
            post: result.data.data.post
          });
        })
        .catch(err => {
          console.log("data", fd);
          console.log("error", err.response.data);
        });
    }
  };

  generateRandomString = () => {
    return Math.random()
      .toString(36)
      .substr(2, 5);
  };

  compareFace = faces => {
    const isCheckin =
      this.props.navigation.getParam("type") === "in" ? true : false;

    const fd = new FormData();
    fd.append("guardImage", {
      name: this.generateRandomString(),
      type: "image/jpeg",
      uri: faces.uri
    });

    this.setState({
      loading: true
    });

    httpService()
      .post(`attendance_compare_face`, fd)
      .then(res => {
        if (
          res.data.message === "No guard found." ||
          res.data.message === "No guard ID given." ||
          res.data.message === "No match found."
        ) {
          this.setState({
            loading: false,
            showModal: true
          });
        } else {
          if (isCheckin) {
            this.props.navigation.navigate("FaceResultIn", {
              guard: res.data.data.guard,
              imageGuard: faces
            });
          } else {
            this.props.navigation.navigate("FaceResultOut", {
              guard: res.data.data.guard,
              imageGuard: faces
            });
          }
        }
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  };

  render() {
    return (
      <Fragment>
        <Modal
          isVisible={this.state.showModal}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
        >
          <View style={styles.modalQuestion}>
            <TouchableOpacity style={styles.close} onPress={this.closeModal}>
              <Text style={styles.textClose}>X</Text>
            </TouchableOpacity>
            <View style={styles.content}>
              <Text style={styles.desc}>No Match Found</Text>
            </View>

            <TouchableOpacity
              style={styles.footer}
              onPress={this.navigateToManual}
            >
              <Text style={styles.text}>CHECK IN MANUAL</Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <View
          style={[
            styles.container,
            {
              flex: 1,
              position: "absolute",
              width: "100%",
              height: "100%"
            }
          ]}
        >
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            type={RNCamera.Constants.Type.front}
            style={styles.preview}
          >
            <View style={styles.btnWrapper}>
              <TouchableOpacity
                style={styles.btnPicture}
                onPress={this.takePicture}
              >
                {this.state.loading ? (
                  <DotIndicator color={colors.light} size={5} />
                ) : (
                  <Text style={styles.text}>SCAN FACE</Text>
                )}
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  preview: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  btnWrapper: {
    position: "absolute",
    alignSelf: "center",
    height: 80,
    alignItems: "center",
    bottom: 0
  },
  btnPicture: {
    borderRadius: 4,
    height: 60,
    width: 200,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.info
  },
  text: {
    fontFamily: "Montserrat-SemiBold",
    color: colors.light
  },
  modalQuestion: {
    borderRadius: 4,
    backgroundColor: colors.light,
    width: 280,
    height: 180,
    alignSelf: "center"
  },
  content: {
    alignItems: "center",
    justifyContent: "center",
    height: 150
  },
  footer: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 50,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    backgroundColor: colors.danger
  },
  desc: {
    fontFamily: "Montserrat-Regular"
  },
  close: {
    backgroundColor: colors.background,
    width: 40,
    height: 40,
    borderRadius: 40,
    position: "absolute",
    right: -10,
    top: -20,
    alignItems: "center",
    justifyContent: "center"
  },
  textClose: {
    fontFamily: "Montserrat-Regular",
    color: colors.text
  },
  textScanning: {
    fontFamily: "Montserrat-Bold",
    color: colors.danger,
    fontSize: 20
  },
  animationWrapper: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.dark,
    position: "absolute",
    left: 0,
    top: 0
  }
});

export default FaceScanner;
