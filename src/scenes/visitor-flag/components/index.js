import React, { Component } from "react";
import { View, Text } from "react-native";
import { List, ListItem, Icon, Left, Body } from "native-base";
import { connect } from "react-redux";
import { colors } from "@styles/";
import { setVisitorManual } from "@src/actions/scanningActions";

const mapStateToProps = state => ({
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  setManual(data) {
    dispatch(setVisitorManual(data));
  }
});

class VisitorFlag extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      scanning: { visitorManual }
    } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: colors.light }}>
        <List>
          <ListItem
            icon
            onPress={() => {
              this.props.setToNfc(true);
            }}
          >
            <Left style={{ width: 40 }}>
              {visitorManual === true && (
                <Icon type="FontAwesome" name="check" />
              )}
            </Left>
            <Body>
              <Text>True</Text>
            </Body>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.setManual(false);
            }}
          >
            <Left style={{ width: 40 }}>
              {visitorManual === false && (
                <Icon type="FontAwesome" name="check" />
              )}
            </Left>
            <Body>
              <Text>false</Text>
            </Body>
          </ListItem>
        </List>
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VisitorFlag);
