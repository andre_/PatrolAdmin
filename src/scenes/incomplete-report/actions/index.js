import * as types from '../constants';
import * as clockingTypes from '../../clocking/constants';
import {API} from '@src/services/APIService';
import store from 'react-native-simple-store';

export const sendReport = (clocking, report, image, audio) => {
  return dispatch => {
    dispatch({type: types.SEND_INCOMPLETE_REPORT_REQUEST});

    const fd = new FormData();
    fd.append('data', JSON.stringify(report));
    fd.append('clocking', JSON.stringify(clocking));

    image.map(x => {
      fd.append('attachments[]', {
        uri: x.uri,
        name: x.fileName,
        type: 'image/jpeg',
      });
    });

    fd.append('attachments[]', {
      uri: audio,
      name: 'test.aac',
      type: 'audio/aac',
    });

    API()
      .post(`https://my.trackerhero.com/api/send_new_incident_v2`, fd)
      .then(res => {
        dispatch({type: 'CLEAR_CP'});
        dispatch({type: 'CLEAR_STORAGE'});
        dispatch({type: types.SEND_INCOMPLETE_REPORT_FULFILLED});
      })
      .catch(err => {
        dispatch({type: types.SEND_INCOMPLETE_REPORT_REJECTED});

        store
          .push('packageOffline', {
            clocking,
            report,
            photos: image,
            audio,
          })
          .then(() => store.get('packageOffline'))
          .then(packageOffline => {
            dispatch({type: clockingTypes.END_CLOCKING});
            dispatch({type: 'CLEAR_CP'});
            dispatch({type: 'CLEAR'});
            dispatch({type: 'CLEAR_STORAGE'});
          })
          .catch(err => {
            console.log(err);
          });
      });
  };
};
