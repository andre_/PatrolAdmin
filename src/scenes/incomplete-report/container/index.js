import { connect } from "react-redux";
import IncompleteReport from "../component";
import { destroyClockingId } from "@src/actions/clockingActions";
import { sendReport } from "../actions";

const mapStateToProps = state => ({
  posts: state.posts,
  flag: state.flag,
  account: state.account,
  patroling: state.patroling,
  connection: state.connection
});

const mapDispatchToProps = dispatch => ({
  destroyClockingId() {
    dispatch(destroyClockingId());
  },
  doSendReport(clocking, report, image, audio) {
    dispatch(sendReport(clocking, report, image, audio));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IncompleteReport);
