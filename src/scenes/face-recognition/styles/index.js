import { ScaledSheet } from "react-native-size-matters";
import { colors } from "@styles";

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.light
  },
  animationWrapper: {
    width: 340,
    height: 340
  },
  btn: {
    alignItems: "center",
    justifyContent: "center",
    width: "50%",
    height: 70,
    borderRadius: 0
  },
  btnText: {
    fontSize: 18,
    fontFamily: "Montserrat-SemiBold"
  },
  buttonWrapper: {
    flexDirection: "row"
  },
  textBig: {
    fontSize: 22,
    fontFamily: "Montserrat-Regular"
  }
});

export default styles;
