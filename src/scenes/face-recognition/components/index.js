import React, { Component, Fragment } from "react";
import {
  View,
  ImageBackground,
  Text,
  TouchableOpacity,
  BackHandler
} from "react-native";
import LottieView from "lottie-react-native";
import { colors } from "@styles";
import styles from "../styles";

const buttonPress = require("../../../assets/button-press.json");
const bg = require("../../../assets/bg.jpg");

class FaceRecognition extends Component {
  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: colors.dark
  };

  navigate = type => {
    this.props.navigation.navigate("FaceRecognition", { type });
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    return (
      <Fragment>
        <ImageBackground source={bg} style={styles.container}>
          <View>
            <Text style={styles.textBig}>ATTENDANCE</Text>
          </View>
          <View style={styles.animationWrapper}>
            <LottieView source={buttonPress} autoPlay />
          </View>
        </ImageBackground>

        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() =>
              this.props.navigation.navigate("FaceScanner", { type: "in" })
            }
          >
            <Text
              style={{ fontFamily: "OpenSans-Regular", color: colors.info }}
            >
              CHECK IN
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btn}
            onPress={() =>
              this.props.navigation.navigate("FaceScanner", { type: "out" })
            }
          >
            <Text
              style={{ fontFamily: "OpenSans-Regular", color: colors.danger }}
            >
              CHECK OUT
            </Text>
          </TouchableOpacity>
        </View>
      </Fragment>
    );
  }
}

export default FaceRecognition;
