import {connect} from 'react-redux';
import SummaryComponent from '../components';

const mapStateToProps = state => ({
  posts: state.posts,
});

export default connect(mapStateToProps)(SummaryComponent);
