import React, { Component } from "react";
import {
  FlatList,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  RefreshControl,
  ActivityIndicator
} from "react-native";
import styles from "../styles";
import axios from "axios";
import api from "@src/constants/api";
import { API as httpService } from "@src/services/APIService";
import { text, colors } from "@styles";

class SummaryComponent extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.info,
      elevation: 0
    },
    headerTintColor: colors.light
  };

  state = {
    summary: [],
    loading: true,
    refreshing: false
  };

  componentDidMount() {
    this._getSummary();
  }

  _getSummary = () => {
    const {
      posts: { posts }
    } = this.props;

    axios
      .get("https://my.trackerhero.com/___refresh_cache___")
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });

    httpService()
      .get(`${api.clocking.summary}?post=${parseInt(posts.post_id)}`)
      .then(res => {
        this.setState({ summary: res.data, loading: false, refreshing: false });
      })
      .catch(err => {
        this.setState({ loading: false, refreshing: false });
        console.log(err);
      });
  };

  getToday = () => {
    let day = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ];
    let month = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];

    let nox = new Date();
    let fullDate = `${day[nox.getDay()]}, ${nox.getDate()} ${
      month[nox.getMonth()]
    } ${nox.getFullYear()}`;

    return fullDate;
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => this._getSummary()
    );
  };

  render() {
    const { summary, refreshing } = this.state;
    let summaryFind = summary.find(v => v);

    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.handleRefresh}
            />
          }
        >
          {this.state.loading && (
            <ActivityIndicator size={"large"} color={colors.info} />
          )}
          {summaryFind &&
            summaryFind.clockings.map((v, k) => (
              <View style={styles.grid} key={k}>
                <Text style={text.p}>
                  {this.getToday()} {v.hour}
                </Text>
                <View style={styles.wrapCp}>
                  {v.missing.map((x, k) => (
                    <View style={styles.cpList} key={k}>
                      <TouchableOpacity
                        style={[
                          styles.btnCp,
                          { backgroundColor: colors.danger }
                        ]}
                      >
                        <Image
                          source={require("../../../assets/failed.png")}
                          style={styles.checklist}
                        />
                      </TouchableOpacity>
                      <Text style={[text.p, { color: colors.dark }]}>
                        {x.description}
                      </Text>
                    </View>
                  ))}

                  {v.clockings.map((x, k) => (
                    <View style={styles.cpList} key={k}>
                      <TouchableOpacity
                        style={[
                          styles.btnCp,
                          { backgroundColor: colors.success }
                        ]}
                      >
                        <Image
                          source={require("../../../assets/success.png")}
                          style={styles.checklist}
                        />
                      </TouchableOpacity>
                      <Text style={[text.p, { color: colors.dark }]}>
                        {x &&
                          x.checkpointDetails &&
                          x.checkpointDetails.description}
                      </Text>
                    </View>
                  ))}
                </View>
                <View style={styles.bottom}>
                  <Text
                    style={[
                      text.p,
                      { color: v.completed ? colors.success : colors.danger }
                    ]}
                  >
                    {v.completed
                      ? "Clocking Completed"
                      : "Clocking Not Complete"}
                  </Text>
                </View>
              </View>
            ))}
        </ScrollView>
      </View>
    );
  }
}

export default SummaryComponent;
