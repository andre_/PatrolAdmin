import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    height: "100%",
    padding: 20
  },
  grid: {
    padding: 15,
    paddingBottom: 0,
    backgroundColor: colors.light,
    borderRadius: 10,
    marginBottom: 20
  },
  btnCp: {
    backgroundColor: colors.success,
    width: 20,
    height: 20,
    borderRadius: 20,
    marginRight: 10
  },
  wrapCp: {
    paddingTop: 10
  },
  cpList: {
    display: "flex",
    alignItems: "center",
    marginBottom: 10,
    display: "flex",
    flexDirection: "row"
  },
  bottom: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderTopWidth: 1,
    borderTopColor: colors.background,
    height: 50
  },
  checklist: {
    width: "100%",
    height: "100%"
  }
};

export default styles;
