import React, { Component } from "react";
import {
  Animated,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  BackHandler
} from "react-native";
import { BallIndicator } from "react-native-indicators";
import { RNCamera } from "react-native-camera";
import { Grid, Col } from "react-native-easy-grid";
import { colors } from "@styles";

const reverseIcon = require("../../assets/icons/reverse-camera.png");

class Camera extends Component {
  state = {
    pictures: [],
    torch: false,
    type: "back",
    loading: false,
    rotation: new Animated.Value(0)
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  capturePict = () => {
    let type = this.props.navigation.getParam("type");

    this.setState({ loading: true });

    if (this.camera) {
      this.camera
        .takePictureAsync()
        .then(data => {
          if (type === "plate") {
            this.props.navigation.state.params.getPlateData(data);
          } else {
            this.props.navigation.state.params.getIcData(data);
          }
          this.props.navigation.goBack();
        })
        .catch(err => {
          alert("Camera error");
          this.setState({ loading: false });
          console.log(err);
        });
    }
  };

  reverseCamera = () => {
    let reverseType = this.state.type === "back" ? "front" : "back";

    this.setState({
      type: reverseType
    });
  };

  render() {
    const AnimatedButton = Animated.createAnimatedComponent(TouchableOpacity);

    return (
      <React.Fragment>
        {this.state.loading ? (
          <View style={styles.loadingWrapper}>
            <Text style={styles.textLoading}>Taking your picture...</Text>
          </View>
        ) : null}

        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          type={RNCamera.Constants.Type[this.state.type]}
          style={styles.preview}
        >
          {/*  */}
          <View style={styles.bottomWrapper}>
            <Grid>
              <Col style={styles.cols}>
                <AnimatedButton
                  style={[styles.btnReverse]}
                  onPress={this.reverseCamera}
                >
                  <Image source={reverseIcon} style={[styles.icon]} />
                </AnimatedButton>
              </Col>
              <Col style={styles.cols}>
                <TouchableOpacity
                  style={styles.btnCapture}
                  onPress={this.capturePict}
                />
              </Col>
              {/* <Col style={styles.cols}>
              <View style={styles.imagePreview}>
                {data.length ? (
                  <View style={styles.marker}>
                    <Text style={styles.textMarker}>{data.length}</Text>
                  </View>
                ) : null}

                {this.state.loading ? (
                  <View style={styles.loadingMarker}>
                    <BallIndicator color={colors.info} size={20} />
                  </View>
                ) : null}

                {data.map((v, key) => (
                  <Image
                    key={key}
                    source={{ uri: v }}
                    style={[styles.imageFull]}
                  />
                ))}
              </View>
            </Col> */}
            </Grid>
          </View>
        </RNCamera>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  cols: {
    alignItems: "center",
    justifyContent: "center"
  },
  bottomWrapper: {
    backgroundColor: colors.black,
    width: "100%",
    height: 140
  },
  btnCapture: {
    width: 70,
    height: 70,
    borderRadius: 80,
    borderWidth: 4,
    borderColor: colors.light,
    backgroundColor: colors.info
  },
  btnReverse: { width: 50, height: 50 },
  btnTorch: { width: 70, height: 70 },
  icon: {
    width: "100%",
    height: "100%"
  },
  imagePreview: {
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  imageFull: {
    width: "100%",
    height: "100%",
    borderRadius: 4,
    position: "absolute"
  },
  marker: {
    width: 25,
    height: 25,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    opacity: 0.8,
    backgroundColor: colors.danger,
    zIndex: 10
  },
  loadingMarker: {
    width: 50,
    height: 50,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.light,
    opacity: 0.6,
    position: "absolute",
    zIndex: 100
  },
  textMarker: {
    fontSize: 12,
    color: colors.light,
    fontFamily: "OpenSans-Bold"
  },
  loadingWrapper: {
    width: "100%",
    height: "100%",
    flex: 1,
    opacity: 0.5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.light,
    position: "absolute",
    zIndex: 100
  },
  textLoading: {
    fontFamily: "OpenSans-Regular",
    color: colors.light,
    fontSize: 18
  }
});

export default Camera;
