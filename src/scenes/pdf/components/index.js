import React, { Component } from "react";
import { View, Text, Dimensions } from "react-native";
import Pdf from "react-native-pdf";

class PdfComponent extends Component {
  render() {
    const source = {
      uri: this.props.navigation.getParam("path"),
      cache: true
    };
    return (
      <View style={{ flex: 1 }}>
        <Pdf
          source={source}
          style={{
            flex: 1,
            width: Dimensions.get("window").width
          }}
        />
      </View>
    );
  }
}

export default PdfComponent;
