import * as types from "../constants";

const initialState = {
  unit: []
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case types.INIT_SOP_ADDRESS: {
      return {
        ...state,
        unit: action.data
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
