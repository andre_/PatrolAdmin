import { connect } from "react-redux";
import SopComponent from "../components";
import { initSopAddress } from "../actions";

const mapStateToProps = state => ({
  posts: state.posts,
  sop: state.sop
});

const mapDispatchToProps = dispatch => ({
  doInitSopAddress(data) {
    dispatch(initSopAddress(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SopComponent);
