import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#f4f4f4"
  },
  header: {
    height: 200,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(0, 0, 0, 0.06)",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 24,
    shadowOpacity: 1
  },
  imageWrap: {
    width: 150,
    alignSelf: "center"
  },
  image: {
    alignSelf: "center",
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  },
  form: {
    width: 300,
    height: 300,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 6,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    marginTop: 20
  },
  inputWrap: {
    height: 80,
    marginBottom: 10
  },
  text: {
    fontFamily: "OpenSans-Regular"
  },
  buttonWrap: {
    width: 300,
    alignSelf: "center",
    marginTop: 20
  },
  button: {
    height: 50,
    backgroundColor: "#2b3a93",
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    borderWidth: 1,
    borderColor: "#707070",
    borderRadius: 4,
    marginTop: 8
  },
  btnModal: {
    height: 40,
    justifyContent: "center"
  },
  label: {
    fontFamily: "OpenSans-Regular"
  },
  modalBottom: {
    backgroundColor: colors.light,
    borderRadius: 4,
    padding: 20,
    width: "100%",
    height: "50%",
    position: "absolute",
    bottom: 0
  },
  list: {
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: colors.text,
    height: 40
  }
};

export default styles;
