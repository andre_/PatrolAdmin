import React, { Component, Fragment } from "react";
import {
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  BackHandler
} from "react-native";
import Modal from "react-native-modal";
import { API } from "@src/services/APIService";
import { colors } from "@styles";
import styles from "../styles";

class SopComponent extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    roomNumber: "",
    address: "",
    type: null,
    nameType: null,
    showTypeSelect: false,
    unitType: []
  };

  componentDidMount() {
    this.getUnitType();

    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  getUnitType = () => {
    const {
      posts: { posts }
    } = this.props;

    API()
      .get(`inspection/types?post_id=${posts.post_id}`)
      .then(res => {
        console.log("inspection data", res.data.data);
        this.setState({ unitType: res.data.data });
      })
      .catch(err => {
        console.log(err);
      });
  };

  setSopUnit = () => {
    const { doInitSopAddress } = this.props;
    const { roomNumber, address, type } = this.state;

    if (roomNumber === "" || address === "") {
      alert("Please complete the form");
    } else {
      doInitSopAddress({ roomNumber, address, type });
      this.props.navigation.navigate("SopDetail");
    }
  };

  showSelect = () => {
    this.setState({ showTypeSelect: !this.state.showTypeSelect });
  };

  closeSelect = () => {
    this.setState({ showTypeSelect: false });
  };

  selectUnit = v => {
    this.setState({
      type: v,
      showTypeSelect: false
    });
  };

  render() {
    return (
      <Fragment>
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.imageWrap}>
              <Image
                source={require("../../../assets/logo.png")}
                style={styles.image}
              />
            </View>
          </View>

          <View style={styles.form}>
            <View style={styles.inputWrap}>
              <Text style={styles.label}>No. Rumah</Text>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                onChangeText={roomNumber => this.setState({ roomNumber })}
              />
            </View>
            <View style={styles.inputWrap}>
              <Text style={styles.label}>Alamat</Text>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                onChangeText={address => this.setState({ address })}
              />
            </View>
            <View style={styles.inputWrap}>
              <Text style={styles.label}>Type</Text>
              <TouchableOpacity
                style={styles.btnModal}
                onPress={this.showSelect}
              >
                <Text
                  style={[
                    styles.text,
                    { color: colors.text, textDecorationLine: "underline" }
                  ]}
                >
                  {this.state.type === null
                    ? "Select type"
                    : this.state.type.type}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.buttonWrap}>
            <TouchableOpacity style={styles.button} onPress={this.setSopUnit}>
              <Text
                style={[
                  styles.text,
                  {
                    fontSize: 16,
                    fontFamily: "OpenSans-SemiBold",
                    color: colors.light
                  }
                ]}
              >
                Next
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <Modal
          isVisible={this.state.showTypeSelect}
          onBackButtonPress={this.showSelect}
          onBackdropPress={this.showSelect}
          onSwipe={this.closeSelect}
          swipeDirection="down"
        >
          <ScrollView contentContainerStyle={styles.modalBottom}>
            {this.state.unitType.map((v, k) => (
              <TouchableOpacity
                style={styles.list}
                key={k}
                onPress={() => this.selectUnit(v)}
              >
                <Text style={[styles.text, { color: colors.text }]}>
                  {v.type}
                </Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </Modal>
      </Fragment>
    );
  }
}

export default SopComponent;
