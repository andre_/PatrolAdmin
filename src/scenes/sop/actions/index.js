import * as types from "../constants";

export const initSopAddress = data => ({
  type: types.INIT_SOP_ADDRESS,
  data
});
