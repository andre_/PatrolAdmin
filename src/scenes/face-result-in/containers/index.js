import { connect } from "react-redux";
import { checkinGuard } from "../actions";
import FaceResultIn from "../components";

const mapStateToProps = state => ({
  account: state.account
});

const mapDispatchToProps = dispatch => ({
  doCheckinGuard(data, img, post_id) {
    dispatch(checkinGuard(data, img, post_id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FaceResultIn);
