import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image
} from "react-native";
import { connect } from "react-redux";
import LottieView from "lottie-react-native";
import { Placeholder, PlaceholderLine, Shine } from "rn-placeholder";
import moment from "moment";
import Api from "../../../api";

import { colors } from "@styles";

const bg = require("../../../assets/bg.jpg");
const check = require("../../../assets/confirmation.png");
const uncheck = require("../../../assets/cancel.png");
const loader = require("../../../assets/lottie/loader.json");

const mapStateToProps = state => ({
  posts: state.posts
});

class FaceResultIn extends React.Component {
  state = {
    loading: false,
    success: false,
    data: null,
    latitude: 0,
    longitude: 0
  };

  componentDidMount() {
    this.checkAI();
    this._initLocation();
  }

  _initLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          coordinate: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
        });
      },
      error => {
        console.log("error get location");
      },
      {}
    );
  };

  generateRandomString = () => {
    return Math.random()
      .toString(36)
      .substr(2, 15);
  };

  checkAI = () => {
    const {
      posts: { posts }
    } = this.props;
    const { latitude, longitude } = this.state;
    let image = this.props.navigation.getParam("image");
    let isCheckin = this.props.navigation.getParam("isCheckin");

    const fd = new FormData();
    fd.append("post_id", posts.post_id);
    fd.append("gps", `${latitude},${longitude}`);
    fd.append("deviceid", "0");
    fd.append("face", {
      name: `${this.generateRandomString()}.jpg`,
      type: "image/jpg",
      uri: image.uri
    });

    if (isCheckin) {
      this.setState({ loading: true });
      console.log("checkin in");

      Api.checkinAI(fd)
        .then(result => {
          this.setState({
            data: result.data.data,
            loading: false
          });
          console.log(result.data.data);
        })
        .catch(err => {
          console.log("data", fd);
          console.log("error", err.response.data);
          this.setState({ loading: false });
        });
    } else {
      this.setState({ loading: true });
      console.log("checkin out");

      Api.checkoutAI(fd)
        .then(result => {
          this.setState({
            data: result.data.data,
            loading: false
          });
          console.log(result.data.data);
        })
        .catch(err => {
          console.log("data", fd);
          console.log("error", err.response.data);
          this.setState({ loading: false });
        });
    }
  };

  render() {
    let isCheckin = this.props.navigation.getParam("isCheckin");
    const { loading, data } = this.state;

    return (
      <ImageBackground style={styles.container} source={bg}>
        {loading ? (
          <View style={styles.header}>
            <Text
              style={[
                styles.text,
                {
                  fontSize: 18,
                  fontFamily: "OpenSans-Bold",
                  color: colors.dark,
                  marginBottom: 4
                }
              ]}
            >
              Please wait
            </Text>
            <Text style={[styles.text, { fontSize: 14 }]}>
              We still inspecting your face
            </Text>
          </View>
        ) : data !== null ? (
          <View style={styles.header}>
            {isCheckin ? (
              <Text style={[styles.text, styles.textWelcome]}>YEAY!</Text>
            ) : (
              <Text style={[styles.text, styles.textWelcome]}>BYE!</Text>
            )}

            {isCheckin ? (
              <Text style={[styles.text, { fontSize: 14 }]}>
                Welcome,{" "}
                <Text
                  style={{
                    color: colors.success,
                    fontFamily: "OpenSans-Bold",
                    textDecorationLine: "underline"
                  }}
                >
                  {data.user.name}
                </Text>
              </Text>
            ) : (
              <Text style={[styles.text, { fontSize: 14 }]}>
                Good bye,{" "}
                <Text
                  style={{
                    color: colors.danger,
                    fontFamily: "OpenSans-Bold",
                    textDecorationLine: "underline"
                  }}
                >
                  {data.user.name}
                </Text>
              </Text>
            )}
          </View>
        ) : (
          <View style={styles.header}>
            <Text style={[styles.text, styles.textWelcome]}>Sorry!</Text>
            <Text style={[styles.text, { fontSize: 14 }]}>
              We can't recognize your face
            </Text>
          </View>
        )}

        <View style={styles.loader}>
          {loading ? (
            <LottieView
              source={loader}
              autoPlay
              loop
              style={{ width: 200, height: 200 }}
            />
          ) : data !== null ? (
            <Image source={check} style={styles.images} />
          ) : (
            <Image source={uncheck} style={styles.images} />
          )}
        </View>

        <View style={styles.bottomWrapper}>
          {loading ? (
            <View style={styles.placeholder}>
              <Placeholder>
                <PlaceholderLine Animation={Shine} />

                <PlaceholderLine width={50} Animation={Shine} />
              </Placeholder>
            </View>
          ) : data !== null ? (
            <View style={styles.wrapper}>
              {isCheckin ? (
                <Text style={[styles.text, styles.textCheckin]}>
                  CHECKIN SUCCESS
                </Text>
              ) : (
                <Text style={[styles.text, styles.textCheckin]}>
                  CHECKOUT SUCCESS
                </Text>
              )}
              <Text style={[styles.text, styles.textTime]}>
                {moment.unix(data.attendance.checkin_time).format("hh:mm:ss")}
              </Text>
              <Text style={[styles.text, { fontSize: 16 }]}>
                {data.post.name}
              </Text>
            </View>
          ) : (
            <View style={styles.wrapper}>
              <Text style={[styles.text, styles.textCheckin]}>
                CHECKIN FAIL
              </Text>
              <Text style={[styles.text, styles.textTime]}>-</Text>
              <Text style={[styles.text, { fontSize: 16 }]}>-</Text>
            </View>
          )}
        </View>

        {loading ? null : data !== null ? (
          <TouchableOpacity
            style={[
              styles.btnSubmit,
              { backgroundColor: isCheckin ? colors.success : colors.danger }
            ]}
            onPress={() => this.props.navigation.popToTop()}
          >
            <Text
              style={[
                styles.text,
                {
                  fontFamily: "OpenSans-Bold",
                  color: colors.light,
                  fontSize: 16
                }
              ]}
            >
              OK
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.btnFail}
            onPress={() => this.props.navigation.goBack()}
          >
            <Text
              style={[
                styles.text,
                {
                  fontFamily: "OpenSans-Bold",
                  color: colors.light,
                  fontSize: 16
                }
              ]}
            >
              TRY AGAIN
            </Text>
          </TouchableOpacity>
        )}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  header: {
    flexDirection: "column",
    height: 120,
    marginBottom: 15,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  text: {
    color: colors.dark,
    fontFamily: "OpenSans-Regular"
  },
  textTime: {
    fontSize: 18,
    marginBottom: 10,
    color: colors.success,
    fontFamily: "OpenSans-Bold"
  },
  textWelcome: {
    fontSize: 18,
    fontFamily: "OpenSans-Bold",
    color: colors.dark,
    marginBottom: 4
  },
  textCheckin: {
    color: colors.dark,
    fontFamily: "OpenSans-Bold",
    fontSize: 18,
    marginBottom: 10
  },
  loader: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    width: 200,
    height: 200
  },
  bottomWrapper: {
    alignSelf: "center",
    alignItems: "center"
  },
  btnFail: {
    borderRadius: 4,
    elevation: 1,
    marginTop: 30,
    width: 160,
    height: 50,
    backgroundColor: colors.danger,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  btnSubmit: {
    borderRadius: 4,
    elevation: 1,
    marginTop: 30,
    width: 160,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  placeholder: {
    width: 250,
    height: 50,
    alignSelf: "center"
  },
  images: {
    width: "100%",
    height: "100%"
  },
  wrapper: {
    alignItems: "center"
  }
});

export default connect(mapStateToProps)(FaceResultIn);
