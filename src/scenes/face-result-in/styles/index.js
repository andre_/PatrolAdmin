import { StyleSheet } from "react-native";
import { colors } from "@styles";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.info,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginBottom: 10,
    fontSize: 20,
    fontFamily: "OpenSans-Regular"
  },
  btnWrapper: {
    justifyContent: "center",
    alignItems: "center",
    height: 140
  },
  lottieWrapper: {
    width: 120,
    height: 120,
    marginBottom: 25
  },
  lottieSuccessWrapper: {
    width: 180,
    height: 180,
    marginBottom: 0
  },
  alreadyCheckinWrapper: {
    height: 40,
    justifyContent: "center"
  },
  images: {
    width: "100%",
    height: "100%"
  }
});

export default styles;
