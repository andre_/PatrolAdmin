import { API } from "@src/services/APIService";
import { initAccount } from "@src/actions/accountActions";

const generateRandomName = () => {
  return Math.random()
    .toString(36)
    .substr(2, 5);
};

export const checkinGuard = (data, img, post_id) => {
  return dispatch => {
    dispatch({ type: "CHECKIN_REQUEST" });

    API()
      .post("checkin", data)
      .then(res => {
        dispatch({ type: "CHECKIN_FULFILLED" });
        if (parseInt(res.data.success)) {
          dispatch(doUploadImage(img, res.data.id, post_id));
        }
      })
      .catch(err => {
        alert("Error checkin");
        dispatch({ type: "CHECKIN_REJECTED" });
      });
  };
};

export const doUploadImage = (data, id, post_id) => {
  return dispatch => {
    dispatch({ type: "UPLOAD_FACE_REQUEST" });

    const obj = new FormData();
    obj.append("attendance_id", id);
    obj.append("image", {
      uri: data.uri,
      type: "image/jpeg",
      name: data.fileName === undefined ? generateRandomName() : data.fileName
    });

    API()
      .post("send_image_attendance", obj)
      .then(res => {
        dispatch({ type: "UPLOAD_FACE_SUCCESS" });
        dispatch(accountRequest(post_id));
      })
      .catch(err => {
        dispatch({ type: "UPLOAD_FACE_REJECTED" });
      });
  };
};

export const cleanGuard = () => ({
  type: types.CLEAN_GUARD
});

export const accountRequest = id => {
  return dispatch => {
    API()
      .post("get_post_detail", { post_id: id })
      .then(res => {})
      .catch(err => {
        console.log("ERR REQUEST ACCOUNT", err);
      });
  };
};
