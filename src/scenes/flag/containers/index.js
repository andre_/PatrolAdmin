import { connect } from "react-redux";
import Flag from "../components";
import {
  setManualVisitor,
  setImagePicker,
  setAttendanceFace,
  setAttendanceCamera,
  setAttendanceType,
  setThreshold,
  setSendBackgroundClocking
} from "../actions";

const mapStateToProps = state => ({
  flag: state.flag
});

const mapDispatchToProps = dispatch => ({
  setVisitor(data) {
    dispatch(setManualVisitor(data));
  },

  setImagePicker(data) {
    dispatch(setImagePicker(data));
  },

  setAttendanceFace(data) {
    dispatch(setAttendanceFace(data));
  },

  setAttendanceType(data) {
    dispatch(setAttendanceType(data));
  },

  setCamera(data) {
    dispatch(setAttendanceCamera(data));
  },

  doSetThreshold(data) {
    dispatch(setThreshold(data));
  },

  setBackground(data) {
    dispatch(setSendBackgroundClocking(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Flag);
