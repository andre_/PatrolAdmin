const SET_MANUAL_VISITOR = "SET_MANUAL_VISITOR";
const SET_ATTENDANCE_FACE = "SET_ATTENDANCE_FACE";
const SET_ATTENDANCE_CAMERA = "SET_ATTENDANCE_CAMERA";
const SET_ATTENDANCE_TYPE = "SET_ATTENDANCE_TYPE";
const SET_IMAGE_PICKER = "SET_IMAGE_PICKER";
const SET_THRESHOLD = "SET_THRESHOLD";
const SET_SEND_BACKGROUND_CLOCKING = "SET_SEND_BACKGROUND_CLOCKING";

export const setManualVisitor = data => ({
  type: SET_MANUAL_VISITOR,
  data
});

export const setAttendanceFace = data => ({
  type: SET_ATTENDANCE_FACE,
  data
});

export const setAttendanceCamera = data => ({
  type: SET_ATTENDANCE_CAMERA,
  data
});

export const setAttendanceType = data => ({
  type: SET_ATTENDANCE_TYPE,
  data
});

export const setImagePicker = data => ({
  type: SET_IMAGE_PICKER,
  data
});

export const setThreshold = data => ({
  type: SET_THRESHOLD,
  data
});

export const setSendBackgroundClocking = data => ({
  type: SET_SEND_BACKGROUND_CLOCKING,
  data
});
