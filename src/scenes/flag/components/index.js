import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  BackHandler,
  Slider
} from "react-native";
import { List, ListItem, Radio } from "native-base";
import { colors, configs } from "@styles";

export default class Flag extends Component {
  static navigationOptions = () => ({
    headerStyle: {
      backgroundColor: colors.info
    },
    headerTintColor: colors.light,
    headerTitle: (
      <Text style={[configs.titleHeader, { fontSize: 14 }]}>FEATURES FLAG</Text>
    ),
    headerRight: <View />
  });

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  handleBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    const {
      flag: {
        usingManual,
        usingFace,
        usingImagePicker,
        attendanceType,
        attendanceCameraVersion,
        threshold,
        sendBackground
      },
      setVisitor,
      setImagePicker,
      setAttendanceFace,
      setAttendanceType,
      setCamera,
      doSetThreshold,
      setBackground
    } = this.props;

    return (
      <ScrollView>
        <List>
          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>Manual Checkout</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={!usingManual}
              style={styles.radio}
              onPress={() => setVisitor(false)}
            />
            <Text>False</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={usingManual}
              style={styles.radio}
              onPress={() => setVisitor(true)}
            />
            <Text>True</Text>
          </ListItem>

          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>Face Recognition</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={!usingFace}
              style={styles.radio}
              onPress={() => setAttendanceFace(false)}
            />
            <Text>Off</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={usingFace}
              style={styles.radio}
              onPress={() => setAttendanceFace(true)}
            />
            <Text>On</Text>
          </ListItem>

          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>Send Background Clocking</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={sendBackground === true}
              style={styles.radio}
              onPress={() => setBackground(true)}
            />
            <Text>True</Text>
          </ListItem>

          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={sendBackground === false}
              style={styles.radio}
              onPress={() => setBackground(false)}
            />
            <Text>False</Text>
          </ListItem>

          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>Attendance Camera Version</Text>
          </ListItem>

          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={attendanceCameraVersion === "v1"}
              style={styles.radio}
              onPress={() => setCamera("v1")}
            />
            <Text>v1</Text>
          </ListItem>

          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={attendanceCameraVersion === "v2"}
              style={styles.radio}
              onPress={() => setCamera("v2")}
            />
            <Text>v2</Text>
          </ListItem>

          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>Attendance Mode</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={attendanceType === "manual"}
              style={styles.radio}
              onPress={() => setAttendanceType("manual")}
            />
            <Text>Manual</Text>
          </ListItem>

          {/* <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={attendanceType === "ai"}
              style={styles.radio}
              onPress={() => setAttendanceType("AI")}
            />
            <Text>AI</Text>
          </ListItem> */}

          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={attendanceType === "nfc"}
              style={styles.radio}
              onPress={() => setAttendanceType("nfc")}
            />
            <Text>NFC</Text>
          </ListItem>

          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>
              Recognition Threshold {Math.floor(threshold)}
            </Text>
          </ListItem>
          <ListItem>
            <Slider
              minimumValue={0}
              maximumValue={100}
              style={{ width: "100%" }}
              value={Math.floor(threshold)}
              onValueChange={value =>
                this.props.doSetThreshold(Math.floor(value))
              }
            />
          </ListItem>

          <ListItem itemDivider style={styles.divider}>
            <Text style={styles.textDivider}>Image Picker</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={!usingImagePicker}
              style={styles.radio}
              onPress={() => setImagePicker(false)}
            />
            <Text>False</Text>
          </ListItem>
          <ListItem>
            <Radio
              color={"#f0ad4e"}
              selectedColor={"#5cb85c"}
              selected={usingImagePicker}
              style={styles.radio}
              onPress={() => setImagePicker(true)}
            />
            <Text>True</Text>
          </ListItem>
        </List>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  divider: {
    backgroundColor: "#d1d1d1"
  },
  textDivider: {
    fontFamily: "OpenSans-Regular",
    color: colors.dark
  },
  radio: {
    marginRight: 10
  }
});
