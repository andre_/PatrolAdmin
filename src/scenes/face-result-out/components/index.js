import React, { Component, Fragment } from "react";
import { View, Text, Image } from "react-native";
import { API } from "@src/services/APIService";
import Button from "@src/commons/button";
import { colors } from "@styles";

import styles from "../styles";

const accountIcon = require("../../../assets/icons/user-account-icon.png");

function Wrapper({ post, user, children }) {
  return (
    <Fragment>
      <View
        style={[
          styles.container,
          {
            backgroundColor: colors.danger
          }
        ]}
      >
        <View style={styles.lottieWrapper}>
          <Image source={accountIcon} style={styles.images} />
        </View>

        <Fragment>
          <Text
            style={[
              styles.text,
              {
                fontFamily: "Montserrat-Bold",
                color: colors.light
              }
            ]}
          >
            {user.name}
          </Text>

          <Text style={[styles.text, { fontSize: 18, color: colors.light }]}>
            {post.name}
          </Text>
        </Fragment>
      </View>

      {children}
    </Fragment>
  );
}

class FaceResultOut extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    loading: false,
    isAlreadyCheckin: false
  };

  doCheckin = () => {
    this.props.navigation.popToTop();
  };

  goBack = () => {
    this.props.navigation.navigate("Home");
  };

  render() {
    const user = this.props.navigation.getParam("user");
    const post = this.props.navigation.getParam("post");

    return (
      <Fragment>
        <Wrapper post={post} user={user}>
          <View
            style={[
              styles.btnWrapper,
              {
                backgroundColor: this.state.isAlreadyCheckin
                  ? colors.light
                  : colors.info
              }
            ]}
          >
            <Fragment>
              <Button
                isLoading={this.state.loading}
                onPress={this.doCheckin}
                title="FINISH"
                textStyle={{ color: colors.text }}
                style={{ marginBottom: 15 }}
              />

              <Button
                title="Its not me"
                type="info"
                onPress={() => this.props.navigation.goBack()}
              />
            </Fragment>
          </View>
        </Wrapper>
      </Fragment>
    );
  }
}

export default FaceResultOut;
