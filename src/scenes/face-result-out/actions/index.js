import { API } from "@src/services/APIService";
import store from "react-native-simple-store";

const generateRandomName = () => {
  return Math.random()
    .toString(36)
    .substr(2, 5);
};

export const checkoutGuard = (data, img, post_id) => {
  return dispatch => {
    dispatch({ type: "CHECKOUT_REQUEST" });

    API()
      .post("checkout", data)
      .then(res => {
        if (parseInt(res.data.success)) {
          dispatch({ type: "CHECKOUT_FULFILLED" });
          dispatch(doUploadImage(img, res.data.id, post_id));
        }
      })
      .catch(err => {
        dispatch({ type: "CHECKOUT_FULFILLED" });
        dispatch({ type: "CLEAN_GUARD" });

        // store
        //   .push("checkout", {
        //     data,
        //     img,
        //     post_id
        //   })
        //   .then(() => store.get("checkout"))
        //   .then(checkout => {})
        //   .catch(err => {
        //     console.log(err);
        //   });
      });
  };
};

export const doUploadImage = (image, id, post_id) => {
  return dispatch => {
    dispatch({ type: "UPLOAD_FACE_REQUEST" });

    const obj = new FormData();
    obj.append("attendance_id", id);
    obj.append("image", {
      uri: image.uri,
      type: "image/jpeg",
      name: image.fileName === undefined ? generateRandomName() : image.fileName
    });

    API()
      .post("send_image_attendance", obj)
      .then(res => {
        dispatch({ type: "UPLOAD_FACE_FULFILLED" });
        dispatch({ type: "CLEAN_GUARD" });
      })
      .catch(err => {
        dispatch({ type: "UPLOAD_FACE_REJECTED" });
        console.log(err);
      });
  };
};
