import { connect } from "react-redux";
import { checkoutGuard } from "../actions";
import FaceResultOut from "../components";

const mapStateToProps = state => ({
  account: state.account
});

const mapDispatchToProps = dispatch => ({
  doCheckout(data, img, post_id) {
    dispatch(checkoutGuard(data, img, post_id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FaceResultOut);
