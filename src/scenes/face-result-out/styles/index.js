import { StyleSheet } from "react-native";
import { colors } from "@styles";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.danger,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginBottom: 10,
    fontFamily: "Montserrat-Regular",
    color: colors.light
  },
  btnWrapper: {
    backgroundColor: colors.danger,
    justifyContent: "center",
    alignItems: "center",
    height: 140
  },
  lottieWrapper: {
    width: 120,
    height: 120,
    marginBottom: 25
  },
  images: {
    width: "100%",
    height: "100%"
  }
});

export default styles;
