import { connect } from "react-redux";
import ExpiredComponent from "../components";

const mapStateToProps = state => {
  return {
    posts: state.posts
  };
};

export default connect(mapStateToProps)(ExpiredComponent);
