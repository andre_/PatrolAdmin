import React, { Component, Fragment } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { API } from "@src/services/APIService";
import { colors } from "@styles";

class ExpiredComponent extends Component {
  constructor(props) {
    super(props);
    this.refreshPage = this.refreshPage.bind(this);
  }

  refreshPage() {
    const {
      posts: { posts }
    } = this.props;

    const obj = {
      post_id: posts.post_id
    };

    API()
      .post("get_post_detail", obj)
      .then(res => {
        if (!res.data.expired) {
          this.props.navigation.navigate("Home");
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <View style={[styles.container, styles.wrapper]}>
        <TouchableOpacity
          style={styles.buttonRefresh}
          onPress={this.refreshPage}
        >
          <Image
            source={require("../../../assets/refresh.png")}
            style={styles.refresh}
          />
        </TouchableOpacity>
        <View style={styles.imageLogo}>
          <Image
            source={require("../../../assets/wallet.png")}
            style={styles.image}
          />
        </View>
        <Text style={styles.text}>Expiration Notice</Text>

        <View>
          <Text style={styles.description}>
            To keep using this app, please contact to the customer service.
          </Text>
        </View>

        <View>
          <Text style={styles.information}>Please check your bill again</Text>
        </View>

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Activate</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light
  },
  wrapper: {
    padding: 20
  },
  imageLogo: {
    width: 108,
    height: 108,
    alignSelf: "center",
    marginTop: 77
  },
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 20,
    width: 164,
    height: 20,
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0,
    textAlign: "left",
    color: "#ff4444"
  },
  button: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 200,
    height: 46,
    borderRadius: 8,
    backgroundColor: colors.info,
    position: "absolute",
    bottom: 30,
    alignSelf: "center"
  },
  buttonText: {
    height: 13,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: "left",
    color: "#ffffff"
  },
  description: {
    alignSelf: "center",
    width: 272,
    height: 50,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "300",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: "left",
    color: "#a2a2a2"
  },
  information: {
    width: 272,
    height: 13,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "300",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: "left",
    color: "#a2a2a2",
    alignSelf: "center"
  },
  refresh: {
    width: 30,
    height: 30
  },
  buttonRefresh: {
    position: "absolute",
    top: 20,
    right: 20
  }
});

export default ExpiredComponent;
