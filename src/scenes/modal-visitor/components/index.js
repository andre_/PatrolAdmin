import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { colors } from "@styles";

class ModalVisitor extends Component {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    let self = this;
    this.timeout = setTimeout(() => {
      self.props.navigation.popToTop();
    }, 1000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>VISITOR SUCCESSFULLY CHECKIN</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.info,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontFamily: "OpenSans-SemiBold",
    color: colors.light,
    fontSize: 16
  }
});

export default ModalVisitor;
