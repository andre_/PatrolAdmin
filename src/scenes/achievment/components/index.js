import React, { Component } from "react";
import { ScrollView, View, Text } from "react-native";
import { configs, colors } from "@styles";
import styles from "../styles";

class Achievment extends Component {
  static navigationOptions = {
    headerTitle: <Text style={configs.titleHeader}>Achivement</Text>,
    headerTitleStyle: {
      color: colors.info,
      textAlign: "center",
      elevation: 0
    },
    headerTintColor: colors.info,
    headerStyle: {
      backgroundColor: colors.info
    },
    headerRight: (
      <View>
        <View style={styles.label}>
          <Text style={configs.textWhite}>120 pts</Text>
        </View>
      </View>
    )
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.header}>
          <Text>Loader</Text>
        </View>
      </ScrollView>
    );
  }
}

export default Achievment;
