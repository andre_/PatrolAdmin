import { colors } from "@styles";

const styles = {
  label: {
    backgroundColor: colors.warning,
    width: 80,
    height: 35,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    marginRight: 15
  },
  header: {
    alignItems: "center",
    justifyContent: "center",
    height: 140,
    backgroundColor: colors.info
  }
};

export default styles;
