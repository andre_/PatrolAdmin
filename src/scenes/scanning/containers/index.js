import { connect } from "react-redux";
import ScanningComponent from "../components";

const mapStateToProps = state => ({
  flag: state.flag
});

export default connect(mapStateToProps)(ScanningComponent);
