import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { API } from "@src/services/APIService";
import { DotIndicator } from "react-native-indicators";
import { colors } from "@styles";

import styles from "../styles";

const closeIcon = require("../../../assets/icons/cancel-icon.png");

const notFound = require("../../../assets/icons/static-cancel.png");
const scanning = require("../../../assets/body-scan.png");

class ScanningComponent extends Component {
  state = {
    found: true
  };

  componentDidMount() {
    this.compareFace();
  }

  generateRandomString = () => {
    return Math.random()
      .toString(36)
      .substr(2, 5);
  };

  compareFace = () => {
    const {
      flag: { threshold }
    } = this.props;

    const picture = this.props.navigation.getParam("picture");
    const isCheckin = this.props.navigation.getParam("isCheckin");
    const fd = new FormData();
    fd.append("guardImage", {
      name: this.generateRandomString(),
      type: "image/jpeg",
      uri: picture.uri
    });

    API()
      .post(`attendance_compare_face`, fd)
      .then(res => {
        console.log(res.data);
        console.log("res", res);
        console.log("thresold", this.props.flag);
        if (
          res.data.message === "No guard found." ||
          res.data.message === "No guard ID given." ||
          res.data.message === "No match found." ||
          res.data.error
        ) {
          this.setState({
            found: false
          });
        } else {
          if (isCheckin) {
            this.props.navigation.navigate("FaceResultIn", {
              guard: res.data.data.guard,
              imageGuard: picture
            });
          } else {
            this.props.navigation.navigate("FaceResultOut", {
              guard: res.data.data.guard,
              imageGuard: picture
            });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  navigateToManual = () => {
    const isCheckin = this.props.navigation.getParam("isCheckin");

    if (isCheckin) {
      this.props.navigation.navigate("HomeCheckin");
    } else {
      this.props.navigation.navigate("HomeCheckout");
    }
  };

  takeAgain = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { found } = this.state;
    const {
      flag: { threshold }
    } = this.props;

    return (
      <React.Fragment>
        <TouchableOpacity
          style={[styles.closeWrapper, { zIndex: 1000 }]}
          onPress={this.takeAgain}
        >
          <Image source={closeIcon} style={styles.closeImg} />
        </TouchableOpacity>

        {found ? (
          <View style={styles.container}>
            <View style={styles.animationWrapper}>
              <Image source={scanning} style={styles.images} />
            </View>

            <View style={styles.textWrapper}>
              <Text style={styles.title}>Identify your face</Text>
              <Text style={[styles.desc, { textAlign: "center" }]}>
                Dont worry. This take a few seconds.
              </Text>

              <DotIndicator color={colors.info} size={10} />
            </View>
          </View>
        ) : (
          <View style={styles.container}>
            <View
              style={[styles.animationWrapper, { width: 100, height: 100 }]}
            >
              <Image source={notFound} style={styles.images} />
            </View>

            <View style={[styles.textWrapper]}>
              <Text style={styles.title}>Face not found</Text>
              <TouchableOpacity
                style={[styles.btnTakeAgain, { marginTop: 10 }]}
                onPress={this.takeAgain}
              >
                <Text style={[styles.desc, { color: colors.light }]}>
                  Take Again
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                marginTop: 10,
                maginBottom: 10,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={styles.title}>OR</Text>
            </View>

            <View style={[styles.textWrapper]}>
              <TouchableOpacity
                style={[
                  styles.btnTakeAgain,
                  { width: 120, backgroundColor: colors.info }
                ]}
                onPress={this.navigateToManual}
              >
                <Text style={[styles.desc, { color: colors.light }]}>
                  Checkin Manual
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </React.Fragment>
    );
  }
}

export default ScanningComponent;
