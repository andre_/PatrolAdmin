import { colors } from "@styles";

const styles = {
  container: {
    flex: 1,
    opacity: 0.5,
    alignItems: "center",
    justifyContent: "center"
  },
  animationWrapper: {
    width: 180,
    height: 180,
    marginBottom: 15
  },
  textWrapper: {
    height: 80,
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    fontSize: 18,
    fontFamily: "OpenSans-SemiBold",
    color: colors.dark,
    marginBottom: 5
  },
  desc: {
    fontFamily: "OpenSans-Regular"
  },
  closeWrapper: {
    position: "absolute",
    right: 20,
    top: 20,
    width: 20,
    height: 20
  },
  closeImg: {
    width: "100%",
    height: "100%"
  },
  btnTakeAgain: {
    backgroundColor: colors.danger,
    width: 100,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 40
  },
  images: {
    width: "100%",
    height: "100%"
  }
};

export default styles;
