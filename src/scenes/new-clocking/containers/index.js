import React, { Component } from "react";
import NewClocking from "../components";
import { connect } from "react-redux";

// import { initScannedCp, clear, submitClocking, sendTelegram } from "../actions";
import {
  initScannedCp,
  clear,
  submitClocking,
  submitTelegram
} from "../../../redux/ducks/clockingRedux";
import { stopSirene } from "@src/actions/notificationActions";
import { isLate } from "@src/actions/accountActions";

const mapStateToProps = state => ({
  account: state.account,
  patroling: state.patroling,
  checkpoint: state.checkpoint,
  notification: state.notification,
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  add(data) {
    dispatch(initScannedCp(data));
  },
  clear() {
    dispatch(clear());
  },
  save(data, copied) {
    dispatch(submitClocking(data, copied));
  },
  telegram(uri) {
    dispatch(submitTelegram(uri));
  },
  stopAlarm() {
    dispatch(stopSirene());
  },
  isLate(val) {
    dispatch(isLate(val));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewClocking);
