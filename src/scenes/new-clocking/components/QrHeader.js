import React, { Component } from "react";
import { TouchableOpacity, Image, StyleSheet } from "react-native";
import { connect } from "react-redux";

const qr = require("../../../assets/icons/qr.png");
const qrActive = require("../../../assets/icons/qr-active.png");

import { setToNfc, setToQr } from "@src/actions/scanningActions";

const mapStateToProps = state => ({
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  setToNfc() {
    dispatch(setToNfc());
  },

  setToQr() {
    dispatch(setToQr());
  }
});

class QrHeader extends Component {
  renderQrActive = () => (
    <TouchableOpacity
      style={styles.imageWrapper}
      onPress={() => this.props.setToNfc()}
    >
      <Image source={qrActive} style={styles.images} />
    </TouchableOpacity>
  );

  renderQr = () => (
    <TouchableOpacity
      style={styles.imageWrapper}
      onPress={() => this.props.setToQr()}
    >
      <Image source={qr} style={styles.images} />
    </TouchableOpacity>
  );

  render() {
    const {
      scanning: { method }
    } = this.props;

    return (
      <React.Fragment>
        {method === "nfc" ? this.renderQr() : this.renderQrActive()}
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  imageWrapper: {
    width: 30,
    height: 30,
    marginRight: 15
  },
  images: {
    width: "100%",
    height: "100%"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QrHeader);
