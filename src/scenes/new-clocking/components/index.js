import React, { Fragment, Component } from "react";
import {
  View,
  Text,
  ScrollView,
  BackHandler,
  Platform,
  Image,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { Icon, ListItem, Left, Right } from "native-base";
import { configs, colors, text } from "@styles";
import LottieView from "lottie-react-native";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import ViewShot from "react-native-view-shot";
import moment from "moment";
import axios from "axios";

import QrHeader from "./QrHeader";

import languange from "@src/constants/languange";
import styles from "../styles";

let SoundPlayer = require("react-native-sound");
let emptyImg = require("../../../assets/lottie/empty-box.json");
const bg = require("../../../assets/bg.jpg");
const qrCode = require("../../../assets/qr.png");
let completedSong = null;

const EmptyCheckpoint = () => (
  <ImageBackground
    source={bg}
    style={[
      styles.emptyCheckpoint,
      {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column"
      }
    ]}
  >
    <View style={styles.emptyImg}>
      <LottieView source={emptyImg} autoPlay />
    </View>
    <Text style={[styles.wrongImgText, { fontSize: 17 }]}>
      Checkpoint Not Found
    </Text>
  </ImageBackground>
);

class NewClocking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      theDate: null,
      hoursBase: null
    };

    completedSong = new SoundPlayer(
      "clockingcompleted.mp3",
      SoundPlayer.MAIN_BUNDLE,
      error => {
        if (error) {
          alert("error playing sound");
        }
      }
    );
  }

  static navigationOptions = {
    headerRight: <QrHeader />,
    headerStyle: {
      backgroundColor: colors.info,
      borderBottomWidth: 0,
      elevation: 0
    },
    headerTintColor: colors.light,
    headerLeft: null
  };

  componentDidMount() {
    this._checkingNfc();

    BackHandler.addEventListener("hardwareBackPress", this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
  }

  _checkPermission() {}

  _checkingNfc = () => {
    NfcManager.isSupported().then(supported => {
      if (supported) {
        // this._startNfc();
        this._startDetection();
      }
    });
  };

  handleBack = () => {
    this._stopDetection();
    this.props.navigation.goBack();
    return true;
  };

  _renderQrItem = () => {
    const {
      checkpoint: { checkpoint },
      patroling: { display }
    } = this.props;

    return checkpoint.map((v, k) => {
      const filtering = display.find(x => x.id === v.id) ? true : false;
      const filterData = display.find(x => x.id === v.id);

      return (
        <ListItem
          style={styles.listItem}
          key={k}
          onPress={() => this.props.navigation.navigate("QrFaster")}
        >
          <View
            style={{
              width: "100%",
              flexDirection: "column"
            }}
          >
            <Text style={styles.textBold}>{v.description}</Text>
            {filterData ? (
              <Text style={styles.textDate}>
                {moment.unix(filterData.time).format("HH:mm:ss DD/MM/YYYY")}
              </Text>
            ) : null}
          </View>
          <Right>
            {filtering ? (
              <View style={styles.listClocking}>
                <Icon
                  type="FontAwesome"
                  name="check"
                  style={{ color: "mediumseagreen" }}
                />
              </View>
            ) : (
              <View style={styles.listClocking}>
                <Image source={qrCode} />
              </View>
            )}
          </Right>
        </ListItem>
      );
    });
  };

  _renderListItem = () => {
    const {
      checkpoint: { checkpoint },
      patroling: { display }
    } = this.props;

    return checkpoint.map((v, k) => {
      const filtering = display.find(x => x.id === v.id) ? true : false;
      const filterData = display.find(x => x.id === v.id);

      return (
        <ListItem style={styles.listItem} key={k}>
          <Text style={styles.textBold}>{v.description}</Text>
          {filterData ? (
            <Text style={styles.textDate}>
              {moment.unix(filterData.time).format("HH:mm:ss DD/MM/YYYY")}
            </Text>
          ) : null}
          <Right>
            {filtering ? (
              <View style={styles.listClocking}>
                <Icon
                  type="FontAwesome"
                  name="check"
                  style={{ color: "mediumseagreen" }}
                />
              </View>
            ) : (
              <View style={styles.listClocking}>
                <Icon type="FontAwesome" name="check" color="mediumseagreen" />
              </View>
            )}
          </Right>
        </ListItem>
      );
    });
  };

  _playSound = () => {
    completedSong.setVolume(1.0);
    completedSong.play(sucess => {
      if (!sucess) {
        alert("error init sound player");
      }
    });
  };

  sendTelegramBro = () => {
    let self = this;
    this.refs.viewShot.capture().then(uri => {
      self.props.telegram(uri);
      self.props.clear();
    });
  };

  saveClocking = () => {
    const {
      patroling: { display, copied }
    } = this.props;

    this._stopDetection();

    this.sendTelegramBro();
    this.props.save(display, copied);
    this.props.isLate(false);

    this._playSound();

    this.props.navigation.goBack();
  };

  clearClock = () => {
    this.props.clear();
  };

  render() {
    const {
      account: {
        account: { post }
      },
      checkpoint: { checkpoint },
      patroling: { display },
      scanning: { method }
    } = this.props;

    return (
      <Fragment>
        {(checkpoint || []).length ? (
          <ScrollView
            style={{
              flex: 1,
              height: "100%",
              backgroundColor: colors.background
            }}
          >
            <ViewShot ref="viewShot" options={{ format: "jpg", quality: 0.9 }}>
              <View style={styles.header}>
                <Text style={styles.textHeader}>
                  {(display || []).length} / {(checkpoint || []).length}
                </Text>
                <Text style={configs.textClocking}>
                  {languange.pointclocking}
                </Text>
              </View>

              <View style={configs.content}>
                <View style={styles.content}>
                  <View>
                    {(display || []).length === (checkpoint || []).length ? (
                      <View
                        style={[
                          configs.notification,
                          { alignSelf: "center", width: "100%" }
                        ]}
                      >
                        <Text style={styles.textWhite}>
                          CLOCKING COMPLETED!
                        </Text>
                      </View>
                    ) : null}

                    <Text
                      style={[
                        text.spaceBottom,
                        text.h4,
                        { fontFamily: "JosefinSans-Bold", textAlign: "center" }
                      ]}
                    >
                      {(post || {}).name}
                    </Text>
                  </View>

                  {method === "nfc"
                    ? this._renderListItem()
                    : this._renderQrItem()}
                </View>

                <View style={[configs.spaceBetween, { marginTop: 20 }]}>
                  {(display || []).length === (checkpoint || []).length ? (
                    <TouchableOpacity
                      style={[styles.btn, styles.btnSuccess]}
                      onPress={this.saveClocking}
                    >
                      <Text style={{ color: colors.light }}>Finish</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={[styles.btn, styles.btnDanger]}
                      onPress={() =>
                        this.props.navigation.navigate("IncompleteReport")
                      }
                    >
                      <Text style={{ color: colors.light }}>End Clocking</Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ViewShot>
          </ScrollView>
        ) : (
          <EmptyCheckpoint />
        )}
      </Fragment>
    );
  }

  _startNfc = () => {
    NfcManager.start({
      onSessionClosedIOS: () => {}
    })
      .then(result => {})
      .catch(error => {
        alert("error");
      });

    if (Platform.OS === "android") {
      NfcManager.getLaunchTagEvent()
        .then(tag => {
          if (tag) {
            this.setState({ tag });
          }
        })
        .catch(err => {});

      NfcManager.isEnabled()
        .then(enabled => {
          this.setState({ enabled });
        })
        .catch(err => {});

      NfcManager.onStateChanged(event => {
        if (event.state === "on") {
          this.setState({ enabled: true });
        } else if (event.state === "off") {
          this.setState({ enabled: false });
        }
      })
        .then(sub => {
          this._stateChangedSubscription = sub;
        })
        .catch(err => {
          console.warn(err);
        });
    }
  };

  callTimestamp = () => {
    return new Promise((resolve, reject) => {
      axios({
        method: "GET",
        url: "https://my.trackerhero.com/api/timestamp",
        timeout: 1000 * 3,
        headers: {
          "Content-type": "application/json"
        }
      })
        .then(res => {
          resolve(res.data.timestamp);
          console.log("server");
        })
        .catch(err => {
          console.log("default");
          resolve(Math.floor(new Date().getTime() / 1000));
        });
    });
  };

  _findItem = id => {
    const {
      checkpoint: { checkpoint }
    } = this.props;

    let findCheckpoint = (checkpoint || []).find(v => v.id === parseInt(id))
      ? true
      : false;

    return findCheckpoint;
  };

  _onTagDiscoveredCopy = tag => {
    this.setState({ tag });
    let obj;
    let text = this._parseText(tag);

    if (this._findItem(text)) {
      this.props.stopAlarm();

      this.callTimestamp()
        .then(data => {
          obj = {
            id: parseInt(text),
            time: data,
            status: "1"
          };
          this.props.add(obj);
        })
        .catch(err => {
          alert("Error scanning");
        });
    } else {
      alert("Checkpoint not recognized");
    }
  };

  _startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscoveredCopy)
      .then(result => {})
      .catch(error => {
        console.warn("registerTagEvent fail", error);
      });
  };

  _stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {})
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  _parseText = tag => {
    if (tag.ndefMessage) {
      return NdefParser.parseText(tag.ndefMessage[0]);
    }
    return null;
  };
}

export default NewClocking;
