import * as types from "../constants";
import { API } from "@src/services/APIService";
import store from "react-native-simple-store";

export const initScannedCp = data => {
  return dispatch => {
    navigator.geolocation.getCurrentPosition(
      position => {
        data.gps = `${position.coords.latitude},${position.coords.longitude}`;
      },
      err => {
        console.log(err);
      },
      {}
    );

    dispatch({ type: types.INIT_SCANNED_CP, data });
  };
};

export const clear = () => ({
  type: "CLEAR_CP"
});

export const clearStorage = () => ({
  type: "CLEAR_STORAGE"
});

export const submitClocking = (data, copied) => {
  return dispatch => {
    dispatch({ type: types.SUBMIT_CLOCKING_REQUEST });

    API()
      .post(`save_clockings?checkpoint=${JSON.stringify(data)}`)
      .then(res => {
        console.log("hasil", res.data);
        dispatch({ type: types.SUBMIT_CLOCKING_FULFILLED });
      })
      .catch(err => {
        dispatch({ type: types.SUBMIT_CLOCKING_REJECTED });

        store
          .push("failedData", copied)
          .then(() => store.get("failedData"))
          .then(failedData => {
            dispatch({ type: "CLEAR_STORAGE" });
          })
          .catch(err => {
            alert("Fail to save to storage");
          });
      });
  };
};

export const sendTelegram = img => {
  return (dispatch, getState) => {
    let fd = new FormData();
    fd.append("image", {
      uri: img,
      name: `image-telegram-${new Date().toLocaleTimeString()}`,
      type: "image/jpeg"
    });

    fd.append("post_id", parseInt(getState().posts.posts.post_id));

    API()
      .post(`send_image_report`, fd)
      .then(res => {
        console.log("send telegram", res.data);
      })
      .catch(err => {
        console.log("err telegram", err);
        console.log("err telegram", err.response);
      });
  };
};

// export const initListClocking = () => {
//   return (dispatch, getState) => {
//     API()
//       .get(`get_list_checkpoint?post_id=${getState().posts.posts.post_id}`)
//       .then(res => {
//         dispatch({ type: types.INIT_LIST_CLOCKING, data: res.data });
//       })
//       .catch(err => {
//         console.log(err.response);
//       });
//   };
// };
