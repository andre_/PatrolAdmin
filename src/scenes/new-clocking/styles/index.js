import { colors } from "@styles";

const styles = {
  container: {
    flex: 1
  },
  header: {
    marginTop: -40,
    height: 200,
    backgroundColor: colors.info,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  textHeader: {
    fontSize: 60,
    color: colors.light,
    fontFamily: "OpenSans-Bold"
  },
  content: {
    position: "relative",
    zIndex: 100,
    marginTop: 10,
    padding: 15,
    paddingLeft: 0,
    backgroundColor: colors.light
  },
  btnFinish: {
    width: "100%",
    backgroundColor: colors.success,
    borderRadius: 20
  },
  textWhite: {
    color: colors.light
  },
  audioRecord: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row"
  },
  playlist: {
    borderWidth: 1,
    borderColor: "#f7f7f7",
    height: 40,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 6,
    paddingRight: 20
  },
  viewUpload: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderRadius: 4,
    width: 80,
    height: 80,
    marginRight: 10,
    borderColor: "#dadada",
    position: "relative"
  },
  listClocking: {
    borderRadius: 50,
    borderWidth: 1,
    width: 40,
    height: 40,
    borderWidth: 1,
    borderColor: 1
  },
  listItem: {
    display: "flex",
    justifyContent: "space-between"
  },
  btn: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    width: "100%",
    borderRadius: 4
  },
  btnDanger: {
    backgroundColor: colors.danger
  },
  btnSuccess: {
    backgroundColor: colors.success
  },
  modal: {
    justifyContent: "center",
    alignItems: "center",
    height: 300
  },
  wrongImg: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginBottom: 14
  },
  emptyImg: {
    width: 340,
    height: 140
  },
  wrongImgSrc: {
    width: "100%",
    height: "100%"
  },
  wrongImgText: {
    fontFamily: "OpenSans-SemiBold",
    fontSize: 16
  },
  textBold: {
    fontFamily: "OpenSans-SemiBold",
    color: colors.dark
  },
  textDate: {
    fontFamily: "OpenSans-Regular"
  }
};

export default styles;
