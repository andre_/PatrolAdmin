import * as types from "../constants";

const initialState = {
  scanned: [],
  display: [],
  copied: []
};

function getUnique(arr, comp) {
  const unique = arr
    .map(e => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
}

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case types.INIT_SCANNED_CP: {
      let scannedData = [...state.scanned, action.data];
      let displayData = [...state.display, action.data];
      let copiedData = [...state.copied, action.data];

      return {
        ...state,
        scanned: getUnique(scannedData, "id"),
        display: getUnique(displayData, "id"),
        copied: getUnique(copiedData, "id")
      };
    }

    case "CLEAR_CP": {
      return {
        ...state,
        scanned: [],
        display: []
      };
    }

    case "CLEAR_STORAGE": {
      return {
        ...state,
        copied: []
      };
    }

    default: {
      return state;
    }
  }
};

export default reducers;
