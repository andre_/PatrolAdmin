import { API } from "@src/services/APIService";
import * as types from "../constants";
import store from "react-native-simple-store";
import moment from "moment";
import { sendReport } from "@src/scenes/report-posts/actions";

export const collectClocking = data => {
  return dispatch => {
    navigator.geolocation.getCurrentPosition(
      position => {
        data.gps = `${position.coords.latitude},${position.coords.longitude}`;
        console.log("position", position);
      },
      err => {
        console.log(err);
      },
      {
        enableHighAccuracy: true
      }
    );
    dispatch({ type: types.INIT_CLOCKING, data });
    // if (
    // if (
    //   getState().clocking.clocking.length ===
    //   getState().checkpoint.checkpoint.length
    // ) {
    //   dispatch(saveClocking());
    // }
  };
};

export const addDummyCheckpoint = data => {
  return (dispatch, getState) => {
    if (getState().timeclock.type === "default") {
      data.clock = `${new Date().toLocaleTimeString()} - ${new Date().toLocaleDateString()}`;
    } else {
      API()
        .get("timestamp")
        .then(res => {
          let timestamp = moment
            .unix(res.data.timestamp)
            .format("HH:mm:ss - DD/MM/YYYY");
          data.clock = timestamp;
          dispatch({ type: types.ADD_DUMMY_CHECKPOINT, data });
        })
        .catch(err => {
          data.clock = `${new Date().toLocaleTimeString()} - ${new Date().toLocaleDateString()}`;
        });
    }
    dispatch({ type: types.ADD_DUMMY_CHECKPOINT, data });
  };
};

export const saveClocking = clocking => {
  return dispatch => {
    dispatch({ type: types.SAVE_CLOCKING_REQUEST });

    API()
      .post(`save_clockings?checkpoint=${JSON.stringify(clocking)}`)
      .then(res => {
        dispatch({ type: types.SAVE_CLOCKING_FULFILLED });
        dispatch({ type: types.END_CLOCKING });
      })
      .catch(err => {
        dispatch({ type: types.SAVE_CLOCKING_REJECTED });

        store
          .push("failedData", clocking)
          .then(() => store.get("failedData"))
          .then(failedData => {
            dispatch({ type: types.END_CLOCKING });
          })
          .catch(err => {
            alert("Fail to save to storage");
          });
      });
  };
};

export const saveEndClocking = (clocking, report, image, audio) => {
  return dispatch => {
    dispatch({ type: types.SAVE_END_REPORT_REQUEST });

    const fd = new FormData();
    fd.append("data", JSON.stringify(report));
    fd.append("clocking", JSON.stringify(clocking));

    image.map(x => {
      fd.append("attachments[]", {
        uri: x.uri,
        name: x.fileName,
        type: "image/jpeg"
      });
    });

    fd.append("attachments[]", {
      uri: audio,
      name: "test.aac",
      type: "audio/aac"
    });

    API()
      .post(`https://my.trackerhero.com/api/send_new_incident_v2`, fd)
      .then(res => {
        dispatch({ type: types.END_CLOCKING });
        dispatch({ type: types.SAVE_END_REPORT_FULFILLED });
      })
      .catch(err => {
        console.log("report err", err);
        dispatch({ type: types.SAVE_END_REPORT_REJECTED });
        store
          .push("packageOffline", {
            clocking,
            report,
            photos: image,
            audio
          })
          .then(() => store.get("packageOffline"))
          .then(packageOffline => {
            dispatch({ type: types.END_CLOCKING });
          })
          .catch(err => {
            console.log(err);
          });
      });
  };
};

export const sendTelegram = img => {
  return (dispatch, getState) => {
    let fd = new FormData();
    fd.append("image", {
      uri: img,
      name: `image-telegram-${new Date().toLocaleTimeString()}`,
      type: "image/jpeg"
    });
    fd.append("post_id", parseInt(getState().posts.posts.post_id));

    API()
      .post(`send_image_report`, fd)
      .then(res => {
        console.log("telegram", res.data);
      })
      .catch(err => {
        // store
        //   .push("screenshot", img)
        //   .then(() => store.get("screenshot"))
        //   .then(screenshot => {
        //     console.log(screenshot);
        //   })
        //   .catch(err => {
        //     console.log(err);
        //   });
      });
  };
};

export const finishClocking = () => {
  return dispatch => {
    dispatch({ type: types.END_CLOCKING });
    dispatch({ type: types.FINISH_CLOCKING });
  };
};

export const endClocking = () => ({
  type: types.END_CLOCKING
});

export const clearCache = () => ({
  type: types.CLEAR_CACHE
});
