import { connect } from "react-redux";
import {
  saveClocking,
  saveEndClocking,
  collectClocking,
  addDummyCheckpoint,
  sendTelegram,
  finishClocking,
  clearCache
} from "../actions";
import { stopSirene } from "@src/actions/notificationActions";
import { isLate } from "@src/actions/accountActions";
import ClockingComponent from "../components";

const mapStateToProps = state => ({
  account: state.account,
  clocking: state.clocking,
  checkpoint: state.checkpoint,
  connection: state.connection,
  notification: state.notification,
  loading: state.loading,
  posts: state.posts,
  scanning: state.scanning
});

const mapDispatchToProps = dispatch => ({
  addDummyCheckpoint(data) {
    dispatch(addDummyCheckpoint(data));
  },

  doSaveClocking(data) {
    dispatch(saveClocking(data));
  },

  doSaveEndClocking(clocking, report, image, audio) {
    dispatch(saveEndClocking(clocking, report, image, audio));
  },

  doCollectClocking(data) {
    dispatch(collectClocking(data));
  },

  doStopSirene() {
    dispatch(stopSirene());
  },

  doClearCache() {
    dispatch(clearCache());
  },

  doLate(data) {
    dispatch(isLate(data));
  },

  doSendTelegram(img) {
    dispatch(sendTelegram(img));
  },

  doFinishClocking() {
    dispatch(finishClocking());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClockingComponent);
