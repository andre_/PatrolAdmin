import React, { Fragment, Component } from "react";
import {
  PermissionsAndroid,
  View,
  Image,
  Text,
  Slider,
  ScrollView,
  BackHandler,
  Platform,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet,
  Alert
} from "react-native";
import {
  Button,
  Icon,
  ListItem,
  Right,
  Spinner,
  Item,
  Label
} from "native-base";
import PopupDialog, { DialogTitle } from "react-native-popup-dialog";
import { AudioRecorder, AudioUtils } from "react-native-audio";
import { configs, colors, text } from "@styles";
import languange from "@src/constants/languange";
import ImagePicker from "react-native-image-picker";
import NfcManager, { NdefParser } from "react-native-nfc-manager";
import Modal from "react-native-modal";
import RNFetchBlob from "rn-fetch-blob";
import ViewShot from "react-native-view-shot";

let SoundPlayer = require("react-native-sound");
let Sound = require("react-native-sound");
let negativeSong = null;
let positiveSong = null;
let completedSong = null;

const RtdType = {
  URL: 0,
  TEXT: 1
};

class ClockingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      albums: [],
      clocking: [],
      loading: false,
      loadingspin: false,
      loadingModal: false,
      loadingReport: false,
      ImageSource: null,
      supported: true,
      warning: false,
      enabled: false,
      isWriting: false,
      urlToWrite: "www.google.com",
      rtdType: RtdType.TEXT,
      parsedText: null,
      tag: {},
      position: {},
      description: null,
      hours: 0,
      minutes: 0,
      seconds: 0,
      showReport: false,
      active: false,
      recording: false,
      paused: false,
      audioPath: AudioUtils.DocumentDirectoryPath + "/test.mp3",
      song: false
    };

    this.getLoc = navigator.geolocation.watchPosition(
      position => {},
      err => {},
      {
        enableHighAccuracy: false,
        distanceFilter: 1
      }
    );

    positiveSong = new SoundPlayer(
      "positive.wav",
      SoundPlayer.MAIN_BUNDLE,
      error => {
        if (error) {
          alert("error playing sound");
        }
      }
    );

    negativeSong = new SoundPlayer(
      "negative.wav",
      SoundPlayer.MAIN_BUNDLE,
      error => {
        if (error) {
          alert("error playing sound");
        }
      }
    );

    completedSong = new SoundPlayer(
      "clockingcompleted.mp3",
      SoundPlayer.MAIN_BUNDLE,
      error => {
        if (error) {
          alert("error playing sound");
        }
      }
    );

    this._showModal = this._showModal.bind(this);
    this._closeModal = this._closeModal.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
    this._openCamera = this._openCamera.bind(this);
    this.closeReport = this.closeReport.bind(this);
    this.openReport = this.openReport.bind(this);
    this.playSoundRecord = this.playSoundRecord.bind(this);
    this._sendReportEndclocking = this._sendReportEndclocking.bind(this);
  }

  componentDidMount() {
    this._checkingNfc();
    this._checkAudioPermission();

    AudioRecorder.onProgress = data => {
      this.setState({
        currentTime: Math.floor(data.currentTime),
        second: Math.floor(data.currentTime)
      });
    };

    AudioRecorder.onFinished = data => {
      if (Platform.OS === "ios") {
        this._finishRecording(data.status === "OK", data.audioFileURL);
      }
    };

    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.getLoc);
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  prepareRecordingPath(audioPath) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000
    });
  }

  _checkPermission() {
    if (Platform.OS !== "android") {
      return Promise.resolve(true);
    }

    const rationale = {
      title: "Microphone Permission",
      message:
        "AudioExample needs access to your microphone so you can record audio."
    };

    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      rationale
    ).then(result => {
      return result === true || result === PermissionsAndroid.RESULTS.GRANTED;
    });
  }

  _checkAudioPermission() {
    this._checkPermission().then(hasPermission => {
      this.setState({ hasPermission });

      if (!hasPermission) return;

      this.prepareRecordingPath(this.state.audioPath);

      AudioRecorder.onProgress = data => {
        this.setState({
          currentTime: Math.floor(data.currentTime),
          second: Math.floor(data.currentTime)
        });
      };

      AudioRecorder.onFinished = data => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === "ios") {
          this._finishRecording(data.status === "OK", data.audioFileURL);
        }
      };
    });
  }

  async _record() {
    if (this.state.recording) {
      console.warn("Already recording!");
      return;
    }

    if (!this.state.hasPermission) {
      console.warn("Can't record, no permission granted!");
      return;
    }

    if (this.state.stoppedRecording) {
      this.prepareRecordingPath(this.state.audioPath);
    }

    this.setState({ active: true, recording: true, paused: false });

    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error);
    }
  }

  async _stop() {
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");
      return;
    }

    this.setState({ stoppedRecording: true, recording: false, paused: false });

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === "android") {
        this._finishRecording(true, filePath);
      }
      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

  playSoundRecord() {
    this.woosh = new Sound(this.state.audioPath, "", err => {
      if (err) {
        alert("error play sound");
      } else {
        this.woosh.play(success => {}, err => {});
      }
    });
  }

  pauseSound() {
    if (!this.woosh) return;
    this.woosh.pause();
    this.setState({ playingText: false });
  }

  _finishRecording(didSucceed, filePath) {
    this.setState({ finished: didSucceed, song: filePath });
    //   `Finished recording of duration ${
    //     this.state.currentTime
    //   } seconds at path: ${filePath}`
    // );
  }
  _checkPermission() {
    if (Platform.OS !== "android") {
      return Promise.resolve(true);
    }

    const rationale = {
      title: "Microphone Permission",
      message:
        "AudioExample needs access to your microphone so you can record audio."
    };

    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      rationale
    ).then(result => {
      return result === true || result === PermissionsAndroid.RESULTS.GRANTED;
    });
  }

  async _record() {
    if (this.state.recording) {
      console.warn("Already recording!");
      return;
    }

    if (!this.state.hasPermission) {
      console.warn("Can't record, no permission granted!");
      return;
    }

    if (this.state.stoppedRecording) {
      this.prepareRecordingPath(this.state.audioPath);
    }

    this.setState({ active: true, recording: true, paused: false });

    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error);
    }
  }

  async _stop() {
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");
      return;
    }

    this.setState({ stoppedRecording: true, recording: false, paused: false });

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === "android") {
        this._finishRecording(true, filePath);
      }
      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

  playSound() {
    this.setState({ playingText: true });

    this.woosh = new Sound(this.state.audioPath, "", err => {
      if (err) {
        alert("error play sound");
      } else {
        this.woosh.play(
          success => {
            if (success) {
              this.setState({ playingText: false });
            } else {
              this.setState({ playingText: false });
            }
          },
          err => {
            alert(err);
          }
        );
      }
    });
  }

  pauseSound() {
    if (!this.woosh) return;
    this.woosh.pause();
    this.setState({ playingText: false });
  }

  _finishRecording(didSucceed, filePath) {
    this.setState({ finished: didSucceed, song: filePath });
  }
  closeReport() {
    this.setState({
      showReport: false
    });
  }

  openReport = () => {
    this.setState({
      showReport: true
    });
  };

  _playSound({ type }) {
    if (type === "positive") {
      if (positiveSong != null) {
        positiveSong.setVolume(1.0);
        positiveSong.play(sucess => {
          if (!sucess) {
            alert("error init sound player");
          }
        });
      }
    }

    if (type === "negative") {
      negativeSong.setVolume(1.0);
      negativeSong.play(sucess => {
        if (!sucess) {
          alert("error init sound player");
        }
      });
    }

    if (type === "completed") {
      completedSong.setVolume(1.0);
      completedSong.play(sucess => {
        if (!sucess) {
          alert("error init sound player");
        }
      });
    }
  }

  _checkingNfc() {
    NfcManager.isSupported().then(supported => {
      this.setState({ supported });
      if (supported) {
        this._startNfc();
        this._startDetection();
      }
    });
  }

  guid() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  _sendReportEndclocking = () => {
    const {
      posts: { posts },
      clocking: { clocking }
    } = this.props;

    let coords = `${this.state.lat},${this.state.lng}`;
    let report = {
      gps: coords,
      priority: "medium",
      description: this.state.description,
      post_id: parseInt(posts.post_id),
      timestamp: JSON.stringify(Math.floor(Date.now() / 1000)),
      request_id: this.guid(),
      type: "terminate"
    };

    const path = `file://${this.state.audioPath}`;

    if (this.state.ImageSource === null) {
      alert("Please attach an image");
    } else if (this.state.description === null) {
      alert("please attach description");
    } else {
      this.props.doSaveEndClocking(clocking, report, this.state.albums, path);
      this.props.navigation.popToTop();
    }
  };

  _openCamera() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.error) {
        alert("Camera error");
      } else {
        this.setState({
          ImageSource: response,
          albums: [...this.state.albums, response]
        });
      }
    });
  }

  handleBackButton() {
    this._stopDetection();
    this.setState({ showReport: false });
    this.props.navigation.goBack();
    return true;
  }

  _finishClockingCopy() {
    const {
      clocking: { clocking }
    } = this.props;

    this._playSound({ type: "completed" });

    this.sendTelegramBro();
    this.props.doSaveClocking(clocking);
    this.props.doClearCache();

    this.props.navigation.popToTop();
  }

  _renderListItem() {
    const {
      checkpoint: { checkpoint },
      scanning: { method },
      clocking: { clocking, dummy }
    } = this.props;

    let isContain;

    if (method === "qrcode") {
      return (
        checkpoint &&
        checkpoint.map((v, keys) => (
          <ListItem
            onPress={() => {
              this.props.shareClocking(v.id);
              this.props.navigation.navigate("QrCode");
            }}
            key={keys}
            style={{
              display: "flex",
              justifyContent: "space-between"
            }}
          >
            <Text style={{ fontSize: 20 }}>{v.description}</Text>
            <Right>
              <View
                style={{
                  borderRadius: 50,
                  width: 40,
                  height: 40,
                  borderWidth: 1,
                  borderColor: 1
                }}
              >
                {clocking && clocking.find(x => x.id === v.id) ? (
                  <Icon
                    type="FontAwesome"
                    name="check"
                    style={{
                      color: "mediumseagreen"
                    }}
                  />
                ) : (
                  <Image source={require("../../../assets/qr.png")} />
                )}
              </View>
            </Right>
          </ListItem>
        ))
      );
    } else {
      return (
        checkpoint &&
        checkpoint.map((v, keys) => {
          isContain = dummy && dummy.find(x => x.id === v.id);

          return (
            <ListItem
              key={keys}
              style={{
                display: "flex",
                justifyContent: "space-between"
              }}
            >
              <Text style={{ fontSize: 16 }}>{v.description} </Text>
              <Text>{isContain ? isContain.clock : ""}</Text>
              <Right>
                <View style={styles.listClocking}>
                  {isContain ? (
                    <Icon
                      type="FontAwesome"
                      name="check"
                      style={{
                        color: "mediumseagreen"
                      }}
                    />
                  ) : (
                    <Icon
                      type="FontAwesome"
                      name="check"
                      style={{
                        color: "#dadada"
                      }}
                    />
                  )}
                </View>
              </Right>
            </ListItem>
          );
        })
      );
    }
  }

  _renderButtonRecord() {
    return (
      <Fragment>
        {this.state.recording ? (
          <TouchableOpacity onPress={() => this._stop()}>
            <Icon
              type="FontAwesome"
              name="stop"
              style={{
                alignSelf: "center",
                position: "relative",
                top: 8,
                marginBottom: 15,
                color: "red"
              }}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => this._record()}>
            <Icon
              type="FontAwesome"
              name="microphone"
              style={{
                alignSelf: "center",
                position: "relative",
                top: 8,
                marginBottom: 15,
                color: "#333"
              }}
            />
          </TouchableOpacity>
        )}
      </Fragment>
    );
  }

  _deleteEntireImage(v) {
    RNFetchBlob.fs
      .unlink(v.path)
      .then(() => {
        const { albums } = this.state;
        this.setState({
          albums: [
            ...albums.splice(0, albums.indexOf(v)),
            ...albums.splice(albums.indexOf(v) + 1, albums.length)
          ]
        });
      })
      .catch(err => {
        alert("fail to delete");
      });
  }

  _showModal() {
    this.setState({
      loadingModal: true
    });
  }

  _closeModal() {
    this.setState({
      loadingModal: false
    });
  }

  sendTelegramBro = () => {
    let self = this;
    this.refs.viewShot.capture().then(uri => {
      self.props.doSendTelegram(uri);
    });
  };

  showModalScreenshot = () => {
    alert("show modal screenshot");
  };

  clockingLongPress = () => {
    Alert.alert(
      "Advanced finish clocking",
      "Are you sure?",
      [
        { text: "Cancel", style: "cancel" },
        { text: "OK", onPress: () => this._finishClockingCopy() }
      ],
      { cancelable: false }
    );
  };

  render() {
    const {
      account: {
        account: { post }
      },
      checkpoint: { checkpoint },
      loading: { show },
      clocking: { clocking }
    } = this.props;

    let isEqual = checkpoint.length === clocking.length;

    return (
      <Fragment>
        <Modal
          isVisible={this.state.loadingModal}
          onBackButtonPress={this._closeModal}
        >
          <View
            style={{
              width: 100,
              height: 100,
              borderRadius: 4,
              alignSelf: "center",
              backgroundColor: colors.light,
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ marginTop: 15 }}>Saving...</Text>
          </View>
        </Modal>

        <Modal isVisible={false}>
          <View
            style={{
              width: 300,
              height: 300,
              borderRadius: 4,
              alignSelf: "center",
              backgroundColor: colors.light,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              padding: 20
            }}
          >
            <Text
              style={{
                marginTop: 15,
                fontFamily: "OpenSans-Regular",
                fontSize: 18,
                textAlign: "center"
              }}
            >
              Will save and send a screenshot in:
            </Text>
          </View>
        </Modal>

        <Modal isVisible={this.state.loadingspin}>
          <View
            style={{
              width: 100,
              height: 100,
              borderRadius: 4,
              alignSelf: "center",
              backgroundColor: colors.light,
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <ActivityIndicator size={"large"} />
            <Text style={{ marginTop: 15 }}>Get server time</Text>
          </View>
        </Modal>

        <Modal isVisible={this.state.showReport}>
          <ScrollView
            style={{
              flex: 1,
              backgroundColor: colors.light
            }}
          >
            <View
              style={{
                height: 60,
                backgroundColor: colors.info,
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Label style={{ color: colors.light }}>
                {languange.endclocking}
              </Label>
            </View>
            <View style={{ padding: 15, flex: 1 }}>
              <View style={{ marginBottom: 15 }}>
                <Text>
                  *Please insert a description and image why you end this
                  clocking session
                </Text>
              </View>
              <Item
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  flexDirection: "column",
                  marginBottom: 15
                }}
              >
                <Label
                  style={{
                    height: 30,
                    alignSelf: "center",
                    alignItems: "center",
                    display: "flex",
                    flexDirection: "row",
                    width: "100%"
                  }}
                >
                  <Text style={{ marginLeft: 20 }}>Description</Text>
                </Label>
                <TextInput
                  style={{ width: "100%" }}
                  placeholder="Insert description here"
                  underlineColorAndroid={"transparent"}
                  onChangeText={text => this.setState({ description: text })}
                />
              </Item>
              <Item
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  flexDirection: "column",
                  marginBottom: 15
                }}
              >
                <Label
                  style={{
                    height: 30,
                    alignSelf: "center",
                    alignItems: "center",
                    display: "flex",
                    flexDirection: "row",
                    width: "100%"
                  }}
                >
                  <Text style={{ marginLeft: 20 }}>Audio</Text>
                </Label>
                <View style={styles.audioRecord}>
                  <View style={styles.time}>
                    <Text>
                      {this.state.hours < 10
                        ? `0${this.state.hours}`
                        : this.state.hours}
                      :{" "}
                      {this.state.seconds < 10
                        ? `0${this.state.seconds}`
                        : this.state.seconds}
                    </Text>
                  </View>
                  {this._renderButtonRecord()}
                </View>
              </Item>
              <Item>
                {this.state.song && (
                  <View style={styles.playlist}>
                    <Slider
                      step={1}
                      minimumValue={0}
                      maximumValue={100}
                      minimumTrackTintColor="#009688"
                      style={{ width: "90%" }}
                    />
                    {this.state.playingText ? (
                      <TouchableOpacity
                        style={{ marginLeft: 12 }}
                        onPress={this.pauseSound}
                      >
                        <Text style={[styles.text, { color: colors.info }]}>
                          <Icon name="pause" />
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        style={{ marginLeft: 12 }}
                        onPress={this.playSoundRecord}
                      >
                        <Text style={[styles.text, { color: colors.info }]}>
                          Play
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                )}
              </Item>
              <Item
                style={{
                  marginBottom: 15,
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  flexDirection: "column"
                }}
              >
                <Label
                  style={{
                    height: 30,
                    alignSelf: "center",
                    alignItems: "center",
                    display: "flex",
                    flexDirection: "row",
                    width: "100%"
                  }}
                >
                  <Text style={{ marginLeft: 20 }}>Image</Text>
                </Label>
                <ScrollView
                  contentContainerStyle={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this._openCamera()}
                    style={{
                      width: 80,
                      height: 80,
                      borderWidth: 1,
                      marginBottom: 20,
                      marginRight: 15,
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      borderStyle: "dashed",
                      borderColor: "#dadada"
                    }}
                  >
                    <Text style={{ fontSize: 10 }}>Add Picture</Text>
                  </TouchableOpacity>

                  {this.state.albums &&
                    this.state.albums.map((v, keys) => (
                      <TouchableOpacity key={keys} style={styles.viewUpload}>
                        <TouchableOpacity
                          onPress={() => this._deleteEntireImage(v)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            width: 25,
                            height: 25,
                            borderRadius: 100,
                            backgroundColor: colors.danger,
                            position: "absolute",
                            zIndex: 10000,
                            top: 5,
                            right: 5
                          }}
                        >
                          <Text style={{ color: colors.light }}>X</Text>
                        </TouchableOpacity>
                        <Image
                          source={{ uri: v.uri }}
                          style={{ width: "100%", height: "100%" }}
                        />
                      </TouchableOpacity>
                    ))}
                </ScrollView>
              </Item>
            </View>
            <View
              style={{
                paddingRight: 15,
                height: 80,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-end"
              }}
            >
              <TouchableOpacity
                style={{
                  height: 50,
                  width: 100,
                  height: 50,
                  marginRight: 15,
                  backgroundColor: colors.danger,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onPress={this.closeReport}
              >
                <Text style={{ color: colors.light }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this._sendReportEndclocking}
                style={{
                  width: 100,
                  height: 50,
                  backgroundColor: colors.success,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                {this.state.loadingReport ? (
                  <ActivityIndicator color={colors.light} />
                ) : (
                  <Text style={{ color: colors.light }}>Submit</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
        </Modal>

        <PopupDialog
          dialogStyle={{
            width: 100,
            height: 100,
            marginTop: -80,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          }}
          ref={popupDialog => {
            this.loadingPop = popupDialog;
          }}
        >
          <View
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <ActivityIndicator color={colors.info} style={{ marginTop: 10 }} />
            <Text
              style={{
                textAlign: "center",
                fontFamily: "OpenSans-Light",
                marginTop: 10
              }}
            >
              Saving...
            </Text>
          </View>
        </PopupDialog>

        <ScrollView
          style={{
            flex: 1,
            height: "100%",
            backgroundColor: colors.background
          }}
        >
          <ViewShot ref="viewShot" options={{ format: "jpg", quality: 0.9 }}>
            <View
              style={{
                width: this.state.normalWidth,
                height: 200,
                backgroundColor: colors.info,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  fontSize: 70,
                  color: colors.light,
                  fontFamily: "OpenSans-Bold"
                }}
              >
                {clocking && clocking.length} /{" "}
                {checkpoint && checkpoint.length}
              </Text>
              <Text style={configs.textClocking}>
                {languange.pointclocking}
              </Text>
            </View>
            <View style={configs.content}>
              <View
                style={{
                  position: "relative",
                  zIndex: 100,
                  marginTop: 10,
                  padding: 15,
                  paddingLeft: 0,
                  backgroundColor: colors.light
                }}
              >
                <View style={{ paddingLeft: 15 }}>
                  {checkpoint &&
                  checkpoint.length === clocking.length &&
                  clocking.length !== 0 ? (
                    <View style={configs.notification}>
                      <Text
                        style={{
                          color: colors.light,
                          fontFamily: "JosefinSans-Light"
                        }}
                      >
                        Guard have completed clocking
                      </Text>
                    </View>
                  ) : null}

                  {this.state.warning ? (
                    <View
                      style={[
                        configs.notification,
                        { backgroundColor: colors.warning }
                      ]}
                    >
                      <Text
                        style={{
                          color: colors.light,
                          fontFamily: "JosefinSans-Light"
                        }}
                      >
                        You must finish clocking right now.{" "}
                        {parseInt(post.clockingtime) -
                          parseInt(
                            post.minute_to_notify_before_clocking_end
                          )}{" "}
                        minutes left
                      </Text>
                    </View>
                  ) : null}

                  <Text
                    style={[
                      text.spaceBottom,
                      text.h4,
                      { fontFamily: "JosefinSans-Bold", textAlign: "center" }
                    ]}
                  >
                    {post.name}
                  </Text>
                </View>

                {show && <Spinner color={colors.info} />}

                {this._renderListItem()}
              </View>

              <View style={[configs.spaceBetween, { marginTop: 20 }]}>
                {isEqual ? (
                  <Button
                    onPress={() => this._finishClockingCopy()}
                    style={[
                      configs.noButtonShadow,
                      configs.textCenter,
                      {
                        width: "100%",
                        backgroundColor:
                          checkpoint.length === clocking.length
                            ? colors.success
                            : colors.danger,
                        borderRadius: 20
                      }
                    ]}
                  >
                    {this.state.loading && (
                      <Spinner
                        color={"#fff"}
                        style={{ position: "absolute", left: 10 }}
                      />
                    )}
                    <Text style={{ color: colors.light }}>Finish</Text>
                  </Button>
                ) : (
                  <TouchableOpacity
                    onPress={this.openReport}
                    onLongPress={this.clockingLongPress}
                    style={[
                      configs.noButtonShadow,
                      configs.textCenter,
                      {
                        width: "100%",
                        height: 50,
                        backgroundColor: colors.danger
                      }
                    ]}
                  >
                    <Text style={{ color: colors.light }}>
                      {languange.end} Clocking
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </ViewShot>
        </ScrollView>

        <PopupDialog
          dialogStyle={{
            width: 300,
            height: 250,
            marginTop: -80
          }}
          dialogTitle={<DialogTitle title="End Clocking" />}
          ref={popupDialog => {
            this.popupDialog = popupDialog;
          }}
        >
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 20
            }}
          >
            <Text>Are you sure end this clocking?</Text>
            <View
              style={{
                position: "absolute",
                right: 4,
                bottom: 18,
                display: "flex",
                flexDirection: "row"
              }}
            >
              <Button
                danger
                onPress={() => this.popupDialog.dismiss()}
                style={{
                  marginRight: 10,
                  width: 80,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>CANCEL</Text>
              </Button>
              <Button
                success
                onPress={() => this._openCamera()}
                style={{
                  marginRight: 10,
                  width: 120,
                  height: 30,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20
                }}
              >
                <Text style={{ color: colors.light }}>Take Picture</Text>
              </Button>
            </View>
          </View>
        </PopupDialog>
      </Fragment>
    );
  }

  _startNfc() {
    NfcManager.start({
      onSessionClosedIOS: () => {}
    })
      .then(result => {})
      .catch(error => {
        console.warn("start fail", error);
        this.setState({ supported: false });
      });

    if (Platform.OS === "android") {
      NfcManager.getLaunchTagEvent()
        .then(tag => {
          if (tag) {
            this.setState({ tag });
          }
        })
        .catch(err => {});
      NfcManager.isEnabled()
        .then(enabled => {
          this.setState({ enabled });
        })
        .catch(err => {});
      NfcManager.onStateChanged(event => {
        if (event.state === "on") {
          this.setState({ enabled: true });
        } else if (event.state === "off") {
          this.setState({ enabled: false });
        } else if (event.state === "turning_on") {
        } else if (event.state === "turning_off") {
        }
      })
        .then(sub => {
          this._stateChangedSubscription = sub;
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }

  _onTagDiscoveredCopy = tag => {
    const {
      clocking: { dummy },
      checkpoint: { checkpoint }
    } = this.props;

    this.setState({ tag });

    let text = this._parseText(tag);

    // Check, is scanned checkpoint available in database
    let result =
      checkpoint && checkpoint.find(v => v.id === parseInt(text))
        ? true
        : false;

    // if scanned checkpoint available in database
    if (result) {
      if (this.props.notification.buzztype === "infinite") {
        this.props.doStopSirene();
      }

      this.props.doLate(false);

      this.props.addDummyCheckpoint({
        id: parseInt(text)
      });

      let obj = {
        id: parseInt(text),
        time: JSON.stringify(Math.floor(Date.now() / 1000)),
        status: "1"
      };

      this._playSound({ type: "positive" });
      this.props.doCollectClocking(obj);
    } else {
      alert("Checkpoint not available");
    }
  };

  _startDetection = () => {
    NfcManager.registerTagEvent(this._onTagDiscoveredCopy)
      .then(result => {})
      .catch(error => {
        console.warn("registerTagEvent fail", error);
      });
  };

  _stopDetection = () => {
    NfcManager.unregisterTagEvent()
      .then(result => {})
      .catch(error => {
        console.warn("unregisterTagEvent fail", error);
      });
  };

  _parseText = tag => {
    if (tag.ndefMessage) {
      return NdefParser.parseText(tag.ndefMessage[0]);
    }
    return null;
  };
}

const styles = StyleSheet.create({
  audioRecord: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row"
  },
  playlist: {
    borderWidth: 1,
    borderColor: "#f7f7f7",
    height: 40,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 6,
    paddingRight: 20
  },
  viewUpload: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderRadius: 4,
    width: 80,
    height: 80,
    marginRight: 10,
    borderColor: "#dadada",
    position: "relative"
  },
  listClocking: {
    borderRadius: 50,
    borderWidth: 1,
    width: 40,
    height: 40,
    borderWidth: 1,
    borderColor: 1
  }
});

export default ClockingComponent;
