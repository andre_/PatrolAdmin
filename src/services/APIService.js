import axios from "axios";

export const API = () => {
  return axios.create({
    baseURL: "https://my.trackerhero.com/api/",
    headers: {
      Accept: "application/json",
      Content: "application/json",
      "X-Requested-With": "XMLHttpRequest"
    }
  });
};

export const httpService = () => {
  return axios.create({
    baseURL: "https://my.trackerhero.com/api/",
    headers: {
      Accept: "application/json",
      Content: "application/json",
      "X-Requested-With": "XMLHttpRequest"
    }
  });
};
